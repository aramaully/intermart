﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmVerifyTS.aspx.vb" Inherits="Ramus.FrmVerifyTS" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<html>
<head id="Head1" runat="server">
    <title>Verify Timesheet</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.
            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Verify Timesheet'; //Set the name
            w.Width = 1250; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 545;            
            w.Top = 10;
            w.Left = 10;
            w.Show(); //Show the window
        };
        function ShowFind() {
            var FindWindow = window.open("FindEmployee.aspx", "FindEmployee", "width=565,height=435,top=200, left=575");
        };
        function FunctionConfirm(Tlb, TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Save") {
                    Index = '1';
                    w.Confirm("Sure to Save", "Confirm", null, SaveFunction);
            }
            else if (TlbCmd == "New") {
                __doPostBack('ToolBar1', '0');
            }
            else if (TlbCmd == "Lock") {
                Index = '3';
                w.Confirm("Sure to Lock All Timesheet?", "Confirm", null, SaveFunction);
            }
            else if (TlbCmd == "Unlock") {
                Index = '4';
                w.Confirm("Sure to Unlock Timesheet?", "Confirm", null, SaveFunction);
            }
        }
        function SaveFunction(Sender, RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        }
        function FunPic() {
            var myArgs = 'nothing';
            myargs = window.showModalDialog('Snap.aspx', myArgs, "dialogHeight:350px;dialogWidth:650px");
            if (myArgs == 'nothing') {
                PageMethods.AjaxSetSession("True");
            }
        }
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 122px;
        }
    </style>

    </head>
<body onload="init();">
    <form id="form2" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <script type="text/javascript">
        var xPos, yPos;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            xPos = $get('PnlGrid').scrollLeft;
            yPos = $get('PnlGrid').scrollTop;
        }
        function EndRequestHandler(sender, args) {
            $get('PnlGrid').scrollLeft = xPos;
            $get('PnlGrid').scrollTop = yPos;
        }
</script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px;
                height: 32px; width: 32px;" runat="server" BorderStyle="None" Visible="True">
                <div id="divImage" style="display: none">
                    <asp:Image ID="img1" runat="server" ImageUrl="/Images/Wait.gif" />
                </div>
            </asp:Panel>
            <div id="toolbar" style="width: 100%;">
                <eo:ToolBar ID="ToolBar1" runat="server" BackgroundImage="00100103" ClientSideOnItemClick="FunctionConfirm"
                    BackgroundImageLeft="00100101" BackgroundImageRight="00100102" SeparatorImage="00100104"
                    Width="100%">
                    <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 1px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: #335ea8 1px solid; background-color: #99afd4; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <Items>
                        <eo:ToolBarItem CommandName="New" ImageUrl="~/images/RefreshDocViewHS.png" ToolTip="Refresh">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Help" ImageUrl="~/images/help.png" ToolTip="Help">
                        </eo:ToolBarItem>
                    </Items>
                    <ItemTemplates>
                        <eo:ToolBarItem Type="Custom">
                            <NormalStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <HoverStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <DownStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="DropDownMenu">
                            <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                            <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; background-image: url(00100106); background-position-x: right;" />
                            <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: none; background-color:transparent; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                        </eo:ToolBarItem>
                    </ItemTemplates>
                    <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                </eo:ToolBar>
            </div>
            <div id="Content" style="width: 100%; height: 100%; padding: 2px; border-right: solid 1px #cddaea;">
                <table class="style1" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            Department</td>
                        <td colspan="3">
                            <asp:DropDownList ID="CmbDept" runat="server" Width="300px" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:Button ID="BtnAdd" runat="server" Text="Update Clockings" Width="98px" />
                            <asp:Button ID="ButProcessOvertime" runat="server" Text="Approve Overtime" 
                                Width="109px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Date</td>
                        <td>
                            <asp:TextBox ID="TxtDate" runat="server" Width="100px"></asp:TextBox>
                            <cc1:CalendarExtender ID="TxtDate_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtDate">
                            </cc1:CalendarExtender>
                        </td>
                        <td>
                            Day</td>
                        <td>
                            <asp:TextBox ID="TxtDay" runat="server" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" class="style1">
                    <tr>
                        <td>
                            Emp ID
                        </td>
                        <td>
                            <asp:TextBox ID="TxtEmpID" runat="server" AutoPostBack="True"></asp:TextBox>
                        </td>
                        <td>
                            Title
                        </td>
                        <td>
                            <asp:TextBox ID="TxtTitle" runat="server" ReadOnly="True" Width="50px"></asp:TextBox>
                        </td>
                        <td>
                            Gender
                        </td>
                        <td>
                            <asp:TextBox ID="TxtGender" runat="server" ReadOnly="True" Width="50px"></asp:TextBox>
                        </td>
                        <td align="right" rowspan="4" valign="top">
                            <asp:Image ID="ImgPhoto" runat="server" Height="97px" Width="87px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Clocked</td>
                        <td>
                            <asp:CheckBox ID="ChkClocked" runat="server" Enabled="False" Text="Yes" />
                        </td>
                        <td>
                            Clock ID</td>
                        <td>
                            <asp:TextBox ID="TxtClockID" runat="server" Width="100px"></asp:TextBox>
                        </td>
                        <td>
                            Category</td>
                        <td>
                            <asp:TextBox ID="TxtCategory" runat="server" Width="100px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Last Name</td>
                        <td colspan="4">
                            <asp:TextBox ID="TxtLast" runat="server" ReadOnly="True" Width="400px"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            First Name</td>
                        <td colspan="4">
                            <asp:TextBox ID="TxtFirst" runat="server" ReadOnly="True" Width="400px"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                            </td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" class="style1">
                    <tr>
                        <td class="style2">
                            Time In</td>
                        <td>
                            <asp:TextBox ID="TxtTimein" runat="server"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="TxtTimein_MaskedEditExtender" runat="server" 
                            acceptampm="false" mask="99:99" cultureampmplaceholder=""
                            culturecurrencysymbolplaceholder=""
                            culturedateformat="" culturedateplaceholder=""
                            culturedecimalplaceholder="" culturethousandsplaceholder=""
                            culturetimeplaceholder="" enabled="True" masktype="Time"
                            targetcontrolid="TxtTimein">
                            </cc1:MaskedEditExtender>
                        </td>
                        <td>
                            Time Out</td>
                        <td>
                            <asp:TextBox ID="TxtTimeOut" runat="server" AutoPostBack="True" Width="128px"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="TxtTimeOut_MaskedEditExtender" runat="server" 
                            acceptampm="false" mask="99:99" cultureampmplaceholder=""
                            culturecurrencysymbolplaceholder=""
                            culturedateformat="" culturedateplaceholder=""
                            culturedecimalplaceholder="" culturethousandsplaceholder=""
                            culturetimeplaceholder="" enabled="True" masktype="Time"
                            targetcontrolid="TxtTimeOut">
                            </cc1:MaskedEditExtender>
                        </td>
                        <td>
                            Normal</td>
                        <td>
                            <asp:TextBox ID="TxtNormal" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <br />
                <asp:Label ID="Lblerror" runat="server" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
                <asp:Panel ID="PnlGrid" runat="server" ScrollBars="Auto" BorderStyle="Inset" 
                    BorderWidth="1px" Height="330px">
                <asp:GridView ID="GdvRoster" runat="server" Width="100%" 
                        AutoGenerateColumns="False">
                    <HeaderStyle BackColor="#003399" Font-Size="Smaller" ForeColor="White" />
                    <Columns>
                        <asp:BoundField DataField="EmpID" HeaderText="EmpID" />
                        <asp:BoundField DataField="Name" HeaderText="Name" />
                        <asp:BoundField DataField="TimeIn" HeaderText="TimeIn" />
                        <asp:BoundField DataField="TimeOut" HeaderText="TimeOut" />
                        <asp:BoundField DataField="Normal" HeaderText="Normal" />
                        <asp:BoundField DataField="OTHrs15" HeaderText="OTHrs1.5" />
                        <asp:BoundField DataField="OTHrs20" HeaderText="OTHrs2.0" />
                        <asp:BoundField DataField="OTHrs30" HeaderText="OTHrs3.0" />
                        <asp:TemplateField HeaderText="HODApproval" ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle Font-Size="Smaller" Font-Bold ="True"  ForeColor="White" Wrap="False" />
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="ChkHODApproval" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="HRApproval" ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle Font-Size="Smaller" Font-bold ="True" ForeColor="White" Wrap="False" />
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="ChkHRApproval" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Shift" HeaderText="Shift" />
                        <asp:BoundField DataField="TimeInRoster" HeaderText="TimeInRoster" />
                        <asp:BoundField DataField="TimeOutRoster" HeaderText="TimeOutRoster" />
                        <asp:BoundField DataField="ShiftHours" HeaderText="ShiftHours" />
                        <asp:BoundField DataField="Type" HeaderText="Leave Type" />
                        <asp:BoundField DataField="Leave" HeaderText="Leave" />
                    </Columns>
                    <SelectedRowStyle BackColor="Yellow" />
                </asp:GridView>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_InitializeRequest);
        prm.add_endRequest(prm_EndRequest);

        function prm_InitializeRequest(sender, args) {
            var panelProg = $get('divImage');
            if (args._postBackElement.id != "Timer1") {
                panelProg.style.display = '';
            }
        };

        function prm_EndRequest(sender, args) {
            var panelProg = $get('divImage');
            panelProg.style.display = 'none';
        };
    </script>

</body>
</html>

