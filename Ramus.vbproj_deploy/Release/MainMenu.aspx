﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MainMenu.aspx.vb" Inherits="Ramus.MainMenu" %>
<html>
<head>
    <title>Ramus Enterprise 1.1 System</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/windowmanager/000.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Listview.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>
    <script language="javascript" type="text/javascript" src="js/IDC_WindowManager.js"></script>
    <script language="javascript" type="text/javascript" src="js/IDC_Listview.js"></script>
    <script language="javascript" type="text/javascript" src="js/IDC_Toolbar.js"></script>
    <script language="javascript" type="text/javascript">
        var windowManager = null;
        var core = null;
        var cmdStart = null;
        var widgets = null;
        var desktopContext = null;
        var startMenu = null;
        function init() {
            //Create a new window manager assigned to the divDesktop object
            windowManager = new IDC_WindowManager(document, document.getElementById('divDesktop'), 'styles/windowmanager/000.css');
            core = windowManager.Core;
            //Create taskbar button and quicklinks
            startMenu = document.getElementById('tblStartmenu');
            startMenu.OriginalParent = startMenu.parentNode;
            cmdStart = windowManager.Desktop.Taskbar.CreateTaskbarButton('images/login32.png', 'images/login.png', 'Start',
                function() {
                    //Manual clean up
                    while (windowManager.Desktop.Taskbar.MultipleWindowsControl.hasChildNodes()) {
                        windowManager.Desktop.Taskbar.MultipleWindowsControl.removeChild(windowManager.Desktop.Taskbar.MultipleWindowsControl.lastChild);
                    }
                    //Manual add to contextmenu container
                    windowManager.Desktop.Taskbar.MultipleWindowsControl.appendChild(startMenu);
                    //Manual show of contextmenu
                    windowManager.ContextMenuManager.Show(windowManager.Desktop.Taskbar.MultipleWindowsOuterControl, 0, null, null, 40,
                            function() { return (windowManager.Desktop.Taskbar.MultipleWindowsOuterControl.offsetWidth - startMenu.offsetWidth) / 2; }
                        );
                });
            cmdStart.AnimationEnabled = true;
            cmdStart.Control.oncontextmenu = null;
        };
        function ShowMenu(item) {
            obj.style.display = "";
        }
    </script>
</head>
<body onload="init();">
    <div style="width: 100%; height: 100%;">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 100%;">
            <tr>
                <td id="divDesktop" valign="top" style="height: 100%;">
                </td>
            </tr>
        </table>
    </div>
    <!-- START MENU - This can be customized with your own HTML -->
    <div style="position: absolute; z-index: -1000; visibility: hidden;">
        <table border="0" cellpadding="0" cellspacing="0" id="tblStartmenu" style="width: 100%;
            height: 100%;">
            <tr>
                <td valign="bottom" class="StartmenuButtonsBack" dontcollapse="true">
                    <span class="StartmenuCurrentUser">
                        <span id="spStartmenuUsername" class="StartmenuCurrentUserText">
                            <asp:Label ID="CurrentUser" runat="server" Text="Label"></asp:Label>
                        </span>
                    </span>
                    <a href="#" onmouseover="spDocContent.style.display='';" onmouseout="spDocContent.style.display='none';"
                        dontcollapse="true" onclick="" class="StartmenuButtonChildren">
                        <img alt="Start Menu" dontcollapse="true" src="images/MasterFiles.ico" border="0" class="StartmenuButtonImg" />
                        <span dontcollapse="true" class="StartmenuButtonTxt">Master Files</span> 
                        <span id="spDocContent"
                            style="display: none;" class="StartmenuSubMenuFrame">
                            <div id="SkillMaster" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmSkillMaster.aspx');">
                                Skill Master</div>
                            <div id="Skill" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmSkill.aspx');">
                                Assign Skill</div>
                            <div id="EquipMaster" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('EquipMaster.aspx');">
                                Equipment Master</div>
                            <div id="LoanTypes" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('LoanTypes.aspx');">
                                Loan Types</div>
                            <div id="LeaveMaster" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('LeaveMaster.aspx');">
                                Leave Master</div>
                            <div id="Bank" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmBank.aspx');">
                                Bank</div> 
                            <div id="PayeSchemes" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmPaye.aspx');">
                                Paye Schemes</div>
                            <div id="CarFringe" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmCar.aspx');">
                                Car Fringe</div>
                            <div id="Holidays" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmHolidays.aspx');">
                                Holidays</div>
                            <div id="Denomination" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmDenom.aspx');">
                                Denomination</div>
                            <div id="Departments" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmDept.aspx');">
                                Departments</div>                            
                            <div id="JobDescription" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmJobDesc.aspx');">
                                Job Description</div>
                             <div id="RemOrder" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmRemOrder.aspx');">
                                Remuneration Order</div>
                        </span>
                    </a>
                    <a href="#" onmouseover="spLinkContext.style.display='';" onmouseout="spLinkContext.style.display='none';"
                        dontcollapse="true" onclick="" class="StartmenuButtonChildren">
                        <img dontcollapse="true" src="images/Emp.png" border="0" class="StartmenuButtonImg" />
                        <span dontcollapse="true" class="StartmenuButtonTxt">Employee Master</span>
                        <span id="spLinkContext" style="display: none;" class="StartmenuSubMenuFrame">
                            <div id="Qualification" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmQualification.aspx');">
                                Qualification</div>
                             <div id="Referance" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmReference.aspx');">
                                Reference</div>
                             <div id="Experience" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmExperience.aspx');">
                                Experience</div>
                            <div id="Location" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmLocation.aspx');">
                                Location</div>
                            <div id="EmpGen" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmEmpGen.aspx');">
                                Create Employee</div>
                            <div id="EmpFinancial" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmEmpFin.aspx');">
                                Employee Details</div>
                            <div id="EmpDuration" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmEmp6month.aspx');">
                                Employee Duration</div>
                        </span>
                    </a>
                    <a href="#" onmouseover="SpnProcess.style.display='';" onmouseout="SpnProcess.style.display='none';"
                           dontcollapse="true" onclick="" class="StartmenuButtonChildren">
                           <img dontcollapse="true" src="images/Process.ico" border="0" class="StartmenuButtonImg" />
                           <span dontcollapse="true" class="StartmenuButtonTxt">Process</span> 
                           <span id="SpnProcess"
                                style="display: none; top:100px" class="StartmenuSubMenuFrame">
                                <div id="ProSalary" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmProSalary.aspx');">
                                    Process Salary</div>
                                <div id="ModSalary" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    style="border-bottom-style: solid; border-width: 1" onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';"
                                    onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';" onclick="windowManager.CreateWindow('FrmModSalary.aspx');">
                                    Modify Salary</div>
                                <div id="LockSalary" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    style="border-bottom-style: solid; border-width: 1" onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';"
                                    onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';" onclick="windowManager.CreateWindow('FrmLockSalary.aspx');">
                                    Lock Salary Records</div>
                                <div id="UpdateLeave" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmUpdateLeave.aspx');">
                                    Update Leave Register</div>
                                <div id="GovComp" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmGovComp.aspx');">
                                    Government Compensation</div>
                                <div id="CareerEvnts" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmCareerEvnts.aspx');">
                                    Employee Career Events</div>
                                <div id="AddNotes" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmAddNotes.aspx');">
                                    Add Notes to Employee File</div>
                                <div id="ReviewEmp" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmReviewEmp.aspx');">
                                    Review Employee File</div>
                           </span>
                    </a>
                    <a href="#" onmouseover="SpnReports.style.display='';" onmouseout="SpnReports.style.display='none';"
                           dontcollapse="true" onclick="" class="StartmenuButtonChildren">
                           <img dontcollapse="true" src="images/folder32.png" border="0" class="StartmenuButtonImg" />
                           <span dontcollapse="true" class="StartmenuButtonTxt">Reports </span>
                           <span id="SpnReports" style="display: none; top:140px;" class="StartmenuSubMenuFrame">
                            <div id="General" runat="server" visible="true" onclick="" class="IDCWMDesktopTaskbarContextMenuItem"
                                    style="background-image:url(images/startMenuButtonChildren.png); background-position:right"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover'; SpnGen.style.display='';" 
                                    onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem'; SpnGen.style.display='none';">
                                   General
                                 <span id="SpnGen" style="display: none; top:0px; left:60px" class="StartmenuSubMenuFrame">
                                <div id="BankLetter" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmBankLetter.aspx');">
                                    Bank Letter</div>
                                 <div id="MCBFloppy" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmInternetBanking.aspx');">
                                    Internet Banking</div>
                                <div id="PaySlip" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmPaySlip.aspx');">
                                    Pay Slip</div>
                                <div id="ChqNCash" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmChqNCash.aspx');">
                                    Cheque & Cash List</div>
                                <div id="PaySheet" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmPaySheet.aspx');">
                                    Pay Sheet / Medical</div>
                                 <div id="PaySheetExcel" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmPaySheetExcel.aspx');">
                                    PaySheet Excel</div>
                                <div id="PaySummary" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmPaySummary.aspx');">
                                    Pay Summary</div>
                                </div>
                                <div id="Emoluments" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                     style="border-top-style: solid; border-width: 1"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmEmol.aspx');">
                                    Emoluments</div>
                                <div id="ElecReturns" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmElecReturns.aspx');">
                                    Electronic Returns</div>
                                <div id="PayeMSummary" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmPayeSummary.aspx');">
                                    PAYE Monthly Summary</div>
                                <div id="EmployersContib" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmEmployersContrib.aspx');">
                                    Employer Monthly Contribution</div>
                                <div id="NpfMon" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmCC19A.aspx');">
                                    NPF Monthly Return (CC19a/b)</div>                                                                         
                                <div id="ExportMNS" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    style="border-bottom-style: solid; border-width: 1"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmMNS.aspx');">
                                    Export MNS / CNP</div> 
                                <div id="EmpFile" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmEmpFile.aspx');">
                                    Employee File</div>   
                                <div id="CareerHist" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmCareerRep.aspx');">
                                    Career History</div>  
                                <div id="Notes" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmNotesRep.aspx');">
                                    Notes</div>
                               <div id="Loans" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    style="border-bottom-style: solid; border-width: 1"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmLoanRep.aspx');">
                                    Loans</div>
                                <div id="AuditTrail" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    style="border-bottom-style: solid; border-width: 1"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmGeneralAudit.aspx');">
                                    Audit Trail</div>  
                              <div id="CustomRep" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmCustomRep.aspx');">
                                    Custom Report Generator</div>                                  
                           </span>
                    </a>
                    <a href="#" onmouseover="SpnUtilities.style.display='';" onmouseout="SpnUtilities.style.display='none';"
                           dontcollapse="true" onclick="" class="StartmenuButtonChildren">
                           <img dontcollapse="true" src="images/Utility.ico" border="0" class="StartmenuButtonImg" />
                           <span dontcollapse="true" class="StartmenuButtonTxt">Utilities </span>
                           <span id="SpnUtilities" style="display: none; top:195px" class="StartmenuSubMenuFrame">
                                <div id="AddUser" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmAddUser.aspx');">
                                    Create / Edit User</div>
                                <div id="ChPassword" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmPassword.aspx');">
                                    Change Password</div>
                                <div id="UserAccess" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmAccess.aspx');">
                                    Access Level</div>
                                <div id="System" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    style="border-bottom-style: solid; border-width: 1" onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';"
                                    onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';" onclick="windowManager.CreateWindow('FrmSystem.aspx');">
                                    System Set Up</div>
                           </span>
                     </a>
                     <a href="#" onmouseover="SpnHRMod.style.display='';" onmouseout="SpnHRMod.style.display='none';"
                           dontcollapse="true" onclick="" class="StartmenuButtonChildren">
                           <img dontcollapse="true" src="images/HRMod.ico" border="0" class="StartmenuButtonImg" />
                           <span dontcollapse="true" class="StartmenuButtonTxt">HR Module </span>
                           <span id="SpnHRMod" style="display: none;" class="StartmenuSubMenuFrame">
                               <div id="Recruitment" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmRecruitment.aspx');">
                                    Recruitment</div>
                                <div id="WarningLetter" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmWarningLetter.aspx');">
                                    Warning Letter</div>
                                <div id="IssuedEquip" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmEquipment.aspx');">
                                    Issued Equipment</div>
                               <div id="CourseMaster" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmCourseMaster.aspx');">
                                    Course Master</div>
                                <div id="IndividualTraining" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    style="border-Bottom-style: solid; border-width: 1"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmIndividualTraining.aspx');">
                                    Individual Training</div>
                                 <div id="TrainingReport" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmTrainingReport.aspx');">
                                    Training Report</div>         
                            </span>
                     </a>                     
                     <a href="#" onmouseover="SpnTimex.style.display='';" onmouseout="SpnTimex.style.display='none';"
                           dontcollapse="true" onclick="" class="StartmenuButtonChildren">
                           <img dontcollapse="true" src="images/Clock.png" border="0" class="StartmenuButtonImg" />
                           <span dontcollapse="true" class="StartmenuButtonTxt">Time Module </span>
                           <span id="SpnTimex" style="display: none; top:250px" class="StartmenuSubMenuFrame">
                                    <div id="DLeave" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmNewDLeaveInput.aspx');">
                                    Daily Leave Input</div>
                                <div id="DivTimPro" runat="server" visible="False" onclick="" class="IDCWMDesktopTaskbarContextMenuItem"
                                        style="background-image:url(images/startMenuButtonChildren.png); background-position:right"
                                        onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover'; SpnTimPro.style.display='';" 
                                        onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem'; SpnTimPro.style.display='none';">
                                       Process
                                       <span id="SpnTimPro" style="display: none; top:0px;"  class="StartmenuSubMenuFrame">
                                        <div id="LeaveApp" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                             onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                            onclick="windowManager.CreateWindow('FrmLeaveApp.aspx');">
                                            Leave Application</div>
                                        <div id="ApproveLeave" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                            onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                            onclick="windowManager.CreateWindow('FrmApproveLeave.aspx');">
                                            Approve Leave</div>
                                       </span>
                                </div>
                                <div id="DivTimRep" runat="server" visible="true" onclick="" class="IDCWMDesktopTaskbarContextMenuItem"
                                        style="background-image:url(images/startMenuButtonChildren.png); background-position:right"
                                        onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover'; SpnTimRep.style.display='';" 
                                        onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem'; SpnTimRep.style.display='none';">
                                       Reports
                                       <span id="SpnTimRep" style="display: none; top:0px;" class="StartmenuSubMenuFrame">
                                        <div id="AttendanceSheet" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                            onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                            onclick="windowManager.CreateWindow('FrmAttendanceSheet.aspx');">
                                            Attendance Sheet</div>
                                        <div id="LeaveRegister" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                            onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                            onclick="windowManager.CreateWindow('FrmLeaveRegister.aspx');">
                                            Leave Register</div>
                                       </span>
                                </div>
                            </span>
                     </a>
                     <a href="#" onclick="windowManager.Alert('Help Not available! Please Contact Vendor');"
                        class="StartmenuButton"> <img src="images/Help.ico" border="0" class="StartmenuButtonImg" />
                        <span class="StartmenuButtonTxt">Help
                        </span>
                    </a>
                    <a href="Default.aspx" class="StartmenuButton">
                        <img src="images/sync32.png" border="0" class="StartmenuButtonImg" />
                        <span class="StartmenuButtonTxt">
                        Log off</span>
                    </a>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
