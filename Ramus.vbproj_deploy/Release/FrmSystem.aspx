﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmSystem.aspx.vb" Inherits="Ramus.FrmSystem" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<html>
<head id="Head1" runat="server">
    <title>System Setup</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.
            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'System Setup'; //Set the name
            w.Width = 825; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 425;            
            w.Top = 10;
            w.Left = 10;
            w.Show(); //Show the window
        };
        function ShowFind() {
            var FindWindow = window.open("FindEmployee.aspx", "FindEmployee", "width=565,height=435,top=200, left=575");
        };
        function FunctionConfirm(Tlb, TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Save") {
                Index = '1';
                w.Confirm("Sure to Save", "Confirm", null, SaveFunction);
            }
        }
        function SaveFunction(Sender, RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        }
        function FunPic() {
            var myArgs = 'nothing';
            myargs = window.showModalDialog('Snap.aspx', myArgs, "dialogHeight:350px;dialogWidth:650px");
            if (myArgs == 'nothing') {
                PageMethods.AjaxSetSession("True");
            }
        }
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        </style>
  </head>
<body onload="init();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px;
                height: 32px; width: 32px;" runat="server" BorderStyle="None" Visible="True">
                <div id="divImage" style="display: none">
                    <asp:Image ID="img1" runat="server" ImageUrl="/Images/Wait.gif" />
                </div>
            </asp:Panel>
            <div id="toolbar" style="width: 100%;">
                <eo:ToolBar ID="ToolBar1" runat="server" BackgroundImage="00100103" ClientSideOnItemClick="FunctionConfirm"
                    BackgroundImageLeft="00100101" BackgroundImageRight="00100102" SeparatorImage="00100104"
                    Width="100%">
                    <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 1px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: #335ea8 1px solid; background-color: #99afd4; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <Items>
                        <eo:ToolBarItem CommandName="Refresh" ImageUrl="~/images/RefreshDocViewHS.png" ToolTip="Refresh">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Save" ImageUrl="00101003" ToolTip="Save">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Help" ImageUrl="~/images/help.png" ToolTip="Help">
                        </eo:ToolBarItem>
                    </Items>
                    <ItemTemplates>
                        <eo:ToolBarItem Type="Custom">
                            <NormalStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <HoverStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <DownStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="DropDownMenu">
                            <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                            <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; background-image: url(00100106); background-position-x: right;" />
                            <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: none; background-color:transparent; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                        </eo:ToolBarItem>
                    </ItemTemplates>
                    <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                </eo:ToolBar>
            </div>
            <div id="Content" style="width: 100%; height: 100%; padding: 2px;
                border-right: solid 1px #cddaea;">                
                <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="3" 
                    Width="100%">
                    <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="General Information">                                                                      
                        <ContentTemplate>
                            <table class="style1">
                               <tr>
                                    <td>
                                        &nbsp;<td>
                                            Name</td>
                                        <td colspan="4">
                                            <asp:TextBox ID="TxtName" runat="server" Width="300px"></asp:TextBox>
                                            <td>
                                                &nbsp;</td>
                                        </td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        Address</td>
                                    <td colspan="4">
                                        <asp:TextBox ID="TxtAddress" runat="server" Height="74px" TextMode="MultiLine" 
                                            Width="300px"></asp:TextBox>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                               <tr>
                                   <td>
                                        &nbsp;</td>
                                    <td>
                                        Telephone</td>
                                    <td>
                                        <asp:TextBox ID="TxtTelephone" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Telefax</td>
                                   <td>
                                        <asp:TextBox ID="TxtTeleFax" runat="server"></asp:TextBox>
                                    </td>
                                   <td width="20%">
                                        &nbsp;</td>
                                    <td>
                                       &nbsp;</td>
                               </tr>                                
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        E-Mail</td>
                                    <td colspan="4">
                                        <asp:TextBox ID="TxtEMail" runat="server" Width="300px"></asp:TextBox>
                                    </td>
                                   <td>
                                        &nbsp;</td>
                                    
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        PAYE Reg No</td> <td colspan="4">
                                        
                                        <asp:TextBox ID="TxtPAYERegNo" runat="server" Width="300px"></asp:TextBox>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    </tr>
                                <tr>
                                    <td>

                                        &nbsp;</td>
                                    <td>
                                        Business</td>
                                    <td colspan="4">
                                        <asp:TextBox ID="TxtBusiness" runat="server" Width="300px"></asp:TextBox>
                                    </td>
                                    <td>
                                    &nbsp;</td>
                                </tr>
                                 <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        Business Reg No</td>
                                    <td colspan="4">
                                        <asp:TextBox ID="TxtBRN" runat="server" Width="300px"></asp:TextBox>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                   <td>
                                        Tax Account No</td>
                                    <td colspan="4">
                                        <asp:TextBox ID="TxtTAN" runat="server" Width="300px"></asp:TextBox>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        Declarant Name</td>
                                    <td colspan="4">
                                        <asp:TextBox ID="TxtDecName" runat="server" Width="300px"></asp:TextBox>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td colspan="4">
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="Default Values for Salary">
                        
                        <HeaderTemplate>
                            Default Values for Salary
                        </HeaderTemplate>
                        
                        <ContentTemplate>
                            <table class="style1">
                                                                                                                                
                                <tr>
                                    
                                    <td>
                                        
                                        National Pension Fund (Employer)<td>
                                            <asp:TextBox ID="TxtNPFEmp" runat="server"></asp:TextBox>
                                            %</td>
                                        <td>
                                            &nbsp;<td>
                                                National Savings Fund</td>
                                            <td>
                                                <asp:TextBox ID="TxtNSF" runat="server"></asp:TextBox>
                                                %</td>
                                            <td>
                                                &nbsp;</td>
                                        </td>
                                    </td>
                                    
                                </tr>
                                
                                <tr>
                                    <td>
                                        Maximum Value</td>
                                    <td>
                                        <asp:TextBox ID="TxtNPFEmpMax" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        Maximum Value</td>
                                    <td>
                                        <asp:TextBox ID="TxtNSFMax" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                                                                                                                
                                <tr>
                                    
                                    <td>
                                        
                                        National Pension Fund (Employee)</td>
                                    
                                   <td>
                                        
                                        <asp:TextBox ID="TxtNPF" runat="server"></asp:TextBox>
                                        %</td>
                                    
                                    <td>
                                        
                                        &nbsp;</td>
                                    
                                    <td>
                                        
                                        IVTB
                                        
                                    </td>
                                    
                                    <td>
                                        
                                        <asp:TextBox ID="TxtIVTB" runat="server"></asp:TextBox>
                                        %</td>
                                    
                                    <td>
                                        
                                        &nbsp;</td>
                                    
                                </tr>
                                
                                <tr>
                                    
                                    <td>
                                        
                                        Maximum Value</td>
                                    
                                    <td>
                                        
                                        <asp:TextBox ID="TxtNPFMax" runat="server"></asp:TextBox>
                                    </td>
                                    
                                    <td>
                                        
                                        &nbsp;</td>
                                    
                                    <td>
                                        
                                        NSF Employee</td>
                                    
                                    <td>
                                        
                                        <asp:TextBox ID="TxtNSFEmployee" runat="server"></asp:TextBox>
                                        %</td>
                                    
                                    <td>
                                        
                                        &nbsp;</td>
                                    
                                </tr>
                                
                               <tr>
                                    
                                    <td>
                                        
                                        Max Basic Salary PAYE not applied</td>
                                    
                                    <td>
                                        
                                        <asp:TextBox ID="TxtTaxExempt" runat="server"></asp:TextBox>
                                    </td>
                                    
                                    <td>
                                        
                                        &nbsp;</td>
                                    
                                    <td>
                                        
                                        NSF Employee Max</td>
                                    
                                    <td>
                                        
                                        <asp:TextBox ID="TxtNSFEmployeeMax" runat="server"></asp:TextBox>
                                    </td>
                                    
                                    <td>
                                        
                                        &nbsp;</td>
                                    
                                </tr>
                                
                                <tr>
                                    
                                    <td>
                                        
                                        Max Transport allowance without Tax</td>
                                    
                                    <td>
                                        
                                        <asp:TextBox ID="TxtMaxTrans" runat="server"></asp:TextBox>
                                    </td>
                                    
                                    <td>
                                        
                                        &nbsp;</td>
                                    
                                    <td>
                                        
                                        Min PAYE</td>
                                    
                                    <td>
                                        
                                        <asp:TextBox ID="TxtMinPAYE" runat="server"></asp:TextBox>
                                    </td>
                                    
                                    <td>
                                        
                                        &nbsp;</td>
                                    
                                </tr>
                                
                                <tr>
                                    
                                    <td>
                                        
                                        Attend Allow</td>
                                    
                                    <td>
                                        
                                        <asp:TextBox ID="TxtAttendAllow" runat="server"></asp:TextBox>
                                    </td>
                                    
                                    <td>
                                        
                                        &nbsp;</td>
                                    
                                    <td>
                                        
                                        Meal Allowance</td>
                                    
                                    <td>
                                        
                                        <asp:TextBox ID="TxtMealAllow" runat="server"></asp:TextBox>
                                    </td>
                                    
                                    <td>
                                        
                                        &nbsp;</td>
                                    
                                </tr>
                                
                                <tr>
                                    
                                    <td>
                                        
                                        Night Allowance</td>
                                    
                                    <td>
                                        
                                        <asp:TextBox ID="TxtNightAllow" runat="server"></asp:TextBox>
                                    </td>
                                    
                                    <td>
                                        
                                        &nbsp;</td>
                                    
                                    <td>
                                        
                                        Attend Bonus Fort.Employee</td>                                                              
                                    
                                    <td>
                                        
                                        <asp:TextBox ID="TxtAttBonusFort" runat="server"></asp:TextBox>
                                    </td>
                                    
                                    <td>
                                        
                                        &nbsp;</td>
                                    
                                </tr>
                                 
                                 <tr>
                                     
                                    <td>
                                        
                                        &nbsp;</td>
                                     
                                    <td>
                                        
                                        &nbsp;</td>
                                     
                                    <td>
                                        
                                        &nbsp;</td>
                                    
                                    <td>
                                        
                                        Max Salary NPF</td>                                                              
                                    <td>
                                        
                                        <asp:TextBox ID="TxtMaxSalNPF" runat="server"></asp:TextBox>
                                     </td>
                                     
                                    <td>
                                        
                                        &nbsp;</td>
                                     
                                </tr>
                                
                            </table>

                        </ContentTemplate>                                                                       

                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel3" runat="server" HeaderText="Leave / Working Hours">
                        
                        <HeaderTemplate>
                            Leave / Working Hours
                        </HeaderTemplate>
                        
                        <ContentTemplate>
                            
                            <table class="style1">
                                
                                <tr>
                                    
                                    <td class="style3">
                                        
                                    
                                    
                                    
                                    <td class="style4">
                                        
                                        Sick Leave Per Year</td>
                                
                                    
                                    <td class="style4">
                                        
                                        <asp:TextBox ID="TxtSick" runat="server" Width="60px"></asp:TextBox>
                                        <td class="style4" width="30%">
                                        </td>
                                        <td class="style4" width="30%">
                                        </td>
                                        </td>
                                    
                                    
                                    
                                    
                                    </tr>
                                
                                <tr>
                                 
                                    <td>
                                        &nbsp;</td>
                                  
                                    <td class="style2">
                                        Local Leave Per Year</td>
                                  
                                    <td width="30%">
                                        
                                        <asp:TextBox ID="TxtLocal" runat="server" Width="60px"></asp:TextBox>
                                    </td>
                                                                     
                                    <td>
                                        &nbsp;</td>
                                    
                                </tr>
                                
                                <tr>
                                    
                                    <td class="style2">
                                        
                                        &nbsp;</td>
                                    
                                    <td>
                                        
                                        No Working Days per Week</td>
                                    
                                    <td>
                                        
                                        <asp:TextBox ID="TxtDays" runat="server" Width="60px"></asp:TextBox>
                                    </td>
                                    
                                    <td width="30%">
                                        
                                        &nbsp;</td>
                                    
                                    <td width="30%">
                                        
                                        &nbsp;</td>
                                    
                               </tr>
                                
                                <tr>
                                    
                                    <td class="style2">
                                        
                                        &nbsp;</td>
                                    
                                    <td>
                                        
                                        WeekDay Normal Hrs</td>
                                    
                                    <td>
                                        
                                        <asp:TextBox ID="TxtWDHrs" runat="server" Width="60px"></asp:TextBox>
                                    </td>
                                    
                                    <td width="30%">
                                        
                                        &nbsp;</td>
                                    
                                <td width="30%">
                                        
                                        &nbsp;</td>
                                  
                                </tr>
                                
                               <tr>
                                    
                                    
                                    <td class="style2">
                                        
                                        &nbsp;</td>
                                    
                                                                 
                                    <td>
                                        
                                        WeekEnd Normal Hrs</td>
                                 
                                    <td>
                                        
                                        <asp:TextBox ID="TxtWEHrs" runat="server" Width="60px"></asp:TextBox>
                                    </td>
                                    
                                                                 
                                    <td width="30%">
                                        
                                        &nbsp;</td>
                                    
                                    <td width="30%">
                                        
                                        &nbsp;</td>
                                    
                                </tr>
                                
                             
                                <tr>
                                    
                                    <td class="style2">
                                        
                                        &nbsp;</td>
                                    
                                 
                                    <td>
                                        
                                        Break Time</td>
                                    
                                 <td>
                                        
                                        <asp:TextBox ID="TxtBrkTime" runat="server" Width="60px"></asp:TextBox>
                                    </td>
                                    
                                 
                                    <td width="30%">
                                        
                                        &nbsp;</td>
                                    
                                   <td width="30%">
                                        
                                        &nbsp;</td>
                                    
                                </tr>
                                
                            </table>
                            
          
                      </ContentTemplate>                                                                       

                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel4" runat="server" HeaderText="Payroll Bank Account">
                        
                        <HeaderTemplate>
                            Payroll Bank Account
                        </HeaderTemplate>
                        
                       <ContentTemplate>
                            
                            <table class="style1">
                                
                                <tr>
                                     
                                     <td width="10%">
                                         
                                        &nbsp;<td>
                                             &nbsp;</td>
                                         <td>
                                             &nbsp;<td width="20%">
                                                 &nbsp;</td>
                                         </td>
                                     </td>
                                     
                                    </td>
                                     
                                     
                                </tr>
                                
                                <tr>
                                    
                                    <td>
                                        
                                        &nbsp;</td>
                                    
                                    <td width="10%">
                                        
                                        Name</td>
                                    
                                    <td>
                                        
                                        <asp:TextBox ID="TxtBank" runat="server" Width="350px"></asp:TextBox>
                                    </td>
                                    
                                    <td width="20%">
                                        
                                        &nbsp;</td>
                                    
                                </tr>
                                
                                <tr>
                                    
                                    <td>
                                        
                                        &nbsp;</td>
                                    
                                    <td width="10%">
                                        
                                        Address</td>
                                    
                                    <td width="20%">
                                        
                                        <asp:TextBox ID="TxtBankAddress" runat="server" Height="95px" 
                                            TextMode="MultiLine" Width="350px"></asp:TextBox>
                                    </td>
                                    
                                    <td>
                                        
                                        &nbsp;</td>
                                    
                                </tr>
                                
                                <tr>
                                    
                                    <td>
                                        
                                        &nbsp;</td>
                                    
                                   <td width="10%">
                                        
                                        Attention</td>
                                    
                                    <td width="20%">
                                        
                                        <asp:TextBox ID="TxtAttention" runat="server" Width="350px"></asp:TextBox>
                                    </td>
                                    
                                    <td>
                                        
                                        &nbsp;</td>
                                    
                                </tr>
                                
                                <tr>
                                    
                                   <td>
                                        
                                        &nbsp;</td>
                                    
                                    <td width="10%">
                                        
                                        Account No</td>
                                    
                                    <td width="20%">
                                        
                                        <asp:TextBox ID="TxtAccountNo" runat="server" Width="350px"></asp:TextBox>
                                    </td>
                                    
                                   <td>
                                        
                                        &nbsp;</td>
                                    
                                </tr>
                                
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td width="10%">
                                        Name1</td>
                                    <td width="20%">
                                        <asp:TextBox ID="TxtName1" runat="server" Width="350px"></asp:TextBox>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td width="10%">
                                        Designation1</td>
                                    <td width="20%">
                                        <asp:TextBox ID="TxtDeg1" runat="server" Width="350px"></asp:TextBox>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td width="10%">
                                        Name2</td>
                                    <td width="20%">
                                        <asp:TextBox ID="TxtName2" runat="server" Width="350px"></asp:TextBox>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td width="10%">
                                        Designation2</td>
                                    <td width="20%">
                                        <asp:TextBox ID="TxtDeg2" runat="server" Width="350px"></asp:TextBox>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                
                            </table>
                            
                        </ContentTemplate>
                        
                    </cc1:TabPanel>
                </cc1:TabContainer>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_InitializeRequest);
        prm.add_endRequest(prm_EndRequest);

        function prm_InitializeRequest(sender, args) {
            var panelProg = $get('divImage');
            if (args._postBackElement.id != "Timer1") {
                panelProg.style.display = '';
            }
        };

        function prm_EndRequest(sender, args) {
            var panelProg = $get('divImage');
            panelProg.style.display = 'none';
        };
    </script>

</body>
</html>

