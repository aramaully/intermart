﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmLocation.aspx.vb" Inherits="Ramus.FrmLocation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<html>
<head id="Head1" runat="server">
    <title>Create / Edit Location</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>
    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.

            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Create / Edit Locations'; //Set the name

            w.Width = 900; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 550;
            w.Top = 10;
            w.Left = 10;              
            w.Show(); //Show the window
        };
        function ShowFind() {
            wfind = window.frameElement.IDCWindow;
            wfind.CreateWindow('FindBank.aspx', w);
            return false;
        };
        function FunctionConfirm(Tlb,TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Save") {
                Index = '1';
                w.Confirm("Sure to Save", "Confirm",null,SaveFunction);
            }
            else if (TlbCmd == "New") {
                __doPostBack('ToolBar1', '0');
            }
            else if (TlbCmd == "Delete") {
                Index = '3';
                w.Confirm("Sure to Delete", "Confirm", null, SaveFunction);
            }
        }
        function SaveFunction(Sender,RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        } 
        function ShowReport() {
            wfind = window.frameElement.IDCWindow;
            wfind.CreateWindow('FrmRpt.aspx', w);
            return false;
        }; 
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style3
        {
            width: 169px;
        }
        .style4
        {
            width: 243px;
        }
        .style5
        {
            width: 157px;
        }
        .style6
        {
            width: 208px;
        }
        .style7
        {
            width: 161px;
        }
        .style8
        {
            width: 157px;
            height: 26px;
        }
        .style9
        {
            height: 26px;
        }
        .style10
        {
            width: 208px;
            height: 26px;
        }
        .style11
        {
            width: 161px;
            height: 26px;
        }
    </style>

</head>
<body onload="init();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px"
                runat="server" Height="32px" Width="32px" BorderStyle="None" Visible="True">
                <div id="divImage" style="display: none">
                    <asp:Image ID="img1" runat="server" ImageUrl="/Images/Wait.gif" />
                </div>
            </asp:Panel>
            <div id="toolbar" style="width: 100%;">
                <eo:ToolBar ID="ToolBar1" runat="server" BackgroundImage="00100103" ClientSideOnItemClick="FunctionConfirm"
                    BackgroundImageLeft="00100101" BackgroundImageRight="00100102" SeparatorImage="00100104"
                    Width="100%">
                    <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 1px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: #335ea8 1px solid; background-color: #99afd4; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <Items>
                        <eo:ToolBarItem CommandName="New" ImageUrl="00101001" ToolTip="New">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Save" ImageUrl="00101003" ToolTip="Save">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Delete" ImageUrl="~/images/delete.png" ToolTip="Delete">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Help" ImageUrl="~/images/help.png" ToolTip="Help">
                        </eo:ToolBarItem>
                    </Items>
                    <ItemTemplates>
                        <eo:ToolBarItem Type="Custom">
                            <NormalStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <HoverStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <DownStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="DropDownMenu">
                            <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                            <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; background-image: url(00100106); background-position-x: right;" />
                            <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: none; background-color:transparent; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                        </eo:ToolBarItem>
                    </ItemTemplates>
                    <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                </eo:ToolBar>
            </div>
            <div id="Content" style="width: 100%; height: 100%; overflow: auto; padding: 2px;
                border-right: solid 1px #cddaea;">
                <table class="style1" width="100%">
                    <tr>
                           <td class="style5">
                                Location</td>
                            <td class="style4">
                                <asp:TextBox ID="TxtLocation" runat="server" Width="150px"></asp:TextBox>
                            </td>
                            <td class="style3">
                                &nbsp;</td>
                            <td class="style6">
                                <asp:Button ID="BtnView" runat="server" Height="25px" Text="View" 
                                    Width="210px" />
                           </td>
                           <td class="style7">
                               &nbsp;</td>
                           <td>
                               &nbsp;</td>
                           <caption>
                           </caption>
                    </tr>
                    <tr>                        
                        <td class="style5">
                            Address</td>
                        <td class="style4">
                            <asp:TextBox ID="TxtAddress" runat="server" Height="25px" Width="226px"></asp:TextBox>
                        </td>
                        <td class="style3">
                            <asp:Label ID="lblMessage" runat="server">
                                </asp:Label>
                        </td>
                        <td class="style6">
                            &nbsp;&nbsp;</td>
                        <td class="style7">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <tr>
                            <td class="style8">
                                Fire Service Max. Capacity</td>
                            <td class="style9">
                                <asp:TextBox ID="TxtFireCap" runat="server" Width="150px"></asp:TextBox>
                            </td>
                            <td class="style9">
                                Fire Clearence Ref.</td>
                            <td class="style10">
                                <asp:TextBox ID="TxtFireVal" runat="server"></asp:TextBox>
                            </td>
                            <td class="style11">
                                Contact Person</td>
                            <td class="style9">
                                <asp:TextBox ID="TxtContact1" runat="server" Height="21px" Width="211px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style5">
                                Health Max. Capacity</td>
                            <td>
                                <asp:TextBox ID="TxtHealthCap" runat="server" Width="150px"></asp:TextBox>
                            </td>
                            <td>
                                Health Clearance Ref.</td>
                            <td class="style6">
                                <asp:TextBox ID="TxtHealthVal" runat="server"></asp:TextBox>
                            </td>
                            <td class="style7">
                                Contact Person</td>
                            <td>
                                <asp:TextBox ID="TxtContact2" runat="server" Height="21px" Width="211px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style5">
                                Lodging Permit No.</td>
                            <td>
                                <asp:TextBox ID="TxtLodgPermit" runat="server" Width="150px"></asp:TextBox>
                            </td>
                            <td>
                                Lodging Permit Validity From</td>
                            <td class="style6">
                                <asp:TextBox ID="TxtLogdFrom" runat="server" Height="22px" Width="83px"></asp:TextBox>
                                <cc1:CalendarExtender ID="TxtLogdFrom_CalendarExtender" runat="server" 
                                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtLogdFrom">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="style7">
                                Contact Person</td>
                            <td>
                                <asp:TextBox ID="TxtContact3" runat="server" Height="21px" Width="211px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style5">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                Lodging Permit Validity To</td>
                            <td class="style6">
                                <asp:TextBox ID="TxtLogdTo" runat="server" Height="21px" Width="83px"></asp:TextBox>
                                <cc1:CalendarExtender ID="TxtLogdTo_CalendarExtender" runat="server" 
                                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtLogdTo">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="style7">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style5">
                                &nbsp;</td>
                            <td colspan="3">
                                &nbsp;</td>
                            <td class="style7">
                                Remarks</td>
                            <td>
                                <asp:TextBox ID="TxtRemarks" runat="server" Height="21px" Width="211px"></asp:TextBox>
                            </td>
                        </tr>
                    </tr>
                </table>
                <asp:Panel ID="Panel8" runat="server" Height="100%" ScrollBars="Vertical" Width="100%"
                    BorderStyle="Inset" BorderWidth="1px">
                    <asp:GridView ID="GdvDept" runat="server" Width="100%" AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="Location" HeaderText="Location" DataFormatString="{0:dd/MM/yyyy}" >
                                <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                <ItemStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FireMaxCapacity" HeaderText="Fire Capacity" >
                                <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                <ItemStyle Wrap="False" Width="50px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FireClear" HeaderText="Fire Clearance">
                                <HeaderStyle Font-Size="Smaller" ForeColor="White" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Contact1" HeaderText="FireContact">
                                <HeaderStyle Font-Size="Smaller" ForeColor="White" />
                            </asp:BoundField>
                            <asp:BoundField DataField="HealthMaxCapacity" HeaderText="Health Capacity">
                                <HeaderStyle Font-Size="Smaller" ForeColor="White" />
                            </asp:BoundField>
                            <asp:BoundField DataField="HealthClear" HeaderText="Health Clearance" DataFormatString="{0:dd/MM/yyyy}" >
                                <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                <ItemStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Contact2" HeaderText="HealthContact">
                                <HeaderStyle Font-Size="Smaller" ForeColor="White" />
                            </asp:BoundField>
                            <asp:BoundField DataField="LodgPermit" HeaderText="Lodging Permit" >
                                <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                <ItemStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="LodgFrom" HeaderText="Valid From" >
                                <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                <ItemStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="LodgTo" HeaderText="Valid To" >
                                <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                <ItemStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Contact3" HeaderText="PermitContact">
                                <HeaderStyle Font-Size="Smaller" ForeColor="White" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Address" HeaderText="Address">
                                <HeaderStyle Font-Size="Smaller" ForeColor="White" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Remarks" HeaderText="Remarks">
                                <HeaderStyle Font-Size="Smaller" ForeColor="White" />
                            </asp:BoundField>
                            
                        </Columns>
                        <HeaderStyle BackColor="#0080C0" />
                        <AlternatingRowStyle BackColor="#CAE4FF" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_InitializeRequest);
        prm.add_endRequest(prm_EndRequest);

        function prm_InitializeRequest(sender, args) {
            var panelProg = $get('divImage');
            if (args._postBackElement.id != "Timer1") {
            panelProg.style.display = '';}
        };

        function prm_EndRequest(sender, args) {
            var panelProg = $get('divImage');
            panelProg.style.display = 'none';
        };
    </script>
</body>
</html>