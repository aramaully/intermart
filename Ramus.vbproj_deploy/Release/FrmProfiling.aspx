﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmProfiling.aspx.vb" Inherits="Ramus.FrmProfiling" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<html>
<head id="Head1" runat="server">
    <title>Profiling Module</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>
    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.

            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Profiling Module'; //Set the name
            w.Width = 1000; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 500;
            w.Top = 10;
            w.Left = 10;
            w.Show(); //Show the window
        };
        function ShowFind() {
            var FindWindow = window.open("FindEmployee.aspx", "FindEmployee", "width=565,height=435,top=200, left=575");
        };
        function FunctionConfirm(Tlb, TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Save") {
                    Index = '2';
                    w.Confirm("Sure to Save", "Confirm", null, SaveFunction);
            }
            else if (TlbCmd == "Find") {
                __doPostBack('ToolBar1', '1');
            }
            else if (TlbCmd == "New") {
                __doPostBack('ToolBar1', '0');
            }
            else if (TlbCmd == "Delete") {
                Index = '4';
                w.Confirm("Sure to Delete", "Confirm", null, SaveFunction);
            }
            else if (TlbCmd == "Next") {
                __doPostBack('ToolBar1', '6');
            }
            else if (TlbCmd == "Previous") {
                __doPostBack('ToolBar1', '7');
            }
        }
        function SaveFunction(Sender, RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        }
        function FunPic() {
            var myArgs = 'nothing';
            myargs = window.showModalDialog('Snap.aspx', myArgs, "dialogHeight:350px;dialogWidth:650px");
            if (myArgs == 'nothing') {
                PageMethods.AjaxSetSession("True");
            }
        }
        function ShowReport() {
            wfind = window.frameElement.IDCWindow;
            wfind.CreateWindow('FrmRpt.aspx', w);
            return false;
        };      
    </script>

    <style type="text/css">
        .style3
        {            width: 0%;
        }
        .style5
        {
            width: 38%;
        }
        .style6
        {
            width: 17%;
        }
        .style7
        {
            width: 268435424px;
        }
        .style8
        {
            width: 54px;
        }
        .style10
        {
            width: 405px;
        }
        </style>

</head>
<body onload="init();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px;
                height: 32px; width: 32px;" runat="server" BorderStyle="None" Visible="True">
                <div id="divImage" style="display: none">
                    <asp:Image ID="img1" runat="server" ImageUrl="/Images/Wait.gif" />
                </div>
            </asp:Panel>
            <div id="toolbar" style="width: 100%;">
                <eo:ToolBar ID="ToolBar1" runat="server" BackgroundImage="00100103" ClientSideOnItemClick="FunctionConfirm"
                    BackgroundImageLeft="00100101" BackgroundImageRight="00100102" SeparatorImage="00100104"
                    Width="100%">
                    <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 1px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: #335ea8 1px solid; background-color: #99afd4; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <Items>
                        <eo:ToolBarItem CommandName="New" ImageUrl="00101001" ToolTip="New">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Find" ImageUrl="~/images/findHS.png" ToolTip="Find Employee">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Save" ImageUrl="00101003" ToolTip="Save">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Delete" ImageUrl="~/images/delete.png" ToolTip="Delete">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Next" ImageUrl="~/images/NavForward.png" ToolTip="Next Record">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Previous" ImageUrl="~/images/NavBack.png" ToolTip="Previous Record">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Help" ImageUrl="~/images/help.png" ToolTip="Help">
                        </eo:ToolBarItem>
                    </Items>
                    <ItemTemplates>
                        <eo:ToolBarItem Type="Custom">
                            <NormalStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <HoverStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <DownStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="DropDownMenu">
                            <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                            <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; background-image: url(00100106); background-position-x: right;" />
                            <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: none; background-color:transparent; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                        </eo:ToolBarItem>
                    </ItemTemplates>
                    <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                </eo:ToolBar>
            </div>
            <div id="Content" style="width: 100%; height: 498px; overflow: auto; padding: 2px;
                border-right: solid 1px #cddaea;">
                <table style="height: 132%; width: 1296px;">
                    <tr>
                        <td class="style6" valign="middle">
                            Employee ID</td>
                        <td class="style3" width="20%" valign="middle">
                            <asp:TextBox ID="TxtEmpID" runat="server" AutoPostBack="True" 
                                ValidationGroup="GrpEmpId"></asp:TextBox>
                        </td>
                        <td class="style3" style="width: 10%" valign="middle" width="20%">
                        </td>
                        <td class="style8" valign="middle">
                            Date</td>
                        <td class="style5" valign="middle" colspan="2">
                            <asp:TextBox ID="TxtDate" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="TxtDate_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtDate">
                            </cc1:CalendarExtender>
                        </td>
                        <td class="style7" valign="middle">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style6">
                            Last Name</td>
                        <td colspan="4">
                            <asp:TextBox ID="TxtLast" runat="server" Width="330px" Height="22px"></asp:TextBox>
                        </td>
                        <td class="style3" colspan="2">
                            &nbsp;</td>
                        <td class="style7">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style6">
                            First Name</td>
                        <td class="style3" colspan="3">
                            <asp:TextBox ID="TxtFirst" runat="server" Width="328px" Height="21px"></asp:TextBox>
                        </td>
                        <td class="style3" colspan="4">
                            <asp:Button ID="BtnView" runat="server" Height="25px" Text="View Report" 
                                Width="129px" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="style6">
                            Address</td>
                        <td class="style3" colspan="3">
                            <asp:TextBox ID="TxtAddress" runat="server" Height="21px" Width="327px"></asp:TextBox>
                        </td>
                        <td class="style3" colspan="4">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style6">
                            Date of Birth</td>
                        <td class="style3" width="20%" colspan="2">
                            <asp:TextBox ID="TxtDOB" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="TxtDOB_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtDOB">
                            </cc1:CalendarExtender>
                        </td>
                        <td class="style8">
                            <div>
                                <asp:Label ID="lblMessage" runat="server"> </asp:Label>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td class="style6">
                            Gender</td>
                        <td class="style3" width="20%" colspan="2">
                            <asp:RadioButton ID="OptMale" runat="server" Checked="True" GroupName="Gender" 
                                Text="Male" />
                            <asp:RadioButton ID="OptFemale" runat="server" GroupName="Gender" 
                                Text="Female" />
                        </td>
                        <td class="style8">
                            Age</td>
                        <td class="style10">
                            <asp:DropDownList ID="CmbAge" runat="server">
                                <asp:ListItem>Select...</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style6">
                            Marital Status</td>
                        <td class="style3" width="20%" colspan="2">
                            <asp:DropDownList ID="CmbStatus" runat="server" Height="16px" Width="127px" >
                                <asp:ListItem>Select...</asp:ListItem>
                                <asp:ListItem>Single</asp:ListItem>
                                <asp:ListItem>Married</asp:ListItem>
                                <asp:ListItem>Divorced</asp:ListItem>
                                <asp:ListItem>Widowed</asp:ListItem>
                                <asp:ListItem>Other</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="style8">
                            Title</td>
                        <td class="style10">
                            <asp:DropDownList ID="CmbTitle" runat="server">
                                <asp:ListItem>Select...</asp:ListItem>
                                <asp:ListItem>Ms</asp:ListItem>
                                <asp:ListItem>Mrs</asp:ListItem>
                                <asp:ListItem>Mr</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style6">
                            Email</td>
                        <td class="style3" colspan="2" width="20%">
                            <asp:TextBox ID="TxtEmail" runat="server" AutoPostBack="True" Height="22px" 
                                ValidationGroup="GrpEmpId" Width="323px"></asp:TextBox>
                        </td>
                        <td class="style8">
                            Phone</td>
                        <td class="style10">
                            <asp:TextBox ID="TxtPhone" runat="server" AutoPostBack="True" 
                                ValidationGroup="GrpEmpId"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style6">
                            NID</td>
                        <td class="style3" colspan="2" width="20%">
                            <asp:TextBox ID="TxtNID" runat="server" AutoPostBack="True" 
                                ValidationGroup="GrpEmpId"></asp:TextBox>
                        </td>
                        <td class="style8">
                            Mobile</td>
                        <td class="style10">
                            <asp:TextBox ID="TxtMobile" runat="server" AutoPostBack="True" 
                                ValidationGroup="GrpEmpId"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style6">
                            Nationality</td>
                        <td class="style3" colspan="2" width="20%">
                            <asp:TextBox ID="TxtNationality" runat="server" AutoPostBack="True" 
                                ValidationGroup="GrpEmpId"></asp:TextBox>
                        </td>
                        <td class="style8">
                            Arm Reach</td>
                        <td class="style10">
                            <asp:TextBox ID="TxtArmReach" runat="server" AutoPostBack="True" 
                                ValidationGroup="GrpEmpId"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style6">
                            Height</td>
                        <td class="style3" colspan="2" width="20%">
                            <asp:TextBox ID="TxtHeight" runat="server" AutoPostBack="True" 
                                ValidationGroup="GrpEmpId"></asp:TextBox>
                        </td>
                        <td class="style8">
                            Weight</td>
                        <td class="style10">
                            <asp:TextBox ID="TxtWeight" runat="server" AutoPostBack="True" 
                                ValidationGroup="GrpEmpId"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style6">
                            Family Details</td>
                        <td class="style3" colspan="2" width="20%">
                            <asp:TextBox ID="TxtFamily" runat="server" Height="87px" MaxLength="255" 
                                TextMode="MultiLine" Width="321px"></asp:TextBox>
                        </td>
                        <td class="style8">
                            Medical Details</td>
                        <td class="style10">
                            <asp:TextBox ID="TxtMed" runat="server" Height="87px" MaxLength="255" 
                                TextMode="MultiLine" Width="321px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style6">
                            Education Details</td>
                        <td class="style3" colspan="2" width="20%">
                            <asp:TextBox ID="TxtEdu" runat="server" Height="87px" MaxLength="255" 
                                TextMode="MultiLine" Width="321px"></asp:TextBox>
                        </td>
                        <td class="style8">
                            Experience Details</td>
                        <td class="style10">
                            <asp:TextBox ID="TxtExp" runat="server" Height="87px" MaxLength="255" 
                                TextMode="MultiLine" Width="321px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style6">
                            Emergency Contact Details</td>
                        <td class="style3" colspan="2" width="20%">
                            <asp:TextBox ID="TxtEmer" runat="server" Height="87px" MaxLength="255" 
                                TextMode="MultiLine" Width="321px"></asp:TextBox>
                        </td>
                        <td class="style8">
                            &nbsp;</td>
                        <td class="style10">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="8" height="100%" valign="top">
                            &nbsp;</td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:Timer ID="Timer1" runat="server" Interval="100" Enabled="False">
            </asp:Timer>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_InitializeRequest);
        prm.add_endRequest(prm_EndRequest);

        function prm_InitializeRequest(sender, args) {
            var panelProg = $get('divImage');
            if (args._postBackElement.id != "Timer1") {
                panelProg.style.display = '';
            }
        };

        function prm_EndRequest(sender, args) {
            var panelProg = $get('divImage');
            panelProg.style.display = 'none';
        };
    </script>
</body>
</html>