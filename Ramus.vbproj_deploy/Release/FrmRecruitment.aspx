﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmRecruitment.aspx.vb" Inherits="Ramus.FrmRecruitment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<html>
<head id="Head1" runat="server">
    <title>Recruitment</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.
            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Recruitment'; //Set the name
            w.Width = 1200; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 560;
            w.Top = 10;
            w.Left = 10;
            w.Show(); //Show the window
        };
        function ShowFindRec() {
            var FindWindow = window.open("FindRecruitment.aspx", "FindRecruitment", "width=565,height=435,top=200, left=575");
        };
        function FunctionConfirm(Tlb, TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Save") {
                Index = '2';
                w.Confirm("Sure to Save", "Confirm", null, SaveFunction);
            }
            else if (TlbCmd == "Find") {
                __doPostBack('ToolBar1', '1');
            }
            else if (TlbCmd == "New") {
                __doPostBack('ToolBar1', '0');
            }
            else if (TlbCmd == "Next") {
                __doPostBack('ToolBar1', '5');
            }
            else if (TlbCmd == "Previous") {
                __doPostBack('ToolBar1', '4');
            }
        }
        function SaveFunction(Sender, RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        }
        function FunPic() {
            var myArgs = 'nothing';
            myargs = window.showModalDialog('Snap.aspx', myArgs, "dialogHeight:350px;dialogWidth:650px");
            if (myArgs == 'nothing') {
                PageMethods.AjaxSetSession("True");
            }
        }
        function ShowReport() {
            wfind = window.frameElement.IDCWindow;
            wfind.CreateWindow('FrmRpt.aspx', w);
            return false;
        };
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
            height: 140px;
        }
        .style29
        {
            height: 12px;
            width: 231px;
        }
        .style30
        {
            width: 254px;
            height: 12px;
        }
        .style33
        {
            height: 20px;
            width: 766px;
        }
        .style34
        {
            height: 6px;
            width: 231px;
        }
        .style35
        {
            width: 254px;
            height: 6px;
        }
        .style48
        {
            width: 254px;
            height: 20px;
        }
        .style49
        {
            height: 12px;
            width: 125px;
        }
        .style50
        {
            height: 6px;
            width: 125px;
        }
        .style51
        {
            width: 125px;
            height: 20px;
        }
        .style52
        {
            height: 12px;
            width: 281px;
        }
        .style53
        {
            height: 6px;
            width: 281px;
        }
        .style54
        {
            height: 20px;
            width: 281px;
        }
        .style55
        {
            height: 12px;
            width: 268px;
        }
        .style56
        {
            height: 6px;
            width: 268px;
        }
        .style57
        {
            height: 20px;
            width: 268px;
        }
        .style59
        {
            width: 231px;
        }
        .style60
        {
            width: 231px;
            height: 20px;
        }
        </style>
</head>
<body onload="init();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px;
                height: 32px; width: 32px;" runat="server" BorderStyle="None" Visible="True">
                <div id="divImage" style="display: none">
                    <asp:Image ID="img1" runat="server" ImageUrl="/Images/Wait.gif" />
                </div>
            </asp:Panel>
            <div id="toolbar" style="width: 100%;">
                <eo:ToolBar ID="ToolBar1" runat="server" BackgroundImage="00100103" ClientSideOnItemClick="FunctionConfirm"
                    BackgroundImageLeft="00100101" BackgroundImageRight="00100102" SeparatorImage="00100104"
                    Width="100%">
                    <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 1px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: #335ea8 1px solid; background-color: #99afd4; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <Items>
                        <eo:ToolBarItem CommandName="New" ImageUrl="00101001" ToolTip="New">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Find" ImageUrl="~/images/findHS.png" ToolTip="Find Employee">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Save" ImageUrl="00101003" ToolTip="Save">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Previous" ImageUrl="~/images/NavBack.png" ToolTip="Previous Record">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Next" ImageUrl="~/images/NavForward.png" ToolTip="Next Record">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Help" ImageUrl="~/images/help.png" ToolTip="Help">
                        </eo:ToolBarItem>
                    </Items>
                    <ItemTemplates>
                        <eo:ToolBarItem Type="Custom">
                            <NormalStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <HoverStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <DownStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="DropDownMenu">
                            <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                            <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; background-image: url(00100106); background-position-x: right;" />
                            <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: none; background-color:transparent; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                        </eo:ToolBarItem>
                    </ItemTemplates>
                    <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                </eo:ToolBar>
            </div>
            <div id="Content" style="width: 100%; height: 117%; padding: 2px;
                border-right: solid 1px #cddaea;">                
            <asp:Panel ID="Panel1" runat="server" BorderStyle="Inset" BorderWidth="1" 
                    Height="621px" Width="1400px">
                <table class="style1">
                    <tr>
                        <td class="style49">
                            Emp ID
                        </td>
                        <td class="style30">
                            <asp:TextBox ID="TxtEmpID" runat="server" AutoPostBack="True"></asp:TextBox>
                        </td>
                        <td class="style52">
                            Date</td>
                        <td class="style55">
                            <asp:TextBox ID="TxtDate" runat="server" Height="21px" Width="108px"></asp:TextBox>
                            <cc1:CalendarExtender ID="TxtDate_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtDate">
                            </cc1:CalendarExtender>
                        </td>
                        <td class="style29">
                            <asp:Button ID="BtnCheckID" runat="server" Height="25px" Text="Check Payroll EmpID" 
                                Width="210px" />
                             </td>
                        <td align="right" rowspan="11" valign="top">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style50">
                            Last Name</td>
                        <td class="style35">
                            <asp:TextBox ID="TxtLast" runat="server" Width="232px" ></asp:TextBox>
                        </td>
                        <td class="style53">
                            Post Applied</td>
                        <td class="style56">
                            <asp:TextBox ID="TxtPostApp" runat="server" Width="232px"></asp:TextBox>
                        </td>
                        <td class="style34">
                            <asp:Button ID="BtnView" runat="server" Height="25px" Text="View" 
                                Width="210px" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style51">
                            First Name</td>
                        <td class="style48">
                            <asp:TextBox ID="TxtFirst" runat="server" Width="233px"></asp:TextBox>
                        </td>
                        <td class="style54">
                            &nbsp;Interviewed Date</td>
                        <td class="style57">
                            <asp:TextBox ID="TxtInterviewDate" runat="server" Height="21px" Width="108px"></asp:TextBox>
                            <cc1:CalendarExtender ID="TxtInterviewDate_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtInterviewDate">
                            </cc1:CalendarExtender>
                        </td>
                        <td class="style60">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style51">
                            Address</td>
                        <td class="style48">
                            <asp:TextBox ID="TxtAddress" runat="server" Width="233px"></asp:TextBox>
                        </td>
                        <td class="style54">
                            Short Listed</td>
                        <td class="style57">
                            <asp:CheckBox ID="ChkShortListed" runat="server" Text="Yes" />
                        </td>
                        <td class="style60">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style51">
                            Email</td>
                        <td class="style48">
                            <asp:TextBox ID="TxtEmail" runat="server" AutoPostBack="True" Height="23px" 
                                ValidationGroup="GrpEmpId" Width="230px"></asp:TextBox>
                        </td>
                        <td class="style54">
                            Rejected</td>
                        <td class="style57">
                            <asp:CheckBox ID="ChkRejected" runat="server" Text="Yes" />
                        </td>
                        <td class="style60">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style51">
                            Age</td>
                        <td class="style48">
                            <asp:DropDownList ID="CmbAge" runat="server" Height="20px" Width="108px">
                                <asp:ListItem>Select...</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="style54">
                            Recruited</td>
                        <td class="style57">
                            <asp:CheckBox ID="ChkRecruited" runat="server" Text="Yes" />
                        </td>
                        <td class="style60">
                            <asp:Button ID="BtnMovetoPayroll" runat="server" Height="26px" Text="Move to Payroll" 
                                Width="218px" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style51">
                            Date of Birth</td>
                        <td class="style48">
                            <asp:TextBox ID="TxtDOB" runat="server" Height="21px" Width="108px"></asp:TextBox>
                            <cc1:CalendarExtender ID="TxtDOB_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtDOB">
                            </cc1:CalendarExtender>
                        </td>
                        <td class="style54">
                            &nbsp;</td>
                        <td class="style57">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                        <td class="style60">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style51">
                            Gender</td>
                        <td class="style48">
                            <asp:DropDownList ID="CmdGender" runat="server" Width="108px">
                                <asp:ListItem>Select...</asp:ListItem>
                                <asp:ListItem>Male</asp:ListItem>
                                <asp:ListItem>Female</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="style54">
                            Nationality</td>
                        <td class="style57">
                            <asp:TextBox ID="TxtNationality" runat="server" AutoPostBack="True" 
                                ValidationGroup="GrpEmpId"></asp:TextBox>
                        </td>
                        <td class="style60">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style51">
                            Marital Stauts</td>
                        <td class="style48">
                            <asp:DropDownList ID="CmbStatus" runat="server" Height="16px" Width="127px">
                                <asp:ListItem>Select...</asp:ListItem>
                                <asp:ListItem>Single</asp:ListItem>
                                <asp:ListItem>Married</asp:ListItem>
                                <asp:ListItem>Divorced</asp:ListItem>
                                <asp:ListItem>Widowed</asp:ListItem>
                                <asp:ListItem>Other</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="style54">
                            Phone</td>
                        <td class="style57">
                            <asp:TextBox ID="TxtPhone" runat="server" Width="108px"></asp:TextBox>
                        </td>
                        <td class="style60">
                            NID</td>
                        <td>
                            <asp:TextBox ID="TxtNID" runat="server" AutoPostBack="True" 
                                ValidationGroup="GrpEmpId"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style51">
                            Arm Reach</td>
                        <td class="style48">
                            <asp:TextBox ID="TxtArmReach" runat="server" AutoPostBack="True" 
                                ValidationGroup="GrpEmpId"></asp:TextBox>
                            cm</td>
                        <td class="style54">
                            Weigth</td>
                        <td class="style57">
                            <asp:TextBox ID="TxtWeight" runat="server" AutoPostBack="True" 
                                ValidationGroup="GrpEmpId"></asp:TextBox>
                            cm</td>
                        <td class="style59">
                            Height</td>
                        <td class="style33">
                            <asp:TextBox ID="TxtHeight" runat="server" AutoPostBack="True" 
                                ValidationGroup="GrpEmpId"></asp:TextBox>
                            cm</td>
                    </tr>
                    <tr>
                        <td class="style51">
                            &nbsp;</td>
                        <td class="style48">
                            &nbsp;</td>
                        <td class="style54">
                            &nbsp;</td>
                        <td class="style57">
                            &nbsp;</td>
                        <td class="style60">
                            &nbsp;</td>
                    </tr>
                </table>
                <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="4" 
                    Height="300px" Width="100%">
                    <cc1:TabPanel ID="TabPanel7" runat="server" HeaderText="QualificationSkills">
                        <HeaderTemplate>
                           Experience
                        </HeaderTemplate>
 <ContentTemplate>
                        <asp:Panel ID="Panel5" runat="server" BorderStyle="Inset" BorderWidth="1px" 
                            Width="1379px">
                    <table class="style1">
                        <tr>
                            <td class="style37">
                                From Date 1</td>
                            <td class="style36">
                                <asp:TextBox ID="TxtFrom1" runat="server" Height="22px" Width="84px"></asp:TextBox>
                                <cc1:CalendarExtender ID="TxtFrom1_CalendarExtender" runat="server" 
                                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtFrom1">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="style47">
                                To Date&nbsp; </td>
                            <td class="style39">
                                <asp:TextBox ID="TxtTo1" runat="server" Height="22px" Width="84px"></asp:TextBox>
                                <cc1:CalendarExtender ID="TxtTo1_CalendarExtender" runat="server" 
                                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtTo1">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="style40">
                                Company</td>
                            <td class="style44">
                                <asp:TextBox ID="TxtCompany1" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style41">
                                Role</td>
                            <td class="style45">
                                <asp:TextBox ID="TxtRole1" runat="server" TextMode="MultiLine" Width="120px"></asp:TextBox>
                            </td>
                            <td class="style42">
                                Responsibility</td>
                            <td class="style46">
                                <asp:TextBox ID="TxtResp1" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style43">
                                Condition</td>
                            <td class="style26">
                                <asp:TextBox ID="TxtCondition1" runat="server" TextMode="MultiLine" 
                                    Width="140px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style37">
                                2</td>
                            <td class="style36">
                                <asp:TextBox ID="TxtFrom2" runat="server" Height="22px" Width="84px"></asp:TextBox>
                                <cc1:CalendarExtender ID="TxtFrom2_CalendarExtender" runat="server" 
                                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtFrom2">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="style47">
                                &nbsp;</td>
                            <td class="style39">
                                <asp:TextBox ID="TxtTo2" runat="server" Height="22px" Width="84px"></asp:TextBox>
                                <cc1:CalendarExtender ID="TxtTo2_CalendarExtender" runat="server" 
                                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtTo2">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="style40">
                                &nbsp;</td>
                            <td class="style44">
                                <asp:TextBox ID="TxtCompany2" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style41">
                                &nbsp;</td>
                            <td class="style45">
                                <asp:TextBox ID="TxtRole2" runat="server" TextMode="MultiLine" Width="120px"></asp:TextBox>
                            </td>
                            <td class="style42">
                                &nbsp;</td>
                            <td class="style46">
                                <asp:TextBox ID="TxtResp2" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style43">
                                &nbsp;</td>
                            <td class="style26">
                                <asp:TextBox ID="TxtCondition2" runat="server" TextMode="MultiLine" 
                                    Width="140px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style37">
                                &nbsp;3</td>
                            <td class="style36">
                                <asp:TextBox ID="TxtFrom3" runat="server" Height="22px" Width="84px"></asp:TextBox>
                                <cc1:CalendarExtender ID="TxtFrom3_CalendarExtender" runat="server" 
                                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtFrom3">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="style47">
                                &nbsp;</td>
                            <td class="style39">
                                <asp:TextBox ID="TxtTo3" runat="server" Height="22px" Width="84px"></asp:TextBox>
                                <cc1:CalendarExtender ID="TxtTo3_CalendarExtender" runat="server" 
                                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtTo3">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="style40">
                                &nbsp;</td>
                            <td class="style44">
                                <asp:TextBox ID="TxtCompany3" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style41">
                                &nbsp;</td>
                            <td class="style45">
                                <asp:TextBox ID="TxtRole3" runat="server" TextMode="MultiLine" Width="120px"></asp:TextBox>
                            </td>
                            <td class="style42">
                                &nbsp;</td>
                            <td class="style46">
                                <asp:TextBox ID="TxtResp3" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style43">
                                &nbsp;</td>
                            <td class="style26">
                                <asp:TextBox ID="TxtCondition3" runat="server" TextMode="MultiLine" 
                                    Width="140px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style37">
                                &nbsp;4</td>
                            <td class="style36">
                                <asp:TextBox ID="TxtFrom4" runat="server" Height="22px" Width="84px"></asp:TextBox>
                                <cc1:CalendarExtender ID="TxtFrom4_CalendarExtender" runat="server" 
                                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtFrom4">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="style47">
                                &nbsp;</td>
                            <td class="style39">
                                <asp:TextBox ID="TxtTo4" runat="server" Height="22px" Width="84px"></asp:TextBox>
                                <cc1:CalendarExtender ID="TxtTo4_CalendarExtender" runat="server" 
                                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtTo4">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="style40">
                                &nbsp;</td>
                            <td class="style44">
                                <asp:TextBox ID="TxtCompany4" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style41">
                                &nbsp;</td>
                            <td class="style45">
                                <asp:TextBox ID="TxtRole4" runat="server" TextMode="MultiLine" Width="120px"></asp:TextBox>
                            </td>
                            <td class="style42">
                                &nbsp;</td>
                            <td class="style46">
                                <asp:TextBox ID="TxtResp4" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style43">
                                &nbsp;</td>
                            <td class="style26">
                                <asp:TextBox ID="TxtCondition4" runat="server" TextMode="MultiLine" 
                                    Width="140px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style37">
                                &nbsp;5</td>
                            <td class="style36">
                                <asp:TextBox ID="TxtFrom5" runat="server" Height="22px" Width="84px"></asp:TextBox>
                                <cc1:CalendarExtender ID="TxtFrom5_CalendarExtender" runat="server" 
                                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtFrom5">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="style47">
                                &nbsp;</td>
                            <td class="style39">
                                <asp:TextBox ID="TxtTo5" runat="server" Height="22px" Width="84px"></asp:TextBox>
                                <cc1:CalendarExtender ID="TxtTo5_CalendarExtender" runat="server" 
                                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtTo5">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="style40">
                                &nbsp;</td>
                            <td class="style44">
                                <asp:TextBox ID="TxtCompany5" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style41">
                                &nbsp;</td>
                            <td class="style45">
                                <asp:TextBox ID="TxtRole5" runat="server" TextMode="MultiLine" Width="120px"></asp:TextBox>
                            </td>
                            <td class="style42">
                                &nbsp;</td>
                            <td class="style46">
                                <asp:TextBox ID="TxtResp5" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style43">
                                &nbsp;</td>
                            <td class="style26">
                                <asp:TextBox ID="TxtCondition5" runat="server" TextMode="MultiLine" 
                                    Width="140px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel9" runat="server" HeaderText="Experience">
                        <HeaderTemplate>
                             Qualification / Skills
                        </HeaderTemplate>
                         <ContentTemplate>
                        <asp:Panel ID="Panel2" runat="server" BorderStyle="Inset" BorderWidth="1px" 
                                 Height="354px">
                    <table class="style1">
                        <tr>
                            <td class="style37">
                                Year 1 Start</td>
                            <td class="style36">
                                <asp:TextBox ID="TxtSYear1" runat="server" Width="65px"></asp:TextBox>
                            </td>
                            <td class="style47">
                                Year End</td>
                            <td class="style39">
                                <asp:TextBox ID="TxtEYear1" runat="server" Width="60px"></asp:TextBox>
                            </td>
                            <td class="style40">
                                Level</td>
                            <td class="style44">
                                <asp:TextBox ID="TxtLevel1" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style41">
                                Course</td>
                            <td class="style45">
                                <asp:TextBox ID="TxtCourse1" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style42">
                                Institution</td>
                            <td class="style46">
                                <asp:TextBox ID="TxtIns1" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style43">
                                Modules</td>
                            <td class="style26">
                                <asp:TextBox ID="TxtMod1" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style37">
                                Year 2</td>
                            <td class="style36">
                                <asp:TextBox ID="TxtSYear2" runat="server" Width="65px"></asp:TextBox>
                            </td>
                            <td class="style47">
                                &nbsp;</td>
                            <td class="style39">
                                <asp:TextBox ID="TxtEYear2" runat="server" Width="60px"></asp:TextBox>
                            </td>
                            <td class="style40">
                                &nbsp;</td>
                            <td class="style44">
                                <asp:TextBox ID="TxtLevel2" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style41">
                                &nbsp;</td>
                            <td class="style45">
                                <asp:TextBox ID="TxtCourse2" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style42">
                                &nbsp;</td>
                            <td class="style46">
                                <asp:TextBox ID="TxtIns2" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style43">
                                &nbsp;</td>
                            <td class="style26">
                                <asp:TextBox ID="TxtMod2" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style37">
                                Year 3</td>
                            <td class="style36">
                                <asp:TextBox ID="TxtSYear3" runat="server" Width="65px"></asp:TextBox>
                            </td>
                            <td class="style47">
                                &nbsp;</td>
                            <td class="style39">
                                <asp:TextBox ID="TxtEYear3" runat="server" Width="60px"></asp:TextBox>
                            </td>
                            <td class="style40">
                                &nbsp;</td>
                            <td class="style44">
                                <asp:TextBox ID="TxtLevel3" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style41">
                                &nbsp;</td>
                            <td class="style45">
                                <asp:TextBox ID="TxtCourse3" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style42">
                                &nbsp;</td>
                            <td class="style46">
                                <asp:TextBox ID="TxtIns3" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style43">
                                &nbsp;</td>
                            <td class="style26">
                                <asp:TextBox ID="TxtMod3" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style37">
                                Year 4</td>
                            <td class="style36">
                                <asp:TextBox ID="TxtSYear4" runat="server" Width="65px"></asp:TextBox>
                            </td>
                            <td class="style47">
                                &nbsp;</td>
                            <td class="style39">
                                <asp:TextBox ID="TxtEYear4" runat="server" Width="60px"></asp:TextBox>
                            </td>
                            <td class="style40">
                                &nbsp;</td>
                            <td class="style44">
                                <asp:TextBox ID="TxtLevel4" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style41">
                                &nbsp;</td>
                            <td class="style45">
                                <asp:TextBox ID="TxtCourse4" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style42">
                                &nbsp;</td>
                            <td class="style46">
                                <asp:TextBox ID="TxtIns4" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style43">
                                &nbsp;</td>
                            <td class="style26">
                                <asp:TextBox ID="TxtMod4" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style37">
                                Year 5</td>
                            <td class="style36">
                                <asp:TextBox ID="TxtSYear5" runat="server" Width="65px"></asp:TextBox>
                            </td>
                            <td class="style47">
                                &nbsp;</td>
                            <td class="style39">
                                <asp:TextBox ID="TxtEYear5" runat="server" Width="60px"></asp:TextBox>
                            </td>
                            <td class="style40">
                                &nbsp;</td>
                            <td class="style44">
                                <asp:TextBox ID="TxtLevel5" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style41">
                                &nbsp;</td>
                            <td class="style45">
                                <asp:TextBox ID="TxtCourse5" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style42">
                                &nbsp;</td>
                            <td class="style46">
                                <asp:TextBox ID="TxtIns5" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td class="style43">
                                &nbsp;</td>
                            <td class="style26">
                                <asp:TextBox ID="TxtMod5" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="InterviewPerson">
                        <HeaderTemplate>
                            Interview Person
                        </HeaderTemplate>
 <ContentTemplate>
                        <asp:Panel ID="Panel3" runat="server" BorderStyle="Inset" BorderWidth="1px">
                    <table class="style1">
                        <tr>
                            <td class="style38">
                                Person 1</td>
                            <td class="style39">
                                <asp:TextBox ID="TxtIntwPerson1" runat="server" Width="200px"></asp:TextBox>
                            </td>
                            <td class="style40">
                                Remarks 1</td>
                            <td class="style40">
                                <asp:TextBox ID="TxtRemark1" runat="server" Width="200px"></asp:TextBox>
                            </td>
                            <td class="style41">
                                </td>
                        </tr>
                        <tr>
                            <td class="style42">
                                Person 2</td>
                            <td class="style43">
                                <asp:TextBox ID="TxtIntwPerson2" runat="server" Width="200px"></asp:TextBox>
                            </td>
                            <td class="style44">
                                Remarks 2</td>
                            <td class="style44">
                                <asp:TextBox ID="TxtRemark2" runat="server" Width="200px"></asp:TextBox>
                            </td>
                            <td class="style45">
                                </td>
                        </tr>
                        <tr>
                            <td class="style37">
                                Person 3</td>
                            <td class="style36">
                                <asp:TextBox ID="TxtIntwPerson3" runat="server" Width="200px"></asp:TextBox>
                            </td>
                            <td class="style26">
                                Remarks 3</td>
                            <td class="style26">
                                <asp:TextBox ID="TxtRemark3" runat="server" Width="200px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="InterviewPerson">
                        <HeaderTemplate>
                            Family Details
                        </HeaderTemplate>
 <ContentTemplate>
                        <asp:Panel ID="Panel4" runat="server" BorderStyle="Inset" BorderWidth="1px">
                    <table class="style1">
                        <tr>
                        <td class="style3" width="20%">
                            <asp:TextBox ID="TxtFamily" runat="server" Height="87px" MaxLength="255" 
                                TextMode="MultiLine" Width="513px"></asp:TextBox>
                        </td>
                        </tr>
                    </table>
                </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel3" runat="server" HeaderText="InterviewPerson">
                        <HeaderTemplate>
                            Medical Details
                        </HeaderTemplate>
 <ContentTemplate>
                        <asp:Panel ID="Panel6" runat="server" BorderStyle="Inset" BorderWidth="1px">
                    <table class="style1">
                        <tr>
                        <td class="style10">
                            <asp:TextBox ID="TxtMed" runat="server" Height="87px" MaxLength="255" 
                                TextMode="MultiLine" Width="513px"></asp:TextBox>
                        </td>
                        </tr>
                    </table>
                </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                </cc1:TabContainer>
                </asp:Panel>                
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:Timer ID="Timer1" runat="server" Interval="100" Enabled="False">
            </asp:Timer>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_InitializeRequest);
        prm.add_endRequest(prm_EndRequest);

        function prm_InitializeRequest(sender, args) {
            var panelProg = $get('divImage');
            if (args._postBackElement.id != "Timer1") {
                panelProg.style.display = '';
            }
        };

        function prm_EndRequest(sender, args) {
            var panelProg = $get('divImage');
            panelProg.style.display = 'none';
        };
    </script>

</body>
</html>

