﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmGroupTraining.aspx.vb" Inherits="Ramus.FrmGroupTraining" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<html>
<head id="Head1" runat="server">
    <title>Employee CheckList Form</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.
            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Employee CheckList Form'; //Set the name
            w.Width = 775; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 460;            
            w.Top = 10;
            w.Left = 10;
            w.Show(); //Show the window
        };
        function ShowFind() {
            var FindWindow = window.open("FindEmployee.aspx", "FindEmployee", "width=565,height=435,top=200, left=575");
        };
        function FunctionConfirm(Tlb, TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Save") {
               Index = '1';
               w.Confirm("Sure to Save", "Confirm", null, SaveFunction);
            }
            else if (TlbCmd == "New") {
                __doPostBack('ToolBar1', '0');
            }
        }
        function SaveFunction(Sender, RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        }
        function FunPic() {
            var myArgs = 'nothing';
            myargs = window.showModalDialog('Snap.aspx', myArgs, "dialogHeight:350px;dialogWidth:650px");
            if (myArgs == 'nothing') {
                PageMethods.AjaxSetSession("True");
            }
        }
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
        }
        .style3
        {
            width: 221px;
            height: 26px;
        }
        .style4
        {
            height: 26px;
        }
    </style>
</head>
<body onload="init();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px;
                height: 32px; width: 32px;" runat="server" BorderStyle="None" Visible="True">
                <div id="divImage" style="display: none">
                    <asp:Image ID="img1" runat="server" ImageUrl="/Images/Wait.gif" />
                </div>
            </asp:Panel>
            <div id="toolbar" style="width: 100%;">
                <eo:ToolBar ID="ToolBar1" runat="server" BackgroundImage="00100103" ClientSideOnItemClick="FunctionConfirm"
                    BackgroundImageLeft="00100101" BackgroundImageRight="00100102" SeparatorImage="00100104"
                    Width="100%">
                    <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 1px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: #335ea8 1px solid; background-color: #99afd4; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <Items>
                        <eo:ToolBarItem CommandName="New" ImageUrl="00101001" ToolTip="New">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Save" ImageUrl="00101003" ToolTip="Save">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Help" ImageUrl="~/images/help.png" ToolTip="Help">
                        </eo:ToolBarItem>
                    </Items>
                    <ItemTemplates>
                        <eo:ToolBarItem Type="Custom">
                            <NormalStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <HoverStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <DownStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="DropDownMenu">
                            <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                            <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; background-image: url(00100106); background-position-x: right;" />
                            <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: none; background-color:transparent; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                        </eo:ToolBarItem>
                    </ItemTemplates>
                    <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                </eo:ToolBar>
            </div>
            <div id="Content" style="width: 100%; height: 100%; padding: 2px;
                border-right: solid 1px #cddaea;">                
            <asp:Panel ID="Panel1" runat="server" BorderStyle="Inset" BorderWidth="1">
                <table class="style1">
                    <tr>
                        <td class="style2">
                            Date</td>
                        <td colspan="3">
                            <asp:TextBox ID="TxtDate" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="TxtDate_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtDate">
                            </cc1:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            Start Date</td>
                        <td>
                            <asp:TextBox ID="TxtStartDate" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="TxtStartDate_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtStartDate">
                            </cc1:CalendarExtender>
                        </td>
                        <td>
                            End Date</td>
                        <td>
                            <asp:TextBox ID="TxtEndDate" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="TxtEndDate_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtEndDate">
                            </cc1:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            Training Title</td>
                        <td colspan="3">
                            <asp:DropDownList ID="CmbTraining" runat="server" AutoPostBack="True" 
                                ValidationGroup="Group1" Width="150px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            Training Name</td>
                        <td colspan="3">
                            <asp:TextBox ID="TxtName" runat="server" Width="450px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            Training Details</td>
                        <td colspan="3">
                            <asp:TextBox ID="TxtContent" runat="server" Height="59px" TextMode="MultiLine" 
                                Width="455px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style3">
                            Training Hours</td>
                        <td colspan="3" class="style4">
                            <asp:TextBox ID="TxtHours" runat="server" Width="35px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            Start Time
                        </td>
                        <td>
                            <asp:TextBox ID="TxtTimein" runat="server"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="TxtTimein_MaskedEditExtender" runat="server" 
                            acceptampm="false" mask="99:99" cultureampmplaceholder=""
                            culturecurrencysymbolplaceholder=""
                            culturedateformat="" culturedateplaceholder=""
                            culturedecimalplaceholder="" culturethousandsplaceholder=""
                            culturetimeplaceholder="" enabled="True" masktype="Time"
                            targetcontrolid="TxtTimein">
                            </cc1:MaskedEditExtender></td>
                        <td>
                            Ending Time
                        </td>
                        <td>
                            <asp:TextBox ID="TxtTimeOut" runat="server" AutoPostBack="True" Width="128px"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="TxtTimeOut_MaskedEditExtender" runat="server" 
                            acceptampm="false" mask="99:99" cultureampmplaceholder=""
                            culturecurrencysymbolplaceholder=""
                            culturedateformat="" culturedateplaceholder=""
                            culturedecimalplaceholder="" culturethousandsplaceholder=""
                            culturetimeplaceholder="" enabled="True" masktype="Time"
                            targetcontrolid="TxtTimeOut">
                            </cc1:MaskedEditExtender></td>
                    </tr>
                    <tr>
                        <td class="style2">
                            &nbsp;</td>
                        <td>
                            <asp:CheckBox ID="ChkAward" runat="server" Text="Award" />
                        </td>
                        <td>
                            <asp:CheckBox ID="ChkNonAward" runat="server" Text="Non-Award" />
                        </td>
                        <td>
                            <asp:CheckBox ID="ChkTainingPassport" runat="server" Text="Training Passport" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style2" colspan="4">
                            <asp:Label ID="LblError" runat="server" Visible="False"></asp:Label>
                        </td>
                    </tr>
                </table>
                </asp:Panel>                
                <asp:Panel ID="PnlGrid" runat="server" ScrollBars="Auto" BorderStyle="Inset" 
                    BorderWidth="1px" Height="330px">
                <asp:GridView ID="GdvCheckList" runat="server" Width="100%" 
                        AutoGenerateColumns="False">
                    <HeaderStyle BackColor="#003399" Font-Size="Smaller" ForeColor="White" />
                    <Columns>
                        <asp:BoundField DataField="EmpID" HeaderText="EmpID" />
                        <asp:BoundField HeaderText="Surmame" DataField="Last" />
                        <asp:BoundField DataField="First" HeaderText="First Name" />
                        <asp:BoundField DataField="Position" HeaderText="Position" />
                        <asp:TemplateField HeaderText="Attended" ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle Font-Size="Smaller" Font-bold ="True" ForeColor="White" Wrap="False" />
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="ChkSubmitted" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="#0080C0" />
                        <AlternatingRowStyle BackColor="#CAE4FF" />
                </asp:GridView>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_InitializeRequest);
        prm.add_endRequest(prm_EndRequest);

        function prm_InitializeRequest(sender, args) {
            var panelProg = $get('divImage');
            if (args._postBackElement.id != "Timer1") {
                panelProg.style.display = '';
            }
        };

        function prm_EndRequest(sender, args) {
            var panelProg = $get('divImage');
            panelProg.style.display = 'none';
        };
    </script>

</body>
</html>