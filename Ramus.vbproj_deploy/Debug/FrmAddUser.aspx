﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmAddUser.aspx.vb" Inherits="Ramus.FrmAddUser" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<html>
<head id="Head1" runat="server">
    <title>Create / Edit Users</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>
    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.

            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Create / Edit Users'; //Set the name

            w.Width = 550; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 400;
            w.Top = 10;
            w.Left = 10;              
            w.Show(); //Show the window
        };
        function ShowFind() {
            var FindWindow = window.open("FindEmployee.aspx", "FindEmployee", "width=565,height=435,top=200, left=575");
        };
        function FunctionConfirm(Tlb,TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Save") {
                var validated = Page_ClientValidate('Group1');
                if (validated) {
                    Index = '2';
                    w.Confirm("Sure to Save", "Confirm", null, SaveFunction);
                }
            }
             else if (TlbCmd == "Find") {
                __doPostBack('ToolBar1', '1');
            }
            else if (TlbCmd == "New") {
                __doPostBack('ToolBar1', '0');
            }
            else if (TlbCmd == "Delete") {
            Index = '4';
            w.Confirm("Sure to Delete", "Confirm", null, SaveFunction);
            }
        }
        function SaveFunction(Sender,RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        }
    </script>

    <style type="text/css">
        .style2
        {
        }
        .style3
        {
            width: 202px;
        }
        .style4
        {
            width: 16%;
        }
    </style>

</head>
<body onload="init();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px"
                runat="server" Height="32px" Width="32px" BorderStyle="None" Visible="True">
                <div id="divImage" style="display: none">
                    <asp:Image ID="img1" runat="server" ImageUrl="/Images/Wait.gif" />
                </div>
            </asp:Panel>
            <div id="toolbar" style="width: 100%;">
                <eo:ToolBar ID="ToolBar1" runat="server" BackgroundImage="00100103" ClientSideOnItemClick="FunctionConfirm"
                    BackgroundImageLeft="00100101" BackgroundImageRight="00100102" SeparatorImage="00100104"
                    Width="100%">
                    <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 1px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: #335ea8 1px solid; background-color: #99afd4; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <Items>
                        <eo:ToolBarItem CommandName="New" ImageUrl="00101001" ToolTip="New">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Find" ImageUrl="~/images/findHS.png" ToolTip="Find Employee">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Save" ImageUrl="00101003" ToolTip="Save">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Delete" ImageUrl="~/images/delete.png" ToolTip="Delete">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Help" ImageUrl="~/images/help.png" ToolTip="Help">
                        </eo:ToolBarItem>
                    </Items>
                    <ItemTemplates>
                        <eo:ToolBarItem Type="Custom">
                            <NormalStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <HoverStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <DownStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="DropDownMenu">
                            <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                            <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; background-image: url(00100106); background-position-x: right;" />
                            <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: none; background-color:transparent; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                        </eo:ToolBarItem>
                    </ItemTemplates>
                    <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                </eo:ToolBar>
            </div>
            <div id="Content" style="width: 100%; height: 357px; overflow: auto; padding: 2px;
                border-right: solid 1px #cddaea;">
                <table style="height: 100%; width: 536px;" width="550px">
                    <tr>
                        <td class="style2">
                            User ID</td>
                        <td class="style3" width="20%">
                            <asp:TextBox ID="TxtUserID" runat="server"></asp:TextBox>
                        </td>
                        <td class="style4">
                            Level</td>
                        <td width="50%">
                            <asp:DropDownList ID="CmbLevel" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            EmpID</td>
                        <td class="style3" width="20%">
                            <asp:TextBox ID="TxtEmpID" runat="server" AutoPostBack="True"></asp:TextBox>
                        </td>
                        <td class="style4">
                            Emp Name</td>
                        <td width="50%">
                            <asp:TextBox ID="TxtEmpName" runat="server" Width="100%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            Dept</td>
                        <td class="style3" width="20%">
                            <asp:DropDownList ID="CmbDepartment" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td class="style4">
                            &nbsp;</td>
                        <td width="50%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style2">
                            Password</td>
                        <td class="style3" width="20%">
                            <asp:TextBox ID="TxtPassword" runat="server" TextMode="Password" 
                                ValidationGroup="Group1"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                ControlToValidate="TxtPassword" ErrorMessage="Enter Password" 
                                ValidationGroup="Group1">*</asp:RequiredFieldValidator>
                        </td>
                        <td class="style4">
                            Re-Enter</td>
                        <td width="50%">
                            <asp:TextBox ID="TxtReEnter" runat="server" TextMode="Password" 
                                ValidationGroup="Group1">*</asp:TextBox>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                ControlToCompare="TxtPassword" ControlToValidate="TxtReEnter" 
                                ErrorMessage="Passwords do not match" Type="String" ValidationGroup="Group1">*</asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2" colspan="4">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                ValidationGroup="Group1" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" height="100%" valign="top">
                            <asp:Panel ID="Panel8" runat="server" Height="100%" ScrollBars="Horizontal"  
                                Width="100%" BorderStyle="Inset" BorderWidth="1px">
                                <asp:GridView ID="GdvUser" runat="server" AutoGenerateColumns="False" 
                                    Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="UserID" HeaderText="User ID" HtmlEncode="False">
                                            <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Name" HeaderText="Name" HtmlEncode="False" 
                                            HtmlEncodeFormatString="False">
                                            <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Password" DataFormatString="{0:*}" 
                                            HeaderText="Password" HtmlEncode="False">
                                            <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="EmpID" HeaderText="EmpID">
                                            <HeaderStyle Font-Size="Smaller" ForeColor="White" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Level" HeaderText="Level">
                                            <HeaderStyle Font-Size="Smaller" ForeColor="White" />
                                        </asp:BoundField>
                                    </Columns>
                                    <HeaderStyle BackColor="#0080C0" />
                                    <AlternatingRowStyle BackColor="#CAE4FF" />
                                </asp:GridView>
                           </asp:Panel>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_InitializeRequest);
        prm.add_endRequest(prm_EndRequest);

        function prm_InitializeRequest(sender, args) {
            var panelProg = $get('divImage');
            if (args._postBackElement.id != "Timer1") {
            panelProg.style.display = '';}
        };

        function prm_EndRequest(sender, args) {
            var panelProg = $get('divImage');
            panelProg.style.display = 'none';
        };
    </script>
</body>
</html>
