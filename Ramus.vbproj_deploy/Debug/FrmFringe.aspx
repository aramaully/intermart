﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmFringe.aspx.vb" Inherits="Ramus.FrmFringe" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<html>
<head id="Head1" runat="server">
    <title>Fringe Benefits</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.
            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Fringe Benefits'; //Set the name
            w.Width = 775; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 425;            
            w.Top = 10;
            w.Left = 10;
            w.Show(); //Show the window
        };
        function ShowFind() {
            var FindWindow = window.open("FindEmployee.aspx", "FindEmployee", "width=565,height=435,top=200, left=575");
        };
        function FunctionConfirm(Tlb, TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Save") {
                    Index = '1';
                    w.Confirm("Sure to Save", "Confirm", null, SaveFunction);
            }
            else if (TlbCmd == "New") {
                __doPostBack('ToolBar1', '0');
            }
        }
        function SaveFunction(Sender, RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        }
        function FunPic() {
            var myArgs = 'nothing';
            myargs = window.showModalDialog('Snap.aspx', myArgs, "dialogHeight:350px;dialogWidth:650px");
            if (myArgs == 'nothing') {
                PageMethods.AjaxSetSession("True");
            }
        }
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</head>
<body onload="init();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <script type="text/javascript">
        var xPos, yPos;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            xPos = $get('PnlGrid').scrollLeft;
            yPos = $get('PnlGrid').scrollTop;
        }
        function EndRequestHandler(sender, args) {
            $get('PnlGrid').scrollLeft = xPos;
            $get('PnlGrid').scrollTop = yPos;
        }
</script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px;
                height: 32px; width: 32px;" runat="server" BorderStyle="None" Visible="True">
                <div id="divImage" style="display: none">
                    <asp:Image ID="img1" runat="server" ImageUrl="/Images/Wait.gif" />
                </div>
            </asp:Panel>
            <div id="toolbar" style="width: 100%;">
                <eo:ToolBar ID="ToolBar1" runat="server" BackgroundImage="00100103" ClientSideOnItemClick="FunctionConfirm"
                    BackgroundImageLeft="00100101" BackgroundImageRight="00100102" SeparatorImage="00100104"
                    Width="100%">
                    <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 1px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: #335ea8 1px solid; background-color: #99afd4; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <Items>
                        <eo:ToolBarItem CommandName="New" ImageUrl="~/images/RefreshDocViewHS.png" ToolTip="Refresh">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Save" ImageUrl="00101003" ToolTip="Save">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Help" ImageUrl="~/images/help.png" ToolTip="Help">
                        </eo:ToolBarItem>
                    </Items>
                    <ItemTemplates>
                        <eo:ToolBarItem Type="Custom">
                            <NormalStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <HoverStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <DownStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="DropDownMenu">
                            <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                            <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; background-image: url(00100106); background-position-x: right;" />
                            <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: none; background-color:transparent; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                        </eo:ToolBarItem>
                    </ItemTemplates>
                    <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                </eo:ToolBar>
            </div>
            <div id="Content" style="width: 100%; height: 100%; padding: 2px;
                border-right: solid 1px #cddaea;">                
            <asp:Panel ID="Panel10" runat="server" BorderStyle="Inset" BorderWidth="1">
                              
                <asp:Panel ID="PnlContent" runat="server" BorderStyle="Inset" BorderWidth="1">
                    <table class="style1">
                        <tr>
                            <td>
                                Department</td>
                            <td>
                                <asp:DropDownList ID="CmbDept" runat="server" AutoPostBack="True" Width="200px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                Month</td>
                            <td>
                                <asp:DropDownList ID="CmbMonth" runat="server" AutoPostBack="True">
                                    <asp:ListItem>January</asp:ListItem>
                                    <asp:ListItem>February</asp:ListItem>
                                    <asp:ListItem>March</asp:ListItem>
                                    <asp:ListItem>April</asp:ListItem>
                                    <asp:ListItem>May</asp:ListItem>
                                    <asp:ListItem>June</asp:ListItem>
                                    <asp:ListItem>July</asp:ListItem>
                                    <asp:ListItem>August</asp:ListItem>
                                    <asp:ListItem>September</asp:ListItem>
                                    <asp:ListItem>October</asp:ListItem>
                                    <asp:ListItem>November</asp:ListItem>
                                    <asp:ListItem>December</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                Year</td>
                            <td>
                                <asp:TextBox ID="TxtYear" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="PnlGrid" runat="server" ScrollBars="Vertical"  
                        BorderStyle="Inset" BorderWidth="1" Width="770px" Height="250px">
                        <asp:GridView ID="GdvFringe" runat="server" AutoGenerateColumns="False" 
                            ToolTip="Click on the Employee ID to get details">
                            <SelectedRowStyle BackColor="Yellow" />
                            <Columns>
                                <asp:BoundField DataField="EmpID" HeaderText="Employee ID" />
                                <asp:TemplateField HeaderText="Interest">                                    
                                    <ItemTemplate>
                                        <asp:TextBox ID="TxtInterest"  Width="100px" runat="server" Text='<%# Eval("Interest") %>'>
                                        </asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tips">
                                    <ItemTemplate>
                                        <asp:TextBox ID="TxtTips" Width="100px" runat="server" Text='<%# Eval("Tips") %>'>
                                        </asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Accomm">
                                     <ItemTemplate>
                                        <asp:TextBox ID="TxtAccommodation" Width="100px" runat="server" Text='<%# Eval("Accommodation") %>'>
                                        </asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Write Off">
                                     <ItemTemplate>
                                        <asp:TextBox ID="TxtWriteOff" Width="100px" runat="server" Text='<%# Eval("WriteOff") %>'>
                                        </asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Expenses">
                                 <ItemTemplate>
                                        <asp:TextBox ID="TxtExpenses" Width="100px" runat="server" Text='<%# Eval("Expenses") %>'>
                                        </asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tax Paid">
                                     <ItemTemplate>
                                        <asp:TextBox ID="TxtTaxPaid" Width="100px" runat="server" Text='<%# Eval("TaxPaid") %>'>
                                        </asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BackColor="#0066CC" Font-Size="Smaller" ForeColor="White" Width="100%" />
                        </asp:GridView>                    
                    </asp:Panel>
                    <table class="style1">
                    <tr>
                        <td>
                            Emp ID
                        </td>
                        <td>
                            <asp:TextBox ID="TxtEmpID" runat="server" AutoPostBack="True"></asp:TextBox>
                        </td>
                        <td>
                            Title
                        </td>
                        <td>
                            <asp:TextBox ID="TxtTitle" runat="server" Width="50px" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            Gender
                        </td>
                        <td>
                            <asp:TextBox ID="TxtGender" runat="server" Width="50px" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td align="right" rowspan="4" valign="top">
                            <asp:Image ID="ImgPhoto" runat="server" Height="97px" Width="87px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Clocked</td>
                        <td>
                            <asp:CheckBox ID="ChkClocked" runat="server" Text="Yes" Enabled="False" />
                        </td>
                        <td>
                            Clock ID</td>
                        <td>
                            <asp:TextBox ID="TxtClockID" runat="server" Width="100px"></asp:TextBox>
                        </td>
                        <td>
                            Category</td>
                        <td>
                            <asp:TextBox ID="TxtCategory" runat="server" Width="100px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Last Name</td>
                        <td colspan="4">
                            <asp:TextBox ID="TxtLast" runat="server" Width="400px" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            First Name</td>
                        <td colspan="4">
                            <asp:TextBox ID="TxtFirst" runat="server" Width="400px" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                </asp:Panel>  
                </asp:Panel> 
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_InitializeRequest);
        prm.add_endRequest(prm_EndRequest);

        function prm_InitializeRequest(sender, args) {
            var panelProg = $get('divImage');
            if (args._postBackElement.id != "Timer1") {
                panelProg.style.display = '';
            }
        };

        function prm_EndRequest(sender, args) {
            var panelProg = $get('divImage');
            panelProg.style.display = 'none';
        };
    </script>

</body>
</html>

