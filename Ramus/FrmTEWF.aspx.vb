﻿Public Partial Class FrmTEWF
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            TxtYear.Text = Date.Today.Year
            CmbMonth.SelectedIndex = Date.Today.Month - 1
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            Session("FieldCount") = 0
            Dim StrSelFormula As String
            Dim DtePayDate As Date = CDate("28-" + CmbMonth.Text + "-" + TxtYear.Text)
            Dim StrDate As String = Format(DtePayDate, "yyyy,MM,dd")
            StrSelFormula = "{Salary.EPZEE} > 0 and {Salary.Date}=date(" & StrDate & ") "

            If ChkDetails.Checked Then
                Session("ReportFile") = "REPZ.rpt"
                Session("RepHead") = "MSWWF Details for the month of " & CmbMonth.Text & ", " & TxtYear.Text
            Else
                Session("ReportFile") = "REPZLetter.rpt"
                Session("RepHead") = "MSWWF Letter for the month of " & CmbMonth.Text & ", " & TxtYear.Text
            End If

            Session("SelFormula") = StrSelFormula
            Session("RepHead") = "EPZ for the month of " & CmbMonth.Text & ", " & TxtYear.Text
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        End If
    End Sub

End Class