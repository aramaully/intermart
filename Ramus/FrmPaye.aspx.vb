﻿Public Partial Class FrmPaye
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            FillGrid()
        End If
    End Sub

    Protected Sub FillGrid()
        Cmd.Connection = Con
        StrSql = "SELECT * FROM Paye"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvPaye.DataSource = Rdr
        GdvPaye.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim Ev As New System.EventArgs

        If e.Item.CommandName = "New" Then
            TxtSchemeCode.Text = ""
            TxtFrom1.Text = ""
            TxtTo1.Text = ""
            TxtPercent1.Text = ""
            TxtFrom2.Text = ""
            TxtTo2.Text = ""
            Txtpercent2.Text = ""
            TxtFrom3.Text = ""
            TxtTo3.Text = ""
            TxtPercent3.Text = ""
            TxtFrom4.Text = ""
            TxtTo4.Text = ""
            TxtPercent4.Text = ""
            TxtFrom5.Text = ""
            TxtTo5.Text = ""
            TxtPercent5.Text = ""
            TxtSchemeCode.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Dim Trans As OleDb.OleDbTransaction
            Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
            Cmd.Connection = Con
            Cmd.Transaction = Trans
            Cmd.CommandText = "DELETE FROM Paye WHERE PayeScheme = '" & TxtSchemeCode.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO Paye (PAYEScheme,[From-1],[To-1],[Taxpercent-1],[From-2],[To-2],[Taxpercent-2] "
                StrSql = StrSql & ",[From-3],[To-3],[Taxpercent-3],[From-4],[To-4],[Taxpercent-4],[From-5],[To-5],[Taxpercent-5]) VALUES('"
                StrSql = StrSql & TxtSchemeCode.Text & "',"
                StrSql = StrSql & Val(TxtFrom1.Text) & ","
                StrSql = StrSql & Val(TxtTo1.Text) & ","
                StrSql = StrSql & Val(TxtPercent1.Text) & ","
                StrSql = StrSql & Val(TxtFrom2.Text) & ","
                StrSql = StrSql & Val(TxtTo2.Text) & ","
                StrSql = StrSql & Val(Txtpercent2.Text) & ","
                StrSql = StrSql & Val(TxtFrom3.Text) & ","
                StrSql = StrSql & Val(TxtTo3.Text) & ","
                StrSql = StrSql & Val(TxtPercent3.Text) & ","
                StrSql = StrSql & Val(TxtFrom4.Text) & ","
                StrSql = StrSql & Val(TxtTo4.Text) & ","
                StrSql = StrSql & Val(TxtPercent4.Text) & ","
                StrSql = StrSql & Val(TxtFrom5.Text) & ","
                StrSql = StrSql & Val(TxtTo5.Text) & ","
                StrSql = StrSql & Val(TxtPercent5.Text) & ")"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                Trans.Commit()
                TxtSchemeCode.Text = ""
                TxtFrom1.Text = ""
                TxtTo1.Text = ""
                TxtPercent1.Text = ""
                TxtFrom2.Text = ""
                TxtTo2.Text = ""
                Txtpercent2.Text = ""
                TxtFrom3.Text = ""
                TxtTo3.Text = ""
                TxtPercent3.Text = ""
                TxtFrom4.Text = ""
                TxtTo4.Text = ""
                TxtPercent4.Text = ""
                TxtFrom5.Text = ""
                TxtTo5.Text = ""
                TxtPercent5.Text = ""
                TxtSchemeCode.Focus()
                FillGrid()
            Catch ex As Exception
                Trans.Rollback()
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Paye WHERE PayeScheme = '" & TxtSchemeCode.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                TxtSchemeCode.Text = ""
                TxtFrom1.Text = ""
                TxtTo1.Text = ""
                TxtPercent1.Text = ""
                TxtFrom2.Text = ""
                TxtTo2.Text = ""
                Txtpercent2.Text = ""
                TxtFrom3.Text = ""
                TxtTo3.Text = ""
                TxtPercent3.Text = ""
                TxtFrom4.Text = ""
                TxtTo4.Text = ""
                TxtPercent4.Text = ""
                TxtFrom5.Text = ""
                TxtTo5.Text = ""
                TxtPercent5.Text = ""
                TxtSchemeCode.Focus()
                FillGrid()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & ex.Message & "','Error');</script>", False)
            End Try
        End If
    End Sub

    Private Sub GdvLoan_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvPaye.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvPaye, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvBank_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvPaye.SelectedIndexChanged
        TxtSchemeCode.Text = GdvPaye.SelectedRow.Cells(0).Text
        TxtFrom1.Text = GdvPaye.SelectedRow.Cells(1).Text
        TxtTo1.Text = GdvPaye.SelectedRow.Cells(2).Text
        TxtPercent1.Text = GdvPaye.SelectedRow.Cells(3).Text
        TxtFrom2.Text = GdvPaye.SelectedRow.Cells(4).Text
        TxtTo2.Text = GdvPaye.SelectedRow.Cells(5).Text
        Txtpercent2.Text = GdvPaye.SelectedRow.Cells(6).Text
        TxtFrom3.Text = GdvPaye.SelectedRow.Cells(7).Text
        TxtTo3.Text = GdvPaye.SelectedRow.Cells(8).Text
        TxtPercent3.Text = GdvPaye.SelectedRow.Cells(9).Text
        TxtFrom4.Text = GdvPaye.SelectedRow.Cells(10).Text
        TxtTo4.Text = GdvPaye.SelectedRow.Cells(11).Text
        TxtPercent4.Text = GdvPaye.SelectedRow.Cells(12).Text
        TxtFrom5.Text = GdvPaye.SelectedRow.Cells(13).Text
        TxtTo5.Text = GdvPaye.SelectedRow.Cells(14).Text
        TxtPercent5.Text = GdvPaye.SelectedRow.Cells(15).Text
    End Sub

    Private Sub GdvBank_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvPaye.SelectedIndexChanging
        GdvPaye.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvPaye.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvPaye.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub
End Class