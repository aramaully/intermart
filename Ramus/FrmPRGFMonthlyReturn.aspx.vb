﻿Imports System.IO
Imports System.Data.OleDb
Partial Public Class FrmPRGFMonthlyReturn
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim CmdTrans As New OleDb.OleDbCommand
    Dim RdrTrans As OleDb.OleDbDataReader
    Dim CmdEmp As New OleDb.OleDbCommand
    Dim RdrEmp As OleDb.OleDbDataReader
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            TxtYear.Text = Date.Today.Year
            CmbMonth.SelectedIndex = Date.Today.Month - 1
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim StrSql As String
        Dim tab As String = ""

        'Employer
        Dim StrCompanyName As String = ""
        Dim StrCompanyRegNum As String = ""
        Dim StrCompanyBRN As String = ""
        Dim StrCompanyPhone As String = ""
        Dim StrCompanyDeclarent As String = ""
        Dim StrCompanyEmail As String = ""

        'Salary
        Dim DtePayDate As Date
        Dim StrSalaryPeriod As String = ""

        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        DtePayDate = CDate("28-" + CmbMonth.Text + "-" + TxtYear.Text)

        StrSql = "Select *From [System]"
        CmdTrans.Connection = Con
        CmdTrans.CommandText = StrSql
        RdrTrans = CmdTrans.ExecuteReader

        While RdrTrans.Read
            StrCompanyRegNum = RdrTrans("PAYERegNo") & ""
            StrCompanyBRN = RdrTrans("BRN") & ""
            StrCompanyName = RdrTrans("Name") & ""
            StrSalaryPeriod = Format(DtePayDate, "yyMM")
            StrCompanyPhone = RdrTrans("Phone") & ""
            'StrCompanyPhone = ""
            StrCompanyDeclarent = RdrTrans("PAYEDeclarant") & ""
            StrCompanyEmail = RdrTrans("Email") & ""
        End While
        RdrTrans.Close()

        Response.Clear()
        Response.ContentType = "text/csv"
        Response.AppendHeader("Content-Disposition", String.Format("attachment; filename=PRGF" & StrSalaryPeriod & ".csv"))

        Dim Dt As DataTable
        Dim Dr As DataRow
        Dim Dc As DataColumn
        Dt = New DataTable()
        Dc = New DataColumn("Col1", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col2", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col3", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col4", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col5", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col6", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col7", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col8", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)

        If e.Item.CommandName = "Print" Then
            Try
                Dim DtHeader1 As DataTable
                Dim DrHeader1 As DataRow
                Dim DcHeader1 As DataColumn

                DtHeader1 = New DataTable()
                DcHeader1 = New DataColumn("First1", Type.GetType("System.String"))
                DtHeader1.Columns.Add(DcHeader1)
                DcHeader1 = New DataColumn("First2", Type.GetType("System.String"))
                DtHeader1.Columns.Add(DcHeader1)
                DcHeader1 = New DataColumn("First3", Type.GetType("System.String"))
                DtHeader1.Columns.Add(DcHeader1)
                DrHeader1 = DtHeader1.NewRow
                DrHeader1.Item("First1") = "MRA"
                DrHeader1.Item("First2") = "PRGF"
                DrHeader1.Item("First3") = "V1.0"
                DtHeader1.Rows.Add(DrHeader1)
                For Each DrHeader1 In DtHeader1.Rows
                    tab = ""
                    For i = 0 To DtHeader1.Columns.Count - 1
                        Response.Write(tab & DrHeader1(i).ToString())
                        tab = ","
                    Next
                    Response.Write(vbCrLf)
                Next

                Dim DtHeader2 As DataTable
                Dim DrHeader2 As DataRow
                Dim DcHeader2 As DataColumn

                DtHeader2 = New DataTable()
                DcHeader2 = New DataColumn("Second1", Type.GetType("System.String"))
                DtHeader2.Columns.Add(DcHeader2)
                DcHeader2 = New DataColumn("Second2", Type.GetType("System.String"))
                DtHeader2.Columns.Add(DcHeader2)
                DcHeader2 = New DataColumn("Second3", Type.GetType("System.String"))
                DtHeader2.Columns.Add(DcHeader2)
                DcHeader2 = New DataColumn("Second4", Type.GetType("System.String"))
                DtHeader2.Columns.Add(DcHeader2)
                DcHeader2 = New DataColumn("Second5", Type.GetType("System.String"))
                DtHeader2.Columns.Add(DcHeader2)
                DcHeader2 = New DataColumn("Second6", Type.GetType("System.String"))
                DtHeader2.Columns.Add(DcHeader2)
                DcHeader2 = New DataColumn("Second7", Type.GetType("System.String"))
                DtHeader2.Columns.Add(DcHeader2)
                DcHeader2 = New DataColumn("Second8", Type.GetType("System.String"))
                DtHeader2.Columns.Add(DcHeader2)

                DrHeader2 = DtHeader2.NewRow
                DrHeader2.Item("Second1") = "Employer Registration Number"
                DrHeader2.Item("Second2") = "Employer Business Registration Number"
                DrHeader2.Item("Second3") = "Employer Name"
                DrHeader2.Item("Second4") = "Tax Period"
                DrHeader2.Item("Second5") = "Telephone Number"
                DrHeader2.Item("Second6") = "Mobile Number"
                DrHeader2.Item("Second7") = "Name of Declarant"
                DrHeader2.Item("Second8") = "Email Address"
                DtHeader2.Rows.Add(DrHeader2)
                For Each DrHeader2 In DtHeader2.Rows
                    tab = ""
                    For i = 0 To DtHeader2.Columns.Count - 1
                        Response.Write(tab & DrHeader2(i).ToString())
                        tab = ","
                    Next
                    Response.Write(vbCrLf)
                Next

                Dr = Dt.NewRow
                Dr.Item("Col1") = StrCompanyRegNum
                Dr.Item("Col2") = StrCompanyBRN
                Dr.Item("Col3") = StrCompanyName
                Dr.Item("Col4") = StrSalaryPeriod
                Dr.Item("Col5") = StrCompanyPhone
                Dr.Item("Col6") = ""
                Dr.Item("Col7") = StrCompanyDeclarent
                Dr.Item("Col8") = StrCompanyEmail
                Dt.Rows.Add(Dr)

                For Each Dr In Dt.Rows
                    tab = ""
                    For i = 0 To Dt.Columns.Count - 1
                        Response.Write(tab & Dr(i).ToString())
                        tab = ","
                    Next
                    Response.Write(vbCrLf)
                Next

                Dim DtHeader3 As DataTable
                Dim DrHeader3 As DataRow
                Dim DcHeader3 As DataColumn

                DtHeader3 = New DataTable()
                DcHeader3 = New DataColumn("Empe1", Type.GetType("System.String"))
                DtHeader3.Columns.Add(DcHeader3)
                DcHeader3 = New DataColumn("Empe2", Type.GetType("System.String"))
                DtHeader3.Columns.Add(DcHeader3)
                DcHeader3 = New DataColumn("Empe3", Type.GetType("System.String"))
                DtHeader3.Columns.Add(DcHeader3)
                DcHeader3 = New DataColumn("Empe4", Type.GetType("System.String"))
                DtHeader3.Columns.Add(DcHeader3)
                DcHeader3 = New DataColumn("Empe5", Type.GetType("System.String"))
                DtHeader3.Columns.Add(DcHeader3)
                DcHeader3 = New DataColumn("Empe6", Type.GetType("System.String"))
                DtHeader3.Columns.Add(DcHeader3)
                DcHeader3 = New DataColumn("Empe7", Type.GetType("System.String"))
                DtHeader3.Columns.Add(DcHeader3)
                DcHeader3 = New DataColumn("Empe8", Type.GetType("System.String"))
                DtHeader3.Columns.Add(DcHeader3)
                DcHeader3 = New DataColumn("Empe9", Type.GetType("System.String"))
                DtHeader3.Columns.Add(DcHeader3)
                DcHeader3 = New DataColumn("Empe10", Type.GetType("System.String"))
                DtHeader3.Columns.Add(DcHeader3)
                DcHeader3 = New DataColumn("Empe11", Type.GetType("System.String"))
                DtHeader3.Columns.Add(DcHeader3)
                DcHeader3 = New DataColumn("Empe12", Type.GetType("System.String"))
                DtHeader3.Columns.Add(DcHeader3)

                DrHeader3 = DtHeader3.NewRow
                DrHeader3.Item("Empe1") = "Employee NID"
                DrHeader3.Item("Empe2") = "Surname of Employee"
                DrHeader3.Item("Empe3") = "Other Names of Employee"
                DrHeader3.Item("Empe4") = "Pension Scheme"
                DrHeader3.Item("Empe5") = "Full Time Employment"
                DrHeader3.Item("Empe6") = "Start Date of Employment"
                DrHeader3.Item("Empe7") = "Monthly basic wage or salary (MUR)"
                DrHeader3.Item("Empe8") = "Allowances (MUR)"
                DrHeader3.Item("Empe9") = "Commission (MUR)"
                DrHeader3.Item("Empe10") = "Total Remuneration on which PRGF contribution amount is calculated"
                DrHeader3.Item("Empe11") = "PRGF Contribution Amount"
                DrHeader3.Item("Empe12") = "Reason for no remuneration"
                DtHeader3.Rows.Add(DrHeader3)
                For Each DrHeader3 In DtHeader3.Rows
                    tab = ""
                    For i = 0 To DtHeader3.Columns.Count - 1
                        Response.Write(tab & DrHeader3(i).ToString())
                        tab = ","
                    Next
                    Response.Write(vbCrLf)
                Next

                Dim Dt3 As DataTable
                Dim Dr3 As DataRow
                Dim Dc3 As DataColumn

                Dt3 = New DataTable()
                Dc3 = New DataColumn("Col9", Type.GetType("System.String"))
                Dt3.Columns.Add(Dc3)
                Dc3 = New DataColumn("Col10", Type.GetType("System.String"))
                Dt3.Columns.Add(Dc3)
                Dc3 = New DataColumn("Col11", Type.GetType("System.String"))
                Dt3.Columns.Add(Dc3)
                Dc3 = New DataColumn("Col12", Type.GetType("System.String"))
                Dt3.Columns.Add(Dc3)
                Dc3 = New DataColumn("Col13", Type.GetType("System.String"))
                Dt3.Columns.Add(Dc3)
                Dc3 = New DataColumn("Col14", Type.GetType("System.String"))
                Dt3.Columns.Add(Dc3)
                Dc3 = New DataColumn("Col15", Type.GetType("System.String"))
                Dt3.Columns.Add(Dc3)
                Dc3 = New DataColumn("Col16", Type.GetType("System.String"))
                Dt3.Columns.Add(Dc3)
                Dc3 = New DataColumn("Col17", Type.GetType("System.String"))
                Dt3.Columns.Add(Dc3)
                Dc3 = New DataColumn("Col18", Type.GetType("System.String"))
                Dt3.Columns.Add(Dc3)
                Dc3 = New DataColumn("Col19", Type.GetType("System.String"))
                Dt3.Columns.Add(Dc3)
                Dc3 = New DataColumn("Col20", Type.GetType("System.String"))
                Dt3.Columns.Add(Dc3)


                StrSql = "SELECT * FROM Salary where [Month] = '" & CmbMonth.Text & "' AND Year='" & TxtYear.Text & "' And BasicSalary <= 200000 order by EmpID"
                CmdTrans.Connection = Con
                CmdTrans.CommandText = StrSql
                RdrTrans = CmdTrans.ExecuteReader
                While RdrTrans.Read
                    CmdEmp.Connection = Con
                    CmdEmp.CommandText = "Select * FROM Employee WHERE EmpID = '" & RdrTrans("EmpID") & "' AND Expatriate=0"
                    RdrEmp = CmdEmp.ExecuteReader
                    'Employee
                    Dim StrSSNo As String = ""
                    Dim StrLast As String = ""
                    Dim StrFirst As String = ""

                    Dim StrPensionScheme As String = "N"
                    Dim StrFullTime As String = "Y"
                    Dim StrHireDate As String = "ddMMyyyy"
                    Dim StrBasicSalary As Integer = 0
                    Dim StrAllowance As Single = 0
                    Dim StrCommission As Single = 0
                    Dim StrTotalRemuneration As Integer = 0
                    Dim StrPRGFContrib As Integer = 0
                    Dim StrReason As String = ""

                    If RdrEmp.Read Then
                        If CmbMonth.Text = "December" Then
                            StrSql = "SELECT * FROM Salary where [Month] = 'Bonus' AND Year='" & TxtYear.Text & "' And EmpID = '" & RdrEmp("EmpID") & "'"
                            Cmd.Connection = Con
                            Cmd.CommandText = StrSql
                            Rdr = Cmd.ExecuteReader
                            If Rdr.Read Then
                                '   add bonus to allowance
                                StrAllowance = Rdr("IfsBonus")
                                '   add bonus to total remuneration 
                                StrTotalRemuneration = Rdr("IfsBonus")
                                '   add pensionEmployer and prgfEmployerContribution to PRGFContribution
                                'StrPRGFContrib = Rdr("PensionEmp") + Rdr("prgfEmployerAmount")
                                StrPRGFContrib = Rdr("prgfEmployerAmount")
                            End If
                            Rdr.Close()
                        End If

                        '#1: NIC
                        StrSSNo = RdrEmp("SSNo") & ""

                        '#2: Lastname
                        StrLast = UCase(RdrEmp("Last"))

                        '#3: Other names
                        StrFirst = UCase(RdrEmp("First"))

                        '#4: Pension scheme
                        If (RdrTrans("PensionEmp") > 0) Then
                            StrPensionScheme = "Y"
                        Else
                            StrPensionScheme = "N"
                        End If

                        'NEW
                        'If (RdrTrans("prgfEmployerAmount") > 0) Then
                        '    StrPensionScheme = "N"
                        'Else
                        '    StrPensionScheme = "Y"
                        'End If

                        '#4: Fulltime employment
                        StrFullTime = "Y"

                        '#5: Hire date
                        StrHireDate = Format(RdrEmp("HireDate"), "ddMMyyyy")

                        '#6: Basic Salary
                        StrBasicSalary = RdrTrans("BasicSalary")

                        '#7: Allowances
                        StrAllowance = StrAllowance + RdrTrans("Overtime") + RdrTrans("IfsBonus") + RdrTrans("AttendanceBonus") + RdrTrans("FixAttBonus")
                        StrAllowance = StrAllowance + RdrTrans("RespAllow") + RdrTrans("MaternityAllow") + RdrTrans("PerfAllow")
                        StrAllowance = StrAllowance + RdrTrans("OtherAllow") + RdrTrans("SpecialBenefit")
                        StrAllowance = StrAllowance + Math.Round((RdrTrans("LocalRefund") + RdrTrans("SickRefund")) * (RdrTrans("DailyRate")), 0)

                        '#8: Commission
                        StrCommission = 0

                        '#9: Total Remuneration
                        StrTotalRemuneration = StrTotalRemuneration + RdrTrans("prgfTotalRemuneration")

                        '#10: PRGF Contribution
                        StrPRGFContrib = StrPRGFContrib + RdrTrans("prgfEmployerAmount")

                        '#11: Reason no remuneration [L:Leave without pay | U:unauthorised leave without pay | D:Exited]
                        If StrTotalRemuneration = 0 Then
                            If (Not IsDBNull(RdrEmp("LastDay"))) Then
                                StrReason = "D"
                                'If (Format(RdrEmp("LastDay"), "ddMMMyyyy") < Format(DateTime.Today, "ddMMMyyyy")) Then
                                'More accurate condition if needed
                                'End If
                            Else
                                StrReason = "L"
                                'StrReason = "U"
                            End If
                        End If

                        Dr3 = Dt3.NewRow
                        Dr3.Item("Col9") = StrSSNo
                        Dr3.Item("Col10") = StrLast
                        Dr3.Item("Col11") = StrFirst
                        Dr3.Item("Col12") = StrPensionScheme
                        Dr3.Item("Col13") = StrFullTime
                        Dr3.Item("Col14") = StrHireDate
                        Dr3.Item("Col15") = StrBasicSalary
                        Dr3.Item("Col16") = StrAllowance
                        Dr3.Item("Col17") = StrCommission
                        Dr3.Item("Col18") = StrTotalRemuneration
                        Dr3.Item("Col19") = StrPRGFContrib
                        Dr3.Item("Col20") = StrReason
                        Dt3.Rows.Add(Dr3)

                    End If
                    RdrEmp.Close()

                End While
                RdrTrans.Close()

                For Each Dr3 In Dt3.Rows
                    tab = ""
                    For i = 0 To Dt3.Columns.Count - 1
                        Response.Write(tab & Dr3(i).ToString())
                        tab = ","
                    Next
                    Response.Write(vbCrLf)
                Next

                Response.End()
                Response.[End]()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

End Class