﻿Public Partial Class FrmEmpFBen
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT Usecar FROM Car"
            CmbComCar.Items.Add("Select...")
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                CmbComCar.Items.Add(Rdr("UseCar"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ClearAll()
        TxtFirst.Text = ""
        TxtLast.Text = ""
        TxtClockID.Text = ""
        TxtCategory.Text = ""
        TxtTitle.Text = ""
        TxtGender.Text = ""
        ImgPhoto.ImageUrl = Nothing
        CmbComCar.SelectedIndex = 0
        TxtCapCar.Text = ""
        TxtAmount.Text = ""
        TxtEmpContributionC.Text = ""
        CmbPropType.SelectedIndex = 0
        CmbTOH.SelectedIndex = 0
        TxtPercentage.Text = ""
        TxtEmpContributionH.Text = ""
        TxtAccBenefit.Text = ""
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ev As New System.EventArgs
        If e.Item.CommandName = "New" Then
            ClearAll()
            TxtEmpID.Text = ""
            TxtEmpID.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Try
                StrSql = "UPDATE Employee SET "
                StrSql = StrSql & "CarUse ='" & IIf(CmbComCar.Text = "Select...", "", CmbComCar.Text) & "',"
                StrSql = StrSql & "CarCapacity ='" & TxtCapCar.Text & "',"
                StrSql = StrSql & "CarAmt ='" & TxtAmount.Text & "',"
                StrSql = StrSql & "CarEmpCont ='" & TxtEmpContributionC.Text & "',"
                StrSql = StrSql & "PropertyType ='" & IIf(CmbPropType.Text = "Select...", "", CmbPropType.Text) & "',"
                StrSql = StrSql & "HouseType ='" & IIf(CmbTOH.Text = "Select...", "", CmbTOH.Text) & "',"
                StrSql = StrSql & "HousePercent ='" & TxtPercentage.Text & "',"
                StrSql = StrSql & "HouseEmpCont ='" & TxtEmpContributionH.Text & "'"
                StrSql = StrSql & "WHERE EmpID = '" & TxtEmpID.Text & "'"

                Cmd.Connection = Con
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                TxtFirst.Text = ""
                TxtLast.Text = ""
                TxtClockID.Text = ""
                TxtCategory.Text = ""
                TxtTitle.Text = ""
                TxtGender.Text = ""
                ImgPhoto.ImageUrl = Nothing
                CmbComCar.SelectedIndex = 0
                TxtCapCar.Text = ""
                TxtAmount.Text = ""
                TxtEmpContributionC.Text = ""
                CmbPropType.SelectedIndex = 0
                CmbTOH.SelectedIndex = 0
                TxtPercentage.Text = ""
                TxtEmpContributionH.Text = ""
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "DELETE FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
                Cmd.ExecuteNonQuery()
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Next" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID > '" & TxtEmpID.Text & "'"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try

        End If
        If e.Item.CommandName = "Previous" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID < '" & TxtEmpID.Text & "' ORDER BY EmpID DESC"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try
        End If
        If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtEmpID.Text)
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
        End If
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        If Rdr.Read Then
            On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
            TxtTitle.Text = Rdr("Title")
            TxtCategory.Text = Rdr("Category")
            TxtGender.Text = Rdr("Gender")
            TxtCategory.Text = Rdr("Category")
            ChkClocked.Checked = IIf(Rdr("Clocked"), True, False)
            TxtClockID.Text = Rdr("BatchNo")
            Session.Add("EmpID", Rdr("EmpID"))
            ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & Rdr("EmpID")

            CmbComCar.Text = Rdr("CarUse").ToString
            TxtCapCar.Text = Rdr("CarCapacity").ToString
            TxtAmount.Text = Rdr("CarAmt").ToString
            TxtEmpContributionC.Text = Rdr("CarEmpCont").ToString
            CmbPropType.Text = Rdr("PropertyType").ToString
            CmbTOH.Text = Rdr("HouseType").ToString
            TxtPercentage.Text = Rdr("HousePercent").ToString
            TxtEmpContributionH.Text = Rdr("HouseEmpCont").ToString
            
        Else
            Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(1))
            ToolBar1_ItemClick(Me, ev)
            'ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('Invalid Employee ID','Error');</script>", False)
        End If
        Rdr.Close()
    End Sub

    Protected Sub TxtCapCar_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtCapCar.TextChanged 
        StrSql = "SELECT *From Car Where UseCar = '" & CmbComCar.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            If Val(TxtCapCar.Text) > Val(Rdr("From-1")) And Val(TxtCapCar.Text) <= Val(Rdr("To-1")) Then
                TxtAmount.Text = Rdr("Amount-1")
            ElseIf Val(TxtCapCar.Text) > Val(Rdr("From-2")) And Val(TxtCapCar.Text) <= Val(Rdr("To-2")) Then
                TxtAmount.Text = Rdr("Amount-2")
            ElseIf Val(TxtCapCar.Text) > Val(Rdr("From-3")) And Val(TxtCapCar.Text) <= Val(Rdr("To-3")) Then
                TxtAmount.Text = Rdr("Amount-3")
            ElseIf Val(TxtCapCar.Text) > Val(Rdr("From-4")) And Val(TxtCapCar.Text) <= Val(Rdr("To-4")) Then
                TxtAmount.Text = Rdr("Amount-4")
            ElseIf Val(TxtCapCar.Text) > Val(Rdr("From-5")) And Val(TxtCapCar.Text) <= Val(Rdr("To-5")) Then
                TxtAmount.Text = Rdr("Amount-5")
            End If
        End If
        Rdr.Close()
    End Sub
End Class
