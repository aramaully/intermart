﻿Imports System.Data.SqlClient
Imports System.Web.Services
Imports System.IO

Imports System.Configuration
Imports System.Data
Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq
Imports System.Data.OleDb
Imports System.Text

Module modPRFG
    Dim Cmd As New OleDb.OleDbCommand
    Dim ConSalary As New OleDb.OleDbConnection
    Dim ConPRGF As New OleDb.OleDbConnection
    Dim Rdr As OleDb.OleDbDataReader
    Dim RdrEmployee As OleDb.OleDbDataReader
    Dim CmdPRGF As New OleDb.OleDbCommand
    Public StrSql As String
    Public Function calculationPRGF(month, year, Con, ConSession, EmployeeID)
        Dim prgfGovtRate As Double
        Dim prgfEmployerRate As Double
        Dim prgfSalaryLimit As Double
        Dim performPRGF As Integer
        Dim prevMonthsProcessed As Integer
        Dim prgfPrevMonths2020 As String
        Dim prgfPrevMonthsArr As String()
        Dim prgfIndex As Integer
        Dim mthPRGF As String
        Dim yrPRGF As String
        Dim employerPension As Double
        Dim totalRemuneration As Double
        Dim prgfGovtAmount As Double
        Dim prgfEmployerAmount As Double
        Dim prgfTotal As Double
        Dim EmpID As String


        'Search for Government and Employer PRGF Rate value
        StrSql = "SELECT * FROM prgfParams WHERE prgfId = 1"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader

        If Not (Rdr.Read) Then
            Rdr.Close()
            Return "Parameters are not defined, Contact the system administrator"
        End If

        'Get parameters value(table: prgfParams)
        performPRGF = Val(Rdr("performPRGF"))
        If (performPRGF = 0) Then
            Return 1
        End If
        prgfGovtRate = Val(Rdr("prgfGovtRate"))
        prgfEmployerRate = Val(Rdr("prgfEmployerRate"))
        prgfSalaryLimit = Val(Rdr("prgfSalaryLimit"))
        prevMonthsProcessed = Val(Rdr("prevMonthsProcessed"))
        prgfPrevMonths2020 = Rdr("prgfPrevMonths2020")
        prgfPrevMonthsArr = prgfPrevMonths2020.Split(";")
        prgfIndex = prgfPrevMonthsArr.Length

        If (prevMonthsProcessed = 0) Then
            prgfIndex = 0
        End If

        Do While prgfIndex <= prgfPrevMonthsArr.Length
            mthPRGF = ""
            yrPRGF = ""

            If prgfIndex < prgfPrevMonthsArr.Length Then
                mthPRGF = prgfPrevMonthsArr(prgfIndex)
                yrPRGF = "2020"
            Else
                mthPRGF = month
                yrPRGF = year
            End If

            'Calculation of Portable Retirement Gratuity Fund
            If ConSalary.State = ConnectionState.Open Then ConSalary.Close()
            ConSalary.ConnectionString = ConSession
            ConSalary.Open()

            If EmployeeID = "ALL" Then
                StrSql = "Select * From Salary Where Salary.Month = '" & mthPRGF & "' And Salary.Year = " & yrPRGF
            Else
                StrSql = "Select * From Salary Where Salary.Month = '" & mthPRGF & "' And Salary.Year = " & yrPRGF & " AND EmpID = '" & EmployeeID & "'"
            End If

            Cmd.Connection = ConSalary
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader

            'If Not (Rdr.Read) Then
            '   Rdr.Close()
            '   Return "No Salary is processed for " & mthPRGF & " " & yrPRGF
            'End If

            If EmployeeID = "ALL" Then
                While Rdr.Read
                    If ConPRGF.State = ConnectionState.Open Then ConPRGF.Close()
                    ConPRGF.ConnectionString = ConSession
                    ConPRGF.Open()

                    StrSql = "Select * From Employee Where EmpID = '" & Rdr("EmpID") & "'"
                    CmdPRGF.Connection = ConPRGF
                    CmdPRGF.CommandText = StrSql
                    RdrEmployee = CmdPRGF.ExecuteReader

                    'Employer ID and Pension
                    EmpID = ""
                    employerPension = 0
                    If (RdrEmployee.Read) Then
                        EmpID = RdrEmployee("EmpID")
                    Else
                        Continue While
                    End If

                    totalRemuneration = 0
                    If (Val(RdrEmployee("Expatriate")) = 0) And (Val(Rdr("BasicSalary")) <= Val(prgfSalaryLimit)) Then
                        'Calculation of Total Remuneration
                        totalRemuneration = Val(Rdr("BasicSalary")) + Val(Rdr("Overtime"))
                        totalRemuneration = totalRemuneration + Val(Rdr("IfsBonus")) + Val(Rdr("AttendanceBonus"))
                        totalRemuneration = totalRemuneration + Val(Rdr("FixAttBonus")) + Val(Rdr("RespAllow")) + Val(Rdr("MaternityAllow"))
                        totalRemuneration = totalRemuneration + Val(Rdr("OtherAllow")) + Val(Rdr("PerfAllow")) + Val(Rdr("SpecialBenefit"))
                        totalRemuneration = totalRemuneration + Math.Round((Val(Rdr("LocalRefund")) + Val(Rdr("SickRefund"))) * (Val(Rdr("DailyRate"))), 0)
                    End If

                    'Calculation of PRGF
                    prgfGovtAmount = 0
                    prgfEmployerAmount = 0
                    If (Val(totalRemuneration) <> 0) Then
                        prgfGovtAmount = CInt((prgfGovtRate / 100) * totalRemuneration)
                        prgfEmployerAmount = CInt((prgfEmployerRate / 100) * totalRemuneration)
                    End If

                    prgfTotal = Int(prgfGovtAmount + prgfEmployerAmount)

                    employerPension = Val(Rdr("PensionEmp"))
                    If (employerPension < prgfEmployerAmount) Then
                        prgfEmployerAmount = prgfEmployerAmount - employerPension
                    Else
                        prgfEmployerAmount = 0
                    End If

                    'Save in table Salary
                    totalRemuneration = Int(totalRemuneration)
                    If ConPRGF.State = ConnectionState.Open Then ConPRGF.Close()
                    ConPRGF.ConnectionString = ConSession
                    ConPRGF.Open()

                    StrSql = "UPDATE Salary Set prgfGovtRate = " & prgfGovtRate & ", prgfGovtAmount = " & prgfGovtAmount & ","
                    StrSql = StrSql & "prgfEmployerRate = " & prgfEmployerRate & ", prgfEmployerAmount = " & prgfEmployerAmount & ","
                    StrSql = StrSql & "prgfTotalRemuneration = " & totalRemuneration & ", prgfTotal = " & prgfTotal
                    StrSql = StrSql & " WHERE Year = " & yrPRGF & " AND month = '" & mthPRGF & "'"
                    StrSql = StrSql & " AND EmpID = '" & EmpID & "'"

                    CmdPRGF.Connection = ConPRGF
                    CmdPRGF.CommandText = StrSql
                    CmdPRGF.ExecuteNonQuery()
                End While

                Rdr.Close()
            Else
                While Rdr.Read
                    If ConPRGF.State = ConnectionState.Open Then ConPRGF.Close()
                    ConPRGF.ConnectionString = ConSession
                    ConPRGF.Open()

                    StrSql = "Select * From Employee Where EmpID = '" & Rdr("EmpID") & "'"
                    CmdPRGF.Connection = ConPRGF
                    CmdPRGF.CommandText = StrSql
                    RdrEmployee = CmdPRGF.ExecuteReader

                    'Employer ID and Pension
                    If Not (RdrEmployee.Read) Then
                        RdrEmployee.Close()
                        Return "Employee: " & Rdr("EmpID") & " does not exist"
                    End If

                    EmpID = RdrEmployee("EmpID")
                    employerPension = Val(RdrEmployee("PensionEmpPaid"))

                    totalRemuneration = 0
                    If (Val(RdrEmployee("Expatriate")) = 0) And (Val(Rdr("BasicSalary")) <= Val(prgfSalaryLimit)) Then
                        'Calculation of Total Remuneration

                        'Calculation of Total Remuneration
                        totalRemuneration = Val(Rdr("BasicSalary")) + Val(Rdr("Overtime"))
                        totalRemuneration = totalRemuneration + Val(Rdr("IfsBonus")) + Val(Rdr("AttendanceBonus"))
                        totalRemuneration = totalRemuneration + Val(Rdr("FixAttBonus")) + Val(Rdr("RespAllow")) + Val(Rdr("MaternityAllow"))
                        totalRemuneration = totalRemuneration + Val(Rdr("OtherAllow")) + Val(Rdr("PerfAllow")) + Val(Rdr("SpecialBenefit"))
                        totalRemuneration = totalRemuneration + Math.Round((Val(Rdr("LocalRefund")) + Val(Rdr("SickRefund"))) * (Val(Rdr("DailyRate"))), 0)
                    End If

                    'Calculation of PRGF
                    prgfGovtAmount = 0
                    prgfEmployerAmount = 0
                    If (Val(totalRemuneration) <> 0) Then
                        prgfGovtAmount = CInt((prgfGovtRate / 100) * totalRemuneration)
                        prgfEmployerAmount = CInt((prgfEmployerRate / 100) * totalRemuneration)
                    End If

                    prgfTotal = Int(prgfGovtAmount + prgfEmployerAmount)

                    If (employerPension < prgfEmployerAmount) Then
                        prgfEmployerAmount = prgfEmployerAmount - employerPension
                    Else
                        prgfEmployerAmount = 0
                    End If

                    'Save in table Salary
                    totalRemuneration = Int(totalRemuneration)
                    If ConPRGF.State = ConnectionState.Open Then ConPRGF.Close()
                    ConPRGF.ConnectionString = ConSession
                    ConPRGF.Open()

                    StrSql = "UPDATE Salary Set prgfGovtRate = " & prgfGovtRate & ", prgfGovtAmount = " & prgfGovtAmount & ","
                    StrSql = StrSql & "prgfEmployerRate = " & prgfEmployerRate & ", prgfEmployerAmount = " & prgfEmployerAmount & ","
                    StrSql = StrSql & "prgfTotalRemuneration = " & totalRemuneration & ", prgfTotal = " & prgfTotal
                    StrSql = StrSql & " WHERE Year = " & yrPRGF & " AND month = '" & mthPRGF & "'"
                    StrSql = StrSql & " AND EmpID = '" & EmpID & "'"

                    CmdPRGF.Connection = ConPRGF
                    CmdPRGF.CommandText = StrSql
                    CmdPRGF.ExecuteNonQuery()
                End While
                Rdr.Close()
            End If

            prgfIndex = prgfIndex + 1
        Loop

        If (prevMonthsProcessed = 0) And (EmployeeID = "ALL") Then
            'Update field prevMonthsProcessed, table prgfParams
            If ConPRGF.State = ConnectionState.Open Then ConPRGF.Close()
            ConPRGF.ConnectionString = ConSession
            ConPRGF.Open()

            StrSql = "UPDATE prgfParams Set prevMonthsProcessed = 1 where prgfId = 1"
            CmdPRGF.Connection = ConPRGF
            CmdPRGF.CommandText = StrSql
            CmdPRGF.ExecuteNonQuery()
        End If

        If ConSalary.State = ConnectionState.Open Then ConSalary.Close()
        If ConPRGF.State = ConnectionState.Open Then ConPRGF.Close()

        Rdr.Close()
        RdrEmployee.Close()

        Cmd.Connection.Close()
        CmdPRGF.Connection.Close()

        Return "1"
    End Function
End Module
