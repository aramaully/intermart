﻿Public Partial Class FrmUpdateLeave
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdEmp As New OleDb.OleDbCommand
    Dim RdrEmp As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdUpdateEmp As New OleDb.OleDbCommand
    Dim StrSqlEmp As String
    Dim IntProLocal As Integer
    Dim IntProSick As Integer
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrAudit As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            TxtYear.Text = Date.Today.Year
        End If
    End Sub

    Private Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Update" Then
            Try
                StrSql = "SELECT Sick,Local FROM [System]"
                Cmd.Connection = Con
                Cmd.CommandText = StrSql
                Rdr = Cmd.ExecuteReader
                Dim IntSick As Integer = 0
                Dim IntLocal As Integer = 0
                Dim EmpLocal As Integer = 0
                Dim EmpSick As Integer = 0

                If Rdr.Read Then
                    EmpSick = Rdr("Sick")
                    EmpLocal = Rdr("Local")
                End If
                Rdr.Close()
                Dim DteFirst As Date = CDate("January 1," + TxtYear.Text)
                Dim DteLast As Date = CDate("December 31," + TxtYear.Text)
                'Cmd.CommandText = "Delete From LeaveHistory Where [Year] = " & TxtYear.Text
                'Cmd.ExecuteNonQuery()
                StrSql = "Select *From Employee Where Working = 1"
                CmdEmp.Connection = Con
                CmdEmp.CommandText = StrSql
                RdrEmp = CmdEmp.ExecuteReader
                CmdUpdateEmp.Connection = Con
                While RdrEmp.Read
                    If DateDiff(DateInterval.Day, RdrEmp("HireDate"), DteFirst) > 364 Then
                        StrSqlEmp = "Update Employee Set [Year] = '" & TxtYear.Text & "', [Local]= " & EmpLocal & " ,Sick = " & EmpSick & " Where EmpID = '" & RdrEmp("EmpID") & "'"
                    Else
                        IntLocal = Math.Round((((((Year(DteLast) - Year(RdrEmp("HireDate"))) * 12) + (Month(DteLast) - Month(RdrEmp("HireDate")))) - 12) / 12) * EmpLocal, 2)
                        IntSick = Math.Round((((((Year(DteLast) - Year(RdrEmp("HireDate"))) * 12) + (Month(DteLast) - Month(RdrEmp("HireDate")))) - 12) / 12) * EmpSick, 2)

                        StrSqlEmp = "Update Employee Set [Year] = '" & TxtYear.Text & "', [Local]= " & IntLocal & " ,Sick = " & IntSick & " Where EmpID = '" & RdrEmp("EmpID") & "'"
                    End If
                    CmdUpdateEmp.CommandText = StrSqlEmp
                    CmdUpdateEmp.ExecuteNonQuery()
                End While

                CmdAudit.Connection = Con
                StrAudit = "Update Leave Register completed"
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Update Leave Register','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()

                LblResults.Visible = True
                LblResults.Text = "Employee's Leave register has been set!"
            Catch ex As Exception
                LblResults.Text = "Update Failed. " & ex.Message
            End Try
        End If
    End Sub
End Class