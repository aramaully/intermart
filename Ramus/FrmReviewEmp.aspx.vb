﻿Public Partial Class FrmReviewEmp
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            TxtFrom.Text = Date.Today
            TxtTo.Text = Date.Today.AddDays(14)
        End If
    End Sub

    Private Sub RefreshList()
        StrSql = "SELECT EmpID, ReviewDate, Last + ' ' + First AS Name,Position from Employee"
        StrSql = StrSql + " WHERE ReviewDate >= '" & Format(CDate(TxtFrom.Text), "dd-MMM-yyyy")
        StrSql = StrSql & "' AND ReviewDate <= '" & Format(CDate(TxtTo.Text), "dd-MMM-yyyy") & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvReview.DataSource = Rdr
        GdvReview.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ClearAll()
        TxtFirst.Text = ""
        TxtLast.Text = ""
        TxtClockID.Text = ""
        TxtCategory.Text = ""
        TxtTitle.Text = ""
        TxtGender.Text = ""
        ImgPhoto.ImageUrl = Nothing
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ev As New System.EventArgs
        If e.Item.CommandName = "New" Then
            TxtNextReview.Text = Date.Today.AddYears(1)
            TxtRemarks.Text = ""
            TxtReveiwedBy.Text = Session("UserName")
            TxtNextReview.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Dim Trans As OleDb.OleDbTransaction
            Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Cmd.Connection = Con
            Cmd.Transaction = Trans
            Try
                Cmd.Connection = Con
                StrSql = "UPDATE Employee SET ReviewDate = '"
                StrSql = StrSql & Format(CDate(TxtNextReview.Text), "dd-MMM-yyyy")
                StrSql = StrSql & "' WHERE EmpID = '" & TxtEmpID.Text & "'"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO Notes (EmpID,[Date],[Note],AddedBy) VALUES('"
                StrSql = StrSql & TxtEmpID.Text & "','"
                StrSql = StrSql & Date.Today.ToString("dd-MMM-yyyy") & "','"
                StrSql = StrSql & TxtRemarks.Text & "','"
                StrSql = StrSql & TxtReveiwedBy.Text & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                Trans.Commit()
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception
                Trans.Rollback()
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "DELETE FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
                Cmd.ExecuteNonQuery()
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Next" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID > '" & TxtEmpID.Text & "'"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try

        End If
        If e.Item.CommandName = "Previous" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID < '" & TxtEmpID.Text & "' ORDER BY EmpID DESC"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try
        End If
        If e.Item.CommandName = "Refresh" Then
            RefreshList()
        End If
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        If Rdr.Read Then
            On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
            TxtTitle.Text = Rdr("Title")
            TxtCategory.Text = Rdr("Category")
            TxtGender.Text = Rdr("Gender")
            TxtCategory.Text = Rdr("Category")
            ChkClocked.Checked = IIf(Rdr("Clocked"), True, False)
            TxtClockID.Text = Rdr("BatchNo")
            TxtBasic.Text = Rdr("BasicSalary")
            Session.Add("EmpID", Rdr("EmpID"))
            ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & Rdr("EmpID")
            'Loan Details
        Else
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('Invalid Employee ID','Error');</script>", False)
        End If
        Rdr.Close()
    End Sub

    Private Sub GdvReview_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvReview.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvReview, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvReview_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvReview.SelectedIndexChanged
        TxtEmpID.Text = GdvReview.SelectedRow.Cells(0).Text
        TxtEmpID_TextChanged(Me, e)
    End Sub

    Private Sub GdvReview_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvReview.SelectedIndexChanging
        GdvReview.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvReview.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvReview.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub
End Class