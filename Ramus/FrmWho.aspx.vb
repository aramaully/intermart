﻿Partial Public Class FrmWho
    Inherits System.Web.UI.Page
    Dim Cmd As New OleDb.OleDbCommand
    Dim Con As New OleDb.OleDbConnection
    Dim Att As New OleDb.OleDbConnection
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdAtt As New OleDb.OleDbCommand
    Dim RdrAtt As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Att.ConnectionString = Session("TimexConn")
        Att.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT DeptCode FROM Dept"
            Rdr = Cmd.ExecuteReader
            CmbDepartment.Items.Add("ALL")
            While Rdr.Read
                CmbDepartment.Items.Add(Rdr("DeptCode"))
            End While
            Rdr.Close()
            RefreshGrid()
            Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(0))
            ToolBar1_ItemClick(Me, ev)
        End If
    End Sub

    Private Sub RefreshGrid()
        StrSql = "SELECT EmpID,[Last],[First],BadgeNo FROM Employee "
        If CmbDepartment.Text <> "ALL" Then
            StrSql = StrSql & "WHERE DeptCode = '" & CmbDepartment.Text & "' And Working = 1"
        Else
            StrSql = StrSql & " Where Working = 1"
        End If
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvWhoIsIn.DataSource = Rdr
        GdvWhoIsIn.DataBind()
        Rdr.Close()
        'GdvWhoIsIn.Columns(5).Visible = False
    End Sub

    Private Sub RefreshGridHOD()
        StrSql = "SELECT EmpID,[Last],[First],BadgeNo FROM Employee Where Working = 1 And (Grade = '4' Or Grade = 5)"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvWhoIsIn.DataSource = Rdr
        GdvWhoIsIn.DataBind()
        Rdr.Close()
        'GdvWhoIsIn.Columns(5).Visible = False
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If e.Item.CommandName = "Refresh" Then
            Dim DteDate As Date = Date.Now
            Dim DteTimeIn As Date
            Dim DteTimeOut As Date
            Dim StrTimeIn As String
            Dim StrTimeOut As String
            For Each Row As GridViewRow In GdvWhoIsIn.Rows
                If Row.Cells(5).Text = "&nbsp;" Then
                    Row.Cells(5).Text = ""
                End If
                CmdAtt.Connection = Att
                CmdAtt.CommandText = "Select Min(CheckTime) AS TimeIn, Max(CheckTime) as TimeOut From CheckInOut Where UserId = '" & Row.Cells(5).Text & "' And CheckTime >= '" & Format(DteDate, "dd/MMM/yyyy 00:00:01") & "' And CheckTime <= '" & Format(DteDate, "dd/MMM/yyyy 23:59:59") & "'"
                RdrAtt = CmdAtt.ExecuteReader
                If RdrAtt.Read Then
                    Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                    StrTimeIn = ""
                    StrTimeOut = ""
                    If Not IsDBNull(RdrAtt("TimeIn")) Then
                        DteTimeIn = RdrAtt("TimeIn")
                        StrTimeIn = DteTimeIn
                    End If
                    If Not IsDBNull(RdrAtt("TimeOut")) Then
                        DteTimeOut = RdrAtt("TimeOut")
                        StrTimeOut = DteTimeOut
                    End If
                    If DteTimeIn = DteTimeOut Then
                        StrTimeOut = ""
                    End If
                    If StrTimeIn <> "" And StrTimeOut = "" Then
                        Row.Cells(3).Text = Mid(StrTimeIn, 12, 5)
                        Row.Cells(4).Text = StrTimeOut
                    Else
                        Row.Cells(3).Text = Mid(StrTimeIn, 12, 5)
                        Row.Cells(4).Text = Mid(StrTimeOut, 12, 5)
                    End If
                End If
                RdrAtt.Close()
            Next
        End If
    End Sub

    Protected Sub ChkSortHOD_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ChkSortHOD.CheckedChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If ChkSortHOD.Checked Then
            RefreshGridHOD()
        Else
            RefreshGrid()
        End If
        Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(0))
        ToolBar1_ItemClick(Me, ev)
    End Sub
End Class