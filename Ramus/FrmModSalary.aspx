﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmModSalary.aspx.vb" Inherits="Ramus.FrmModSalary" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<html>
<head id="Head1" runat="server">
    <title>Modify Salary</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.
            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Modify Salary'; //Set the name
            w.Width = 775; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 570;            
            w.Top = 10;
            w.Left = 10;
            w.Show(); //Show the window
        };
        function ShowFind() {
            var FindWindow = window.open("FindEmployee.aspx", "FindEmployee", "width=565,height=435,top=200, left=575");
        };
        function FunctionConfirm(Tlb, TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Save") {
                    Index = '2';
                    w.Confirm("Sure to Save", "Confirm", null, SaveFunction);
            }
            else if (TlbCmd == "Find") {
                __doPostBack('ToolBar1', '1');
            }
            else if (TlbCmd == "Delete") {
                Index = '4';
                w.Confirm("Sure to Delete", "Confirm", null, SaveFunction);
            }
            else if (TlbCmd == "New") {
                __doPostBack('ToolBar1', '0');
            }
            else if (TlbCmd == "Next") {
                __doPostBack('ToolBar1', '7');
            }
            else if (TlbCmd == "Previous") {
                __doPostBack('ToolBar1', '6');
            }
        }
        function SaveFunction(Sender, RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        }
        function FunPic() {
            var myArgs = 'nothing';
            myargs = window.showModalDialog('Snap.aspx', myArgs, "dialogHeight:350px;dialogWidth:650px");
            if (myArgs == 'nothing') {
                PageMethods.AjaxSetSession("True");
            }
        }
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
            height: 100px;
        }
        </style>
</head>
<body onload="init();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px;
                height: 32px; width: 32px;" runat="server" BorderStyle="None" Visible="True">
                <div id="divImage" style="display: none">
                    <asp:Image ID="img1" runat="server" ImageUrl="/Images/Wait.gif" />
                </div>
            </asp:Panel>
            <div id="toolbar" style="width: 100%;">
                <eo:ToolBar ID="ToolBar1" runat="server" BackgroundImage="00100103" ClientSideOnItemClick="FunctionConfirm"
                    BackgroundImageLeft="00100101" BackgroundImageRight="00100102" SeparatorImage="00100104"
                    Width="100%">
                    <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 1px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: #335ea8 1px solid; background-color: #99afd4; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <Items>
                        <eo:ToolBarItem CommandName="New" ImageUrl="00101001" ToolTip="New">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Find" ImageUrl="~/images/findHS.png" ToolTip="Find Employee">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Save" ImageUrl="00101003" ToolTip="Save">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Delete" ImageUrl="~/images/delete.png" ToolTip="Delete">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Previous" ImageUrl="~/images/NavBack.png" ToolTip="Previous Record">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Next" ImageUrl="~/images/NavForward.png" ToolTip="Next Record">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Help" ImageUrl="~/images/help.png" ToolTip="Help">
                        </eo:ToolBarItem>
                    </Items>
                    <ItemTemplates>
                        <eo:ToolBarItem Type="Custom">
                            <NormalStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <HoverStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <DownStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="DropDownMenu">
                            <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                            <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; background-image: url(00100106); background-position-x: right;" />
                            <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: none; background-color:transparent; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                        </eo:ToolBarItem>
                    </ItemTemplates>
                    <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                </eo:ToolBar>
            </div>
            <div id="Content" style="width: 100%; height: 100%; padding: 2px;
                border-right: solid 1px #cddaea;">                
            <asp:Panel ID="Panel10" runat="server" BorderStyle="Inset" BorderWidth="1">
                <table class="style1" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Label ID="Label98" runat="server" Font-Bold="True" ForeColor="#FF3300" 
                                Text="Category"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="CmbCategory" runat="server" AutoPostBack="True">
                                <asp:ListItem>Monthly</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            Date</td>
                        <td>
                            <asp:DropDownList ID="CmbMonth" runat="server">
                                <asp:ListItem>January</asp:ListItem>
                                <asp:ListItem>February</asp:ListItem>
                                <asp:ListItem>March</asp:ListItem>
                                <asp:ListItem>April</asp:ListItem>
                                <asp:ListItem>May</asp:ListItem>
                                <asp:ListItem>June</asp:ListItem>
                                <asp:ListItem>July</asp:ListItem>
                                <asp:ListItem>August</asp:ListItem>
                                <asp:ListItem>September</asp:ListItem>
                                <asp:ListItem>October</asp:ListItem>
                                <asp:ListItem>November</asp:ListItem>
                                <asp:ListItem>December</asp:ListItem>
                                <asp:ListItem>Bonus</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="TxtDate" runat="server" Visible="False" Width="75px"></asp:TextBox>
                            <cc1:CalendarExtender ID="TxtDate_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtDate">
                            </cc1:CalendarExtender>
                        </td>
                        <td>
                            Year</td>
                        <td>
                            <asp:TextBox ID="TxtYear" runat="server" Width="60px" AutoPostBack="True"></asp:TextBox>
                        </td>
                        <td align="right" rowspan="6" valign="top">
                            <asp:Image ID="ImgPhoto" runat="server" Height="97px" Width="87px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Emp ID
                        </td>
                        <td>
                            <asp:TextBox ID="TxtEmpID" runat="server" AutoPostBack="True"></asp:TextBox>
                        </td>
                        <td>
                            Title
                        </td>
                        <td>
                            <asp:TextBox ID="TxtTitle" runat="server" ReadOnly="True" Width="50px"></asp:TextBox>
                        </td>
                        <td>
                            Gender
                        </td>
                        <td>
                            <asp:TextBox ID="TxtGender" runat="server" ReadOnly="True" Width="50px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Clocked</td>
                        <td>
                            <asp:CheckBox ID="ChkClocked" runat="server" Text="Yes" Enabled="False" />
                        </td>
                        <td>
                            Clock ID</td>
                        <td>
                            <asp:TextBox ID="TxtClockID" runat="server" Width="100px"></asp:TextBox>
                        </td>
                        <td>
                            Category</td>
                        <td>
                            <asp:TextBox ID="TxtCategory" runat="server" Width="100px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Last Name</td>
                        <td colspan="4">
                            <asp:TextBox ID="TxtLast" runat="server" Width="400px" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                            </td>
                    </tr>
                    <tr>
                        <td>
                            First Name</td>
                        <td colspan="4">
                            <asp:TextBox ID="TxtFirst" runat="server" Width="400px" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                            <asp:Label ID="LblAccess" runat="server" ForeColor="Red" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" align="center">
                        <marquee behavior="scroll" direction="Left"> 
                            <div><asp:Label ID="LblMsg" runat="server" Font-Bold="True" ForeColor="#FF3300"></asp:Label></div>
                        </marquee>
                        </td>
                    </tr>
                </table>
                </asp:Panel>                
                <asp:Panel ID="PnlContent" runat="server" BorderStyle="Inset" BorderWidth="1">
                    <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" 
                        Height="123px" Width="100%" BorderStyle="Inset" BorderWidth="1px" 
                        AutoPostBack="True">                  
                        
                    <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="Normal"><HeaderTemplate>
Normal
                        </HeaderTemplate>
<ContentTemplate>
    <table cellpadding="0" cellspacing="0" class="style1">
        <tr>
            <td>Arrears</td><td><asp:TextBox ID="TxtMArrears" runat="server" Width="80px" AutoPostBack="True"></asp:TextBox></td>
            <td>Local Refund</td><td><asp:TextBox ID="TxtMLRefund" runat="server" Width="80px" AutoPostBack="True"></asp:TextBox></td>
            <td>EOY Bonus</td><td><asp:TextBox ID="TxtMEOYBonus" runat="server" AutoPostBack="True" Width="80px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Travel Allowance</td><td><asp:TextBox ID="TxtMTravel" runat="server" Width="80px" Height="22px" AutoPostBack="True"></asp:TextBox></td>
            <td>Sick Refund</td><td><asp:TextBox ID="TxtMSRefund" runat="server" Width="80px" AutoPostBack="True"></asp:TextBox></td>
            <td>Fringe Benefit</td><td><asp:TextBox ID="TxtFringe" runat="server" AutoPostBack="True" Width="80px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Other Allowance</td><td><asp:TextBox ID="TxtMOtherAllow" runat="server" AutoPostBack="True" Width="80px"></asp:TextBox></td>
            <td>Resp. Allow</td><td><asp:TextBox ID="TxtNiteAllow" runat="server" AutoPostBack="True" Width="80px"></asp:TextBox></td>
            <td>Deduction/Medical</td><td><asp:TextBox ID="TxtMOtherDeduction" runat="server" AutoPostBack="True" Width="80px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Passage Benefit</td><td><asp:TextBox ID="TxtMSPassageBenefit" Enabled="True" runat="server" AutoPostBack="True" Width="80px"></asp:TextBox></td>
            <td>Short</td><td><asp:TextBox ID="TxtDShort" runat="server" AutoPostBack="True" Width="80px"></asp:TextBox></td>
            <td>CrCard</td><td><asp:TextBox ID="TxtDCrCard" runat="server" AutoPostBack="True" Width="80px"></asp:TextBox></td>
            <td>&#160;</td><td><td>&#160;</td><td>&#160;</td>
        </tr>
        <tr>
            <td>Ex Gratia Payment</td><td><asp:TextBox ID="TxtSpecialBenefit" Enabled="True" runat="server" AutoPostBack="True" Width="80px"></asp:TextBox></td>
            <td>Other Deduction</td><td><asp:TextBox ID="TxtOthDeduction" Enabled="True" runat="server" AutoPostBack="True" Width="80px"></asp:TextBox></td>
        </tr>
    </table>
</ContentTemplate>
</cc1:TabPanel><cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="Overtime"><ContentTemplate>
<table class="style1"><tr><td class="style2">&#160;</td><td>Hrs</td><td>Amount</td><td>&#160;</td><td>Hrs</td><td>Amount</td><td>&#160;</td><td>Hrs</td><td>Amount</td><td class="style3">&#160;</td><td>Hrs</td><td>Amount</td></tr><tr><td>O.T. 1.0</td><td><asp:TextBox ID="TxtM10Hrs" runat="server" Width="50px" AutoPostBack="True"></asp:TextBox></td><td><asp:TextBox ID="TxtM10Amount" runat="server" Width="75px" AutoPostBack="True"></asp:TextBox></td><td class="style2">O.T. 1.5</td><td><asp:TextBox ID="TxtM15Hrs" runat="server" Width="50px" AutoPostBack="True"></asp:TextBox></td><td><asp:TextBox ID="TxtM15Amount" runat="server" Width="75px" AutoPostBack="True"></asp:TextBox></td><td>O.T. 2.0</td><td><asp:TextBox ID="TxtM20Hrs" runat="server" Width="50px" AutoPostBack="True"></asp:TextBox></td><td><asp:TextBox ID="TxtM20Amount" runat="server" Width="75px" AutoPostBack="True"></asp:TextBox></td><td class="style3">O.T. 3.0</td><td><asp:TextBox ID="TxtM30Hrs" runat="server" Width="50px" AutoPostBack="True"></asp:TextBox></td><td><asp:TextBox ID="TxtM30Amount" runat="server" Width="75px" AutoPostBack="True"></asp:TextBox></td></tr><caption>&#160;</td><tr><td class="style2">&#160;</td><td>&#160;</td><td>&#160;</td><td>&#160;</td><td>&#160;</td><td>&#160;</td><td class="style3">&#160;</td><td>&#160;</td></tr></caption></table>
                        </ContentTemplate>
</cc1:TabPanel><cc1:TabPanel ID="TabPanel3" runat="server" HeaderText="Special"><HeaderTemplate>
Special
                        </HeaderTemplate>
<ContentTemplate><table class="style1" border="1"><tr><td>&#160;</td><td align="center" colspan="2"><asp:Label ID="Label3" runat="server" Font-Bold="True" Text="Taxable"></asp:Label></td><td align="center" colspan="2"><asp:Label ID="Label4" runat="server" Font-Bold="True" Text="Non - Taxable"></asp:Label></td></tr><tr><td>&#160;</td><td><asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Description"></asp:Label></td><td><asp:Label ID="Label6" runat="server" Font-Bold="True" Text="Amount"></asp:Label></td><td><asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Description"></asp:Label></td><td><asp:Label ID="Label5" runat="server" Font-Bold="True" Text="Amount"></asp:Label></td></tr><tr><td>Allowance</td><td><asp:TextBox ID="TxtMTADesc" runat="server" Width="250px"></asp:TextBox></td><td><asp:TextBox ID="TxtMTAAmount" runat="server" Width="75px" CausesValidation="True"></asp:TextBox></td><td><asp:TextBox ID="TxtMNTADesc" runat="server" Width="250px"></asp:TextBox></td><td><asp:TextBox ID="TxtMNTAAmount" runat="server" Width="75px" AutoPostBack="True"></asp:TextBox></td></tr></table></ContentTemplate>
</cc1:TabPanel><cc1:TabPanel ID="TabPanel4" runat="server" HeaderText="Fringe Benefit"><HeaderTemplate>
Fringe Benefit
                        </HeaderTemplate>
<ContentTemplate><table class="style1"><tr><td>Car Benefit</td><td><asp:TextBox ID="TxtMCarBenefit" runat="server" Width="75px" AutoPostBack="True"></asp:TextBox></td><td>Interest</td><td><asp:TextBox ID="TxtMInterest" runat="server" Width="75px" AutoPostBack="True"></asp:TextBox></td><td>Expenses</td><td><asp:TextBox ID="TxtMExpenses" runat="server" Width="75px" AutoPostBack="True"></asp:TextBox></td></tr><tr><td>House Benefit</td><td><asp:TextBox ID="TxtMHouseBenefit" runat="server" Width="75px" AutoPostBack="True"></asp:TextBox></td><td>Tips</td><td><asp:TextBox ID="TxtMTips" runat="server" Width="75px" AutoPostBack="True"></asp:TextBox></td><td>Tax Paid</td><td><asp:TextBox ID="TxtMTaxPaid" runat="server" Width="75px" AutoPostBack="True"></asp:TextBox></td></tr><tr><td>Accommodation Benefit</td><td><asp:TextBox ID="TxtMAccBenefit" runat="server" Width="75px" AutoPostBack="True"></asp:TextBox></td><td>Write off</td><td><asp:TextBox ID="TxtMWriteOff" runat="server" Width="75px" AutoPostBack="True"></asp:TextBox></td><td>&#160;</td><td>&#160;</td></tr></table></ContentTemplate>
</cc1:TabPanel></cc1:TabContainer>
                            <cc1:TabContainer ID="TabContainer2" runat="server" ActiveTabIndex="6" 
                        Height="228px" Width="100%" BorderStyle="Solid" BorderWidth="1px" 
                        Enabled="False">
                        <cc1:TabPanel ID="TabPanel5" runat="server" HeaderText="Salary Payable"><ContentTemplate><table cellpadding="0" cellspacing="0" class="style1"><tr><td><asp:Label ID="Label7" runat="server" Text="Emp Basic" Font-Bold="True"></asp:Label></td><td><asp:TextBox ID="TxtNSEmpBasic" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label16" runat="server" Text="Passage Benefit"></asp:Label></td><td><asp:TextBox ID="TxtNSPassageBenefit" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label24" runat="server" ForeColor="#FF3300" Text="NPF Employee"></asp:Label></td><td><asp:TextBox ID="TxtNSNpfEmp" runat="server" Enabled="False" Width="75px"></asp:TextBox></td></tr><tr><td><asp:Label ID="Label8" runat="server" Text="Basic Salary + YOS - Late"></asp:Label></td><td><asp:TextBox ID="TxtNSBasicSalary" runat="server" Width="75px" Enabled="False" ReadOnly="True"></asp:TextBox></td><td><asp:Label ID="Label17" runat="server" Text="Pension Emp"></asp:Label></td><td><asp:TextBox ID="TxtNSAttendBonus" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label25" runat="server" ForeColor="#FF3300" Text="Deduction/Medical"></asp:Label></td><td><asp:TextBox ID="TxtNSOtherDeduction" runat="server" Enabled="False" Width="75px"></asp:TextBox></td></tr><tr><td><asp:Label ID="Label9" runat="server" Text="YOS Increment" ForeColor="Black"></asp:Label></td><td><asp:TextBox ID="TxtNSBasicDeduction" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label18" runat="server" Text="Performance Allowance"></asp:Label></td><td><asp:TextBox ID="TxtNSPerfBonus" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label26" runat="server" ForeColor="#FF3300" Text="Short"></asp:Label></td><td><asp:TextBox ID="TxtNSShort" runat="server" Enabled="False" Width="75px"></asp:TextBox></td></tr><tr><td><asp:Label ID="Label10" runat="server" Text="Overtime"></asp:Label></td><td><asp:TextBox ID="TxtNSOvertime" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label19" runat="server" Text="Maternity Allowances"></asp:Label></td><td><asp:TextBox ID="TxtNSFixedAllow" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label99" runat="server" ForeColor="#FF3300" Text="CCardCharge"></asp:Label></td><td><asp:TextBox ID="TxtNSCCardCharges" runat="server" Enabled="False" Width="75px"></asp:TextBox></td></tr><tr><td><asp:Label ID="Label11" runat="server" Text="Other Allowances"></asp:Label></td><td><asp:TextBox ID="TxtNSOtherAllow" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td>
                            Resp/Meal Allow</td><td><asp:TextBox ID="TxtNSMealAllow" runat="server" Enabled="False" Width="75px"></asp:TextBox><asp:TextBox ID="TxtNSNiteAllow" runat="server" Enabled="False" Width="75px"></asp:TextBox></td><td><asp:Label ID="Label28" runat="server" ForeColor="#FF3300" Text="Meal Deduct"></asp:Label></td><td><asp:TextBox ID="TxtNSMealDeduct" runat="server" Enabled="False" Width="75px"></asp:TextBox></td></tr><tr><td><asp:Label ID="Label12" runat="server" Text="Travel"></asp:Label></td><td><asp:TextBox ID="TxtNSTravel" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label281" runat="server" ForeColor="#FF3300" Text="EPZ Employee"></asp:Label></td><td><asp:TextBox ID="TxtEPZEE" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label30" runat="server" ForeColor="#FF3300" Text="NSF Employee"></asp:Label></td><td><asp:TextBox ID="TxtNSNsfEmp" runat="server" Enabled="False" Width="75px"></asp:TextBox></td></tr><tr><td><asp:Label ID="Label13" runat="server" Text="Arrears"></asp:Label></td><td><asp:TextBox ID="TxtNSArrears" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label102" runat="server" ForeColor="#FF3300" Text="Loan Repay 1"></asp:Label></td><td><asp:TextBox ID="TxtNSLoanRepay1" runat="server" Enabled="False" Width="75px"></asp:TextBox></td><td><asp:Label ID="Label31" runat="server" Text="Payable" Font-Bold="True"></asp:Label></td><td><asp:TextBox ID="TxtNSPayable" runat="server" Width="75px" Enabled="False"></asp:TextBox></td></tr><tr><td><asp:Label ID="Label14" runat="server" Text="EOY Bonus"></asp:Label></td><td><asp:TextBox ID="TxtNSEOYBonus" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label22" runat="server" ForeColor="#FF3300" Text="Loan Repay 2"></asp:Label></td><td><asp:TextBox ID="TxtNSLoanRepay" runat="server" Enabled="False" Width="75px"></asp:TextBox></td><td><asp:Label ID="Label32" runat="server" Text="Deduction" Font-Bold="True" ForeColor="#FF3300"></asp:Label></td><td><asp:TextBox ID="TxtNSDeduction" runat="server" Width="75px" Enabled="False"></asp:TextBox></td></tr><tr><td><asp:Label ID="Label15" runat="server" Text="Leave Refund"></asp:Label></td><td><asp:TextBox ID="TxtNSLeaveRefund" runat="server" Width="75px" Height="22px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label23" runat="server" ForeColor="#FF3300" Text="PAYE"></asp:Label></td><td><asp:TextBox ID="TxtNSPaye" runat="server" AutoPostBack="True" Enabled="False" Width="75px"></asp:TextBox></td><td><asp:Label ID="Label33" runat="server" Text="Net Payable" Font-Bold="True"></asp:Label></td><td><asp:TextBox ID="TxtNSNetPayable" runat="server" Width="75px" Enabled="False"></asp:TextBox></td></tr></table></ContentTemplate></cc1:TabPanel>
                        <cc1:TabPanel ID="TabPanel6" runat="server" HeaderText="PAYE Calculation"><ContentTemplate><table cellpadding="0" cellspacing="0" class="style1"><tr><td><asp:Label ID="Label34" runat="server" Text="Basic Salary"></asp:Label></td><td><asp:TextBox ID="TxtNPBasicSalary" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label43" runat="server" Text="Pension Emp"></asp:Label></td><td><asp:TextBox ID="TxtNPAttBonus" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label52" runat="server" Font-Bold="True" Text="Current Gross"></asp:Label></td><td><asp:TextBox ID="TxtNPCurrentGross" runat="server" Width="75px" Enabled="False"></asp:TextBox></td></tr><tr><td><asp:Label ID="Label35" runat="server" Text="Overtime"></asp:Label></td><td><asp:TextBox ID="TxtNPOvertime" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label44" runat="server" Text="Fringe Benefit"></asp:Label></td><td><asp:TextBox ID="TxtNPFringeBenefit" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label53" runat="server" Font-Bold="True" Text="Prev Gross"></asp:Label></td><td><asp:TextBox ID="TxtNPPrevGross" runat="server" Width="75px" Enabled="False"></asp:TextBox></td></tr><tr><td><asp:Label ID="Label36" runat="server" Text="Other Allowances"></asp:Label></td><td><asp:TextBox ID="TxtNPOtherAllow" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label45" runat="server" Text="Maternity Allowance"></asp:Label></td><td><asp:TextBox ID="TxtNPFixedAllow" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label54" runat="server" Font-Bold="True" ForeColor="#FF3300" Text="IET"></asp:Label></td><td><asp:TextBox ID="TxtNPIet" runat="server" Width="75px" Enabled="False"></asp:TextBox></td></tr><tr><td><asp:Label ID="Label37" runat="server" Text="Transport"></asp:Label></td><td><asp:TextBox ID="TxtNPTransport" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td>
                            Resp. Allowance</td><td><asp:TextBox ID="TxtNPNiteAllow" runat="server" Enabled="False" Width="75px"></asp:TextBox></td><td><asp:Label ID="Label55" runat="server" Font-Bold="True" ForeColor="#0066CC" Text="Net Chargeable"></asp:Label></td><td><asp:TextBox ID="TxtNPNetCharge" runat="server" ForeColor="#0066CC" Width="75px" Enabled="False"></asp:TextBox></td></tr><tr><td><asp:Label ID="Label38" runat="server" Text="Arrears"></asp:Label></td><td><asp:TextBox ID="TxtNPArrears" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td>
                            Passage Benefit</td><td><asp:TextBox ID="TxtNPPassageBenefit" runat="server" Enabled="False" Width="75px"></asp:TextBox></td><td>
                            &nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td><asp:Label ID="Label39" runat="server" Text="EOY Bonus"></asp:Label></td><td><asp:TextBox ID="TxtNPEOYBonus" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td>
                            &nbsp;</td><td>&nbsp;</td><td><asp:Label ID="Label58" runat="server" Font-Bold="True" ForeColor="#FF3300" Text="Current PAYE"></asp:Label></td><td><asp:TextBox ID="TxtNPCurrentPaye" runat="server" Width="75px" Enabled="False"></asp:TextBox></td></tr><tr><td><asp:Label ID="Label40" runat="server" Text="Leave Refund"></asp:Label></td><td><asp:TextBox ID="TxtNPLeaveRefund" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td>
                            &nbsp;</td><td>&nbsp;</td><td><asp:Label ID="Label59" runat="server" Font-Bold="True" ForeColor="#FF3300" Text="Prev PAYE"></asp:Label></td><td><asp:TextBox ID="TxtNPPrevPaye" runat="server" Width="75px" Enabled="False"></asp:TextBox></td></tr><tr><td><asp:Label ID="Label41" runat="server" Text="Meal Allowance"></asp:Label></td><td><asp:TextBox ID="TxtNPMealAllow" runat="server" Width="75px" Visible=True  Enabled="False"></asp:TextBox></td><td>
                            &nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td><asp:Label ID="Label42" runat="server" Text="Performance Allow"></asp:Label></td><td><asp:TextBox ID="TxtNPPerfBonus" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td>
                            &nbsp;</td><td>&nbsp;</td><td><asp:Label ID="Label61" runat="server" Font-Bold="True" ForeColor="#009900" Text="PAYE Payable"></asp:Label></td><td><asp:TextBox ID="TxtNPPaye" runat="server" Width="75px" Enabled="False"></asp:TextBox></td></tr></table></ContentTemplate></cc1:TabPanel>
                        <cc1:TabPanel ID="TabPanel7" runat="server" HeaderText="Employer's Contribution"><ContentTemplate><table class="style1"><tr><td><asp:Label ID="Label62" runat="server" Text="National Pension Scheme"></asp:Label></td><td><asp:TextBox ID="TxtNENpf" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label65" runat="server" Text="EPZ Employer"></asp:Label></td><td><asp:TextBox ID="TxtEPZER" runat="server" Width="75px" Enabled="False"></asp:TextBox></td></tr><tr><td><asp:Label ID="Label63" runat="server" Text="National Savings Fund"></asp:Label></td><td><asp:TextBox ID="TxtNENsf" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label100" runat="server" Text="Medical Employer"></asp:Label></td><td><asp:TextBox ID="TxtNEMedicalEmployer" runat="server" Width="75px" Enabled="False"></asp:TextBox></td></tr><tr><td><asp:Label ID="Label64" runat="server" Text="IVTB"></asp:Label></td><td><asp:TextBox ID="TxtNEIvtb" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label101" runat="server" Text="Pension Plan Employer"></asp:Label></td><td><asp:TextBox ID="TxtNEPensionEmployer" runat="server" Width="75px" Enabled="False"></asp:TextBox></td></tr></table></ContentTemplate></cc1:TabPanel>
                        <cc1:TabPanel ID="TabPanel9" runat="server" HeaderText="Loans"><HeaderTemplate>Loans</HeaderTemplate><ContentTemplate><asp:GridView ID="GdvLoan" runat="server" AutoGenerateColumns="False" Width="501px"><Columns><asp:BoundField DataField="Type" HeaderText="Loan Type" /><asp:BoundField DataField="LoanAmount" HeaderText="Amount" /></Columns><HeaderStyle BackColor="#0099CC" ForeColor="White" /><AlternatingRowStyle BackColor="#99CCFF" /></asp:GridView></ContentTemplate></cc1:TabPanel>
                        <cc1:TabPanel ID="TabPanel10" runat="server" HeaderText="Leaves"><ContentTemplate><asp:GridView ID="GdvLeave" runat="server" AutoGenerateColumns="False" 
                                    Width="501px"><Columns><asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:dd/MM/yyyy}" /><asp:BoundField DataField="Type" HeaderText="Leave Type" /><asp:BoundField DataField="Leave" HeaderText="Days" /></Columns><AlternatingRowStyle BackColor="#99CCFF" /><HeaderStyle BackColor="#0099CC" ForeColor="White" /></asp:GridView></ContentTemplate></cc1:TabPanel>

                        <cc1:TabPanel ID="TabPanel8" runat="server" HeaderText="Fringe Benefit"><ContentTemplate><table class="style1"><tr><td><asp:Label ID="Label89" runat="server" Text="Car Benefit"></asp:Label></td><td><asp:TextBox ID="TxtNFCarBenefit" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label94" runat="server" Text="Write Off"></asp:Label></td><td><asp:TextBox ID="TxtNFWriteOff" runat="server" Width="75px" Enabled="False"></asp:TextBox></td></tr><tr><td><asp:Label ID="Label90" runat="server" Text="Housing Benefit"></asp:Label></td><td><asp:TextBox ID="TxtNFHousingBenefit" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td>
                            Expenses</td><td><asp:TextBox ID="TxtNFExpenses" runat="server" Width="75px" Enabled="False"></asp:TextBox></td></tr><tr><td><asp:Label ID="Label91" runat="server" Text="Accommodation Benefit"></asp:Label></td><td><asp:TextBox ID="TxtNFAccBenefit" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label96" runat="server" Text="Tax Paid"></asp:Label></td><td><asp:TextBox ID="TxtNFTaxPaid" runat="server" Width="75px" Enabled="False"></asp:TextBox></td></tr><tr><td><asp:Label ID="Label92" runat="server" Text="Interest"></asp:Label></td><td><asp:TextBox ID="TxtNFInterest" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label20" runat="server" Text="Employee Fringe"></asp:Label></td><td><asp:TextBox ID="TxtEmpFringe" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td>
                            &nbsp;</td><td>&nbsp;</td></tr><tr><td><asp:Label ID="Label93" runat="server" Text="Tips"></asp:Label></td><td><asp:TextBox ID="TxtNFTips" runat="server" Width="75px" Enabled="False"></asp:TextBox></td><td><asp:Label ID="Label97" runat="server" Font-Bold="True" Text="Total Fringe Benefit"></asp:Label></td><td><asp:TextBox ID="TxtNFTotalFringe" runat="server" Width="75px" Enabled="False"></asp:TextBox></td></tr></table></ContentTemplate></cc1:TabPanel>
                    </cc1:TabContainer>
                </asp:Panel> 
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_InitializeRequest);
        prm.add_endRequest(prm_EndRequest);

        function prm_InitializeRequest(sender, args) {
            var panelProg = $get('divImage');
            if (args._postBackElement.id != "Timer1") {
                panelProg.style.display = '';
            }
        };

        function prm_EndRequest(sender, args) {
            var panelProg = $get('divImage');
            panelProg.style.display = 'none';
        };
    </script>

</body>
</html>
