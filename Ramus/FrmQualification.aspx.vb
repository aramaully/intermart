﻿Partial Public Class FrmQualification
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrAudit As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            FillGrid()
        End If
    End Sub

    Protected Sub FillGrid()
        Cmd.Connection = Con
        StrSql = "SELECT * FROM Qualifications order by EmpID,QYrStart"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvQualification.DataSource = Rdr
        GdvQualification.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "New" Then
            TxtEmpID.Text = ""
            TxtYrStart.Text = ""
            TxtQYrEnd.Text = ""
            TxtQLevel.Text = ""
            TxtQCourses.Text = ""
            TxtQInstitution.Text = ""
            TxtQModules.Text = ""
            TxtQRemarks.Text = ""
            TxtRecNo.Text = ""
            TxtEmpID.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Dim Trans As OleDb.OleDbTransaction
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
            Cmd.Connection = Con
            Cmd.Transaction = Trans
            Cmd.CommandText = "Select * FROM Qualifications WHERE RecNo = " & Val(TxtRecNo.Text) & ""
            Rdr = Cmd.ExecuteReader
            If Rdr.Read Then
                Cmd.CommandText = "Delete FROM Qualifications WHERE RecNo = " & Val(TxtRecNo.Text) & ""
            End If
            Rdr.Close()
            Try
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO Qualifications (EmpID,QYrStart,QYrEnd,QLevel,QCourse,QInstitution,QModule,QRemarks) VALUES('"
                StrSql = StrSql & TxtEmpID.Text & "','"
                StrSql = StrSql & TxtYrStart.Text & "','"
                StrSql = StrSql & TxtQYrEnd.Text & "','"
                StrSql = StrSql & TxtQLevel.Text & "','"
                StrSql = StrSql & TxtQCourses.Text & "','"
                StrSql = StrSql & TxtQInstitution.Text & "','"
                StrSql = StrSql & TxtQModules.Text & "','"
                StrSql = StrSql & TxtQRemarks.Text & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                Trans.Commit()

                TxtEmpID.Text = ""
                TxtYrStart.Text = ""
                TxtQYrEnd.Text = ""
                TxtQLevel.Text = ""
                TxtQCourses.Text = ""
                TxtQInstitution.Text = ""
                TxtQModules.Text = ""
                TxtQRemarks.Text = ""
                TxtRecNo.Text = ""
                TxtEmpID.Focus()
                FillGrid()

                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Qualification for EmpID  " & TxtEmpID.Text & " Created / Edited. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Create / Edit Qualification','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                Trans.Rollback()
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Cmd.Connection = Con
            Cmd.CommandText = "Select * FROM Qualifications WHERE RecNo = " & Val(TxtRecNo.Text) & ""
            Rdr = Cmd.ExecuteReader
            If Rdr.Read Then
                Cmd.CommandText = "Delete FROM Qualifications WHERE RecNo = " & Val(TxtRecNo.Text) & ""
                Cmd.ExecuteNonQuery()
            End If
            Rdr.Close()
            Try
                Cmd.ExecuteNonQuery()
                TxtEmpID.Text = ""
                TxtYrStart.Text = ""
                TxtQYrEnd.Text = ""
                TxtQLevel.Text = ""
                TxtQCourses.Text = ""
                TxtQInstitution.Text = ""
                TxtQModules.Text = ""
                TxtQRemarks.Text = ""
                TxtRecNo.Text = ""
                TxtEmpID.Focus()
                FillGrid()
                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Qualification for EmpID  " & TxtEmpID.Text & " Deleted. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Delete Qualification','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtEmpID.Text)
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
        End If
    End Sub

    Private Sub GdvQualification_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvQualification.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvQualification, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvQualification_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvQualification.SelectedIndexChanged
        TxtRecNo.Text = GdvQualification.SelectedRow.Cells(0).Text.Replace("&nbsp;", "")
        TxtEmpID.Text = GdvQualification.SelectedRow.Cells(1).Text.Replace("&nbsp;", "")
        TxtYrStart.Text = GdvQualification.SelectedRow.Cells(2).Text.Replace("&nbsp;", "")
        TxtQYrEnd.Text = GdvQualification.SelectedRow.Cells(3).Text.Replace("&nbsp;", "")
        TxtQLevel.Text = GdvQualification.SelectedRow.Cells(4).Text.Replace("&nbsp;", "")
        TxtQCourses.Text = GdvQualification.SelectedRow.Cells(5).Text.Replace("&nbsp;", "")
        TxtQInstitution.Text = GdvQualification.SelectedRow.Cells(6).Text.Replace("&nbsp;", "")
        TxtQModules.Text = GdvQualification.SelectedRow.Cells(7).Text.Replace("&nbsp;", "")
        TxtQRemarks.Text = GdvQualification.SelectedRow.Cells(8).Text.Replace("&nbsp;", "")
    End Sub

    Private Sub GdvQualification_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvQualification.SelectedIndexChanging
        GdvQualification.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvQualification.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvQualification.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        FillGrid()
    End Sub

    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnDelete.Click
        Cmd.Connection = Con
        Cmd.CommandText = "Select * FROM Qualifications WHERE RecNo = " & Val(TxtRecNo.Text) & ""
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            Cmd.CommandText = "Delete FROM Qualifications WHERE RecNo = " & Val(TxtRecNo.Text) & ""
        End If
        Rdr.Close()
        Try
            Cmd.ExecuteNonQuery()
            TxtRecNo.Text = ""
            TxtEmpID.Text = ""
            TxtYrStart.Text = ""
            TxtQYrEnd.Text = ""
            TxtQLevel.Text = ""
            TxtQCourses.Text = ""
            TxtQInstitution.Text = ""
            TxtQModules.Text = ""
            TxtQRemarks.Text = ""
            TxtEmpID.Focus()
            FillGrid()

            'Audit
            CmdAudit.Connection = Con
            StrAudit = "Qualification for EmpID  " & TxtEmpID.Text & " Deleted. "
            CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Delete Qualification','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
            CmdAudit.ExecuteNonQuery()
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
        End Try
    End Sub
End Class