﻿Public Partial Class FrmIndividualTraining
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            FillGrid()
            TxtDate.Text = Date.Today
            Cmd.Connection = Con
            Cmd.CommandText = "Select CourseCode From CourseMaster"
            Rdr = Cmd.ExecuteReader
            CmbTraining.Items.Add(" ")
            While Rdr.Read
                CmbTraining.Items.Add(Rdr("CourseCode"))
            End While
            Rdr.Close()
            CmbTraining.SelectedIndex = 0
        End If
    End Sub

    Protected Sub FillGrid()
        Cmd.Connection = Con
        If Trim(TxtEmpID.Text) <> "" Then
            StrSql = "Select EmpID,Name,[Date],StartDate,EndDate,TrainingTitle,TrainingDetails,TrainingHours,StartTime,EndTime,RecNo From IndividualTraining Where EmpID = '" & TxtEmpID.Text & "'"
        Else
            StrSql = "Select EmpID,Name,[Date],StartDate,EndDate,TrainingTitle,TrainingDetails,TrainingHours,StartTime,EndTime,RecNo From IndividualTraining"
        End If
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvCheckList.DataSource = Rdr
        GdvCheckList.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ClearAll()
        TxtFirst.Text = ""
        TxtLast.Text = ""
        TxtClockID.Text = ""
        TxtCategory.Text = ""
        TxtTitle.Text = ""
        TxtGender.Text = ""
        TxtStartDate.Text = ""
        TxtEndDate.Text = ""
        CmbTraining.SelectedIndex = 0
        TxtName.Text = ""
        TxtContent.Text = ""
        TxtHours.Text = ""
        TxtTimein.Text = ""
        TxtTimeOut.Text = ""
        ImgPhoto.ImageUrl = Nothing
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ev As New System.EventArgs
        Dim StrInsert As String
        Dim StrName As String
        If e.Item.CommandName = "New" Then
            ClearAll()
            TxtEmpID.Text = ""
            TxtEmpID.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            If TxtDate.Text = "" Then
                LblError.Visible = True
                LblError.Text = "Please Input Date!!!"
                Exit Sub
            End If
            If CmbTraining.Text = "" Then
                LblError.Visible = True
                LblError.Text = "Please Select Training Title!!!"
                Exit Sub
            End If
            If TxtStartDate.Text = "" Then
                LblError.Visible = True
                LblError.Text = "Please Select Start Date!!!"
                Exit Sub
            End If
            If TxtEndDate.Text = "" Then
                LblError.Visible = True
                LblError.Text = "Please Select End Date!!!"
                Exit Sub
            End If
            StrName = TxtFirst.Text & " " & TxtLast.Text
            If Trim(StrName) = "" Then
                StrName = TxtFullName.Text
            End If
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Cmd.Connection = Con
            'Cmd.CommandText = "Delete From IndividualTraining Where EmpID = '" & TxtEmpID.Text & "' And Date='" & Format(CDate(TxtDate.Text), "dd-MMM-yyyy") & "'"
            Cmd.CommandText = "Delete From IndividualTraining Where RecNo = " & Val(TxtRecNo.Text) & ""
            Try
                Cmd.ExecuteNonQuery()
                StrInsert = "Insert Into IndividualTraining(EmpID,Name,[Date],StartDate,EndDate,TrainingTitle,"
                StrInsert = StrInsert & "TrainingDetails,TrainingHours,StartTime,EndTime)Values('"
                StrInsert = StrInsert & TxtEmpID.Text & "','"
                StrInsert = StrInsert & StrName & "','"
                StrInsert = StrInsert & Format(CDate(TxtDate.Text), "dd-MMM-yyyy") & "','"
                StrInsert = StrInsert & Format(CDate(TxtStartDate.Text), "dd-MMM-yyyy") & "','"
                StrInsert = StrInsert & Format(CDate(TxtEndDate.Text), "dd-MMM-yyyy") & "','"
                StrInsert = StrInsert & CmbTraining.Text & "','"
                StrInsert = StrInsert & TxtContent.Text & "','"
                StrInsert = StrInsert & Val(TxtHours.Text) & "','"
                StrInsert = StrInsert & TxtTimein.Text & "','"
                StrInsert = StrInsert & TxtTimeOut.Text & "')"
                ''If ChkAward.Checked Then
                ''    StrInsert = StrInsert & 1 & ","
                ''Else
                ''    StrInsert = StrInsert & 0 & ","
                ''End If
                ''If ChkNonAward.Checked Then
                ''    StrInsert = StrInsert & 1 & ","
                ''Else
                ''    StrInsert = StrInsert & 0 & ","
                ''End If
                ''If ChkTainingPassport.Checked Then
                ''    StrInsert = StrInsert & 1 & ")"
                ''Else
                ''    StrInsert = StrInsert & 0 & ")"
                ''End If
                Cmd.CommandText = StrInsert
                Cmd.ExecuteNonQuery()
                TxtFirst.Text = ""
                TxtLast.Text = ""
                TxtClockID.Text = ""
                TxtCategory.Text = ""
                TxtTitle.Text = ""
                TxtGender.Text = ""
                ImgPhoto.ImageUrl = Nothing
                TxtStartDate.Text = ""
                TxtEndDate.Text = ""
                CmbTraining.SelectedIndex = 0
                TxtName.Text = ""
                TxtContent.Text = ""
                TxtHours.Text = ""
                TxtTimein.Text = ""
                TxtTimeOut.Text = ""
                ''ChkAward.Checked = False
                ''ChkNonAward.Checked = False
                ''ChkTainingPassport.Checked = False
                FillGrid()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "DELETE FROM Employee WHERE RecNo = " & TxtRecNo.Text & ""
                Cmd.ExecuteNonQuery()
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Next" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID > '" & TxtEmpID.Text & "'"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try

        End If
        If e.Item.CommandName = "Previous" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID < '" & TxtEmpID.Text & "' ORDER BY EmpID DESC"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try
        End If
        If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtEmpID.Text)
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
        End If
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        If Rdr.Read Then
            On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
            TxtTitle.Text = Rdr("Title")
            TxtCategory.Text = Rdr("Category")
            TxtGender.Text = Rdr("Gender")
            TxtCategory.Text = Rdr("Category")
            ChkClocked.Checked = IIf(Rdr("Clocked"), True, False)
            TxtClockID.Text = Rdr("BatchNo")
            Session.Add("EmpID", Rdr("EmpID"))
            ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & Rdr("EmpID")
        Else
            Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(1))
            ToolBar1_ItemClick(Me, ev)
            'ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('Invalid Employee ID','Error');</script>", False)
        End If
        Rdr.Close()
        FillGrid()
    End Sub

    Protected Sub GdvCheckList_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GdvCheckList.SelectedIndexChanged
        Try
            TxtEmpID.Text = GdvCheckList.SelectedRow.Cells(0).Text.Replace("&nbsp;", "")
            TxtFullName.Text = GdvCheckList.SelectedRow.Cells(1).Text.Replace("&nbsp;", "")
            TxtDate.Text = GdvCheckList.SelectedRow.Cells(2).Text.Replace("&nbsp;", "")
            TxtStartDate.Text = GdvCheckList.SelectedRow.Cells(3).Text.Replace("&nbsp;", "")
            TxtEndDate.Text = GdvCheckList.SelectedRow.Cells(4).Text.Replace("&nbsp;", "")
            CmbTraining.Text = GdvCheckList.SelectedRow.Cells(5).Text.Replace("&nbsp;", "")
            TxtContent.Text = GdvCheckList.SelectedRow.Cells(6).Text.Replace("&nbsp;", "")
            TxtHours.Text = GdvCheckList.SelectedRow.Cells(7).Text.Replace("&nbsp;", "")
            TxtTimein.Text = GdvCheckList.SelectedRow.Cells(8).Text.Replace("&nbsp;", "")
            TxtTimeOut.Text = GdvCheckList.SelectedRow.Cells(9).Text.Replace("&nbsp;", "")
            TxtRecNo.Text = GdvCheckList.SelectedRow.Cells(10).Text.Replace("&nbsp;", "")
            'ChkAward.Checked = DirectCast(GdvCheckList.SelectedRow.Cells(9).Controls(0), CheckBox).Checked
            'ChkNonAward.Checked = DirectCast(GdvCheckList.SelectedRow.Cells(10).Controls(0), CheckBox).Checked
            'ChkTainingPassport.Checked = DirectCast(GdvCheckList.SelectedRow.Cells(11).Controls(0), CheckBox).Checked
        Catch

        End Try
    End Sub

    Private Sub GdvCheckList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvCheckList.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvCheckList, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvCheckList_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvCheckList.SelectedIndexChanging
        GdvCheckList.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvCheckList.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvCheckList.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Protected Sub CmbTraining_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbTraining.SelectedIndexChanged
        Dim CmdTrMaster As New OleDb.OleDbCommand
        Dim RdrTrMaster As OleDb.OleDbDataReader
        CmdTrMaster.Connection = Con
        CmdTrMaster.CommandText = "Select *From CourseMaster Where CourseCode = '" & CmbTraining.Text & "'"
        RdrTrMaster = CmdTrMaster.ExecuteReader
        If RdrTrMaster.Read Then
            TxtName.Text = RdrTrMaster("Name")
            'TxtHours.Text = RdrTrMaster("Hours")
            'TxtContent.Text = RdrTrMaster("Content")
        End If
        RdrTrMaster.Close()
    End Sub
End Class