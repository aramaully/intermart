﻿Public Partial Class FrmPayeSummary
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            TxtYear.Text = Date.Today.Year
            CmbMonth.SelectedIndex = Date.Today.Month - 1
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            Session("FieldCount") = 0
            Dim StrSelFormula As String
            If CmbMonth.Text = "Bonus" Then
                StrSelFormula = "{Salary.Month} = 'Bonus' And {Salary.Year} = '" & TxtYear.Text & "' And ({Salary.Paye} > 0) "
            Else
                Dim DtePayDate As Date = CDate("28-" + CmbMonth.Text + "-" + TxtYear.Text)
                Dim StrDate As String = Format(DtePayDate, "yyyy,MM,dd")
                StrSelFormula = "{Salary.Date}=date(" & StrDate & ") And ({Salary.Paye} > 0) "
            End If
            If ChkPAYEList.Checked Then
                Session("ReportFile") = "RptPayeSummary.rpt"
            Else
                Session("ReportFile") = "RPaye.rpt"
            End If
            Session("SelFormula") = StrSelFormula
            Session("RepHead") = "Paye Summary for the month of " & CmbMonth.Text & ", " & TxtYear.Text
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        End If
    End Sub
End Class