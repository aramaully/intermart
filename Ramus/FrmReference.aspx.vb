﻿Public Partial Class FrmReference
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrAudit As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            FillGrid()
        End If
    End Sub

    Protected Sub FillGrid()
        Cmd.Connection = Con
        StrSql = "SELECT * FROM Reference order by EmpID,RefDate"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvRef.DataSource = Rdr
        GdvRef.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "New" Then
            TxtEmpID.Text = ""
            TxtRefDate.Text = ""
            TxtRefName.Text = ""
            TxtRefCompany.Text = ""
            TxtRefPosition.Text = ""
            TxtRefContact.Text = ""
            TxtRecNo.Text = ""
            TxtEmpID.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Dim Trans As OleDb.OleDbTransaction
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
            Cmd.Connection = Con
            Cmd.Transaction = Trans
            Cmd.CommandText = "Select * FROM Reference WHERE RecNo = " & Val(TxtRecNo.Text) & ""
            Rdr = Cmd.ExecuteReader
            If Rdr.Read Then
                Cmd.CommandText = "Delete FROM Reference WHERE RecNo = " & Val(TxtRecNo.Text) & ""
            End If
            Rdr.Close()
            Try
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO Reference (EmpID,RefDate,RefName,RefCompany,RefPosition,RefContact) VALUES('"
                StrSql = StrSql & TxtEmpID.Text & "','"
                StrSql = StrSql & TxtRefDate.Text & "','"
                StrSql = StrSql & TxtRefName.Text & "','"
                StrSql = StrSql & TxtRefCompany.Text & "','"
                StrSql = StrSql & TxtRefPosition.Text & "','"
                StrSql = StrSql & TxtRefContact.Text & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                Trans.Commit()

                TxtRecNo.Text = ""
                TxtEmpID.Text = ""
                TxtRefDate.Text = ""
                TxtRefName.Text = ""
                TxtRefCompany.Text = ""
                TxtRefPosition.Text = ""
                TxtRefContact.Text = ""
                TxtEmpID.Focus()
                FillGrid()

                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Reference for Emp ID " & TxtEmpID.Text & " Created / Edited. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Create / Edit Reference','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                Trans.Rollback()
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Reference WHERE RecNo = " & Val(TxtRecNo.Text) & ""
            Try
                Cmd.ExecuteNonQuery()
                TxtEmpID.Text = ""
                TxtRefDate.Text = ""
                TxtRefName.Text = ""
                TxtRefCompany.Text = ""
                TxtRefPosition.Text = ""
                TxtRefContact.Text = ""
                TxtEmpID.Focus()
                FillGrid()
                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Reference for Emp ID " & TxtEmpID.Text & " Deleted. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Delete Reference','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtEmpID.Text)
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
        End If
    End Sub

    Private Sub GdvRef_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvRef.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvRef, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvRef.SelectedIndexChanged
        TxtRecNo.Text = GdvRef.SelectedRow.Cells(0).Text.Replace("&nbsp;", "")
        TxtEmpID.Text = GdvRef.SelectedRow.Cells(1).Text.Replace("&nbsp;", "")
        TxtRefDate.Text = GdvRef.SelectedRow.Cells(2).Text.Replace("&nbsp;", "")
        TxtRefName.Text = GdvRef.SelectedRow.Cells(3).Text.Replace("&nbsp;", "")
        TxtRefCompany.Text = GdvRef.SelectedRow.Cells(4).Text.Replace("&nbsp;", "")
        TxtRefPosition.Text = GdvRef.SelectedRow.Cells(5).Text.Replace("&nbsp;", "")
        TxtRefContact.Text = GdvRef.SelectedRow.Cells(6).Text.Replace("&nbsp;", "")
    End Sub

    Private Sub GdvRef_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvRef.SelectedIndexChanging
        GdvRef.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvRef.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvRef.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        FillGrid()
    End Sub

    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnDelete.Click
        Cmd.Connection = Con
        Cmd.CommandText = "Select * FROM Reference WHERE RecNo = " & Val(TxtRecNo.Text) & ""
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            Cmd.CommandText = "Delete FROM Reference WHERE RecNo = " & Val(TxtRecNo.Text) & ""
        End If
        Rdr.Close()
        Try
            Cmd.ExecuteNonQuery()
            TxtRecNo.Text = ""
            TxtEmpID.Text = ""
            TxtRefDate.Text = ""
            TxtRefName.Text = ""
            TxtRefCompany.Text = ""
            TxtRefPosition.Text = ""
            TxtRefContact.Text = ""
            TxtEmpID.Focus()
            FillGrid()

            'Audit
            CmdAudit.Connection = Con
            StrAudit = "Reference for Emp ID " & TxtEmpID.Text & " Deleted. "
            CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Delete Reference','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
            CmdAudit.ExecuteNonQuery()
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
        End Try
    End Sub
End Class