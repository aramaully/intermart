﻿Partial Public Class FrmEmp6month

    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            FillGrid()
        End If
    End Sub

    Protected Sub FillGrid()
        Cmd.Connection = Con
        StrSql = "SELECT * FROM Employee"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvDept.DataSource = Rdr
        GdvDept.DataBind()
        For Each Row As GridViewRow In GdvDept.Rows
            If Row.Cells(3).Text >= 0.5 And Row.Cells(3).Text < 1 Then
                Row.Cells(3).BackColor = Color.Red
                'Row.Cells(4).BackColor = Color.Red
                'Row.Cells(5).BackColor = Color.Red
            End If
        Next
        Rdr.Close()
    End Sub


    ' ''    Private Sub GdvDept_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvDept.RowDataBound
    ' ''        On Error Resume Next
    ' ''        If e.Row.RowType = DataControlRowType.DataRow Then
    ' ''            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
    ' ''            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvDept, "Select$" & e.Row.RowIndex)
    ' ''        End If
    ' ''    End Sub



    ' ''    Private Sub GdvDept_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvDept.SelectedIndexChanging
    ' ''        GdvDept.SelectedIndex = e.NewSelectedIndex
    ' ''    End Sub

    ' ''    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
    ' ''        For i = 0 To Me.GdvDept.Rows.Count - 1
    ' ''            ClientScript.RegisterForEventValidation(Me.GdvDept.UniqueID, "Select$" & i)
    ' ''        Next
    ' ''        MyBase.Render(writer)
    ' ''    End Sub

End Class