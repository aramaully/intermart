﻿Public Partial Class FindClockID
    Inherits System.Web.UI.Page
    Dim TimeCon As New OleDb.OleDbConnection
    Dim TimeCmd As New OleDb.OleDbCommand
    Dim TimeRdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        TimeCon.ConnectionString = Session("TimexConn")
        TimeCon.Open()
        If Not Page.IsPostBack Then
            TxtSearch.Text = Session("PstrCode")
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Refresh" Then
            TimeCmd.Connection = TimeCon
            StrSql = "SELECT UserID,[Name] FROM UserInfo "
            If TxtSearch.Text <> "" Then
                StrSql = StrSql & "WHERE [Name] LIKE '%" & TxtSearch.Text & "%'"
            End If
            TimeCmd.CommandText = StrSql
            TimeRdr = TimeCmd.ExecuteReader
            GdvFindClockID.DataSource = TimeRdr
            GdvFindClockID.DataBind()
        End If
    End Sub

    Private Sub GdvFindClockID_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFindClockID.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFindClockID, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFindClockID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFindClockID.SelectedIndexChanged
        TxtClockID.Value = GdvFindClockID.SelectedRow.Cells(0).Text
        GdvFindClockID.SelectedRow.Focus()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CloseMe", "GetID();", True)
    End Sub

    Private Sub GdvFindClockID_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFindClockID.SelectedIndexChanging
        GdvFindClockID.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvFindClockID.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFindClockID.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub
End Class