﻿Public Partial Class FrmExperience
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrAudit As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            FillGrid()
        End If
    End Sub

    Protected Sub FillGrid()
        Cmd.Connection = Con
        StrSql = "SELECT * FROM Experience order by EmpID,ExpFromDate desc"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvExp.DataSource = Rdr
        GdvExp.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "New" Then
            TxtEmpID.Text = ""
            TxtExpFromDate.Text = ""
            TxtExpToDate.Text = ""
            TxtExpCompany.Text = ""
            TxtExpRole.Text = ""
            TxtExpResp.Text = ""
            TxtExpCondition.Text = ""
            TxtRecNo.Text = ""
            TxtEmpID.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Dim Trans As OleDb.OleDbTransaction
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
            Cmd.Connection = Con
            Cmd.Transaction = Trans
            Cmd.CommandText = "DELETE FROM Experience WHERE RecNo = " & Val(TxtRecNo.Text) & ""
            Try
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO Experience (EmpID,ExpFromDate,ExpToDate,ExpCompany,ExpRole,ExpResp,ExpCondition) VALUES('"
                StrSql = StrSql & TxtEmpID.Text & "','"
                StrSql = StrSql & TxtExpFromDate.Text & "','"
                StrSql = StrSql & TxtExpToDate.Text & "','"
                StrSql = StrSql & TxtExpCompany.Text & "','"
                StrSql = StrSql & TxtExpRole.Text & "','"
                StrSql = StrSql & TxtExpResp.Text & "','"
                StrSql = StrSql & TxtExpCondition.Text & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                Trans.Commit()

                TxtEmpID.Text = ""
                TxtExpFromDate.Text = ""
                TxtExpToDate.Text = ""
                TxtExpCompany.Text = ""
                TxtExpRole.Text = ""
                TxtExpResp.Text = ""
                TxtExpCondition.Text = ""
                TxtEmpID.Focus()
                FillGrid()

                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Experience for " & TxtEmpID.Text & " Created / Edited. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Create / Edit Experience','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                Trans.Rollback()
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Experience WHERE RecNo = " & Val(TxtRecNo.Text) & ""
            Try
                Cmd.ExecuteNonQuery()
                TxtEmpID.Text = ""
                TxtExpFromDate.Text = ""
                TxtExpToDate.Text = ""
                TxtExpCompany.Text = ""
                TxtExpRole.Text = ""
                TxtExpResp.Text = ""
                TxtExpCondition.Text = ""
                TxtRecNo.Text = ""
                TxtEmpID.Focus()
                FillGrid()

                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Experience for " & TxtEmpID.Text & " Deleted. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Delete Experience','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtEmpID.Text)
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
        End If
    End Sub

    Private Sub GdvExp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvExp.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvExp, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvExp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvExp.SelectedIndexChanged
        TxtEmpID.Text = GdvExp.SelectedRow.Cells(0).Text.Replace("&nbsp;", "")
        TxtExpFromDate.Text = GdvExp.SelectedRow.Cells(1).Text.Replace("&nbsp;", "")
        TxtExpToDate.Text = GdvExp.SelectedRow.Cells(2).Text.Replace("&nbsp;", "")
        TxtExpCompany.Text = GdvExp.SelectedRow.Cells(3).Text.Replace("&nbsp;", "")
        TxtExpRole.Text = GdvExp.SelectedRow.Cells(4).Text.Replace("&nbsp;", "")
        TxtExpResp.Text = GdvExp.SelectedRow.Cells(5).Text.Replace("&nbsp;", "")
        TxtExpCondition.Text = GdvExp.SelectedRow.Cells(6).Text.Replace("&nbsp;", "")
        TxtRecNo.Text = GdvExp.SelectedRow.Cells(7).Text.Replace("&nbsp;", "")
    End Sub

    Private Sub GdvExp_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvExp.SelectedIndexChanging
        GdvExp.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvExp.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvExp.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        FillGrid()
    End Sub

    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnDelete.Click
        Cmd.Connection = Con
        Cmd.CommandText = "DELETE FROM Experience WHERE RecNo = " & Val(TxtRecNo.Text) & ""
        Try
            Cmd.ExecuteNonQuery()
            TxtEmpID.Text = ""
            TxtExpFromDate.Text = ""
            TxtExpToDate.Text = ""
            TxtExpCompany.Text = ""
            TxtExpRole.Text = ""
            TxtExpResp.Text = ""
            TxtExpCondition.Text = ""
            TxtRecNo.Text = ""
            TxtEmpID.Focus()
            FillGrid()

            'Audit
            CmdAudit.Connection = Con
            StrAudit = "Experience for " & TxtEmpID.Text & " Deleted. "
            CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Delete Experience','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
            CmdAudit.ExecuteNonQuery()
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
        End Try
    End Sub
End Class