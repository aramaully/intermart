﻿Public Partial Class FrmPaySummary
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            CmbMonthFrom.SelectedIndex = 0
            CmbMonthTo.SelectedIndex = Date.Today.Month - 1
            TxtYearFrom.Text = Date.Today.Year
            TxtYearTo.Text = Date.Today.Year
            TxtDateFrom.Text = CDate("28-January-" & TxtYearFrom.Text)
            TxtDateTo.Text = CDate("28-" & CmbMonthTo.Text & "-" & TxtYearTo.Text)
            CmbDept.SelectedIndex = 0
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT * FROM Outlet"
            Rdr = Cmd.ExecuteReader
            CmbDept.Items.Add("ALL")
            While Rdr.Read
                CmbDept.Items.Add(Rdr("OutletName"))
            End While
            Rdr.Close()
            TxtEmpID.Text = "ALL"
        End If
    End Sub

    Protected Sub ClearAll()
        TxtFirst.Text = ""
        TxtLast.Text = ""
        TxtClockID.Text = ""
        TxtCategory.Text = ""
        TxtTitle.Text = ""
        TxtGender.Text = ""
        ImgPhoto.ImageUrl = Nothing
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ev As New System.EventArgs
        If e.Item.CommandName = "New" Then
            ClearAll()
            TxtEmpID.Text = "ALL"
        End If
        If e.Item.CommandName = "Print" Then
            Session("FieldCount") = 0
            Dim StrSelFormula As String

            If CmbDept.Text = "Management" Then
                If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
                Else
                    Exit Sub
                End If
            End If
            If ChkDS.Checked Then
                Session("ReportFile") = "RPaysheetMonthSum.rpt" '"RPaysheetSummaryDept.rpt"
            Else
                Session("ReportFile") = "RPaysheetSummary.rpt"
            End If

            Dim DtePayDateFrom As Date = CDate("28-" + CmbMonthFrom.Text + "-" + TxtYearFrom.Text)
            Dim StrDateFrom As String = Format(DtePayDateFrom, "yyyy,MM,dd hh,mm,ss")
            Dim DtePayDateto As Date = CDate("28-" + CmbMonthTo.Text + "-" + TxtYearTo.Text)
            Dim StrDateTo As String = Format(DtePayDateto, "yyyy,MM,dd hh,mm,ss")


            'Dim DtePayDateFrom As Date = CDate(TxtDateFrom.Text)
            'Dim DtePayDateto As Date = CDate(TxtDateTo.Text)
            'Dim StrDateFrom As String = Format(DtePayDateFrom, "yyyy,MM,dd")
            'Dim StrDateTo As String = Format(DtePayDateto, "yyyy,MM,dd")

            StrSelFormula = "{Salary.Date} >= Date(" & Format(CDate(TxtDateFrom.Text), "yyyy,MM,dd") & ") "
            StrSelFormula = StrSelFormula & " And {Salary.Date} <= Date(" & Format(CDate(TxtDateTo.Text), "yyyy,MM,dd") & ") "

            'StrSelFormula = "{Salary.Date}>=Datetime(" & StrDateFrom & ") "
            'StrSelFormula = StrSelFormula & "And {Salary.Date}<=Datetime(" & StrDateTo & ")"

            If TxtEmpID.Text <> "ALL" Then
                StrSelFormula = StrSelFormula & " And {Salary.EmpID} = '" & TxtEmpID.Text & "'"
            End If
            If CmbDept.Text <> "ALL" Then
                StrSelFormula = StrSelFormula & " And {Salary.OutletName} = '" & CmbDept.Text & "'"
            End If

            Session("SelFormula") = StrSelFormula
            Session("RepHead") = "Pay Summary from " & TxtDateFrom.Text & " to " & TxtDateTo.Text
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        End If
        If e.Item.CommandName = "Delete" Then
            'Try
            '    Cmd.Connection = Con
            '    Cmd.CommandText = "DELETE FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
            '    Cmd.ExecuteNonQuery()
            '    TxtEmpID_TextChanged(Me, ev)
            'Catch ex As Exception
            '    ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            'End Try
        End If
        If e.Item.CommandName = "Next" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID > '" & TxtEmpID.Text & "'"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try

        End If
        If e.Item.CommandName = "Previous" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID < '" & TxtEmpID.Text & "' ORDER BY EmpID DESC"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try
        End If
        If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtEmpID.Text)
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
        End If
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
            StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Else
            StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "' And OutletName = '" & Session("Level") & "'"
        End If
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        If TxtEmpID.Text = "ALL" Then Exit Sub
        If Rdr.Read Then
            On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
            TxtTitle.Text = Rdr("Title")
            TxtCategory.Text = Rdr("Category")
            TxtGender.Text = Rdr("Gender")
            TxtCategory.Text = Rdr("Category")
            ChkClocked.Checked = IIf(Rdr("Clocked"), True, False)
            TxtClockID.Text = Rdr("BatchNo")
            Session.Add("EmpID", Rdr("EmpID"))
            ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & Rdr("EmpID")
            'Personal Details
        Else
            Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(1))
            ToolBar1_ItemClick(Me, ev)
            'ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('Invalid Employee ID','Error');</script>", False)
        End If
        Rdr.Close()
    End Sub

    Protected Sub CmbCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbCategory.SelectedIndexChanged
        If CmbCategory.SelectedIndex = 0 Then
            CmbMonthFrom.SelectedIndex = 0
            CmbMonthTo.SelectedIndex = Date.Today.Month - 1
            CmbMonthFrom.Visible = True
            CmbMonthTo.Visible = True
            TxtDateFrom.Visible = False
            TxtDateTo.Visible = False
            TxtDateFrom.Text = CDate("1-January-" & Date.Today.Year)
            TxtDateTo.Text = Date.Today
        Else
            CmbMonthFrom.Visible = False
            CmbMonthTo.Visible = False
            TxtDateFrom.Visible = True
            TxtDateTo.Visible = True
            TxtDateFrom.Text = CDate("1-January-" & Date.Today.Year)
            TxtDateTo.Text = Date.Today
        End If
    End Sub

    Protected Sub CmbMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbMonthFrom.SelectedIndexChanged, CmbMonthTo.SelectedIndexChanged, TxtYearFrom.TextChanged, TxtYearTo.TextChanged
        If CmbCategory.Text = "Monthly" Then
            TxtDateFrom.Text = CDate("28-" & CmbMonthFrom.Text & "-" & TxtYearFrom.Text)
            TxtDateTo.Text = CDate("28-" & CmbMonthTo.Text & "-" & TxtYearTo.Text)
        End If
    End Sub
End Class