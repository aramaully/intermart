﻿Public Partial Class FrmPassword
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrAudit As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            TxtUserID.Text = Session("UserID")
            If Session("Level") = "Leave Application" Then
                TxtUserID.Enabled = False
            End If
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim Ev As New System.EventArgs

        If e.Item.CommandName = "New" Then
            TxtUserID.Text = ""
            TxtName.Text = ""
            TxtOldPassword.Text = ""
            TxtNewPassword.Text = ""
            TxtReEnter.Text = ""
            TxtUserID.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Cmd.Connection = Con
            Try
                StrSql = "UPDATE [User] SET Password = '"
                StrSql = StrSql & TxtNewPassword.Text & "' "
                StrSql = StrSql & "WHERE UserID = '" & TxtUserID.Text & "'"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Login Password changed for User " & TxtUserID.Text & "."
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Leave Record','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
    End Sub

    Protected Sub TxtOldPassword_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtOldPassword.TextChanged
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT * FROM [User] WHERE UserID = '" & TxtUserID.Text & "'"
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            If TxtOldPassword.Text <> Rdr("Password") Then
                LblMsg.Text = "Wrong Password Sorry!"
                LblMsg.Visible = True
                ToolBar1.Items(1).Disabled = True
            Else
                TxtName.Text = Rdr("Name").ToString
                LblMsg.Text = ""
                LblMsg.Visible = False
                ToolBar1.Items(1).Disabled = False
            End If
        Else
            LblMsg.Text = "Invalid User Sorry!"
            LblMsg.Visible = True
            ToolBar1.Items(1).Disabled = True
        End If
    End Sub

    Protected Sub TxtNewPassword_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtNewPassword.TextChanged

    End Sub
End Class