﻿Public Partial Class FrmTrainingReport
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim CmdShift As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Dim StrSelFormula As String
            Dim DteFromDate As Date = Format(CDate(TxtDateFrom.Text), "dd/MM/yyyy")
            Dim DteToDate As Date = Format(CDate(TxtDateTo.Text), "dd/MM/yyyy")
            Dim StrDateFrom As String = Format(DteFromDate, "yyyy,MM,dd")
            Dim StrDateTo As String = Format(DteToDate, "yyyy,MM,dd")
            If ChkIndividualReport.Checked Then
                Session("ReportFile") = "RptIndividualTraining.rpt"
                StrSelFormula = "{IndividualTraining.Date}>=date(" & StrDateFrom & ") And {IndividualTraining.Date}<=date(" & StrDateTo & ")"
            Else
                'Session("ReportFile") = "RptGroupTraining.rpt"
                'StrSelFormula = "{GroupTraining.Date}>=date(" & StrDateFrom & ") And {GroupTraining.Date}<=date(" & StrDateTo & ")"
            End If
            Session("SelFormula") = StrSelFormula
            Session("RepHead") = "Training List"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        End If
    End Sub

End Class