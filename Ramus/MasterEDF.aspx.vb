﻿Public Partial Class MasterEDF
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrAudit As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            FillGrid()
        End If
    End Sub

    Protected Sub FillGrid()
        Cmd.Connection = Con
        StrSql = "SELECT * FROM MasterEDF order by Category"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvDept.DataSource = Rdr
        GdvDept.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "New" Then
            TxtCategory.Text = ""
            TxtAmount.Text = ""
            TxtCategory.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
                Lblerror.Visible = False
                Lblerror.Text = ""
            Else
                Lblerror.Visible = True
                Lblerror.Text = "No Access Right to Update EDF!!!"
                Exit Sub
            End If

            Dim Trans As OleDb.OleDbTransaction
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
            Cmd.Connection = Con
            Cmd.Transaction = Trans
            Cmd.CommandText = "DELETE FROM MasterEDF WHERE Category = '" & TxtCategory.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO MasterEDF (Category,Amount) VALUES('"
                StrSql = StrSql & TxtCategory.Text & "','"
                StrSql = StrSql & TxtAmount.Text & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                Trans.Commit()
                'Audit
                Dim CmdAudit As New OleDb.OleDbCommand
                Dim StrAudit As String
                CmdAudit.Connection = Con
                StrAudit = "New EDF " & TxtCategory.Text & " Created "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','New EDF Category','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()

                TxtCategory.Text = ""
                TxtAmount.Text = ""
                TxtCategory.Focus()
                FillGrid()

                'Audit
                CmdAudit.Connection = Con
                StrAudit = "New EDF " & TxtCategory.Text & " Created / Edited. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Create / Edit Department','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                Trans.Rollback()
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM MasterEDF WHERE Category = '" & TxtCategory.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                TxtCategory.Text = ""
                TxtAmount.Text = ""
                TxtCategory.Focus()
                FillGrid()

                'Audit
                CmdAudit.Connection = Con
                StrAudit = "New EDF " & TxtCategory.Text & " Deleted. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Delete Department','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
    End Sub

    Private Sub GdvDept_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvDept.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvDept, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvDept_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvDept.SelectedIndexChanged
        TxtCategory.Text = GdvDept.SelectedRow.Cells(0).Text.Replace("&nbsp;", "")
        TxtAmount.Text = GdvDept.SelectedRow.Cells(1).Text.Replace("&nbsp;", "")
    End Sub

    Private Sub GdvDept_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvDept.SelectedIndexChanging
        GdvDept.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvDept.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvDept.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub TxtOutletcode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtCategory.TextChanged
        FillGrid()
    End Sub

End Class