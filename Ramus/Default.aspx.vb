﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Partial Public Class _Default
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Session("LoggedIn") = False
        TxtUserName.Focus()
    End Sub
    Protected Sub ButLogin_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButLogin.Click
        Dim PstrServer As String
        Dim PstrDatabase As String

        Dim Con As New OleDbConnection
        Dim StrSql As String
        PstrServer = "payrollbb\sqlexpress"
        PstrDatabase = "InterMart"
        'PstrDatabase = "InterMartTest"

        'PstrServer = "(local)"
        PstrDatabase = "InterMart"
        Dim CmdUser As New OleDb.OleDbCommand
        Dim RdrUser As OleDb.OleDbDataReader
        Session("LoggedIn") = False
        Try
            Con.Close()
            Session("ConnString") = "Provider=SQLOLEDB.1;Data Source=" & PstrServer & ";User ID=admin;PWD=swathi*;Initial Catalog=" & PstrDatabase
            Con.ConnectionString = Session("ConnString")
            Con.Open()
        Catch ex As Data.OleDb.OleDbException
            MsgBox("Cannot Connect to Database. Please Specify Correct Server and Database", MsgBoxStyle.Critical, "Database Error")
            Exit Sub
        Finally
        End Try
        StrSql = "SELECT * FROM [User] WHERE UserID = '" & TxtUserName.Text & "'"
        CmdUser.Connection = Con
        CmdUser.CommandText = StrSql
        RdrUser = CmdUser.ExecuteReader
        If RdrUser.Read Then
            If RdrUser("Password") = TxtPassword.Text Then
                Session("LoggedIn") = True
                Session("UserID") = RdrUser("UserID").ToString
                Session("Server") = PstrServer
                Session("Database") = PstrDatabase
                Session("UserName") = RdrUser("Name").ToString
                Session("Level") = RdrUser("Level").ToString
                Session("EmpID") = RdrUser("EmpID").ToString

                ' Person log in the system.
                Dim cmdlog As New OleDb.OleDbCommand
                Dim struser As String = RdrUser("userid").ToString
                Dim strtime As String = Date.Now.ToShortTimeString()
                Dim strdate As String = Date.Now.ToShortDateString()
                cmdlog.Connection = Con
                cmdlog.CommandText = "insert into userlog values('" & struser & "','" & strdate & "','" & strtime & "')"
                cmdlog.ExecuteNonQuery()

                'cmdlog.connection = con
                'cmdlog.commandtext = "backup database intermart to disk = 'c:\ramusbackupdatabase\ramuspayroll.bak'"
                'cmdlog.executenonquery()

                Response.Redirect("MainMenu.aspx")

            Else
                LblMsg.Text = "Invalid Password"
            End If
        Else
            LblMsg.Text = "Invalid User Name"
        End If
        RdrUser.Close()
    End Sub
    Protected Sub ButCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButCancel.Click
        Session("LoggedIn") = False
        Response.Write("<script language='javascript'> { window.close();}</script>")
    End Sub
End Class