﻿Public Partial Class LoanTypes
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            FillGrid()
        Else
            'Dim eventArg As String = Request("__EVENTARGUMENT") & " " & Request("__EVENTTARGET")
            'TxtName.Text = eventArg
        End If
    End Sub

    Protected Sub FillGrid()
        Cmd.Connection = Con
        StrSql = "SELECT * FROM LoanType"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvLoan.DataSource = Rdr
        GdvLoan.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim Ev As New System.EventArgs

        If e.Item.CommandName = "New" Then
            TxtLoanID.Text = ""
            TxtName.Text = ""
            TxtLoanID.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Dim Trans As OleDb.OleDbTransaction
            Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
            Cmd.Connection = Con
            Cmd.Transaction = Trans
            Cmd.CommandText = "DELETE FROM LoanType WHERE LoanID = '" & TxtLoanID.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO LoanType (LoanID,Name) VALUES('"
                StrSql = StrSql & TxtLoanID.Text & "','"
                StrSql = StrSql & TxtName.Text & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                Trans.Commit()
                TxtLoanID.Text = ""
                TxtName.Text = ""
                TxtLoanID.Focus()
                FillGrid()
            Catch ex As Exception
                Trans.Rollback()
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM LoanType WHERE LoanID = '" & TxtLoanID.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                TxtLoanID.Text = ""
                TxtName.Text = ""
                TxtLoanID.Focus()
                FillGrid()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
    End Sub

    Private Sub GdvLoan_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvLoan.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvLoan, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvLoan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvLoan.SelectedIndexChanged
        TxtLoanID.Text = GdvLoan.SelectedRow.Cells(0).Text
        TxtName.Text = GdvLoan.SelectedRow.Cells(1).Text
    End Sub

    Private Sub GdvLoan_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvLoan.SelectedIndexChanging
        GdvLoan.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvLoan.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvLoan.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub
End Class