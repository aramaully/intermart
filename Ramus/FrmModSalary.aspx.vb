﻿Public Partial Class FrmModSalary
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdLock As New OleDb.OleDbCommand
    Dim RdrLock As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdAccess As New OleDb.OleDbCommand
    Dim CmdCheckEmp As New OleDb.OleDbCommand
    Dim RdrCheckEmp As OleDb.OleDbDataReader
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrAudit As String
    Dim Str15Hrs As String
    Dim Str20Hrs As String
    Dim Str30Hrs As String
    Dim Str10Hrs As String
    Dim Str1Amt As String
    Dim Str15Amt As String
    Dim str20Amt As String
    Dim Str30amt As String
    Dim StrMDays As String
    Dim StrMeal As String
    Dim StrMDeduct As String
    Dim StrArrears As String
    Dim StrTravel As String
    Dim StrOAllow As String
    Dim strNiteAllow As String
    Dim StrLRef As String
    Dim StrSRef As String
    Dim StrAttBonus As String
    Dim StrEOY As String
    Dim StrFringe As String
    Dim StrODeduct As String
    Dim StrCarB As String
    Dim StrHouseB As String
    Dim StrAccB As String
    Dim StrIntB As String
    Dim StrTipsB As String
    Dim StrWoff As String
    Dim StrExpB As String
    Dim StrTPaidB As String
    Dim strPRGF As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            TabPanel3.Visible = False
            TabPanel4.Visible = False
            TabPanel8.Visible = False
            CmbMonth.SelectedIndex = Date.Today.Month - 1
            TxtYear.Text = Date.Today.Year
            TxtDate.Text = CDate("28-" & CmbMonth.Text & "-" & TxtYear.Text)
        End If
        Dim js As String
        js = "javascript:" & Page.GetPostBackEventReference(Me, "@@@@@buttonPostBack") & ";"
        TxtEmpID.Attributes.Add("onchange", js)
        CmbMonth.Attributes.Add("onchange", js)
        TxtYear.Attributes.Add("onchange", js)
    End Sub

    Private Sub CmbCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbCategory.SelectedIndexChanged
           if CmbCategory.SelectedIndex=3 then
            CmbMonth.Visible = True
            TxtDate.Visible = False
        Else
            CmbMonth.Visible = False
            TxtDate.Visible = True
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ev As New System.EventArgs
        'If e.Item.CommandName = "New" Then
        '    ClearAll()
        '    TxtEmpID.Text = ""
        '    TxtEmpID.Focus()
        'End If

        StrSql = "SELECT locked FROM Salary WHERE Year = " & TxtYear.Text & " And Month = '" & CmbMonth.Text & "' And Locked = 1"
        CmdLock.Connection = Con
        CmdLock.CommandText = StrSql
        RdrLock = CmdLock.ExecuteReader
        If RdrLock.Read Then
            LblAccess.Visible = True
            LblAccess.Text = "Cannot Save Data,Salary Already Locked For This Employee !!!"
            Exit Sub
        Else
            If e.Item.CommandName = "Save" Then
                If Trim(TxtEmpID.Text) = "" Then
                    LblAccess.Visible = True
                    LblAccess.Text = "EmpID cannot be Blank !!!"
                    Exit Sub
                End If
                If Request.Cookies("ModSalDetail") IsNot Nothing Then
                    Dim lvGen As HttpCookie = Request.Cookies("ModSalDetail")
                    StrTPaidB = Server.HtmlDecode(lvGen.Values("TaxPaid"))
                    StrExpB = Server.HtmlDecode(lvGen.Values("Expenses"))
                    StrTipsB = Server.HtmlDecode(lvGen.Values("Tips"))
                    StrWoff = Server.HtmlDecode(lvGen.Values("WriteOff"))
                    StrIntB = Server.HtmlDecode(lvGen.Values("Interest"))
                    StrAccB = Server.HtmlDecode(lvGen.Values("Accommodation"))
                    StrHouseB = Server.HtmlDecode(lvGen.Values("HouseBenefit"))
                    StrCarB = Server.HtmlDecode(lvGen.Values("CarBenefit"))
                    Str30Hrs = Server.HtmlDecode(lvGen.Values("OTHrs30"))
                    Str20Hrs = Server.HtmlDecode(lvGen.Values("OTHrs20"))
                    Str15Hrs = Server.HtmlDecode(lvGen.Values("OTHrs15"))
                    Str10Hrs = Server.HtmlDecode(lvGen.Values("OTHrs1"))
                    StrODeduct = Server.HtmlDecode(lvGen.Values("Others"))
                    StrFringe = Server.HtmlDecode(lvGen.Values("FringeBenefits"))
                    StrEOY = Server.HtmlDecode(lvGen.Values("IfsBonus"))
                    StrAttBonus = Server.HtmlDecode(lvGen.Values("PensionEmp"))
                    StrSRef = Server.HtmlDecode(lvGen.Values("SickRefund"))
                    StrLRef = Server.HtmlDecode(lvGen.Values("LocalRefund"))
                    StrOAllow = Server.HtmlDecode(lvGen.Values("OtherAllow"))
                    StrTravel = Server.HtmlDecode(lvGen.Values("TravelAllowance"))
                    StrArrears = Server.HtmlDecode(lvGen.Values("Arrears"))
                    Str1Amt = Server.HtmlDecode(lvGen.Values("OT1Amt"))
                    Str15Amt = Server.HtmlDecode(lvGen.Values("OT15Amt"))
                    str20Amt = Server.HtmlDecode(lvGen.Values("OT20Amt"))
                    Str30amt = Server.HtmlDecode(lvGen.Values("OT30Amt"))
                End If

                Dim strSpecialBenefit As Long

                strSpecialBenefit = 0
                If (TxtSpecialBenefit.Text <> "") Then
                    strSpecialBenefit = Val(TxtSpecialBenefit.Text)
                End If

                Dim strOthDeduction As Long

                strOthDeduction = 0
                If (TxtOthDeduction.Text <> "") Then
                    strOthDeduction = Val(TxtOthDeduction.Text)
                End If


                Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                StrSql = "UPDATE Salary SET "
                StrSql = StrSql & "[Month] = '" & CmbMonth.Text & "',"
                StrSql = StrSql & "[Date] = '" & Format(CDate(TxtDate.Text), "yyyy-MM-dd") & "',"
                StrSql = StrSql & "[Year] = " & Val(TxtYear.Text) & ","
                StrSql = StrSql & "BasicSalary = " & Val(TxtNSBasicSalary.Text) & ","
                StrSql = StrSql & "IFSBonus = " & Val(TxtNSEOYBonus.Text) & ","
                If StrEOY <> TxtNSEOYBonus.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','EOY for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & StrEOY & "','" & TxtNSEOYBonus.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                StrSql = StrSql & "NPFEmployee = " & Val(TxtNSNpfEmp.Text) & ","
                StrSql = StrSql & "LoanRepay = " & Val(TxtNSLoanRepay.Text) & ","
                StrSql = StrSql & "LoanRepay1 = " & Val(TxtNSLoanRepay1.Text) & ","
                StrSql = StrSql & "PAYE = " & Val(TxtNSPaye.Text) & ","
                StrSql = StrSql & "EDF = " & Val(TxtNPIet.Text) & ","
                StrSql = StrSql & "NPFEmployer = " & Val(TxtNENpf.Text) & ","
                StrSql = StrSql & "EPZER = " & Val(TxtEPZER.Text) & ","
                StrSql = StrSql & "EPZEE = " & Val(TxtEPZEE.Text) & ","
                StrSql = StrSql & "NSF = " & Val(TxtNENsf.Text) & ","
                StrSql = StrSql & "IVTB = " & Val(TxtNEIvtb.Text) & ","
                StrSql = StrSql & "LocalRefund = " & Math.Round(Val(TxtMLRefund.Text), 2) & ","
                If StrLRef <> TxtMLRefund.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','Local Refund for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & StrLRef & "','" & TxtMLRefund.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                StrSql = StrSql & "SickRefund = " & Math.Round(Val(TxtMSRefund.Text), 2) & ","
                If StrSRef <> TxtMSRefund.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','Sick Refund for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & StrSRef & "','" & TxtMSRefund.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                StrSql = StrSql & "PensionEmp = " & Val(TxtNSAttendBonus.Text) & ","
                If StrAttBonus <> TxtNSAttendBonus.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','Pension for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & StrAttBonus & "','" & TxtNSAttendBonus.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                'HRS OT
                StrSql = StrSql & "OTHrs1 = " & Math.Round(Val(TxtM10Hrs.Text), 0) & ","
                If Str10Hrs <> TxtM10Hrs.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','OT Hrs 1.0 for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & Str10Hrs & "','" & TxtM10Hrs.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                StrSql = StrSql & "OTHrs15 = " & Math.Round(Val(TxtM15Hrs.Text), 0) & ","
                If Str15Hrs <> TxtM15Hrs.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','OT Hrs 1.5 for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & Str15Hrs & "','" & TxtM15Hrs.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                StrSql = StrSql & "OTHrs20 = " & Math.Round(Val(TxtM20Hrs.Text), 0) & ","
                If Str20Hrs <> TxtM20Hrs.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','OT Hrs 2.0 " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & Str20Hrs & "','" & TxtM20Hrs.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                StrSql = StrSql & "OTHRs30 = " & Math.Round(Val(TxtM30Hrs.Text), 0) & ","
                If Str30Hrs <> TxtM30Hrs.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','OT Hrs 3.0 for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & Str30Hrs & "','" & TxtM30Hrs.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                'Amount OT
                StrSql = StrSql & "OT1Amt = " & Math.Round(Val(TxtM10Amount.Text), 0) & ","
                If Str1Amt <> TxtM10Amount.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','OT Amt 1.0 for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & Str1Amt & "','" & TxtM10Amount.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                StrSql = StrSql & "OT15Amt = " & Math.Round(Val(TxtM15Amount.Text), 0) & ","
                If Str15Amt <> TxtM15Amount.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','OT Amt 1.5 for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & Str15Amt & "','" & TxtM15Amount.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                StrSql = StrSql & "OT20Amt = " & Math.Round(Val(TxtM20Amount.Text), 0) & ","
                If str20Amt <> TxtM20Amount.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','OT Amt 2.0 for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & str20Amt & "','" & TxtM20Amount.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                StrSql = StrSql & "OT30Amt = " & Math.Round(Val(TxtM30Amount.Text), 0) & ","
                If Str30amt <> TxtM30Amount.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','OT Amt 3.0 for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & Str30amt & "','" & TxtM30Amount.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                '
                StrSql = StrSql & "Overtime = " & Math.Round(Val(TxtNSOvertime.Text), 0) & ","
                StrSql = StrSql & "DailyRate = " & Val(Session("SngDRate")) & ","
                StrSql = StrSql & "OtherAllow = " & Val(TxtNSOtherAllow.Text) & ","
                If StrOAllow <> TxtNSOtherAllow.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','Other Allowances for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & StrOAllow & "','" & TxtNSOtherAllow.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                StrSql = StrSql & "Arrears = " & Val(TxtNSArrears.Text) & ","
                If StrArrears <> TxtNSArrears.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','Arrears for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & StrArrears & "','" & TxtNSArrears.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                StrSql = StrSql & "Others = " & Val(TxtNSOtherDeduction.Text) & ","
                If StrODeduct <> TxtNSOtherDeduction.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','Other Deductions for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & StrODeduct & "','" & TxtNSOtherDeduction.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                StrSql = StrSql & "MTAllowAmt = " & Val(TxtMTAAmount.Text) & ","
                StrSql = StrSql & "MNTAllowAmt = " & Val(TxtMNTAAmount.Text) & ","
                StrSql = StrSql & "MTAllowDes = '" & TxtMTADesc.Text & "',"
                StrSql = StrSql & "MNTAllowDes = '" & TxtMNTADesc.Text & "',"
                StrSql = StrSql & "EmpBasic = " & Val(TxtNSEmpBasic.Text) & ","
                StrSql = StrSql & "RespAllow = " & Val(TxtNPNiteAllow.Text) & ","
                StrSql = StrSql & "MealAllow = " & Val(TxtNPMealAllow.Text) & ","
                StrSql = StrSql & "MaternityAllow = " & Val(TxtNSFixedAllow.Text) & ","
                StrSql = StrSql & "PerfAllow = " & Val(TxtNSPerfBonus.Text) & ","
                StrSql = StrSql & "PassageBenefit = " & Val(TxtNSPassageBenefit.Text) & ","
                StrSql = StrSql & "Short = " & Val(TxtNSShort.Text) & ","
                StrSql = StrSql & "CreditCard = " & Val(TxtNSCCardCharges.Text) & ","
                StrSql = StrSql & "ExemptPassageBenefit = " & Val(TxtNFTips.Text) & ","
                StrSql = StrSql & "SpecialBenefit = " & strSpecialBenefit & ","
                StrSql = StrSql & "OtherDeduct = " & strOthDeduction & ","

                '***Fringe***
                StrSql = StrSql & "CarBenefit = " & Val(TxtNFCarBenefit.Text) & ","
                If StrCarB <> TxtNFCarBenefit.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','Car Benefit for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & StrCarB & "','" & TxtNFCarBenefit.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                StrSql = StrSql & "HouseBenefit = " & Val(TxtNFHousingBenefit.Text) & ","
                If StrHouseB <> TxtNFHousingBenefit.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','House Benefit for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & StrHouseB & "','" & TxtNFHousingBenefit.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                StrSql = StrSql & "Accommodation = " & Val(TxtNFAccBenefit.Text) & ","
                If StrAccB <> TxtNFAccBenefit.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','Accomodation Benefit for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & StrAccB & "','" & TxtNFAccBenefit.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                StrSql = StrSql & "Interest = " & Val(TxtNFInterest.Text) & ","
                If StrIntB <> TxtNFInterest.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','Interest Benefit for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & StrIntB & "','" & TxtNFInterest.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                StrSql = StrSql & "WriteOff = " & Val(TxtNFWriteOff.Text) & ","
                If StrWoff <> TxtNFWriteOff.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','Write Off Benefit for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & StrWoff & "','" & TxtNFWriteOff.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                StrSql = StrSql & "Expenses = " & Val(TxtNFExpenses.Text) & ","
                If StrExpB <> TxtNFExpenses.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','Expenses Benefit for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & StrExpB & "','" & TxtNFExpenses.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                StrSql = StrSql & "TaxPaid = " & Val(TxtNFTaxPaid.Text) & ","
                If StrEOY <> TxtNFTaxPaid.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','Tax Paid Benefit for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & StrTPaidB & "','" & TxtNFTaxPaid.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                StrSql = StrSql & "TravelAllowance = " & Val(TxtNSTravel.Text) & ","
                If StrTravel <> TxtNSTravel.Text Then
                    CmdAudit.Connection = Con
                    CmdAudit.CommandText = "Insert Into SalMasterAudit Values('" & TxtEmpID.Text & "','Travel Allowance for " & CmbMonth.Text & " " & TxtYear.Text & " changes from ','" & StrTravel & "','" & TxtNSTravel.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdAudit.ExecuteNonQuery()
                End If
                StrSql = StrSql & "NSFEmployee = " & Val(TxtNSNsfEmp.Text) & " "
                StrSql = StrSql & "WHERE EmpID = '" & TxtEmpID.Text & "' "
                StrSql = StrSql & "AND [Date] = '" & Format(CDate(TxtDate.Text), "yyyy-MM-dd") & "'"

                Try
                    Cmd.Connection = Con
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()

                    'Process PRGF
                    strPRGF = modPRFG.calculationPRGF(CmbMonth.Text, TxtYear.Text, Con, Session("ConnString"), TxtEmpID.Text)
                    If (strPRGF <> "1") Then
                        LblAccess.Visible = True
                        LblAccess.Text = strPRGF
                        Exit Sub
                    End If

                    LblAccess.Visible = True
                    LblAccess.Text = "Data saved for this employee!!!"
                    'ClearAll()
                Catch ex As Exception
                    ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
                End Try
            End If
            If e.Item.CommandName = "Delete" Then
                Try
                    Cmd.Connection = Con
                    Cmd.CommandText = "DELETE FROM Salary WHERE EmpID = '" & TxtEmpID.Text & "' And [Month] = '" & CmbMonth.Text & "' And [Year] = '" & TxtYear.Text & "'"
                    Cmd.ExecuteNonQuery()
                    CmdAudit.Connection = Con
                    StrAudit = "Salary Record Deleted for the month " & CmbMonth.Text & " " & TxtYear.Text & " for EmpID " & TxtEmpID.Text & " "
                    CmdAudit.CommandText = "Insert Into SalMasterAudit values('" & Session("UserID") & "','Salary Record Deleted','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                    CmdAudit.ExecuteNonQuery()
                    TxtEmpID_TextChanged(Me, ev)
                Catch ex As Exception
                    ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
                End Try
            End If
            ''''
        End If
        RdrLock.Close()
        If e.Item.CommandName = "Next" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID > '" & TxtEmpID.Text & "'"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try

        End If
        If e.Item.CommandName = "Previous" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID < '" & TxtEmpID.Text & "' ORDER BY EmpID DESC"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try
        End If
        If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtEmpID.Text)
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
        End If
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged

        If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
            StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Else
            StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "' And OutletName = '" & Session("Level") & "'"
        End If

        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        If Rdr.Read Then
            On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
            TxtTitle.Text = Rdr("Title")
            TxtCategory.Text = Rdr("Category")
            TxtGender.Text = Rdr("Gender")
            TxtCategory.Text = Rdr("Category")
            ChkClocked.Checked = IIf(Rdr("Clocked"), True, False)
            TxtClockID.Text = Rdr("BatchNo")
            Session("HousePercent") = Rdr("HousePercent")
            Session("TravelMode") = Rdr("TravelMode")
            Session("TotEdf") = Rdr("TotEdf")
            Session("EDFFactor") = Rdr("EDFFactor")
            Session("Expat") = Rdr("Expat")
            Session.Add("EmpID", Rdr("EmpID"))
            Session("PAYEScheme") = Rdr("PAYEScheme")
            Session("DOB") = Format(Rdr("DOB"), "yyyy-MM-dd")
            Session("NpsPaid") = IIf(Rdr("NPSPaid"), "True", "False")
            Session("SngDRate") = Math.Round(Rdr("BasicSalary") / 26, 2)
            Session("EmpBasic") = Rdr("BasicSalary")
            ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & Rdr("EmpID")

        Else
            Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(1))
            ToolBar1_ItemClick(Me, ev)
            'ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('Invalid Employee ID','Error');</script>", False)
        End If
        Rdr.Close()

        If TxtEmpID.Text <> "" Then GetSalDetails()
    End Sub

    Private Sub ClearAll()
        TxtFirst.Text = ""
        TxtLast.Text = ""
        TxtCategory.Text = ""
        TxtClockID.Text = ""
        TxtGender.Text = ""
        TxtTitle.Text = ""
        ChkClocked.Checked = False
        ImgPhoto.ImageUrl = Nothing
        'Modifiyable Fields
        TxtM10Amount.Text = ""
        TxtM10Hrs.Text = ""
        TxtM15Amount.Text = ""
        TxtM15Hrs.Text = ""
        TxtM20Amount.Text = ""
        TxtM20Hrs.Text = ""
        TxtM30Amount.Text = ""
        TxtM30Hrs.Text = ""
        TxtMAccBenefit.Text = ""
        TxtMArrears.Text = ""
        TxtMSPassageBenefit.Text = ""
        'TxtMAttBonus.Text = ""
        TxtMCarBenefit.Text = ""
        TxtMExpenses.Text = ""
        TxtMHouseBenefit.Text = ""
        TxtMInterest.Text = ""
        TxtMLRefund.Text = ""
        TxtMNTAAmount.Text = ""
        TxtMNTADesc.Text = ""
        TxtMOtherAllow.Text = ""
        TxtMOtherDeduction.Text = ""
        TxtOthDeduction.Text = ""
        TxtNiteAllow.Text = ""
        TxtMSRefund.Text = ""
        TxtMTAAmount.Text = ""
        TxtMTADesc.Text = ""
        TxtMTaxPaid.Text = ""
        TxtDCrCard.Text = ""
        TxtDShort.Text = ""

        TxtMTips.Text = ""
        TxtM15Hrs.Text = ""
        TxtMWriteOff.Text = ""
        TxtMEOYBonus.Text = ""
        'Non Modify Salary Payable Fields
        TxtNSArrears.Text = ""
        TxtNSAttendBonus.Text = ""
        TxtNSBasicDeduction.Text = ""
        TxtNSBasicSalary.Text = ""
        TxtNSDeduction.Text = ""
        TxtNSEmpBasic.Text = ""
        TxtNSEOYBonus.Text = ""
        TxtNSFixedAllow.Text = ""
        TxtNSLeaveRefund.Text = ""
        TxtNSLoanRepay.Text = ""
        TxtNSLoanRepay1.Text = ""
        TxtNSPassageBenefit.Text = ""
        TxtNSShort.Text = ""
        TxtNSNetPayable.Text = ""
        TxtNSNpfEmp.Text = ""
        TxtNSNsfEmp.Text = ""
        TxtNSOtherAllow.Text = ""
        TxtNSOtherDeduction.Text = ""
        TxtNSOvertime.Text = ""
        TxtNSPayable.Text = ""
        TxtNSPaye.Text = ""
        TxtNSPerfBonus.Text = ""
        TxtNSTravel.Text = ""
        TxtNSCCardCharges.Text = ""
        TxtNSNiteAllow.Text = ""
        TxtNSMealDeduct.Text = ""
        TxtNSPassageBenefit.Text = ""
        TxtSpecialBenefit.Text = ""

        'Paye Calculations Fields
        TxtNPArrears.Text = ""
        TxtNPAttBonus.Text = ""
        TxtNPBasicSalary.Text = ""
        TxtNPCurrentGross.Text = ""
        TxtNPCurrentPaye.Text = ""
        TxtNPEOYBonus.Text = ""
        TxtNPFixedAllow.Text = ""
        TxtNPFringeBenefit.Text = ""
        TxtNPIet.Text = ""
        TxtNPLeaveRefund.Text = ""
        TxtNPMealAllow.Text = ""
        TxtOthDeduction.Text = ""
        TxtNPNetCharge.Text = ""
        TxtNPOtherAllow.Text = ""
        TxtNPOvertime.Text = ""
        TxtNPPaye.Text = ""
        TxtNPPerfBonus.Text = ""
        TxtNPPrevGross.Text = ""
        TxtNPPrevPaye.Text = ""
        TxtNPTransport.Text = ""
        TxtNPNiteAllow.Text = ""
        TxtNPFringeBenefit.Text = ""
        'Employers Contribution Fields
        TxtNEIvtb.Text = ""
        TxtNENpf.Text = ""
        TxtNENsf.Text = ""
        TxtEPZER.Text = ""
        TxtEPZEE.Text = ""
        TxtNEMedicalEmployer.Text = ""
        TxtNEPensionEmployer.Text = ""

        'Fringe Benefit Fields
        TxtNFAccBenefit.Text = ""
        TxtNFCarBenefit.Text = ""
        TxtNFExpenses.Text = ""
        TxtNFHousingBenefit.Text = ""
        TxtNFInterest.Text = ""
        TxtNFTaxPaid.Text = ""

        TxtNFTotalFringe.Text = ""
        TxtNFWriteOff.Text = ""
        TxtMTravel.Text = ""
        'Leave grid
        GdvLeave.DataSource = Nothing
        GdvLeave.DataBind()
        'Loan Repayment Grid
        GdvLoan.DataSource = Nothing
        GdvLoan.DataBind()
    End Sub

    Private Sub GetSalDetails()
        LblAccess.Visible = False
        LblAccess.Text = ""

        ' ''If Session("Level") = "Administrator" Then
        ' ''    StrSql = "SELECT * FROM Salary WHERE EmpID = '" & TxtEmpID.Text & "' "
        ' ''    StrSql = StrSql & "AND [Date] = '" & Format(CDate(TxtDate.Text), "yyyy-MM-dd") & "'"
        ' ''ElseIf Session("Level") = "Beau Bassin" Then
        ' ''    StrSql = "SELECT * FROM Salary WHERE EmpID = '" & TxtEmpID.Text & "' "
        ' ''    StrSql = StrSql & "AND OutletName = 'Beau Bassin' AND [Date] = '" & Format(CDate(TxtDate.Text), "yyyy-MM-dd") & "'"
        ' ''ElseIf Session("Level") = "Ebene" Then
        ' ''    StrSql = "SELECT * FROM Salary WHERE EmpID = '" & TxtEmpID.Text & "' "
        ' ''    StrSql = StrSql & "AND OutletName = 'Ebene' AND [Date] = '" & Format(CDate(TxtDate.Text), "yyyy-MM-dd") & "'"
        ' ''Else
        ' ''    StrSql = "SELECT * FROM Salary WHERE EmpID = '" & TxtEmpID.Text & "' "
        ' ''    StrSql = StrSql & "AND OutletName = 'Grand Baie' AND [Date] = '" & Format(CDate(TxtDate.Text), "yyyy-MM-dd") & "'"
        ' ''End If

        If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
            StrSql = "SELECT * FROM Salary WHERE EmpID = '" & TxtEmpID.Text & "' "
            StrSql = StrSql & "AND [Date] = '" & Format(CDate(TxtDate.Text), "yyyy-MM-dd") & "'"
        Else
            StrSql = "SELECT * FROM Salary WHERE EmpID = '" & TxtEmpID.Text & "' "
            StrSql = StrSql & "AND OutletName = '" & Session("Level") & "' AND [Date] = '" & Format(CDate(TxtDate.Text), "yyyy-MM-dd") & "'"
        End If


        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            Session("SngHrRate") = Rdr("HourlyRate").ToString
            'Session("SngDRate") = Math.Round(Rdr("EmpBasic") / 22, 2) 'Rdr("DailyRate").ToString
            'Employers Contributions
            TxtNENpf.Text = Rdr("NPFEmployer").ToString
            TxtNEIvtb.Text = Rdr("IVTB").ToString
            TxtNENsf.Text = Rdr("NSF").ToString
            TxtEPZER.Text = Rdr("EPZER").ToString
            TxtEPZEE.Text = Rdr("EPZEE").ToString
            TxtNEMedicalEmployer.Text = Rdr("MedicalContribEr").ToString
            'Salary Payable
            TxtNSArrears.Text = Rdr("Arrears").ToString
            TxtNSBasicDeduction.Text = Rdr("YOSAmt").ToString
            TxtNSEmpBasic.Text = Session("EmpBasic") 'Rdr("EmpBasic").ToString
            TxtNSBasicSalary.Text = Rdr("BasicSalary").ToString
            TxtNPBasicSalary.Text = Rdr("BasicSalary").ToString
            TxtNSDeduction.Text = "0"
            TxtNSEOYBonus.Text = Rdr("IfsBonus").ToString
            TxtNPEOYBonus.Text = Rdr("IfsBonus").ToString
            TxtNSPerfBonus.Text = Rdr("PerfAllow").ToString
            TxtNPPerfBonus.Text = Rdr("PerfAllow").ToString

            TxtNSPaye.Text = Rdr("PAYE").ToString
            TxtNPPaye.Text = Rdr("PAYE").ToString

            TxtNSFixedAllow.Text = Rdr("MaternityAllow").ToString
            TxtNPFixedAllow.Text = Rdr("MaternityAllow").ToString
            TxtNSLeaveRefund.Text = "0"
            TxtNSLoanRepay.Text = Rdr("LoanRepay").ToString
            TxtNSLoanRepay1.Text = Rdr("LoanRepay1").ToString

            'TxtMSPassageBenefit.Text = 0
            'TxtNSPassageBenefit.Text = 0
            TxtMSPassageBenefit.Text = Rdr("PassageBenefit").ToString
            TxtNSPassageBenefit.Text = Rdr("PassageBenefit").ToString
            'If (Val(Rdr("PassageBenefit")) - Val(Rdr("ExemptPassageBenefit"))) > 0 Then
            '    TxtNPPassageBenefit.Text = (Val(Rdr("PassageBenefit")) - Val(Rdr("ExemptPassageBenefit")))
            'Else
            '    TxtNPFringeBenefit.Text = 0
            'End If
            TxtNSShort.Text = Rdr("Short").ToString
            TxtDShort.Text = Rdr("Short").ToString
            TxtNSCCardCharges.Text = Rdr("CreditCard").ToString
            TxtDCrCard.Text = Rdr("CreditCard").ToString
            TxtNSNetPayable.Text = "0"
            TxtNSNpfEmp.Text = Rdr("NPFEmployee").ToString
            TxtNSNsfEmp.Text = Rdr("NSFEmployee").ToString
            TxtNSOtherAllow.Text = Rdr("OtherAllow").ToString
            TxtNSOtherDeduction.Text = Rdr("Others").ToString
            TxtNSOvertime.Text = Rdr("OverTime").ToString
            TxtNSPayable.Text = "0"
            TxtNSTravel.Text = "0"
            TxtMTravel.Text = Rdr("TravelAllowance").ToString
            TxtNSTravel.Text = Rdr("TravelAllowance").ToString
            TxtNSAttendBonus.Text = Rdr("PensionEmp").ToString
            TxtNSMealDeduct.Text = Rdr("MealDed").ToString
            'Editable Values
            TxtMArrears.Text = Rdr("Arrears").ToString
            TxtM15Hrs.Text = Rdr("TravelAllowance").ToString
            TxtMOtherAllow.Text = Rdr("OtherAllow").ToString
            TxtMLRefund.Text = Rdr("LocalRefund").ToString
            TxtMSRefund.Text = Rdr("SickRefund").ToString
            TxtNPEOYBonus.Text = Rdr("IfsBonus").ToString
            TxtMEOYBonus.Text = Rdr("IfsBonus").ToString
            TxtNSAttendBonus.Text = Rdr("PensionEmp").ToString
            TxtNPAttBonus.Text = Rdr("PensionEmp").ToString
            TxtMOtherDeduction.Text = Rdr("Others").ToString
            TxtOthDeduction.Text = Rdr("OtherDeduct").ToString

            TxtNiteAllow.Text = Rdr("RespAllow").ToString
            TxtNSNiteAllow.Text = Rdr("RespAllow").ToString

            TxtNSMealAllow.Text = Rdr("MealAllow").ToString
            TxtNPMealAllow.Text = Rdr("MealAllow").ToString

            TxtM10Hrs.Text = Rdr("OTHrs1").ToString
            TxtM15Hrs.Text = Rdr("OTHrs15").ToString
            TxtM20Hrs.Text = Rdr("OTHrs20").ToString
            TxtM30Hrs.Text = Rdr("OTHrs30").ToString

            TxtMTAAmount.Text = Rdr("MTAllowAmt").ToString
            TxtMNTAAmount.Text = Rdr("MNTAllowAmt").ToString
            TxtMTADesc.Text = Rdr("MTAllowDes").ToString
            TxtMNTADesc.Text = Rdr("MNTAllowDes").ToString

            'fringe benefit
            TxtMCarBenefit.Text = Val(Rdr("CarBenefit").ToString) - Val(Rdr("CarEmp").ToString)
            TxtMHouseBenefit.Text = Rdr("HouseBenefit").ToString
            TxtMAccBenefit.Text = Rdr("Accommodation").ToString
            TxtMInterest.Text = Rdr("Interest").ToString
            TxtMTips.Text = Rdr("Tips").ToString
            TxtMWriteOff.Text = Rdr("WriteOff").ToString
            TxtMExpenses.Text = Rdr("Expenses").ToString
            TxtMTaxPaid.Text = Rdr("TaxPaid").ToString

            TxtNFCarBenefit.Text = Val(Rdr("CarBenefit").ToString) - Val(Rdr("CarEmp").ToString)
            TxtNFHousingBenefit.Text = Rdr("HouseBenefit").ToString
            TxtNFAccBenefit.Text = Rdr("Accommodation").ToString
            TxtNFInterest.Text = Rdr("Interest").ToString
            TxtNFTips.Text = Rdr("ExemptPassageBenefit").ToString
            TxtNFWriteOff.Text = Rdr("WriteOff").ToString
            TxtNFExpenses.Text = Rdr("Expenses").ToString
            TxtNFTaxPaid.Text = Rdr("TaxPaid").ToString
            TxtEmpFringe.Text = Rdr("FringeBenefits").ToString
            TxtSpecialBenefit.Text = Rdr("SpecialBenefit").ToString

            'Audit
            Dim ModSalDetail As New HttpCookie("ModSalDetail")
            ModSalDetail.Values("LastTime") = Date.Now.ToShortTimeString()
            ModSalDetail.Values("Arrears") = Rdr("Arrears").ToString
            ModSalDetail.Values("TravelAllowance") = Rdr("TravelAllowance").ToString
            ModSalDetail.Values("OtherAllow") = Rdr("OtherAllow").ToString
            ModSalDetail.Values("LocalRefund") = Rdr("LocalRefund").ToString
            ModSalDetail.Values("SickRefund") = Rdr("SickRefund").ToString
            ModSalDetail.Values("PensionEmp") = Rdr("PensionEmp").ToString
            ModSalDetail.Values("IfsBonus") = Rdr("IfsBonus").ToString
            ModSalDetail.Values("FringeBenefits") = Rdr("FringeBenefits").ToString
            ModSalDetail.Values("Others") = Rdr("Others").ToString
            ModSalDetail.Values("OTHrs1") = Rdr("OTHrs1").ToString
            ModSalDetail.Values("OTHrs15") = Rdr("OTHrs15").ToString
            ModSalDetail.Values("OTHrs20") = Rdr("OTHrs20").ToString
            ModSalDetail.Values("OTHrs30") = Rdr("OTHrs30").ToString
            'Fringe
            ModSalDetail.Values("CarBenefit") = Rdr("CarBenefit").ToString
            ModSalDetail.Values("HouseBenefit") = Rdr("HouseBenefit").ToString
            ModSalDetail.Values("Accommodation") = Rdr("Accommodation").ToString
            ModSalDetail.Values("Interest") = Rdr("Interest").ToString
            ModSalDetail.Values("Tips") = Rdr("Tips").ToString
            ModSalDetail.Values("WriteOff") = Rdr("WriteOff").ToString
            ModSalDetail.Values("Expenses") = Rdr("Expenses").ToString
            ModSalDetail.Values("TaxPaid") = Rdr("TaxPaid").ToString
            ModSalDetail.Expires = Date.Now.AddDays(1)
            Response.Cookies.Add(ModSalDetail)

            LblAccess.Text = ""
        Else
            LblAccess.Visible = True
            LblAccess.Text = "No Salary Record for this employee in File..."
        End If
        Rdr.Close()

        If CmbMonth.Text <> "Bonus" Then
            Dim CmdTime As New OleDb.OleDbCommand
            Dim RdrTime As OleDb.OleDbDataReader
            Dim TimexCon As New OleDb.OleDbConnection
            TimexCon.ConnectionString = Session("ConnString") ' Database of desktop Payroll TNT
            TimexCon.Open()

            Dim DteFirst As Date
            Dim DteEnd As Date
            DteFirst = CDate("19," & Format(CmbMonth.SelectedIndex + 1, "00") & "," & TxtYear.Text)
            DteEnd = DateAdd("m", -1, DteFirst)

            DteEnd = CDate("20/" & Format(CmbMonth.SelectedIndex + 1, "00") & "/" & TxtYear.Text)
            DteFirst = DteFirst.AddMonths(-1)

            CmdTime.Connection = TimexCon
            StrSql = "SELECT * FROM Leave "
            StrSql = StrSql & "Where Date >= '" & DteFirst.ToString("yyyy-MM-dd") & "' "
            StrSql = StrSql & "AND Date <= '" & DteEnd.ToString("yyyy-MM-dd") & "' "
            StrSql = StrSql & "AND EmpID = '" & TxtEmpID.Text & "'  ORDER BY [Date]"
            CmdTime.CommandText = StrSql
            RdrTime = CmdTime.ExecuteReader
            GdvLeave.DataSource = RdrTime
            GdvLeave.DataBind()
            RdrTime.Close()
        End If

        'calling functions
        CalcLRefund()
        CalcOT()
        CalcTotFringe()
        CalcTotPayable()
        CalcTotDeduction()
        CalcNetPayable()
        CalcPAYE()

        Dim e As New System.EventArgs
        TxtMSPassageBenefit_TextChanged(Me, e)
        TxtMArrears_TextChanged(Me, e)
        TxtMTravel_TextChanged(Me, e)
        TxtMOtherAllow_TextChanged(Me, e)
        TxtMLRefund_TextChanged(Me, e)
        TxtMSRefund_TextChanged(Me, e)
        TxtNiteAllow_TextChanged(Me, e)
    End Sub

    Protected Sub TxtYear_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtYear.TextChanged
        'CmbCategory_SelectedIndexChanged(Me, e)
        CmbMonth_SelectedIndexChanged(Me, e)
    End Sub

    Private Sub CmbMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbMonth.SelectedIndexChanged
        If CmbMonth.Text = "Bonus" Then
            TxtDate.Text = CDate("20-December" & "-" & TxtYear.Text)
            If TxtEmpID.Text <> "" Then
                TxtEmpID_TextChanged(Me, e)
            End If
        Else
            TxtDate.Text = CDate("28-" & CmbMonth.Text & "-" & TxtYear.Text)
            If TxtEmpID.Text <> "" Then
                TxtEmpID_TextChanged(Me, e)
            End If
        End If
    End Sub

    Private Sub CalcOT()
        Dim SngOT10 As Single = Math.Round(Val(TxtM10Hrs.Text) * 1 * Session("SngHrRate"), 0)
        Dim SngOT15 As Single = Math.Round(Val(TxtM15Hrs.Text) * 1.5 * Session("SngHrRate"), 0)
        Dim SngOT20 As Single = Math.Round(Val(TxtM20Hrs.Text) * 2 * Session("SngHrRate"), 0)
        Dim SngOT30 As Single = Math.Round(Val(TxtM30Hrs.Text) * 3 * Session("SngHrRate"), 0)
        TxtM10Amount.Text = SngOT10
        TxtM15Amount.Text = SngOT15
        TxtM20Amount.Text = SngOT20
        TxtM30Amount.Text = SngOT30
        TxtNSOvertime.Text = SngOT10 + SngOT15 + SngOT20 + SngOT30
        TxtNPOvertime.Text = SngOT10 + SngOT15 + SngOT20 + SngOT30
        CalcPAYE()
    End Sub

    Private Sub CalcLRefund()
        'TxtNSLeaveRefund.Text = Math.Round(CSng((Val(TxtMSRefund.Text) + Val(TxtMLRefund.Text)) * (Session("SngDRate") * 1)), 0)
        TxtNSLeaveRefund.Text = Math.Round((Math.Round(Val(TxtMSRefund.Text), 2) + Val(TxtMLRefund.Text)) * (Session("SngDRate")), 2)
        TxtNPLeaveRefund.Text = TxtNSLeaveRefund.Text
        CalcPAYE()
    End Sub

    Private Sub CalcTotFringe()
        TxtNFTotalFringe.Text = CLng(Val(TxtNFCarBenefit.Text) + Val(TxtEmpFringe.Text) + Val(TxtNFHousingBenefit.Text) + Val(TxtNFAccBenefit.Text) + Val(TxtNFInterest.Text) + Val(TxtNFWriteOff.Text) + Val(TxtNFExpenses.Text) + Val(TxtNFTaxPaid.Text))
        TxtNPFringeBenefit.Text = TxtNFTotalFringe.Text
        CalcPAYE()
    End Sub

    Private Sub CalcTotPayable()
        TxtNSPayable.Text = Val(TxtNSBasicSalary.Text) + Val(TxtNSOvertime.Text) + Val(TxtNSOtherAllow.Text)
        TxtNSPayable.Text = Val(TxtNSPayable.Text) + Val(TxtNSTravel.Text) + Val(TxtNSArrears.Text) + Val(TxtNSEOYBonus.Text)
        TxtNSPayable.Text = Val(TxtNSPayable.Text) + Val(TxtNSLeaveRefund.Text) + Val(TxtNSAttendBonus.Text) + Val(TxtNSPassageBenefit.Text) + Val(TxtSpecialBenefit.Text)
        TxtNSPayable.Text = Val(TxtNSPayable.Text) + Val(TxtNSPerfBonus.Text) + Val(TxtNSFixedAllow.Text) + Val(TxtNSNiteAllow.Text)
        CalcPAYE()
    End Sub

    Private Sub CalcTotDeduction()
        TxtNSDeduction.Text = Val(TxtNSLoanRepay1.Text) + Val(TxtNSLoanRepay.Text) + Val(TxtNSPaye.Text) + Val(TxtNSNpfEmp.Text) + Val(TxtEPZEE.Text)
        TxtNSDeduction.Text = Val(TxtNSDeduction.Text) + Val(TxtNSOtherDeduction.Text) + Val(TxtNSShort.Text) + Val(TxtNSCCardCharges.Text)
        TxtNSDeduction.Text = Val(TxtNSDeduction.Text) + Val(TxtNSMealDeduct.Text) + Val(TxtNSNsfEmp.Text) + Val(TxtOthDeduction.Text)
        CalcPAYE()
    End Sub

    Private Sub CalcNetPayable()
        TxtNSNetPayable.Text = Val(TxtNSPayable.Text) - Val(TxtNSDeduction.Text)
        CalcPAYE()
    End Sub

    Private Sub CalcHouseBenefit()
        Dim LngTotEmol As Single
        Dim LngHousePercent As Long
        LngHousePercent = Val(Session("HousePercent") & "")
        LngTotEmol = 0
        LngTotEmol = Val(TxtNSBasicSalary.Text) + Val(TxtNSOvertime.Text) + Val(TxtNSOtherAllow.Text)
        LngTotEmol = LngTotEmol + Val(TxtNSArrears.Text) + Val(TxtNSAttendBonus.Text)
        LngTotEmol = LngTotEmol + Val(TxtNSPerfBonus.Text) + Val(TxtNSFixedAllow.Text)
        LngTotEmol = LngTotEmol + Val(TxtNFCarBenefit.Text) + Val(TxtNFAccBenefit.Text) + Val(TxtNFInterest.Text)
        LngTotEmol = LngTotEmol + Val(TxtNFWriteOff.Text) + Val(TxtNFExpenses.Text) + Val(TxtNFTaxPaid.Text)
        TxtMHouseBenefit.Text = Math.Round((LngHousePercent * LngTotEmol) / 100, 0)
        TxtNFHousingBenefit.Text = TxtMHouseBenefit.Text
        CalcPAYE()
    End Sub

    Private Sub CalcPAYE()
        Dim LngNetCh As Long
        Dim LngPaye As Long = 0
        Dim LngGrosCh As Long
        ' NEW BUDGET
        Dim LngPrevGrosCh As Long
        Dim LngGrosChEOY As Long
        Dim LngGrosEmol As Long
        Dim IntEDFMonth As Integer
        Dim LngPrevPAYE As Long
        Dim SngSickRefund As Single
        Dim SngAnualRefund As Single
        Dim DteSalDateFrm As Date
        Dim DteSalDateTo As Date
        Dim LngEdf As Long
        Dim LngTaxBenefit As Long = 0
        Dim LngHousingBen As Long = 0
        Dim LngChargeableExpat As Long = 0
        Dim LngTaxEpat As Long = 0
        ' START OF CUMULATIVE PAYE SYSTEM
        LngGrosCh = 0
        LngPrevGrosCh = 0
        LngGrosChEOY = 0
        LngGrosEmol = 0
        IntEDFMonth = 1
        LngPrevPAYE = 0
        Dim DteSalDate As Date = CDate(TxtDate.Text)
        If DteSalDate < CDate("31/12/2009") Or DteSalDate >= CDate("28/07/2015") Then
            If Month(DteSalDate) >= 7 Then
                DteSalDateFrm = CDate("28/07" + "/" + TxtYear.Text)
                DteSalDateTo = DteSalDate
            Else
                DteSalDateFrm = CDate("28/07" + "/" + CStr((Val(TxtYear.Text) - 1)))
                DteSalDateTo = DteSalDate
            End If
        Else
            DteSalDateFrm = CDate("28/01" + "/" + TxtYear.Text)
            DteSalDateTo = DteSalDate
        End If
        ' I)CALC. GROSS CHARGE FOR CURRENT MONTH
        LngGrosCh = Val(TxtNPBasicSalary.Text)
        LngGrosCh = LngGrosCh + Val(TxtNPOvertime.Text)
        LngGrosCh = LngGrosCh + Val(TxtNPOtherAllow.Text)
        LngGrosCh = LngGrosCh + Val(TxtNPTransport.Text)
        LngGrosCh = LngGrosCh + Val(TxtNPArrears.Text)
        LngGrosCh = LngGrosCh + Val(TxtNPEOYBonus.Text)
        LngGrosCh = LngGrosCh + Val(TxtNPLeaveRefund.Text)
        'LngGrosCh = LngGrosCh + Val(TxtNPMealAllow.Text)
        LngGrosCh = LngGrosCh + Val(TxtNPPerfBonus.Text) ' pref allow
        LngGrosCh = LngGrosCh + Val(TxtNPAttBonus.Text)     'pension emp
        LngGrosCh = LngGrosCh + Val(TxtNPFringeBenefit.Text)
        LngGrosCh = LngGrosCh + Val(TxtNPFixedAllow.Text)   'meternity allow
        LngGrosCh = LngGrosCh + Val(TxtNPNiteAllow.Text)    'resp allow
        LngGrosCh = LngGrosCh + Val(TxtNPMealAllow.Text)
        LngGrosCh = LngGrosCh + Val(TxtNPPassageBenefit.Text)
        LngGrosCh = LngGrosCh + Val(TxtSpecialBenefit.Text)
        TxtNPCurrentGross.Text = LngGrosCh
        ' II)CALC. GROSS CHARGE AND PAYE FOR PREVIOUS MONTHS FROM JULY
        StrSql = "SELECT * FROM Salary WHERE EmpID = '" & TxtEmpID.Text & "' "
        StrSql = StrSql & "And [Date] >='" & Format(DteSalDateFrm, "yyyy-MM-dd") & "' And [Date] < '"
        StrSql = StrSql & Format(DteSalDateTo, "yyyy-MM-dd") & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        While Rdr.Read
            IntEDFMonth = IntEDFMonth + 1
            LngPrevGrosCh = LngPrevGrosCh + Rdr("BasicSalary")
            LngPrevGrosCh = LngPrevGrosCh + Rdr("Overtime")
            LngPrevGrosCh = LngPrevGrosCh + Rdr("OtherAllow")
            LngPrevGrosCh = LngPrevGrosCh + Rdr("Arrears")
            LngPrevGrosCh = LngPrevGrosCh + Rdr("IFSBonus")
            'LngPrevGrosCh = LngPrevGrosCh + Rdr("MealAllow")
            LngPrevGrosCh = LngPrevGrosCh + Rdr("PensionEmp")
            LngPrevGrosCh = LngPrevGrosCh + Rdr("RespAllow")
            LngPrevGrosCh = LngPrevGrosCh + Rdr("MealAllow")
            LngPrevGrosCh = LngPrevGrosCh + Rdr("MaternityAllow")
            LngPrevGrosCh = LngPrevGrosCh + Rdr("FringeBenefits")
            LngPrevGrosCh = LngPrevGrosCh + Rdr("PerfAllow")
            LngPrevGrosCh = LngPrevGrosCh + Rdr("SpecialBenefit")
            LngPrevGrosCh = LngPrevGrosCh + Val(Rdr("CarBenefit").ToString) - Val(Rdr("CarEmp").ToString) + Val(Rdr("Interest").ToString) + Val(Rdr("Tips").ToString) + Val(Rdr("WriteOff").ToString)
            LngPrevGrosCh = LngPrevGrosCh + Val(Rdr("Expenses").ToString) + Val(Rdr("Accommodation").ToString) + Val(Rdr("TaxPaid").ToString)
            LngPrevGrosCh = LngPrevGrosCh + Val(Rdr("HouseBenefit").ToString) - Val(Rdr("HouseEmp").ToString)
            SngSickRefund = 0
            SngAnualRefund = 0
            SngSickRefund = Math.Round(Val(Rdr("SickRefund").ToString) * Rdr("DailyRate"), 2)
            SngAnualRefund = Math.Round(Val(Rdr("LocalRefund").ToString) * Rdr("DailyRate"), 2)
            LngPrevGrosCh = LngPrevGrosCh + SngAnualRefund
            'Add excess transport allowance if any to the gross chargeable
            If Rdr("TravelAllowance") > Rdr("TravelExempt") Then
                'Add difference to gross chargeable
                LngPrevGrosCh = LngPrevGrosCh + (Rdr("TravelAllowance") - Rdr("TravelExempt"))
            End If
            If Rdr("PassageBenefit") > Rdr("ExemptPassageBenefit") Then
                'Add difference to gross chargeable
                LngPrevGrosCh = LngPrevGrosCh + (Rdr("PassageBenefit") - Rdr("ExemptPassageBenefit"))
            End If
            'If Session("Expat") Then
            '    LngPrevPAYE = LngPrevPAYE + Val(Rdr("TaxBenefit").ToString)
            'Else
            LngPrevPAYE = LngPrevPAYE + Val(Rdr("Paye").ToString)
            'End If

        End While
        Rdr.Close()
        ' III)ADD I & II
        If CmbMonth.Text = "Bonus" And Val(TxtYear.Text) = 2011 Then
            IntEDFMonth = 1
            LngPrevGrosCh = 0
            LngPrevPAYE = 0
        End If
        LngGrosEmol = LngGrosCh + LngPrevGrosCh
        TxtNPPrevGross.Text = LngPrevGrosCh
        ' IV) Deduct EDF
        LngEdf = 0
        LngEdf = Val(Session("TotEdf")) * (IntEDFMonth / Val(Session("EDFFactor")))
        TxtNPIet.Text = LngEdf
        ' V) Calculate netchargeable for tax
        LngNetCh = LngGrosEmol - LngEdf
        TxtNPNetCharge.Text = LngNetCh

        Dim SngCummEmolument As Single = 0
        Dim SngIETNO As Single = 0
        Dim SngCheckCumm As Single = 0
        SngIETNO = IntEDFMonth
        SngCummEmolument = LngGrosEmol
        SngCheckCumm = CLng(SngCummEmolument / SngIETNO)

        ' VI) Apply PAYE
        Dim IntTaxExempt As Integer = 0
        Dim IntMinPaye As Integer = 0
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT TaxExempt,MinPaye FROM [System]"
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            IntTaxExempt = Rdr("TaxExempt")
            IntMinPaye = Rdr("MinPaye")
        End If
        Rdr.Close()
        StrSql = "SELECT * FROM PAYE WHERE PAYEScheme='" & Session("PAYEScheme") & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            LblMsg.Text = ""
            If LngGrosCh > IntTaxExempt Then
                If SngCheckCumm >= Rdr("From-1") And SngCheckCumm <= Rdr("To-1") Then
                    LngPaye = LngPaye + CLng(LngNetCh * Rdr("TaxPercent-1") / 100)
                ElseIf SngCheckCumm >= Rdr("From-2") Then
                    LngPaye = LngPaye + CLng(LngNetCh * Rdr("TaxPercent-2") / 100)
                End If
            End If
        Else
            LblMsg.Text = "PAYE Scheme for " & TxtLast.Text & " not set"
        End If
        Rdr.Close()
        'VII) Calculate actual PAYE i.e Current - Previous
        ''If Session("Expat") Then
        ''    LngChargeableExpat = LngNetCh + LngTaxBenefit + LngHousingBen
        ''    TxtNPNetCharge.Text = LngChargeableExpat
        ''    LngTaxEpat = CLng((LngChargeableExpat * 0.15))
        ''    TxtNPPaye.Text = LngTaxEpat
        ''    TxtNPCurrentPaye.Text = LngTaxEpat
        ''    LngPaye = LngTaxEpat - LngPrevPAYE
        ''    TxtNPPrevPaye.Text = LngPrevPAYE
        ''    'END OF CUMULATIVE PAYE SYSTEM
        ''    TxtNPPaye.Text = IIf(LngPaye < IntMinPaye, 0, LngPaye)
        ''Else
        TxtNPCurrentPaye.Text = LngPaye
        LngPaye = LngPaye - LngPrevPAYE
        TxtNPPrevPaye.Text = LngPrevPAYE
        'END OF CUMULATIVE PAYE SYSTEM
        TxtNPPaye.Text = IIf(LngPaye < IntMinPaye, 0, LngPaye)
        TxtNSPaye.Text = IIf(LngPaye < IntMinPaye, 0, LngPaye)
        ''End If

    End Sub

    Protected Sub TxtMCarBenefit_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtMCarBenefit.TextChanged
        TxtNFCarBenefit.Text = Val(TxtMCarBenefit.Text)
        CalcTotFringe()
        CalcTotDeduction()
        CalcPAYE()
    End Sub

    Protected Sub TxtMHouseBenefit_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtMHouseBenefit.TextChanged
        TxtNFHousingBenefit.Text = Val(TxtMHouseBenefit.Text)
        CalcTotFringe()
        CalcTotDeduction()
        CalcPAYE()
    End Sub

    Protected Sub TxtMAccBenefit_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtMAccBenefit.TextChanged
        TxtNFAccBenefit.Text = Val(TxtMAccBenefit.Text)
        CalcTotFringe()
        CalcTotDeduction()
        CalcPAYE()
    End Sub

    Protected Sub TxtMInterest_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtMInterest.TextChanged
        TxtNFInterest.Text = Val(TxtMInterest.Text)
        CalcTotFringe()
        CalcTotDeduction()
        CalcPAYE()
    End Sub

    'Protected Sub TxtMTips_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtMTips.TextChanged
    '    'TxtNFTips.Text = Val(TxtMTips.Text)
    '    'CalcTotFringe()
    '    'CalcTotDeduction()
    '    'CalcPAYE()
    'End Sub

    Protected Sub TxtMWriteOff_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtMWriteOff.TextChanged
        TxtNFWriteOff.Text = Val(TxtMWriteOff.Text)
        CalcTotFringe()
        CalcTotDeduction()
        CalcPAYE()
    End Sub

    Protected Sub TxtMExpenses_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtMExpenses.TextChanged
        TxtNFExpenses.Text = Val(TxtMExpenses.Text)
        CalcTotFringe()
        CalcTotDeduction()
        CalcPAYE()
    End Sub

    Protected Sub TxtMTaxPaid_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtMTaxPaid.TextChanged
        TxtNFTaxPaid.Text = Val(TxtMTaxPaid.Text)
        CalcTotFringe()
        CalcTotDeduction()
        CalcPAYE()
    End Sub
    Private Sub TxtMSPassageBenefit_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtMSPassageBenefit.TextChanged
        'If Trim(CmbMonth.Text) = "December" Then
        Dim PassDateFrom As Date
        Dim PassDateTo As Date
        PassDateFrom = CDate("28/12" + "/" + CStr((Val(TxtYear.Text) - 1)))
        PassDateTo = CDate("28/11" + "/" + CStr((Val(TxtYear.Text))))

        'TxtMSPassageBenefit.Text = 0
        'TxtNSPassageBenefit.Text = 0
        TxtNPPassageBenefit.Text = 0

        TxtNSPassageBenefit.Text = Val(TxtMSPassageBenefit.Text)
        Dim SngTotalBasic As Single = 0
        Dim SngExemptPass As Single = 0
        Dim CmdPasseage As New OleDb.OleDbCommand
        Dim RdrPassage As OleDb.OleDbDataReader
        StrSql = "SELECT sum(BasicSalary) as TotSalary, sum(RespAllow) as TotResp,sum(PensionEmp) as penEmp  FROM Salary WHERE EmpID = '" & Trim(TxtEmpID.Text) & "' and [Date] >= '" & Format(PassDateFrom, "yyyy-MM-dd") & "' and [Date] <= '" & Format(PassDateTo, "yyyy-MM-dd") & "'"
        CmdPasseage.Connection = Con
        CmdPasseage.CommandText = StrSql
        RdrPassage = CmdPasseage.ExecuteReader
        If RdrPassage.Read Then
            SngTotalBasic = RdrPassage("TotSalary") + RdrPassage("TotResp") + RdrPassage("penEmp")
        End If
        SngExemptPass = Math.Round(SngTotalBasic * 0.06)
        If Val(TxtMSPassageBenefit.Text) - SngExemptPass > 0 Then
            TxtNPPassageBenefit.Text = Val(TxtMSPassageBenefit.Text) - SngExemptPass
        End If
        TxtNFTips.Text = SngExemptPass
        CalcTotPayable()
        CalcTotDeduction()
        CalcNetPayable()
        'End If
    End Sub

    Protected Sub TxtMArrears_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtMArrears.TextChanged
        TxtNSArrears.Text = Val(TxtMArrears.Text)
        TxtNPArrears.Text = Val(TxtMArrears.Text)
        CalcTotPayable()
        CalcTotDeduction()
        CalcNetPayable()
        CalcPAYE()
    End Sub

    Protected Sub TxtMTravel_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtMTravel.TextChanged
        TxtNSTravel.Text = Val(TxtMTravel.Text)
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT TransportExcess FROM [System]"
        Dim IntTransportExcess As Integer = 0
        Try
            IntTransportExcess = Cmd.ExecuteScalar
        Catch ex As Exception

        End Try
        If Val(TxtMTravel.Text) > IntTransportExcess Then
            TxtNPTransport.Text = Val(TxtMTravel.Text) - IntTransportExcess
        Else
            TxtNPTransport.Text = 0
        End If
        CalcTotPayable()
        CalcTotDeduction()
        CalcNetPayable()
        CalcPAYE()
    End Sub

    Protected Sub TxtMOtherAllow_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtMOtherAllow.TextChanged
        TxtNSOtherAllow.Text = Val(TxtMOtherAllow.Text)
        TxtNPOtherAllow.Text = Val(TxtMOtherAllow.Text)
        CalcTotPayable()
        CalcTotDeduction()
        CalcNetPayable()
        CalcPAYE()
    End Sub
    'Protected Sub TxtMMDays_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtMMDays.TextChanged
    '    Cmd.Connection = Con
    '    Cmd.CommandText = "Select Meal From System"
    '    Rdr = Cmd.ExecuteReader
    '    If Rdr.Read Then
    '        Session("Meal") = Rdr("Meal")
    '    End If
    '    Rdr.Close()
    '    TxtMMeal.Text = Session("Meal") * Val(TxtMMDays.Text)
    '    TxtNSMeal.Text = TxtMMeal.Text
    '    TxtNPMealAllow.Text = TxtMMeal.Text
    '    CalcTotPayable()
    '    CalcTotDeduction()
    '    CalcNetPayable()
    '    CalcPAYE()
    'End Sub
    'Protected Sub TxtNiteAllowDays_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtNiteAllowDays.TextChanged
    '    Cmd.Connection = Con
    '    Cmd.CommandText = "Select * from System"
    '    Rdr = Cmd.ExecuteReader
    '    If Rdr.Read Then
    '        Session("Nite") = Rdr("NightAllow")
    '    End If
    '    Rdr.Close()
    '    TxtNiteAllow.Text = Math.Round(((Session("Nite") / 100) * Session("SngDRate")) * Val(TxtNiteAllowDays.Text), 2)
    '    TxtNSNiteAllow.Text = TxtNiteAllow.Text
    '    TxtNPNiteAllow.Text = TxtNiteAllow.Text

    '    CalcTotPayable()
    '    CalcTotDeduction()
    '    CalcNetPayable()
    '    CalcPAYE()
    'End Sub

    Protected Sub TxtMLRefund_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtMLRefund.TextChanged
        CalcLRefund()
        CalcTotPayable()
        CalcTotDeduction()
        CalcNetPayable()
        CalcPAYE()
    End Sub

    Protected Sub TxtMSRefund_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtMSRefund.TextChanged
        CalcLRefund()
        CalcTotPayable()
        CalcTotDeduction()
        CalcNetPayable()
        CalcPAYE()
    End Sub

    'Protected Sub TxtMAttBonus_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtMAttBonus.TextChanged
    '    TxtNSAttendBonus.Text = Val(TxtMAttBonus.Text)
    '    TxtNPAttBonus.Text = Val(TxtMAttBonus.Text)
    '    CalcTotPayable()
    '    CalcTotDeduction()
    '    CalcNetPayable()
    '    CalcPAYE()
    'End Sub

    Protected Sub TxtNiteAllow_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtNiteAllow.TextChanged
        TxtNSNiteAllow.Text = Val(TxtNiteAllow.Text)
        TxtNPNiteAllow.Text = Val(TxtNiteAllow.Text)
        CalcTotPayable()
        CalcTotDeduction()
        CalcNetPayable()
        CalcPAYE()
    End Sub

    Protected Sub TxtMOtherDeduction_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtMOtherDeduction.TextChanged
        TxtNSOtherDeduction.Text = Val(TxtMOtherDeduction.Text)
        CalcTotDeduction()
        CalcNetPayable()
    End Sub


    Protected Sub TxtM15Hrs_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtM15Hrs.TextChanged
        CalcOT()
        CalcTotPayable()
        CalcTotDeduction()
        CalcNetPayable()
        CalcPAYE()
    End Sub

    Protected Sub TxtM20Hrs_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtM20Hrs.TextChanged
        CalcOT()
        CalcTotPayable()
        CalcTotDeduction()
        CalcNetPayable()
        CalcPAYE()
    End Sub

    Protected Sub TxtM30Hrs_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtM30Hrs.TextChanged
        CalcOT()
        CalcTotPayable()
        CalcTotDeduction()
        CalcNetPayable()
        CalcPAYE()
    End Sub

    Protected Sub TxtNSPaye_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtNSPaye.TextChanged
        CalcTotDeduction()
    End Sub

    Protected Sub TxtMEOYBonus_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtMEOYBonus.TextChanged
        TxtNSEOYBonus.Text = Val(TxtMEOYBonus.Text)
        TxtNPEOYBonus.Text = Val(TxtMEOYBonus.Text)
        CalcTotPayable()
        CalcTotDeduction()
        CalcNetPayable()
        CalcPAYE()
    End Sub

    Private Sub TxtDCrCard_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtDCrCard.TextChanged
        TxtNSCCardCharges.Text = Val(TxtDCrCard.Text)
        CalcTotDeduction()
        CalcNetPayable()
    End Sub

    Private Sub TxtDShort_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtDShort.TextChanged
        TxtNSShort.Text = Val(TxtDShort.Text)
        CalcTotDeduction()
        CalcNetPayable()
    End Sub

    Protected Sub TxtM10Hrs_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtM10Hrs.TextChanged
        CalcOT()
        CalcTotPayable()
        CalcTotDeduction()
        CalcNetPayable()
        CalcPAYE()
    End Sub

    Private Sub TxtSpecialBenefit_TextChanged(sender As Object, e As EventArgs) Handles TxtSpecialBenefit.TextChanged
        CalcTotPayable()
        CalcTotDeduction()
        CalcNetPayable()
        CalcPAYE()
    End Sub

    Private Sub TxtOthDeduction_TextChanged(sender As Object, e As EventArgs) Handles TxtOthDeduction.TextChanged
        TxtOthDeduction.Text = Val(TxtOthDeduction.Text)
        CalcTotDeduction()
        CalcNetPayable()
    End Sub
End Class