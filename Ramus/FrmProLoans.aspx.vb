﻿Public Partial Class FrmProLoans
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT * FROM LoanType"
            Rdr = Cmd.ExecuteReader
            CmbType.Items.Add("Select...")
            While Rdr.Read
                CmbType.Items.Add(Rdr("Name"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ClearAll()
        TxtFirst.Text = ""
        TxtLast.Text = ""
        TxtClockID.Text = ""
        TxtCategory.Text = ""
        TxtTitle.Text = ""
        TxtGender.Text = ""
        ImgPhoto.ImageUrl = Nothing
    End Sub

    Private Sub ClearLoan()
        TxtInstallment.Text = ""
        TxtInterest.Text = ""
        TxtNoOfPayments.Text = ""
        TxtPaidTodate.Text = ""
        TxtPaidTodate.Text = ""
        TxtAmount.Text = ""
        TxtAmtPaid.Text = ""
        TxtDate.Text = Date.Today
        TxtDescription.Text = ""
        TxtDuration.Text = ""
        CmbModePay.SelectedIndex = 0
        CmbType.SelectedIndex = 0
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ev As New System.EventArgs
        If e.Item.CommandName = "New" Then
            ClearLoan()
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT MAX(LoanID) FROM Loan"
            TxtLoanID.Text = Cmd.ExecuteScalar.ToString
            TxtLoanID.Text = Format(Val(TxtLoanID.Text) + 1, "0000000")
        End If
        If e.Item.CommandName = "Save" Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT COUNT(*) FROM Loan WHERE LoanID = '" & TxtLoanID.Text & "'"
            If Cmd.ExecuteScalar = 0 Then
                StrSql = "INSERT INTO Loan (LoanID,Type,DateTaken,LoanAmount,LoanDuration,InterestCharged,"
                StrSql = StrSql & "LoanDesc,ModePay,Installment,PaidTodate,EmpID,Payments,PayDate1,"
                StrSql = StrSql & "PayDate2,PayDate3,Paydate4,Settled) VALUES('"
                StrSql = StrSql & TxtLoanID.Text & "','"
                StrSql = StrSql & CmbType.Text & "','"
                StrSql = StrSql & Format(CDate(TxtDate.Text), "dd-MMM-yyyy") & "',"
                StrSql = StrSql & Val(TxtAmount.Text) & ","
                StrSql = StrSql & Val(TxtDuration.Text) & ","
                StrSql = StrSql & Val(TxtInterest.Text) & ",'"
                StrSql = StrSql & TxtDescription.Text & "','"
                StrSql = StrSql & CmbModePay.Text & "',"
                StrSql = StrSql & Val(TxtInstallment.Text) & ","
                StrSql = StrSql & Val(TxtPaidTodate.Text) & ",'"
                StrSql = StrSql & TxtEmpID.Text & "',"
                StrSql = StrSql & Val(TxtNoOfPayments.Text) & ",'"
                If CmbModePay.SelectedIndex = 2 Then
                    StrSql = StrSql & Format(CDate(TxtDate.Text), "MMMM") & "','"
                    StrSql = StrSql & Format(CDate(TxtDate.Text).AddMonths(3), "MMMM") & "','"
                    StrSql = StrSql & Format(CDate(TxtDate.Text).AddMonths(6), "MMMM") & "','"
                    StrSql = StrSql & Format(CDate(TxtDate.Text).AddMonths(9), "MMMM") & "')"
                End If
                If CmbModePay.SelectedIndex = 3 Then
                    StrSql = StrSql & Format(CDate(TxtDate.Text), "MMMM") & "','"
                    StrSql = StrSql & "','"
                    StrSql = StrSql & "','"
                    StrSql = StrSql & "')"
                End If
                If CmbModePay.SelectedIndex = 1 Then
                    StrSql = StrSql & "','"
                    StrSql = StrSql & "','"
                    StrSql = StrSql & "','"
                    StrSql = StrSql & "',"
                End If
                StrSql = StrSql & 0 & ")"
            Else
                StrSql = "UPDATE Loan SET "
                StrSql = StrSql & "Type = '" & CmbType.Text & "',"
                StrSql = StrSql & "DateTaken = '" & Format(CDate(TxtDate.Text), "dd-MMM-yyyy") & "',"
                StrSql = StrSql & "LoanAmount = " & Val(TxtAmount.Text) & ","
                StrSql = StrSql & "LoanDuration = " & Val(TxtDuration.Text) & ","
                StrSql = StrSql & "InterestCharged = " & Val(TxtInterest.Text) & ","
                StrSql = StrSql & "LoanDesc = '" & TxtDescription.Text & "',"
                StrSql = StrSql & "ModePay = '" & CmbModePay.Text & "',"
                StrSql = StrSql & "Installment = " & Val(TxtInstallment.Text) & ","
                StrSql = StrSql & "PaidTodate = " & Val(TxtPaidTodate.Text) & ","
                StrSql = StrSql & "EmpID = '" & TxtEmpID.Text & "',"
                StrSql = StrSql & "Payments = " & Val(TxtNoOfPayments.Text) & ","
                If CmbModePay.SelectedIndex = 2 Then
                    StrSql = StrSql & "PayDate1 = '" & Format(CDate(TxtDate.Text), "MMMM") & "',"
                    StrSql = StrSql & "PayDate2 = '" & Format(CDate(TxtDate.Text).AddMonths(3), "MMMM") & "',"
                    StrSql = StrSql & "PayDate3 = '" & Format(CDate(TxtDate.Text).AddMonths(6), "MMMM") & "',"
                    StrSql = StrSql & "PayDate4 = '" & Format(CDate(TxtDate.Text).AddMonths(9), "MMMM") & "'"
                End If
                If CmbModePay.SelectedIndex = 3 Then
                    StrSql = StrSql & "PayDate1 = '" & Format(CDate(TxtDate.Text), "MMMM") & "',"
                    StrSql = StrSql & "PayDate2 = '',"
                    StrSql = StrSql & "PayDate3 = '',"
                    StrSql = StrSql & "PayDate4 = ''"
                End If
                If CmbModePay.SelectedIndex = 1 Then
                    StrSql = StrSql & "PayDate1 = '',"
                    StrSql = StrSql & "PayDate2 = '',"
                    StrSql = StrSql & "PayDate3 = '',"
                    StrSql = StrSql & "PayDate4 = ''"
                End If
                StrSql = StrSql & " WHERE LoanID = '" & TxtLoanID.Text & "'"
            End If
            Try
                Cmd.Connection = Con
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "DELETE FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
                Cmd.ExecuteNonQuery()
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Next" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID > '" & TxtEmpID.Text & "'"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try

        End If
        If e.Item.CommandName = "Previous" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID < '" & TxtEmpID.Text & "' ORDER BY EmpID DESC"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try
        End If
        If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtEmpID.Text)
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
        End If
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        If Rdr.Read Then
            On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
            TxtTitle.Text = Rdr("Title")
            TxtCategory.Text = Rdr("Category")
            TxtGender.Text = Rdr("Gender")
            TxtCategory.Text = Rdr("Category")
            ChkClocked.Checked = IIf(Rdr("Clocked"), True, False)
            TxtClockID.Text = Rdr("BatchNo")
            TxtBasic.Text = Rdr("BasicSalary")
            Session.Add("EmpID", Rdr("EmpID"))
            ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & Rdr("EmpID")
            'Loan Details
        Else
            Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(1))
            ToolBar1_ItemClick(Me, ev)
            'ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('Invalid Employee ID','Error');</script>", False)
        End If
        Rdr.Close()
        Cmd.CommandText = "SELECT * FROM Loan WHERE EmpID = '" & TxtEmpID.Text & "'"
        Rdr = Cmd.ExecuteReader
        GdvLoan.DataSource = Rdr
        GdvLoan.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub TxtLoanID_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtLoanID.TextChanged
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT * FROM Loan WHERE LoanID = '" & TxtLoanID.Text & "'"
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            On Error Resume Next
            TxtEmpID.Text = Rdr("EmpID")
            TxtDate.Text = Rdr("DateTaken")
            TxtAmount.Text = Rdr("LoanAmount")
            TxtDuration.Text = Rdr("LoanDuration")
            TxtDescription.Text = Rdr("LoanDesc")
            CmbModePay.Text = Rdr("ModePay")
            TxtInterest.Text = Rdr("InterestCharged")
            CmbType.Text = Rdr("Type")
            TxtPaidTodate.Text = Rdr("PaidTodate")
            TxtInstallment.Text = Rdr("Installment")
            TxtNoOfPayments.Text = Rdr("Payments")
            TxtAmtPaid.Text = Rdr("AmountPaid")
        End If
        Rdr.Close()
    End Sub

    Private Sub CalcInstallment()
        On Error Resume Next
        Dim FVal, PVal, APR, TotPmts, Payment, Period As Single
        FVal = 0    ' Usually 0 for a loan.
        PVal = Val(TxtAmount.Text)
        APR = Val(TxtInterest.Text)
        If APR > 1 Then APR = APR / 100 ' Ensure proper form.
        Select Case CmbModePay.SelectedIndex
            Case 1
                TotPmts = Val(TxtDuration.Text)
                Period = 12
            Case 2
                TotPmts = Val(TxtDuration.Text) / 3
                Period = 4
            Case 3
                TotPmts = Val(TxtDuration.Text) / 12
                Period = 1
        End Select
        Payment = Pmt(APR / Period, TotPmts, -PVal, FVal)
        TxtInstallment.Text = CLng(Payment)
        TxtNoOfPayments.Text = Str(TotPmts)
        'IntIndex = Month(CDate(TxtDate.Text)) - 1
    End Sub

    Private Sub GdvLoan_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvLoan.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvLoan, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvLoan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvLoan.SelectedIndexChanged
        TxtLoanID.Text = GdvLoan.SelectedRow.Cells(0).Text
        TxtLoanID_TextChanged(Me, e)
    End Sub

    Private Sub GdvLoan_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvLoan.SelectedIndexChanging
        GdvLoan.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvLoan.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvLoan.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Protected Sub ButCalc_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButCalc.Click
        CalcInstallment()
    End Sub

    Private Sub TxtPaidTodate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtPaidTodate.TextChanged
        TxtNoOfPayments.Text = CInt(Val(TxtNoOfPayments.Text) - Val(TxtPaidTodate.Text))
        TxtAmtPaid.Text = Format(Val(TxtPaidTodate.Text) * Val(TxtInstallment.Text), "#,###,##0")
    End Sub
End Class