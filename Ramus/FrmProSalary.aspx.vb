﻿Public Partial Class FrmProSalary
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim ConEmployee As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdLock As New OleDb.OleDbCommand
    Dim RdrLock As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrAudit As String
    Dim StrCat As String = ""
    Dim StrCatMNS As String = ""
    Dim StrStatus As String = ""
    Dim BStatusError As Boolean
    Dim StrStatusError As String
    Dim StrBankID As String = ""
    Dim strPRGF As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()

        ConEmployee.ConnectionString = Session("ConnString")
        ConEmployee.Open()

        If Not Page.IsPostBack Then
            'Outlet
            Cmd.Connection = Con
            Cmd.CommandText = "Select OutletName from Outlet Order by OutletCode"
            CmbOutlet.Items.Add("ALL")
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                CmbOutlet.Items.Add(Rdr("OutletName"))
            End While
            Rdr.Close()
            ' Check Access
            If Session("Level") = "Administrator" Then
                CmbOutlet.Text = "ALL"
                ButAuthorised.Visible = True
            ElseIf Session("Level") = "Beau Bassin" Then
                CmbOutlet.Text = "Beau Bassin"
            ElseIf Session("Level") = "Ebene" Then
                CmbOutlet.Text = "Ebene"
            ElseIf Session("Level") = "Grand Baie" Then
                CmbOutlet.Text = "Grand Baie"
            End If
            'CmbOutlet.Enabled = False
            CmbMonth.SelectedIndex = Month(Date.Today) - 1
            TxtYear.Text = Year(Date.Today)
            GetDate()
            CmbCategory_SelectedIndexChanged(Me, e)
            ResetCheck()
        End If
    End Sub

    Private Sub ResetCheck()
        Session.Add("ProcessAgain", "False")
        Session.Add("ProcesswithoutLocking", "False")
    End Sub

    Private Sub GetDate()
        If CmbMonth.Text = "Bonus" Then
            TxtFromDate.Text = CDate("01/" & Format(CmbMonth.SelectedIndex, "00") & "/" & TxtYear.Text)
            TxtDate.Text = Date.DaysInMonth(Val(TxtYear.Text), CmbMonth.SelectedIndex) & "/" & Format(CmbMonth.SelectedIndex, "00") & "/" & TxtYear.Text
        Else
            TxtFromDate.Text = CDate("01/" & Format(CmbMonth.SelectedIndex + 1, "00") & "/" & TxtYear.Text)
            TxtDate.Text = Date.DaysInMonth(Val(TxtYear.Text), CmbMonth.SelectedIndex + 1) & "/" & Format(CmbMonth.SelectedIndex + 1, "00") & "/" & TxtYear.Text
        End If
        LblDate.Text = "Month From " & TxtFromDate.Text & " to " & TxtDate.Text
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Try
            If e.Item.CommandName = "Find" Then
                Session.Add("PstrCode", TxtEmpID.Text)
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
            End If
            If e.Item.CommandName = "Save" Then
                If TxtEmpID.Text = "" Then
                    LblSalaryPro.Visible = True
                    LblSalaryPro.Text = "Please Enter EmpID or ALL to Process all Employee!!!"
                    Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(0))
                    ToolBar1_ItemClick(Me, ev)
                    Exit Sub
                End If
                If TxtFromDateOT.Text = "" And TxtToDateOT.Text = "" Then
                    LblSalaryPro.Visible = True
                    LblSalaryPro.Text = "Please Enter From and To Date for Overtime!!!"
                    Exit Sub
                End If
                If Session("Level") <> "Administrator" Then
                    If CmbOutlet.Text = "Management" Then
                        LblSalaryPro.Visible = True
                        LblSalaryPro.Text = "Please Select Outlet before Processing!!!"
                        Exit Sub
                    End If
                End If
                If CmbMonth.Text = "Bonus" Then
                    StrSql = "SELECT locked FROM Salary WHERE Year = " & TxtYear.Text & " And Month = '" & CmbMonth.Text & "' And Locked = 1"
                    CmdLock.Connection = Con
                    CmdLock.CommandText = StrSql
                    RdrLock = CmdLock.ExecuteReader
                    If RdrLock.Read Then
                        LblSalaryPro.Visible = True
                        LblSalaryPro.Text = "Bonus Already Locked !!!"
                    Else
                        Bonus()
                        LblSalaryPro.Visible = True
                        LblSalaryPro.Text = "Bonus Sucessfully Processed !!!"
                        CmdAudit.Connection = Con
                        StrAudit = "Bonus Processed "
                        CmdAudit.CommandText = "Insert Into SalMasterAudit values('" & Session("UserID") & "','Bonus Processed','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                        CmdAudit.ExecuteNonQuery()
                    End If
                    RdrLock.Close()
                Else
                    StrSql = "SELECT locked FROM Salary WHERE Year = " & TxtYear.Text & " And Month = '" & CmbMonth.Text & "' And Locked = 1"
                    CmdLock.Connection = Con
                    CmdLock.CommandText = StrSql
                    RdrLock = CmdLock.ExecuteReader
                    If RdrLock.Read Then
                        LblSalaryPro.Visible = True
                        LblSalaryPro.Text = "Salary Processing Already Locked !!!"
                    Else
                        Process()
                        LblSalaryPro.Visible = True
                        If BStatusError Then
                            LblSalaryPro.Text = "Error in Salary Processing !!!"
                            LblError.Visible = True
                            LblError.Text = StrStatusError
                        Else
                            LblSalaryPro.Text = "Salary Sucessfully Processed !!!"
                            CmdAudit.Connection = Con
                            'StrAudit = "Salary Processed "
                            LblError.Visible = False
                            CmdAudit.CommandText = "Insert Into SalMasterAudit values('" & Session("UserID") & "','Salary Processed','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                            CmdAudit.ExecuteNonQuery()
                        End If

                    End If
                    RdrLock.Close()
                End If
            End If
        Catch ex As Exception
            LblError.Text = "error : " + ex.Message
        End Try

    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
            StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
            ButAuthorised.Visible = True
        Else
            StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "' And OutletName = '" & Session("Level") & "'"
        End If

        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            On Error Resume Next
            CmbOutlet.Text = "ALL"
            CmbOutlet.Enabled = False
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
            TxtDept.Text = Rdr("DeptName")
            CmbCategory.Text = Rdr("Category")
            Session.Add("EmpID", Rdr("EmpID"))
            ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & Rdr("EmpID")
        Else
            If TxtEmpID.Text <> "ALL" Then
                Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(0))
                ToolBar1_ItemClick(Me, ev)
            Else
                TxtFirst.Text = ""
                TxtLast.Text = ""
                CmbOutlet.Enabled = True
                LblSalaryPro.Visible = False
            End If
        End If
        Rdr.Close()
        ResetCheck()
    End Sub

    Private Sub Process()
        Try


            ' *** As per the selection of Category define Salary Date and Last Salary Date
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            'If ChkNotLocked.Checked Then
            '    Session("ProcesswithoutLocking") = "True"
            'End If
            'If ChkProcessAgain.Checked Then
            '    Session("ProcessAgain") = True
            'End If
            Dim DteDate As Date = CDate(TxtDate.Text)
            Dim DteSalDate As Date
            Dim DteFirst As Date
            Dim DteLast As Date
            Dim DteEndDate As Date
            Dim DtePrevSalDate As Date
            Dim DteFirstFreelance As Date
            Dim DteLastFreelance As Date
            Dim DteFromOT As Date
            Dim DteToOT As Date

            If CmbCategory.SelectedIndex = 2 Then
                DteFirst = DteDate.AddDays(-6)
                DteLast = DteDate.AddDays(-7)
                DteSalDate = DteDate
                DteEndDate = DteSalDate
                StrCat = "Weekly"
            ElseIf CmbCategory.SelectedIndex = 1 Then
                DteFirst = DteDate.AddDays(-13)
                DteLast = DteDate.AddDays(-14)
                DteSalDate = DteDate
                DteEndDate = DteSalDate
                StrCat = "Fortnightly"
            ElseIf CmbCategory.SelectedIndex = 0 Then
                DteSalDate = CDate("28/" & Format(CmbMonth.SelectedIndex + 1, "00") & "/" & TxtYear.Text)
                DteLast = DteSalDate.AddMonths(-1)
                DtePrevSalDate = DteSalDate
                If CmbMonth.Text = "January" Then
                    DteFirstFreelance = CDate("20/12/" & Val(TxtYear.Text) - 1)
                    DteLastFreelance = CDate("19/" & Format(CmbMonth.SelectedIndex + 1, "00") & "/" & TxtYear.Text)
                Else
                    DteFirstFreelance = CDate("20/" & Format(CmbMonth.SelectedIndex, "00") & "/" & TxtYear.Text)
                    DteLastFreelance = CDate("19/" & Format(CmbMonth.SelectedIndex + 1, "00") & "/" + TxtYear.Text)
                End If
                DteFirst = CDate(TxtFromDate.Text)
                DteEndDate = DteDate
                DteFromOT = CDate(TxtFromDateOT.Text)
                DteToOT = CDate(TxtToDateOT.Text)
                StrCat = "Monthly"
                StrCatMNS = "M"
            End If

            Dim SngWDays As Single
            Dim SngWDHrs As Single
            Dim SngWorkingDays As Single
            Dim LngNetBasic As Long
            Dim LngTravel As Long
            Dim BlnNotAttBonus As Boolean
            Dim BlnEOYBonus As Boolean
            Dim IntDaysPay As Integer
            Dim IntDaysPay2 As Integer
            Dim DtePays As Date
            Dim IntWorkingDays As Integer
            Dim BlnContinue As Boolean = True
            Dim SngBasic As Single
            Dim SngBasicExpat As Single
            Dim SngOT15 As Single
            Dim SngOT20 As Single
            Dim SngOT30 As Single
            Dim SngOvertime As Single
            Dim DteStartMonth As Date
            Dim DteEndMonth As Date
            Dim IntAttendAllow As Integer
            Dim SngSysNPFEmployee As Single
            Dim SngSysNPFEmployer As Single
            Dim LngMaxSalNPF As Single
            Dim IntSysNPFEmployeeMax As Integer
            Dim IntSysNPFEmployerMax As Integer
            Dim SngSysNSF As Single
            Dim SngSysNSFEmployee As Single
            Dim IntSysNSFMax As Integer
            Dim IntSysNSFEmployeeMax As Single
            Dim SngSysIVTB As Single
            Dim IntSysTransportExcess As Integer
            Dim IntSysTaxExempt As Integer
            Dim IntSysMinPAYE As Integer
            Dim SngExpatDed As Single
            Dim SngNiteAllow As Single
            Dim SngAttFort As Single
            Dim SngWEHrs As Single
            Dim SngSalaryMeal As Single = 0
            Dim SngOT1Amt As Single = 0
            Dim SngOT15Amt As Single = 0
            Dim SngOT20Amt As Single = 0
            Dim SngOT30Amt As Single = 0
            Dim SngLoanRepay As Single = 0
            Dim SngLoanRepay1 As Single = 0
            Dim IntLeftEmpPH As Integer = 0
            Dim DteLastWorking As Date
            Dim StrMCBNonMCB As String
            Dim DteNotWorkingStart As Date
            Dim DteNotWorkingLeft As Date
            Dim StrPayeRegNo As String
            Dim StrCompanyName As String
            Dim SngEPZEE As Single = 0
            Dim SngEPZER As Single = 0
            Dim SngPensionSystem As Single = 0
            Dim SngMedical As Single = 0

            ' Start to Process
            'Get System parameters
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT * FROM [System]"
            Rdr = Cmd.ExecuteReader
            If Rdr.Read Then
                StrCompanyName = Rdr("Name") & ""
                StrPayeRegNo = Rdr("PAYERegNo") & ""
                SngNiteAllow = CSng(Rdr("NightAllow"))
                SngAttFort = CSng(Rdr("AttBonusFort"))
                SngWEHrs = CSng(Rdr("WEHrs"))
                SngWDHrs = CSng(Rdr("WDHrs"))
                IntSysNPFEmployerMax = CInt(Rdr("NPFEmployerMax"))
                IntSysNPFEmployeeMax = CInt(Rdr("NPFEmployeeMax"))
                SngSysNPFEmployee = CSng(Rdr("NPFEmployee"))
                SngSysNPFEmployer = CSng(Rdr("NPFEmployer"))
                SngSysNSF = CSng(Rdr("NSF"))
                IntSysNSFMax = CInt(Rdr("NSFMax"))
                SngSysNSFEmployee = CSng(Rdr("NSFEmployee"))
                IntSysNSFEmployeeMax = CInt(Rdr("NSFEmployeeMax"))
                SngWorkingDays = CSng(Rdr("WorkingDays"))
                IntWorkingDays = 26
                Session("Meal") = CSng(Rdr("Meal"))
                IntAttendAllow = CInt(Rdr("Attendance"))
                SngSysIVTB = CSng(Rdr("IVTB"))
                IntSysTransportExcess = CInt(Rdr("TransportExcess"))
                IntSysTaxExempt = CInt(Rdr("TaxExempt"))
                IntSysMinPAYE = CInt(Rdr("MinPAYE"))
                LngMaxSalNPF = CSng(Rdr("MaxSalNPS"))
                SngEPZEE = CSng(Rdr("EPZEE"))
                SngEPZER = CSng(Rdr("EPZER"))
                SngPensionSystem = CSng(Rdr("PensionEmp"))
            End If
            Rdr.Close()

            StrSql = "Select count(*) FROM Employee WHERE Recruit = 1 and Category = '" & StrCat & "'"
            StrAudit = "Salary for All Employee Processed for the month of " & CmbMonth.Text & ""
            If CmbOutlet.Text <> "ALL" Then
                StrSql = StrSql & " AND Outletname = '" & CmbOutlet.Text & "'"
            End If
            If TxtEmpID.Text <> "ALL" Then
                StrSql = StrSql & " AND EmpID = '" & TxtEmpID.Text & "'"
                StrAudit = "Salary " & CmbMonth.Text & " For EmpID " & TxtEmpID.Text & " Processed."
            End If
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                LblDate.Text = "No Employees in the Seletected Category!"
                Exit Sub
            Else
                StrSql = "Select * FROM Employee WHERE Recruit = 1 and Category = '" & StrCat & "'"
                If CmbOutlet.Text <> "ALL" Then
                    StrSql = StrSql & " AND Outletname = '" & CmbOutlet.Text & "'"
                End If
                If TxtEmpID.Text <> "ALL" Then
                    StrSql = StrSql & " AND EmpID = '" & TxtEmpID.Text & "'"
                End If
            End If
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader

            Dim Trans As OleDb.OleDbTransaction
            'Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
            Dim CmdSal As New OleDb.OleDbCommand
            Dim RdrSal As OleDb.OleDbDataReader
            Dim RdrTime As OleDb.OleDbDataReader
            Dim SalaryCon As New OleDb.OleDbConnection
            'TimeSheet
            Dim CmdTSheet As New OleDb.OleDbCommand

            If SalaryCon.State = ConnectionState.Open Then SalaryCon.Close()
            SalaryCon.ConnectionString = Session("ConnString")
            SalaryCon.Open()

            Dim TimexCon As New OleDb.OleDbConnection
            TimexCon.ConnectionString = Session("ConnString") ' Database of desktop Payroll TNT
            TimexCon.Open()
            Dim TimeTrans As OleDb.OleDbTransaction
            TimeTrans = TimexCon.BeginTransaction(IsolationLevel.ReadCommitted)

            Dim CmdTime As New OleDb.OleDbCommand
            Dim CmdDelete As New OleDb.OleDbCommand
            Dim CmdRoster As New OleDb.OleDbCommand
            'Cmd.Transaction = Trans
            CmdTime.Connection = TimexCon
            CmdTime.Transaction = TimeTrans

            CmdSal.Connection = SalaryCon
            'Try
            While Rdr.Read
                BlnContinue = True
                StrSql = "SELECT COUNT(*) FROM Salary WHERE Category = '" & StrCat & "' "
                StrSql = StrSql & "AND Date = '" & DteSalDate.ToString("yyyy-MM-dd") & "'  AND EmpID='" & Rdr("EmpID") & "'"
                CmdSal.CommandText = StrSql
                If CmdSal.ExecuteScalar >= 0 Then
                    ' *** Calculate the Hourly and Daily rate for this employee
                    ' *** Any customization for their way of calculting to be done here.
                    Dim SngHourlyRate As Single = 0
                    Dim SngDailyRate As Single = 0
                    Dim IntTravel As Integer = 0
                    Dim IntMeal As Integer = 0
                    Dim SngProdBonus As Single = 0
                    Dim SngMaternityAllow As Single = 0
                    Dim SngRespAllow As Single = 0
                    Dim SngMealAllow As Single = 0
                    Dim SngFringe As Single = 0
                    Dim SngOtherAllow As Single = 0
                    Dim SngArrears As Single = 0
                    Dim SngTimeNiteAllow As Single = 0
                    Dim SngNightAllow As Single = 0
                    Dim SngIFSBonus As Single = 0
                    Dim SngLocalRefund As Single = 0
                    Dim LngEdf As Long
                    Dim LngGrosCh As Long
                    Dim IntTravelTrips As Integer
                    Dim IntTravelNonTax As Integer
                    Dim IntTravelTax As Integer
                    Dim IntTravelAllow As Integer
                    Dim DteStartAttBonus As Date
                    Dim DteEndAttBonus As Date
                    Dim IntAttendBonus As Single = 0
                    Dim BlnServiceCharge As Boolean
                    BlnServiceCharge = True
                    Dim DteStartSal As Date
                    Dim DteEndSal As Date
                    Dim IntPHDays As Integer = 0
                    'Select Case Rdr("Category")
                    'Case "Monthly"
                    SngDailyRate = Math.Round(Val(Rdr("BasicSalary") & "") / 26, 2)
                    SngHourlyRate = Math.Round(Val(Rdr("BasicSalary") & "") / 26 / 8, 2)
                    'End Select

                    ' *** calcualation of Local, Sick and Unpaid Leave
                    ' *** Populate Leave Recordset with Leave Records for each employee for this period.
                    Dim SngSick As Single = 0
                    Dim SngLocal As Single = 0
                    Dim SngUnpaid As Single = 0
                    Dim SngUnpaidAttBonus As Single = 0
                    Dim DteLeaveStart As Date
                    Dim DteLeaveEnd As Date
                    If CmbMonth.Text = "January" Then
                        DteLeaveStart = CDate("20/12/" & Val(TxtYear.Text) - 1)
                        DteLeaveEnd = CDate("19/" & Format(CmbMonth.SelectedIndex + 1, "00") & "/" & TxtYear.Text)
                        DteStartSal = CDate("20/12/" & Val(TxtYear.Text) - 1)
                        DteEndSal = CDate("19/" & Format(CmbMonth.SelectedIndex + 1, "00") & "/" & TxtYear.Text)
                    Else
                        DteLeaveStart = CDate("20/" & Format(CmbMonth.SelectedIndex, "00") & "/" & TxtYear.Text)
                        DteLeaveEnd = CDate("19/" & Format(CmbMonth.SelectedIndex + 1, "00") & "/" + TxtYear.Text)
                        DteStartSal = CDate("20/" & Format(CmbMonth.SelectedIndex, "00") & "/" & TxtYear.Text)
                        DteEndSal = CDate("19/" & Format(CmbMonth.SelectedIndex + 1, "00") & "/" + TxtYear.Text)
                    End If

                    StrSql = "SELECT Leave.Type,Leave.Leave,LeaveMaster.Paid"
                    StrSql = StrSql & " FROM Leave,LeaveMaster "
                    StrSql = StrSql & "WHERE Leave.Type = LeaveMaster.Name "
                    StrSql = StrSql & "AND Date >= '" & DteFromOT.ToString("yyyy-MM-dd") & "' "
                    StrSql = StrSql & "AND Date <= '" & DteToOT.ToString("yyyy-MM-dd") & "' "
                    StrSql = StrSql & "AND EmpID = '" & Rdr("EmpID") & "'  ORDER BY [Date]"
                    CmdTime.CommandText = StrSql
                    RdrTime = CmdTime.ExecuteReader
                    While RdrTime.Read
                        If RdrTime(0) = "Local Leave" Then
                            SngLocal = SngLocal + RdrTime("Leave")
                        End If
                        If RdrTime(0) = "Sick Leave" Then
                            SngSick = SngSick + RdrTime("Leave")
                        End If
                        If Not RdrTime(2) Then
                            SngUnpaid = SngUnpaid + RdrTime("Leave")
                        End If
                    End While
                    RdrTime.Close()

                    ''Get PH Days
                    StrSql = ""
                    StrSql = "SELECT * From Holiday where Date >= '" & DteStartSal.ToString("yyyy-MM-dd") & "' And Date <= '" & DteEndSal.ToString("yyyy-MM-dd") & "' Order by [Date]"
                    CmdSal.CommandText = StrSql
                    RdrSal = CmdSal.ExecuteReader
                    While RdrSal.Read
                        IntPHDays = IntPHDays + 1
                    End While
                    RdrSal.Close()

                    'Check if Salary exists, dont update Local and Sick in Employee Table
                    Dim CmdCheckRecord As New OleDb.OleDbCommand
                    Dim RdrCheckRecord As OleDb.OleDbDataReader
                    Dim BCheckRecord As Boolean = True
                    StrSql = "SELECT * FROM Salary WHERE Category = '" & StrCat & "' "
                    StrSql = StrSql & "AND Date = '" & DteSalDate.ToString("yyyy-MM-dd") & "'  AND EmpID='" & Rdr("EmpID") & "'"
                    CmdCheckRecord.Connection = Con
                    CmdCheckRecord.CommandText = StrSql
                    RdrCheckRecord = CmdCheckRecord.ExecuteReader
                    If RdrCheckRecord.Read Then
                        BCheckRecord = False
                    End If
                    RdrCheckRecord.Close()

                    ' *** Delete all salary records if processed earlier.
                    StrSql = "Delete FROM Salary WHERE EmpID = '" & Rdr("EmpId") & "' "
                    StrSql = StrSql & "AND [Date] = '" & DteSalDate.ToString("yyyy-MM-dd") & "'"
                    CmdSal.CommandText = StrSql
                    CmdSal.ExecuteNonQuery()
                    ' Start Calculating Salary
                    LngNetBasic = Val(Rdr("BasicSalary") & "")
                    BlnNotAttBonus = False
                    BlnEOYBonus = True

                    If Rdr("EmpID") = "118" Then
                        BlnEOYBonus = True
                    End If

                    ' *** If employee stopped working check the last date fall within
                    ' *** this month. If it does calculate partial month.
                    StrStatus = "NORMAL"
                    DteEndDate = DteDate
                    If Not Rdr("Working") Then
                        IntDaysPay = 0
                        IntDaysPay2 = 0
                        If Rdr("HireDate") <= DteFirst And Rdr("LastDay") >= DteFirst And Rdr("LastDay") <= DteEndDate Then
                            StrStatus = "LEFT"
                            BlnEOYBonus = False
                            DtePays = DteFirst
                            'Check Leave record
                            DteNotWorkingStart = DteFirst
                            DteNotWorkingLeft = Rdr("LastDay")
                            For I = 0 To CLng(DateDiff("d", DteFirst, Rdr("LastDay")))
                                If Weekday(DtePays) > 1 And Weekday(DtePays) <= 7 Then
                                    IntDaysPay = IntDaysPay + 1
                                End If
                                DtePays = DtePays.AddDays(1)
                            Next I
                            If IntDaysPay >= IntWorkingDays Then
                                LngNetBasic = Rdr("BasicSalary")
                                SngBasic = LngNetBasic
                            ElseIf IntDaysPay < IntWorkingDays Then
                                DteLastWorking = Rdr("LastDay")
                                'Holiday
                                StrSql = ""
                                StrSql = "SELECT * From Holiday where Date >= '" & Format(DteFirst, "yyyy-MM-dd") & "' And Date <= '" & Format(DteLastWorking, "yyyy-MM-dd") & "' Order by [Date]"
                                CmdSal.CommandText = StrSql
                                RdrSal = CmdSal.ExecuteReader
                                While RdrSal.Read
                                    'IntLeftEmpPH = IntLeftEmpPH + 1
                                    IntLeftEmpPH = 0
                                End While
                                RdrSal.Close()

                                LngNetBasic = Math.Round((((IntDaysPay - IntLeftEmpPH) * Rdr("BasicSalary")) / IntWorkingDays), 0)
                                SngBasic = LngNetBasic
                                'SngUnpaid = SngUnpaid + (IntWorkingDays - IntDaysPay)
                                If SngUnpaid > 0 Then
                                    BlnNotAttBonus = True
                                End If
                            End If
                        ElseIf Rdr("HireDate") >= DteFirst And Rdr("LastDay") >= DteFirst And Rdr("LastDay") <= DteEndDate Then
                            StrStatus = "LEFT"
                            BlnEOYBonus = False
                            DtePays = DteFirst
                            'Check Leave Record
                            DteNotWorkingStart = DteFirst   'Rdr("HireDate")
                            DteNotWorkingLeft = Rdr("LastDay")
                            While DtePays <= Rdr("LastDay")
                                If Weekday(DtePays) > 1 And Weekday(DtePays) <= 7 Then
                                    IntDaysPay = IntDaysPay + 1
                                End If
                                DtePays = DtePays.AddDays(1)
                            End While
                            If IntDaysPay >= IntWorkingDays Then
                                LngNetBasic = Rdr("BasicSalary")
                                SngBasic = LngNetBasic
                            ElseIf IntDaysPay < IntWorkingDays Then
                                LngNetBasic = Math.Round(((IntDaysPay * Rdr("BasicSalary")) / IntWorkingDays), 0)
                                SngBasic = LngNetBasic
                                'SngUnpaid = SngUnpaid + (IntWorkingDays - IntDaysPay)
                                If SngUnpaid > 0 Then
                                    BlnNotAttBonus = True
                                End If
                            End If
                            LngTravel = Math.Round((Rdr("TravelAllowance") * IntDaysPay) / IntWorkingDays, 0)
                        Else
                            BlnContinue = False
                        End If
                    End If
                    ' *** If the Hire date is later than this month do not calculate
                    If Rdr("HireDate") > DteEndDate Then
                        BlnContinue = False
                    End If
                    'If the employee's salary has to be calculated ie still working or if left partial salary
                    'has to be calculated.
                    If BlnContinue Then
                        'If this employee joined this month then deduct the days not worked.
                        IntDaysPay = 0
                        IntDaysPay2 = 0
                        If Rdr("HireDate") >= DteFirst And Rdr("HireDate") <= DteEndDate And Rdr("Working") Then
                            StrStatus = "NEW"
                            BlnNotAttBonus = True
                            BlnServiceCharge = False
                            DtePays = Rdr("HireDate")
                            'Check Leave Record
                            DteNotWorkingStart = Rdr("HireDate")
                            DteNotWorkingLeft = DteEndDate
                            While DtePays <= DteEndDate
                                If Weekday(DtePays) > 1 And Weekday(DtePays) <= 7 Then
                                    IntDaysPay = IntDaysPay + 1
                                End If
                                DtePays = DtePays.AddDays(1)
                            End While
                            If IntDaysPay = IntWorkingDays Then
                                LngNetBasic = Rdr("BasicSalary")
                            ElseIf IntDaysPay < IntWorkingDays Then
                                DteLastWorking = Rdr("HireDate")
                                'Holiday
                                StrSql = ""
                                StrSql = "SELECT * From Holiday where Date >= '" & Format(DteFirst, "yyyy-MM-dd") & "' And Date <= '" & Format(DteLastWorking, "yyyy-MM-dd") & "' Order by [Date]"
                                CmdSal.CommandText = StrSql
                                RdrSal = CmdSal.ExecuteReader
                                While RdrSal.Read
                                    'IntLeftEmpPH = IntLeftEmpPH + 1
                                    IntLeftEmpPH = 0
                                End While
                                RdrSal.Close()
                                LngNetBasic = Math.Round((((IntDaysPay - IntLeftEmpPH) * Rdr("BasicSalary")) / IntWorkingDays), 0)
                            End If
                        End If
                        'Employee Joins and Left same month
                        If Not Rdr("Working") Then
                            If Rdr("HireDate") >= DteFirst And Rdr("LastDay") <= DteEndDate Then
                                StrStatus = "LEFT"
                                BlnNotAttBonus = True
                                BlnServiceCharge = False
                                DtePays = Rdr("HireDate")
                                DteEndDate = Rdr("LastDay")
                                'Check Leave Record
                                DteNotWorkingStart = Rdr("HireDate")
                                DteNotWorkingLeft = DteEndDate
                                While DtePays <= DteEndDate
                                    If Weekday(DtePays) > 1 And Weekday(DtePays) <= 7 Then
                                        IntDaysPay = IntDaysPay + 1
                                    End If
                                    DtePays = DtePays.AddDays(1)
                                End While
                                If IntDaysPay = IntWorkingDays Then
                                    LngNetBasic = Rdr("BasicSalary")
                                ElseIf IntDaysPay < IntWorkingDays Then
                                    DteLastWorking = Rdr("HireDate")

                                    LngNetBasic = Math.Round((((IntDaysPay - IntLeftEmpPH) * Rdr("BasicSalary")) / IntWorkingDays), 0)
                                End If
                            End If
                        End If
                        DteEndDate = DteDate
                        If Rdr("Working") Then
                            If (LngNetBasic - Math.Round((SngUnpaid * SngDailyRate), 0)) > 0 Then
                                SngBasic = LngNetBasic - Math.Round((SngUnpaid * SngDailyRate), 0)
                            Else
                                SngBasic = 0
                            End If
                        Else
                            'Check Leave Record for Not Working Employee.
                            SngUnpaid = 0
                            StrSql = "SELECT Leave.Type,Leave.Leave,LeaveMaster.Paid"
                            StrSql = StrSql & " FROM Leave,LeaveMaster "
                            StrSql = StrSql & "WHERE Leave.Type = LeaveMaster.Name "
                            StrSql = StrSql & "AND Date >= '" & Format(DteNotWorkingStart, "yyyy-MM-dd") & "' "
                            StrSql = StrSql & "AND Date <= '" & Format(DteNotWorkingLeft, "yyyy-MM-dd") & "' "
                            StrSql = StrSql & "AND EmpID = '" & Rdr("EmpID") & "'  ORDER BY [Date]"
                            CmdTime.CommandText = StrSql
                            RdrTime = CmdTime.ExecuteReader
                            While RdrTime.Read
                                If Not RdrTime(2) Then
                                    SngUnpaid = SngUnpaid + RdrTime("Leave")
                                End If
                            End While
                            RdrTime.Close()
                            If (LngNetBasic - Math.Round((SngUnpaid * SngDailyRate), 0)) > 0 Then
                                SngBasic = LngNetBasic - Math.Round((SngUnpaid * SngDailyRate), 0)
                            Else
                                SngBasic = 0
                            End If
                        End If

                        SngBasicExpat = 0
                        SngExpatDed = 0
                        SngOtherAllow = 0
                        SngArrears = 0
                        SngIFSBonus = 0
                        SngNightAllow = 0
                        SngTimeNiteAllow = 0
                        SngLocalRefund = 0

                        SngProdBonus = Val(Rdr("PerfAllow"))
                        SngMaternityAllow = Val(Rdr("MaternityAllow"))
                        SngRespAllow = Val(Rdr("RespAllow"))
                        SngMealAllow = Val(Rdr("MealAllow"))
                        SngMedical = Val(Rdr("MedicalContrib"))
                        SngFringe = Val(Rdr("Fringe"))
                        DteStartMonth = DteFirst.AddMonths(-1)
                        DteEndMonth = DteEndDate.AddMonths(-1)
                        '***********Taking From TimeSheet*************
                        Dim DteStartTimeSheet As Date
                        Dim DteEndTimeSheet As Date
                        Dim SngHours As Single
                        Dim SngDailyTransport As Single = 0
                        Dim IntDaysPresent As Integer = 0
                        Dim SngTSMeal As Single = 0
                        Dim SngOT10 As Single = 0
                        Dim SngOT10Amt As Single = 0
                        Dim SngLateness As Single = 0
                        Dim SngLatenessAmt As Single = 0


                        SngOT15 = 0
                        SngOT20 = 0
                        SngOT30 = 0
                        SngDailyTransport = 0
                        SngHours = 0
                        SngOvertime = 0
                        SngOT15Amt = 0
                        SngOT20Amt = 0
                        SngOT30Amt = 0
                        If Rdr("OTPaid") Then
                            'Get Values from Daily Input table
                            DteStartTimeSheet = Format(DteFromOT, "yyyy-MM-dd")
                            DteEndTimeSheet = Format(DteToOT, "yyyy-MM-dd")
                            StrSql = "Select *From DailyInput Where EmpID = '" & Rdr("EmpID") & "' And [Date] >= '" & Format(DteStartTimeSheet, "yyyy-MM-dd") & "' And [Date] <= '" & Format(DteEndTimeSheet, "yyyy-MM-dd") & "' order by date"
                            CmdTime.CommandText = StrSql
                            RdrTime = CmdTime.ExecuteReader
                            While RdrTime.Read
                                SngOT10 = SngOT10 + Math.Round(RdrTime("OT10"), 0)
                                SngOT15 = SngOT15 + Math.Round(RdrTime("OT15"), 0)
                                SngOT20 = SngOT20 + Math.Round(RdrTime("OT20"), 0)
                                SngOT30 = SngOT30 + Math.Round(RdrTime("OT30"), 0)
                                SngLateness = SngLateness + Math.Round(RdrTime("Lateness"), 0)
                            End While
                            RdrTime.Close()
                            ' *** Calculate Overtime if any
                            SngOT10Amt = Math.Round(SngOT10 * 1 * SngHourlyRate, 0)
                            SngOT15Amt = Math.Round(SngOT15 * 1.5 * SngHourlyRate, 0)
                            SngOT20Amt = Math.Round(SngOT20 * 2 * SngHourlyRate, 0)
                            SngOT30Amt = Math.Round(SngOT30 * 3 * SngHourlyRate, 0)
                            SngOvertime = SngOT10Amt + SngOT15Amt + SngOT20Amt + SngOT30Amt
                        End If
                        IntTravelAllow = 0
                        'IntTravelAllow = SngDailyTransport

                        'Get Values from Monthly Input Table
                        Dim SngMonthlyOtherAllow As Single = 0
                        Dim SngMonthlyTransport As Single = 0
                        Dim SngMonthlyShort As Single = 0
                        Dim SngMonthlyCreditCard As Single = 0
                        Dim SngMonthlyYOSAmt As Single = 0
                        Dim SngExGratia As Single = 0
                        Dim SngTotalDeduct As Single = 0

                        StrSql = "SELECT * FROM MonthlyInput WHERE Month = '" & Trim(CmbMonth.Text) & "' and year = " & Val(TxtYear.Text) & " and EmpID = '" & Rdr("EmpID") & "' "
                        'StrSql = StrSql & "AND [Date] = '" & Format(DteSalDate, "yyyy-MM-dd") & "'"
                        CmdSal.CommandText = StrSql
                        RdrSal = CmdSal.ExecuteReader
                        If RdrSal.Read Then
                            SngMonthlyOtherAllow = Val(RdrSal("OtherAllow") & "")
                            SngMonthlyTransport = Val(RdrSal("Travel") & "")
                            SngMonthlyShort = Val(RdrSal("Short") & "")
                            SngMonthlyCreditCard = Val(RdrSal("CreditCard") & "")
                            SngMonthlyYOSAmt = Val(RdrSal("YOSAmt") & "")
                            SngExGratia = Val(RdrSal("ExGratia") & "")
                            SngTotalDeduct = Val(RdrSal("OtherDeduct") & "")
                        End If
                        RdrSal.Close()
                        IntTravelAllow = 0
                        IntTravelAllow = SngMonthlyTransport

                        'remove lateness from basic salary
                        If SngLateness > 0 Then
                            SngBasic = SngBasic - Math.Round((SngLateness * SngHourlyRate), 0)
                            SngLatenessAmt = Math.Round((SngLateness * SngHourlyRate), 0)
                        End If
                        SngBasic = SngBasic + SngMonthlyYOSAmt
                        '*****Travel/Meal Allowance******
                        IntTravelTrips = 0
                        IntTravelNonTax = 0
                        IntTravelTax = 0
                        If Not IsDBNull(Rdr("TravelMode")) Then
                            If Rdr("TravelMode") = "Fixed" Then
                                IntTravelAllow = 0
                                IntTravelAllow = Rdr("TravelAllowance")
                            End If
                        End If
                        SngSalaryMeal = 0
                        IntAttendBonus = 0

                        'Employee Pension
                        Dim SngPensionEmp As Single = 0
                        If Rdr("PensionEmpPaid") Then
                            SngPensionEmp = Math.Round((SngPensionSystem / 100) * LngNetBasic, 0)
                        Else
                            SngPensionEmp = 0
                        End If
                        'Loan Repay
                        SngLoanRepay = 0
                        If Rdr("Loan1") Then
                            If DteSalDate >= Rdr("LoanFrom1") And DteSalDate <= Rdr("LoanTo1") Then
                                Dim NoMonth As String
                                NoMonth = DateDiff("m", Rdr("LoanFrom1"), Rdr("LoanTo1")) + 1
                                SngLoanRepay = Math.Round(Rdr("AmtTaken1") / Val(NoMonth), 0)
                            End If
                        End If
                        SngLoanRepay1 = 0
                        If Rdr("Loan2") Then
                            If DteSalDate >= Rdr("LoanFrom2") And DteSalDate <= Rdr("LoanTo2") Then
                                Dim NoMonth As String
                                NoMonth = DateDiff("m", Rdr("LoanFrom2"), Rdr("LoanTo2")) + 1
                                SngLoanRepay1 = Math.Round(Rdr("AmtTaken2") / Val(NoMonth), 0)
                            End If
                        End If
                        'EPZ contribution
                        Dim CmdSys As New OleDb.OleDbCommand
                        Dim RdrSys As OleDb.OleDbDataReader
                        CmdSys.Connection = Con
                        CmdSys.CommandText = "SELECT * FROM [System]"
                        RdrSys = CmdSys.ExecuteReader
                        If RdrSys.Read Then
                            If Rdr("EPZPaid") Then
                                SngEPZEE = CSng(RdrSys("EPZEE"))
                                SngEPZER = CSng(RdrSys("EPZER"))
                            Else
                                SngEPZEE = 0
                                SngEPZER = 0
                            End If
                            If Rdr("BasicSalary") > 30000 Then
                                SngEPZEE = 0
                                SngEPZER = 0
                            End If
                        End If
                        RdrSys.Close()

                        'Categorize the employee as per age to see how NPf,Nsf paid
                        Dim IntNpfEmployee As Integer = 0
                        Dim IntNpfEmployer As Integer = 0
                        Dim IntNsfEmployee As Integer = 0
                        Dim IntNsfEmployer As Integer = 0
                        Dim IntIVTB As Integer = 0
                        Dim SngAge As Single = 0
                        Dim SngYOS As Single = 0

                        SngYOS = 0
                        SngAge = 0
                        SngAge = Math.Round((DateDiff(DateInterval.Day, Rdr("DOB"), DteEndDate) / 365), 2)
                        'SngYOS = Math.Round((DateDiff(DateInterval.Day, Rdr("HireDate"), DteEndDate) / 365), 2)

                        If Rdr("NpsPaid") Then
                            If CLng(SngSysNPFEmployee * SngBasic / 100) > IntSysNPFEmployeeMax Then
                                IntNpfEmployee = IntSysNPFEmployeeMax
                            Else
                                IntNpfEmployee = Math.Round((SngSysNPFEmployee * SngBasic / 100), 0, MidpointRounding.AwayFromZero)
                            End If
                            If CLng(SngSysNPFEmployer * SngBasic / 100) > IntSysNPFEmployerMax Then
                                IntNpfEmployer = IntSysNPFEmployerMax
                            Else
                                IntNpfEmployer = Math.Round((SngSysNPFEmployer * SngBasic / 100), 0, MidpointRounding.AwayFromZero)
                            End If
                            If SngSysNSF * SngBasic / 100 > IntSysNSFMax Then
                                IntNsfEmployer = IntSysNSFMax
                            Else
                                IntNsfEmployer = Math.Round((SngSysNSF * SngBasic / 100), 0, MidpointRounding.AwayFromZero)
                            End If
                            If CLng(SngSysNSFEmployee * SngBasic / 100) > IntSysNSFEmployeeMax Then
                                IntNsfEmployee = IntSysNSFEmployeeMax
                            Else
                                IntNsfEmployee = Math.Round(SngSysNSFEmployee * SngBasic / 100, 0, MidpointRounding.AwayFromZero)
                            End If
                            'IntIVTB = Math.Round((SngBasic * SngSysIVTB / 100), 0, MidpointRounding.AwayFromZero)

                            StrSql = "SELECT * FROM PAYE WHERE PAYESCheme = 'Levy'"
                            CmdSal.CommandText = StrSql
                            RdrSal = CmdSal.ExecuteReader
                            IntIVTB = 0
                            If RdrSal.Read Then
                                If SngBasic >= RdrSal("From-1") And SngBasic <= RdrSal("To-1") Then
                                    IntIVTB = Math.Round((SngBasic * RdrSal("TaxPercent-1") / 100), 0, MidpointRounding.AwayFromZero)
                                ElseIf SngBasic >= RdrSal("From-2") Then
                                    IntIVTB = Math.Round((SngBasic * RdrSal("TaxPercent-2") / 100), 0, MidpointRounding.AwayFromZero)
                                End If
                            End If
                            RdrSal.Close()
                        End If

                    'Check Retirement Age
                    Dim StrEmpMonth As String
                    Dim IntEmpYear As Integer
                    StrEmpMonth = Month(Rdr("DOB"))
                    IntEmpYear = Year(Rdr("DOB"))
                    StrSql = "Select * From RetireAge where BMonthNo = " & StrEmpMonth & " AND BirthYear = " & IntEmpYear & ""
                    CmdSal.CommandText = StrSql
                    RdrSal = CmdSal.ExecuteReader
                    If RdrSal.Read Then
                        If CDate(DteSalDate) >= CDate(Rdr("DOB")) And CDate(DteSalDate) <= DateAdd("M", 0, CDate(RdrSal("Date"))) Then
                        Else
                            ''Pay only levy and Employer 6%
                            IntNpfEmployee = 0
                            IntNsfEmployer = 0
                            IntNsfEmployee = 0
                        End If
                    End If
                    RdrSal.Close()
                    If (SngAge >= 65 And SngAge < 70) Then
                        IntNpfEmployee = 0  '' NPS Not paid by employee
                        IntNsfEmployer = 0  '' NSF not paid by employer
                        IntNsfEmployee = 0
                    End If
                    If Rdr("NpsPaid") And (SngAge >= 70 Or SngAge < 18) Then
                        IntNpfEmployee = 0  '' NPS Not paid by employee
                        IntNpfEmployer = 0  '' NPS Not paid by employer either
                        IntNsfEmployer = 0  '' NSF not paid by employer
                        IntIVTB = 0 '' IVTB also not paid by employer
                        IntNsfEmployee = 0
                    End If

                    '***Fringe Benefits****
                    Dim IntInterest As Integer = 0
                    Dim IntTips As Integer = 0
                    Dim IntWriteOff As Integer = 0
                    Dim IntAccommodation As Integer = 0
                    Dim IntTaxPaid As Integer = 0
                    Dim IntExpenses As Integer = 0
                    StrSql = "SELECT * FROM SalData WHERE EmpID = '" & Rdr("EmpID") & "' "
                    StrSql = StrSql & "AND [Date] = '" & Format(DteSalDate, "yyyy-MM-dd") & "'"
                    CmdSal.CommandText = StrSql
                    RdrSal = CmdSal.ExecuteReader
                    If RdrSal.Read Then
                        IntInterest = RdrSal("Interest")
                        IntTips = RdrSal("Tips")
                        IntWriteOff = RdrSal("WriteOff")
                        IntAccommodation = RdrSal("Accommodation")
                        IntTaxPaid = RdrSal("TaxPaid")
                        IntExpenses = RdrSal("Expenses")
                    End If
                    RdrSal.Close()

                    Dim LngTotEmol As Long = 0
                    LngTotEmol = SngBasic + SngOvertime
                    LngTotEmol = LngTotEmol + IntInterest + IntTips + IntWriteOff
                    LngTotEmol = LngTotEmol + IntExpenses + IntAccommodation + IntTaxPaid
                    Dim LngHousePercent As Long = Rdr("HousePercent")
                    LngHousePercent = Math.Round((LngHousePercent * LngTotEmol) / 100, 0)

                    Dim SngPassageBenefit As Single = 0
                    Dim SngExemptPassageBenefit As Single = 0
                    'SngPassageBenefit = Val(Rdr("PassageBenefit"))
                    'SngExemptPassageBenefit = Math.Round(0.06 * Rdr("EmpBasic"), 0)

                    'PAYE Calculation
                    LngGrosCh = 0
                    Dim LngPrevGrosCh As Long = 0
                    Dim LngGrosChEOY As Long = 0
                    Dim LngGrosEmol As Long = 0
                    Dim IntEDFMonth As Long = 1
                    Dim LngPrevPAYE As Long = 0
                    Dim SngSickRefund As Single = 0
                    Dim SngAnualRefund As Single = 0
                    Dim LngTravelExempt As Long = 0
                    Dim LngTaxBenefit As Long = 0
                    Dim DteSalDateFrm As Date
                    Dim DteSalDateTo As Date
                    Dim LngChargeableExpat As Long = 0
                    Dim LngTaxEpat As Long = 0
                    If DteSalDate < CDate("31/12/2009") Or DteSalDate >= CDate("28/07/2015") Then
                        If Month(DteSalDate) >= 7 Then
                            DteSalDateFrm = CDate("28/07" + "/" + TxtYear.Text)
                            DteSalDateTo = DteSalDate
                        Else
                            DteSalDateFrm = CDate("28/07" + "/" + CStr((Val(TxtYear.Text) - 1)))
                            DteSalDateTo = DteSalDate
                        End If
                    Else
                        DteSalDateFrm = CDate("28/01" + "/" + TxtYear.Text)
                        DteSalDateTo = DteSalDate
                    End If
                    ' I)CALC. GROSS CHARGE FOR CURRENT MONTH

                    LngGrosCh = SngBasic
                    LngGrosCh = LngGrosCh + SngOvertime
                    LngGrosCh = LngGrosCh + SngOtherAllow
                    LngGrosCh = LngGrosCh + SngArrears
                    LngGrosCh = LngGrosCh + SngIFSBonus
                    LngGrosCh = LngGrosCh + SngSalaryMeal
                    LngGrosCh = LngGrosCh + IntAttendBonus
                    LngGrosCh = LngGrosCh + SngNightAllow
                    LngGrosCh = LngGrosCh + SngFringe
                    LngGrosCh = LngGrosCh + SngProdBonus + SngLocalRefund + SngMaternityAllow + SngRespAllow + SngMealAllow + SngExGratia

                        If IntTravelAllow > IntSysTransportExcess Then
                        'Add difference to gross chargeable
                        LngGrosCh = LngGrosCh + IntTravelAllow - IntSysTransportExcess
                    End If
                    'If SngPassageBenefit > SngExemptPassageBenefit Then
                    '    LngGrosCh = LngGrosCh + (SngPassageBenefit - SngExemptPassageBenefit)
                    'End If
                    LngGrosCh = LngGrosCh + (SngPassageBenefit - 0)
                    'Add Fringe Benefit*************
                    LngGrosCh = LngGrosCh + Rdr("CarAmt") - Rdr("CarEmpCont") + IntInterest
                    LngGrosCh = LngGrosCh + IntTips + IntWriteOff
                    LngGrosCh = LngGrosCh + IntExpenses + IntAccommodation + IntTaxPaid
                    LngGrosCh = LngGrosCh + LngHousePercent
                    ' II)CALC. GROSS CHARGE AND PAYE FOR PREVIOUS MONTHS FROM JULY
                    StrSql = "SELECT * FROM Salary WHERE EmpID='" & Rdr("EmpID") & "' "
                    StrSql = StrSql & "And Date >='" & Format(DteSalDateFrm, "yyyy-MM-dd") & "' "
                    StrSql = StrSql & "And Date < '" & Format(DteSalDateTo, "yyyy-MM-dd") & "'"
                    CmdSal.CommandText = StrSql
                    RdrSal = CmdSal.ExecuteReader
                    While RdrSal.Read
                        IntEDFMonth = IntEDFMonth + 1
                        LngPrevGrosCh = LngPrevGrosCh + RdrSal("BasicSalary")
                        LngPrevGrosCh = LngPrevGrosCh + RdrSal("Overtime")
                        LngPrevGrosCh = LngPrevGrosCh + RdrSal("OtherAllow")
                        LngPrevGrosCh = LngPrevGrosCh + RdrSal("Arrears")
                        LngPrevGrosCh = LngPrevGrosCh + RdrSal("IFSBonus")
                        LngPrevGrosCh = LngPrevGrosCh + RdrSal("MealAllow")
                        LngPrevGrosCh = LngPrevGrosCh + RdrSal("PensionEmp")
                        LngPrevGrosCh = LngPrevGrosCh + RdrSal("NightAllow")
                        LngPrevGrosCh = LngPrevGrosCh + RdrSal("FringeBenefits")
                        LngPrevGrosCh = LngPrevGrosCh + RdrSal("PerfAllow")
                        LngPrevGrosCh = LngPrevGrosCh + RdrSal("MaternityAllow")
                        LngPrevGrosCh = LngPrevGrosCh + RdrSal("RespAllow")
                        LngPrevGrosCh = LngPrevGrosCh + RdrSal("SpecialBenefit")
                        'LngPrevGrosCh = LngPrevGrosCh + RdrSal("MealAllow")
                        SngSickRefund = Math.Round(Val(RdrSal("SickRefund").ToString) * RdrSal("DailyRate"), 2)
                        SngAnualRefund = Math.Round(Val(RdrSal("LocalRefund").ToString) * RdrSal("DailyRate"), 2)

                        LngPrevGrosCh = LngPrevGrosCh + SngAnualRefund
                        LngTravelExempt = RdrSal("TravelExempt")
                        If RdrSal("TravelAllowance") > RdrSal("TravelExempt") Then
                            'Add difference to gross chargeable
                            LngPrevGrosCh = LngPrevGrosCh + (RdrSal("TravelAllowance") - RdrSal("TravelExempt"))
                        End If

                        If RdrSal("PassageBenefit") > RdrSal("ExemptPassageBenefit") Then
                            'Add difference to gross chargeable
                            LngPrevGrosCh = LngPrevGrosCh + (RdrSal("PassageBenefit") - RdrSal("ExemptPassageBenefit"))
                        End If

                        '********Fringe Benefit***********
                        LngPrevGrosCh = LngPrevGrosCh + Val(RdrSal("CarBenefit") & "") - Val(RdrSal("CarEmp") & "")
                        LngPrevGrosCh = LngPrevGrosCh + Val(RdrSal("Interest") & "") + Val(RdrSal("Tips") & "")
                        LngPrevGrosCh = LngPrevGrosCh + Val(RdrSal("WriteOff") & "")
                        LngPrevGrosCh = LngPrevGrosCh + Val(RdrSal("Expenses") & "")
                        LngPrevGrosCh = LngPrevGrosCh + Val(RdrSal("Accommodation") & "") + Val(RdrSal("TaxPaid") & "")
                        LngPrevGrosCh = LngPrevGrosCh + Val(RdrSal("HouseBenefit") & "") - Val(RdrSal("HouseEmp") & "")
                        'If Rdr("Expat") Then
                        'LngPrevPAYE = LngPrevPAYE + RdrSal("TaxBenefit")
                        'Else
                        LngPrevPAYE = LngPrevPAYE + RdrSal("PAYE")
                        'End If
                    End While
                    RdrSal.Close()
                    LngGrosEmol = LngGrosCh + LngPrevGrosCh
                    ' IV) Deduct EDF
                    LngEdf = 0
                    LngEdf = Rdr("TotEdf") * (IntEDFMonth / Rdr("EDFFactor"))
                    Dim LngNetCh As Long = LngGrosEmol - LngEdf
                    Dim SngCummEmolument As Single = 0
                    Dim SngIETNO As Single = 0
                    Dim SngCheckCumm As Single = 0
                    SngIETNO = IntEDFMonth
                    SngCummEmolument = LngGrosEmol
                    SngCheckCumm = CLng(SngCummEmolument / SngIETNO)

                    StrSql = "SELECT * FROM PAYE WHERE PAYESCheme = '" & Rdr("PAYEScheme") & "'"
                    CmdSal.CommandText = StrSql
                    RdrSal = CmdSal.ExecuteReader
                    Dim LngPaye As Long = 0
                    If LngGrosCh > IntSysTaxExempt Then
                        If RdrSal.Read Then
                            If SngCheckCumm >= RdrSal("From-1") And SngCheckCumm <= RdrSal("To-1") Then
                                LngPaye = LngPaye + CLng(LngNetCh * RdrSal("TaxPercent-1") / 100)
                            ElseIf SngCheckCumm >= RdrSal("From-2") Then
                                LngPaye = LngPaye + CLng(LngNetCh * RdrSal("TaxPercent-2") / 100)
                            End If
                        Else
                            LblDate.Text = "PAYE Scheme for " + Rdr("Title") + Rdr("Last") + " not set"
                        End If
                        'VII) Calculate actual PAYE i.e Current - Previousw
                        LngPaye = LngPaye - LngPrevPAYE
                        If LngPaye < 0 Then
                            LngPaye = 0
                        End If
                        LngTaxBenefit = 0
                        'End If

                    End If
                    RdrSal.Close()

                    'BankID
                    StrMCBNonMCB = ""
                    If Not IsDBNull(Rdr("BankID")) Then
                        If Rdr("BankID") = "MCB" Then
                            StrMCBNonMCB = "MCB"
                        Else
                            StrMCBNonMCB = "NON MCB"
                        End If
                        If Rdr("ModePay") <> "Bank" Then
                            StrMCBNonMCB = "NON MCB"
                        End If
                    End If
                    Dim StrBankCode As String
                    StrBankCode = ""
                    If Rdr("ModePay") = "Bank" Then
                        StrBankID = Rdr("BankID")
                        StrSql = "SELECT * FROM Bank WHERE BankID = '" & Rdr("BankID") & "'"
                        CmdSal.CommandText = StrSql
                        RdrSal = CmdSal.ExecuteReader
                        If RdrSal.Read Then
                            StrBankCode = RdrSal("BankCode")
                        End If
                        RdrSal.Close()
                    Else
                        StrBankID = ""
                    End If

                    '' ''Update Employee Master for Age and YOS
                    ' ''Dim CheckDate As Date
                    ' ''CheckDate = Format(CDate(Now), "dd-MMM-yyyy")
                    ' ''SngAge = Math.Round((DateDiff(DateInterval.Day, Rdr("DOB"), CheckDate) / 365), 2)
                    ' ''SngYOS = Math.Round((DateDiff(DateInterval.Day, Rdr("HireDate"), Date.Today) / 365), 2)
                    ' ''StrSql = "Update Employee set Age = " & SngAge & ", YOS = " & SngYOS & " where EmpID = '" & Rdr("EmpID") & "'"
                    ' ''CmdSal.CommandText = StrSql
                    ' ''CmdSal.ExecuteNonQuery()

                    'Now start building insert query
                    StrSql = ""
                    StrSql = "INSERT INTO Salary (EmpID,LastName,FirstName,Month,Date,Year,ModePay,DeptCode,OutletName,"
                    StrSql = StrSql & "BankID,BankCode,AccountNo,MCBNONMCB,Position,JobCode,EmpBasic,BasicSalary,EPZEE, EPZER, "
                    StrSql = StrSql & "HourlyRate,DailyRate,SL,LL,OTHrs15,OTHrs20,OTHrs30,OT15Amt,OT20Amt,OT30Amt,OTHrs1,OT1Amt,"
                    StrSql = StrSql & "Overtime,Days,Locked,OtherAllow,Short,CreditCard,Others,Arrears,IfsBonus,TravelAllowance,"
                    StrSql = StrSql & "NpfEmployee,NpfEmployer,Nsf,"
                    StrSql = StrSql & "NsfEmployee,IVTB,CarBenefit,CarEmp,HouseEmp,HousePercent,TravelExempt,HouseBenefit,"
                    '' ''
                    ''StrSql = StrSql & "PPP,Emol,LoanRepay,LocalRefund,SickRefund,MTAllowAmt,MTDeductAmt,MNTAllowAmt,MNTDeductAmt,"
                    ''StrSql = StrSql & "MEFPAempyee,MEFPAempyer,EWF,Tuition,MEFPADepend,FixAttBonus,TaxPaid"

                    StrSql = StrSql & "PerfAllow,PensionEmp,FringeBenefits,MealDays,MealDed,NightAllow,NiteDays,PHDays,SlTaken,LlTaken,"
                    StrSql = StrSql & "DaysWork,MaternityAllow,EDF,RespAllow,MealAllow,TravelTrips,"
                        '' ''
                        StrSql = StrSql & "PAYE,TotEDF,Age,YOService,YOSAmt,PassageBenefit,ExemptPassageBenefit,LoanRepay1,LoanRepay,Lateness,"
                        StrSql = StrSql & "LatenessAmt,Category,MNSCat,Gender,Status,SpecialBenefit,OtherDeduct)"
                        StrSql = StrSql & "VALUES('"
                    StrSql = StrSql & Rdr("EmpID") & "','"
                    StrSql = StrSql & Rdr("Last") & "','"
                    StrSql = StrSql & Rdr("First") & "','"
                    StrSql = StrSql & CmbMonth.Text & "','"
                    StrSql = StrSql & DteSalDate.ToString("yyyy-MM-dd") & "','"
                    StrSql = StrSql & Val(TxtYear.Text) & "','"
                    StrSql = StrSql & Rdr("ModePay") & "" & "','"
                    StrSql = StrSql & Rdr("DeptCode") & "" & "','"
                    StrSql = StrSql & Rdr("OutletName") & "" & "','"
                    StrSql = StrSql & StrBankID & "" & "','"
                    StrSql = StrSql & StrBankCode & "" & "','"
                    StrSql = StrSql & Rdr("AccountNo") & "" & "','"
                    StrSql = StrSql & StrMCBNonMCB & "" & "','"
                    StrSql = StrSql & Rdr("Position") & "" & "','"
                    StrSql = StrSql & Rdr("JobCode") & "" & "',"
                    StrSql = StrSql & Rdr("BasicSalary") & ","
                    StrSql = StrSql & Math.Round(SngBasic, 2) & "," ' basic salary
                    StrSql = StrSql & Math.Round(SngEPZEE, 2) & ","
                    StrSql = StrSql & Math.Round(SngEPZER, 2) & ","
                    StrSql = StrSql & Math.Round(SngHourlyRate, 2) & ","
                    StrSql = StrSql & Math.Round(SngDailyRate, 2) & ","
                    StrSql = StrSql & Math.Round(SngSick, 2) & ","
                    StrSql = StrSql & Math.Round(SngLocal, 2) & ","
                    StrSql = StrSql & Math.Round(SngOT15, 2) & ","
                    StrSql = StrSql & Math.Round(SngOT20, 2) & ","
                    StrSql = StrSql & Math.Round(SngOT30, 2) & ","
                    StrSql = StrSql & Math.Round(SngOT15Amt, 2) & ","
                    StrSql = StrSql & Math.Round(SngOT20Amt, 2) & ","
                    StrSql = StrSql & Math.Round(SngOT30Amt, 2) & ","
                    StrSql = StrSql & Math.Round(SngOT10, 2) & ","
                    StrSql = StrSql & Math.Round(SngOT10Amt, 2) & ","
                    StrSql = StrSql & Math.Round(SngOvertime, 0) & ","
                    StrSql = StrSql & Math.Round(SngUnpaid, 2) & ","
                    StrSql = StrSql & 0 & "," ' locked 
                    StrSql = StrSql & Math.Round(SngMonthlyOtherAllow, 2) & ","
                    StrSql = StrSql & Math.Round(SngMonthlyShort, 2) & ","
                    StrSql = StrSql & Math.Round(SngMonthlyCreditCard, 2) & ","
                    StrSql = StrSql & Math.Round(SngMedical, 2) & ","
                    StrSql = StrSql & 0 & ","
                    StrSql = StrSql & 0 & ","
                    StrSql = StrSql & IntTravelAllow & ","
                    StrSql = StrSql & IntNpfEmployee & ","
                    StrSql = StrSql & IntNpfEmployer & ","
                    StrSql = StrSql & IntNsfEmployer & ","
                    StrSql = StrSql & IntNsfEmployee & ","
                    StrSql = StrSql & IntIVTB & ","
                    StrSql = StrSql & Rdr("CarAmt") & ","
                    StrSql = StrSql & Rdr("CarEmpCont") & ","
                    StrSql = StrSql & 0 & ","
                    StrSql = StrSql & LngHousePercent & ","
                    StrSql = StrSql & IntSysTransportExcess & ","
                    StrSql = StrSql & 0 & ","
                    ''
                    ''StrSql = StrSql & 0 & ","
                    ''StrSql = StrSql & 0 & ","
                    ''StrSql = StrSql & 0 & ","
                    ''StrSql = StrSql & 0 & ","
                    ''StrSql = StrSql & 0 & ","
                    ''StrSql = StrSql & 0 & ","
                    ''StrSql = StrSql & 0 & ","
                    ''StrSql = StrSql & 0 & ","
                    ''StrSql = StrSql & 0 & ","
                    ''StrSql = StrSql & 0 & ","
                    ''StrSql = StrSql & 0 & ","
                    ''StrSql = StrSql & 0 & ","
                    ''StrSql = StrSql & 0 & ","
                    ''StrSql = StrSql & 0 & ","
                    ''StrSql = StrSql & 0 & ","
                    ''StrSql = StrSql & 0 & ","
                    '' ''
                    StrSql = StrSql & Math.Round(SngProdBonus, 2) & "," & Math.Round(SngPensionEmp, 2) & ","
                    StrSql = StrSql & Math.Round(SngFringe, 2) & ","
                    StrSql = StrSql & 0 & "," & Math.Round(SngNightAllow, 2) & "," & Math.Round(SngTimeNiteAllow, 2) & ","
                    StrSql = StrSql & IntPHDays & "," & Math.Round(SngSick, 2) & ","
                    StrSql = StrSql & Math.Round(SngLocal, 2) & "," & 26 & ","
                    StrSql = StrSql & Math.Round(SngSalaryMeal, 2) & "," & Math.Round(SngMaternityAllow, 2) & ","
                    StrSql = StrSql & Math.Round(LngEdf, 2) & "," & Math.Round(SngRespAllow, 2) & "," & Math.Round(SngMealAllow, 0) & ","
                    StrSql = StrSql & Math.Round(IntDaysPresent, 2) & ","
                    '' ''
                    StrSql = StrSql & IIf(LngPaye < IntSysMinPAYE, 0, LngPaye) & ","
                    StrSql = StrSql & Rdr("TotEdf") & ","
                    StrSql = StrSql & SngAge & ","
                    StrSql = StrSql & SngYOS & ","
                    StrSql = StrSql & SngMonthlyYOSAmt & ","
                    StrSql = StrSql & SngPassageBenefit & ","
                    StrSql = StrSql & SngExemptPassageBenefit & ","
                    StrSql = StrSql & SngLoanRepay1 & ","
                    StrSql = StrSql & SngLoanRepay & ","
                    StrSql = StrSql & SngLateness & ","
                    StrSql = StrSql & SngLatenessAmt & ",'"
                    StrSql = StrSql & StrCat & "'" & ",'"
                    StrSql = StrSql & StrCatMNS & "'" & ",'"
                    StrSql = StrSql & Rdr("Gender") & "'" & ",'"
                    StrSql = StrSql & StrStatus & "', "
                    StrSql = StrSql & Math.Round(SngExGratia, 2) & ","
                    StrSql = StrSql & Math.Round(SngTotalDeduct, 2) & ")"
                    CmdSal.CommandText = StrSql
                    ''Try
                    CmdSal.ExecuteNonQuery()

                    BStatusError = False
                    StrStatusError = ""
                End If
                End If
            End While
            'Trans.Commit()

            'Process PRGF
            strPRGF = modPRFG.calculationPRGF(CmbMonth.Text, TxtYear.Text, Con, Session("ConnString"), TxtEmpID.Text)
            If (strPRGF <> "1") Then
                LblSalaryPro.Visible = True
                LblSalaryPro.Text = strPRGF
                Exit Sub
            End If

        Catch ex As Exception
            'Trans.Rollback() LblError.Text = "error : " + ex.Message
            BStatusError = True
            StrStatusError = "Salary Error for EmpID " & Rdr("EmpID") + " " + ex.Message
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
        End Try
    End Sub

    Protected Sub CmbCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbCategory.SelectedIndexChanged
        ' **** As per the category display relevant controls
        ' ''Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        ' ''Dim DteDate As Date = CDate(TxtDate.Text).ToString("dd-MM-yyyy")
        ' ''Dim DteFrom As Date = CDate(TxtDate.Text).ToString("dd-MM-yyyy")
        ' ''If CmbCategory.SelectedIndex = 0 Then
        ' ''    TxtDate.Visible = True
        ' ''    CmbMonth.Visible = True
        ' ''    If CmbMonth.Text = "Bonus" Then
        ' ''        DteFrom = CDate("01/" & Format(CmbMonth.SelectedIndex, "00") & "/" & TxtYear.Text)
        ' ''    Else
        ' ''        If CmbMonth.Text = "January" Then
        ' ''            DteFrom = CDate("20/12" + "/" + CStr((Val(TxtYear.Text) - 1)))
        ' ''        Else
        ' ''            DteFrom = CDate("20/" & Format(CmbMonth.SelectedIndex, "00") & "/" & TxtYear.Text)
        ' ''        End If
        ' ''    End If
        ' ''    TxtFromDate.Text = DteFrom.ToString("dd/MM/yyyy")

        ' ''    LblDate.Text = "Month From " & TxtFromDate.Text & " to " & TxtDate.Text
        ' ''End If
        ' **** As per the category display relevant controls
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim DteDate As Date = CDate(TxtDate.Text)
        Dim DteFrom As Date = CDate(TxtFromDate.Text)

        TxtDate.Visible = True
        CmbMonth.Visible = True
        'If CmbMonth.Text = "Bonus" Then
        '    DteFrom = CDate("01/" & Format(CmbMonth.SelectedIndex, "00") & "/" & TxtYear.Text)
        'Else
        '    DteFrom = CDate("01/" & Format(CmbMonth.SelectedIndex + 1, "00") & "/" & TxtYear.Text)
        'End If

        TxtFromDate.Text = DteFrom.ToString("dd/MM/yyyy")
        TxtDate.Text = DteDate.ToString("dd/MM/yyyy")
        LblDate.Text = "Month From " & TxtFromDate.Text & " to " & TxtDate.Text

    End Sub

    Protected Sub CmbMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbMonth.SelectedIndexChanged
        GetDate()
    End Sub

    Protected Sub TxtYear_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtYear.TextChanged
        GetDate()
    End Sub

    Protected Sub TxtDate_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtDate.TextChanged
        CmbCategory_SelectedIndexChanged(Me, e)
        ResetCheck()
    End Sub
    Protected Sub TxtFromDate_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtFromDate.TextChanged
        CmbCategory_SelectedIndexChanged(Me, e)
        ResetCheck()
    End Sub
    Function IsLeapYear(ByVal inYear As Integer) As Boolean
        IsLeapYear = ((inYear Mod 4 = 0) _
                   And (inYear Mod 100 <> 0) _
                    Or (inYear Mod 400 = 0))
    End Function
    Protected Sub Bonus()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")

        Dim Cmd As New OleDb.OleDbCommand
        Dim Rdr As OleDb.OleDbDataReader

        Dim CmdPrevSal As New OleDb.OleDbCommand
        Dim RdrPrevSal As OleDb.OleDbDataReader
        Dim CmdSal As New OleDb.OleDbCommand
        Dim RdrSal As OleDb.OleDbDataReader
        Dim CmdDelete As New OleDb.OleDbCommand
        Dim LngSysMinPAYE As Long = 0
        Dim SngTravel_Exepmt As Single = 0
        Dim LngBonus As Long
        Dim LngBonusExpat As Long
        Dim DteSalDate As Date
        Dim DteEndDate As Date
        Dim IntYear As Integer
        Dim IntBonusDays As Integer

        CmdPrevSal.Connection = Con
        CmdSal.Connection = Con
        CmdDelete.Connection = Con
        Cmd.Connection = Con

        DteEndDate = CDate("31/12/" & TxtYear.Text)
        DteSalDate = CDate("20/" & Format(CmbMonth.SelectedIndex, "00") & "/" & TxtYear.Text)

        StrCat = "Monthly"
        StrCatMNS = "M"
        StrStatus = "NORMAL"

        IntYear = Val(TxtYear.Text)
        If IsLeapYear(IntYear) Then
            IntYear = 366
        Else
            IntYear = 365
        End If

        If TxtEmpID.Text = "ALL" Then
            StrSql = "Select * FROM Employee WHERE Working = 1"
        Else
            StrSql = "Select * FROM Employee WHERE Working = 1 And EmpID = '" & TxtEmpID.Text & "'"
        End If
        If CmbOutlet.Text <> "ALL" Then
            StrSql = StrSql & " AND Outletname = '" & CmbOutlet.Text & "'"
        End If

        Cmd.CommandText = "Select *From [System]"
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            LngSysMinPAYE = Rdr("TaxExempt")
            SngTravel_Exepmt = Rdr("TransportExcess")
        End If
        Rdr.Close()

        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader

        'Delete Bonus Records if exists
        If TxtEmpID.Text = "ALL" Then
            StrSql = "Delete FROM Salary WHERE Month = 'Bonus'"
            StrSql = StrSql & " AND [Date] = '" & DteSalDate.ToString("dd-MMM-yyyy") & "'"
        Else
            StrSql = "Delete FROM Salary WHERE Month = 'Bonus' And EmpID = '" & TxtEmpID.Text & "'"
            StrSql = StrSql & " AND [Date] = '" & DteSalDate.ToString("dd-MMM-yyyy") & "'"
        End If
        If CmbOutlet.Text <> "ALL" Then
            StrSql = StrSql & " AND Outletname = '" & CmbOutlet.Text & "'"
        End If
        CmdDelete.CommandText = StrSql
        CmdDelete.ExecuteNonQuery()
        While Rdr.Read
            If Rdr("EmpID") = "HUR01" Then
                LngBonusExpat = 0
            End If
            LngBonusExpat = 0
            LngBonus = 0

            CmdPrevSal.CommandText = "Select * From Salary Where EmpID = '" & Rdr("EmpID") & "' And year = " & TxtYear.Text & " And [Date] < '" & DteSalDate.ToString("yyyy-MM-dd") & "'"
            RdrPrevSal = CmdPrevSal.ExecuteReader
            While RdrPrevSal.Read
                LngBonus = LngBonus + RdrPrevSal("BasicSalary")
                LngBonus = LngBonus + RdrPrevSal("Overtime")
            End While
            RdrPrevSal.Close()
            LngBonus = LngBonus / 12
            LngBonusExpat = Math.Round(Val(Rdr("BasicSalary").ToString) / 12, 2)
            LngBonus = LngBonus + LngBonusExpat

            ' '' ''Bonus Calculation for Employee more than 1 year
            IntBonusDays = 0
            IntBonusDays = Math.Round((DateDiff(DateInterval.Day, Rdr("HireDate"), DteEndDate)), 2)
            ''Paid Bonus amount the highest
            If IntBonusDays >= IntYear Then
                If Rdr("BasicSalary") > LngBonus Then
                    LngBonus = Rdr("BasicSalary")
                End If
            End If

            'PAYE Calculation
            Dim LngGrosCh As Long
            Dim LngEdf As Long
            LngGrosCh = 0
            Dim LngPrevGrosCh As Long = 0
            Dim LngGrosChEOY As Long = 0
            Dim LngGrosEmol As Long = 0
            Dim IntEDFMonth As Long = 1
            Dim LngPrevPAYE As Long = 0
            Dim SngSickRefund As Single = 0
            Dim SngAnualRefund As Single = 0
            Dim LngTravelExempt As Long = 0
            Dim LngTaxBenefit As Long = 0
            Dim DteSalDateFrm As Date
            Dim DteSalDateTo As Date
            Dim LngChargeableExpat As Long = 0
            Dim LngTaxEpat As Long = 0


            If DteSalDate < CDate("31/12/2009") Or DteSalDate >= CDate("28/07/2015") Then
                If Month(DteSalDate) >= 7 Then
                    DteSalDateFrm = CDate("28/07" + "/" + TxtYear.Text)
                    DteSalDateTo = DteSalDate
                Else
                    DteSalDateFrm = CDate("28/07" + "/" + CStr((Val(TxtYear.Text) - 1)))
                    DteSalDateTo = DteSalDate
                End If
            Else
                DteSalDateFrm = CDate("28/01" + "/" + TxtYear.Text)
                DteSalDateTo = DteSalDate
            End If
            ' I)CALC. GROSS CHARGE FOR CURRENT MONTH
            LngGrosCh = LngBonus
            ' II)CALC. GROSS CHARGE AND PAYE FOR PREVIOUS MONTHS FROM JULY
            StrSql = "SELECT * FROM Salary WHERE EmpID='" & Rdr("EmpID") & "' "
            StrSql = StrSql & "And Date >='" & Format(DteSalDateFrm, "yyyy-MM-dd") & "' "
            StrSql = StrSql & "And Date < '" & Format(DteSalDateTo, "yyyy-MM-dd") & "'"
            CmdSal.CommandText = StrSql
            RdrSal = CmdSal.ExecuteReader
            While RdrSal.Read
                IntEDFMonth = IntEDFMonth + 1
                LngPrevGrosCh = LngPrevGrosCh + RdrSal("BasicSalary")
                LngPrevGrosCh = LngPrevGrosCh + RdrSal("Overtime")
                LngPrevGrosCh = LngPrevGrosCh + RdrSal("OtherAllow")
                LngPrevGrosCh = LngPrevGrosCh + RdrSal("Arrears")
                LngPrevGrosCh = LngPrevGrosCh + RdrSal("IFSBonus")
                'LngPrevGrosCh = LngPrevGrosCh + RdrSal("MealAllow")
                LngPrevGrosCh = LngPrevGrosCh + RdrSal("PensionEmp")
                LngPrevGrosCh = LngPrevGrosCh + RdrSal("NightAllow")
                LngPrevGrosCh = LngPrevGrosCh + RdrSal("FringeBenefits")
                LngPrevGrosCh = LngPrevGrosCh + RdrSal("PerfAllow")
                LngPrevGrosCh = LngPrevGrosCh + RdrSal("MaternityAllow")
                LngPrevGrosCh = LngPrevGrosCh + RdrSal("RespAllow")
                LngPrevGrosCh = LngPrevGrosCh + RdrSal("MealAllow")
                SngSickRefund = Math.Round(Val(RdrSal("SickRefund").ToString) * RdrSal("DailyRate"), 2)
                SngAnualRefund = Math.Round(Val(RdrSal("LocalRefund").ToString) * RdrSal("DailyRate"), 2)
                LngPrevGrosCh = LngPrevGrosCh + SngAnualRefund
                LngTravelExempt = RdrSal("TravelExempt")
                If RdrSal("TravelAllowance") > RdrSal("TravelExempt") Then
                    'Add difference to gross chargeable
                    LngPrevGrosCh = LngPrevGrosCh + (RdrSal("TravelAllowance") - RdrSal("TravelExempt"))
                End If
                If RdrSal("PassageBenefit") > RdrSal("ExemptPassageBenefit") Then
                    LngPrevGrosCh = LngPrevGrosCh + (RdrSal("PassageBenefit") - RdrSal("ExemptPassageBenefit"))
                End If
                '********Fringe Benefit***********
                LngPrevGrosCh = LngPrevGrosCh + Val(RdrSal("CarBenefit") & "") - Val(RdrSal("CarEmp") & "")
                LngPrevGrosCh = LngPrevGrosCh + Val(RdrSal("Interest") & "") + Val(RdrSal("Tips") & "")
                LngPrevGrosCh = LngPrevGrosCh + Val(RdrSal("WriteOff") & "")
                LngPrevGrosCh = LngPrevGrosCh + Val(RdrSal("Expenses") & "")
                LngPrevGrosCh = LngPrevGrosCh + Val(RdrSal("Accommodation") & "") + Val(RdrSal("TaxPaid") & "")
                LngPrevGrosCh = LngPrevGrosCh + Val(RdrSal("HouseBenefit") & "") - Val(RdrSal("HouseEmp") & "")
                LngPrevPAYE = LngPrevPAYE + RdrSal("PAYE")
            End While
            RdrSal.Close()
            If CmbMonth.Text = "Bonus" And Val(TxtYear.Text) = 2011 Then
                IntEDFMonth = 1
                LngPrevGrosCh = 0
                LngPrevPAYE = 0
            End If
            LngGrosEmol = LngGrosCh + LngPrevGrosCh
            ' IV) Deduct EDF
            LngEdf = 0
            LngEdf = Rdr("TotEdf") * (IntEDFMonth / Rdr("EDFFactor"))
            Dim LngNetCh As Long = LngGrosEmol - LngEdf
            Dim SngCummEmolument As Single = 0
            Dim SngIETNO As Single = 0
            Dim SngCheckCumm As Single = 0
            SngIETNO = IntEDFMonth
            SngCummEmolument = LngGrosEmol
            SngCheckCumm = CLng(SngCummEmolument / SngIETNO)

            StrSql = "SELECT * FROM PAYE WHERE PAYESCheme = '" & Rdr("PAYEScheme") & "'"
            CmdSal.CommandText = StrSql
            RdrSal = CmdSal.ExecuteReader
            Dim LngPaye As Long = 0
            If LngGrosCh > LngSysMinPAYE Then
                If RdrSal.Read Then
                    If SngCheckCumm >= RdrSal("From-1") And SngCheckCumm <= RdrSal("To-1") Then
                        LngPaye = LngPaye + CLng(LngNetCh * RdrSal("TaxPercent-1") / 100)
                    ElseIf SngCheckCumm >= RdrSal("From-2") Then
                        LngPaye = LngPaye + CLng(LngNetCh * RdrSal("TaxPercent-2") / 100)
                    End If
                Else
                    LblDate.Text = "PAYE Scheme for " + Rdr("Title") + Rdr("Last") + " not set"
                End If
                LngPaye = LngPaye - LngPrevPAYE
                If LngPaye < 0 Then
                    LngPaye = 0
                End If
                LngTaxBenefit = 0
                'End If
            End If
            RdrSal.Close()

            'BankID
            Dim StrMCBNonMCB As String
            Dim DeptCode As String
            Dim AccountNo As String
            Dim Position As String
            Dim ModePay As String
            Dim BankID As String

            StrMCBNonMCB = ""
            If IsDBNull(Rdr("BankID")) Then
                StrMCBNonMCB = ""
                BankID = ""
            Else
                BankID = Rdr("BankID")
                If Rdr("BankID") = "MCB" Then
                    StrMCBNonMCB = "MCB"
                Else
                    StrMCBNonMCB = "NON MCB"
                End If
            End If
            If IsDBNull(Rdr("ModePay")) Then
                ModePay = ""
            Else
                ModePay = Rdr("ModePay")
                If Trim(Rdr("ModePay")) <> "Bank" Then
                    StrMCBNonMCB = "NON MCB"
                End If
            End If

            Dim StrBankCode As String
            StrBankCode = ""
            If Rdr("ModePay") = "Bank" Then
                StrBankID = Rdr("BankID")
                StrSql = "SELECT * FROM Bank WHERE BankID = '" & Rdr("BankID") & "'"
                CmdSal.CommandText = StrSql
                RdrSal = CmdSal.ExecuteReader
                If RdrSal.Read Then
                    StrBankCode = RdrSal("BankCode")
                End If
                RdrSal.Close()
            Else
                StrBankID = ""
            End If

            If IsDBNull(Rdr("DeptCode")) Then
                DeptCode = ""
            Else
                DeptCode = Rdr("DeptCode")
            End If
            If IsDBNull(Rdr("AccountNo")) Then
                AccountNo = ""
            Else
                AccountNo = Rdr("AccountNo")
            End If
            If IsDBNull(Rdr("Position")) Then
                Position = ""
            Else
                Position = Rdr("Position")
            End If

            ''''
            StrSql = "INSERT INTO Salary (EmpID,LastName,FirstName,[Month],[Date],[Year],ModePay,DeptCode,OutletName,"
            StrSql = StrSql & "BankID,BankCode,AccountNo,MCBNONMCB,Position,JobCode,EmpBasic,BasicSalary,EPZEE,EPZER,"
            StrSql = StrSql & "HourlyRate,DailyRate,SL,LL,OTHrs15,OTHrs20,OTHrs30,OT15Amt,OT20Amt,OT30Amt,OTHrs1,OT1Amt,"
            StrSql = StrSql & "Overtime,[Days],Locked,OtherAllow,Others,Arrears,IfsBonus,TravelAllowance,"
            StrSql = StrSql & "NpfEmployee,NpfEmployer,Nsf,"
            StrSql = StrSql & "NsfEmployee,IVTB,CarBenefit,CarEmp,HouseEmp,HousePercent,TravelExempt,HouseBenefit,"
            StrSql = StrSql & "PerfAllow,PensionEmp,FringeBenefits,MealDays,MealDed,NightAllow,NiteDays,PHDays,SlTaken,LlTaken,"
            StrSql = StrSql & "DaysWork,MaternityAllow,EDF,RespAllow,MealAllow,TravelTrips,"
            StrSql = StrSql & "PAYE,PassageBenefit,ExemptPassageBenefit,TotEDF,Age,LoanRepay,LoanRepay1,AdvTotalAmt,Short,CreditCard,Category,MNSCat,Gender,Lateness,LatenessAmt,Status)"
            StrSql = StrSql & "VALUES('"
            StrSql = StrSql & Rdr("EmpID") & "','"
            StrSql = StrSql & Rdr("Last") & "','"
            StrSql = StrSql & Rdr("First") & "','"
            StrSql = StrSql & CmbMonth.Text & "','"
            StrSql = StrSql & DteSalDate.ToString("yyyy-MM-dd") & "','"
            StrSql = StrSql & Val(TxtYear.Text) & "','"
            StrSql = StrSql & ModePay & "" & "','"
            StrSql = StrSql & DeptCode & "" & "','"
            StrSql = StrSql & Rdr("OutletName") & "" & "','"
            StrSql = StrSql & StrBankID & "" & "','"
            StrSql = StrSql & StrBankCode & "" & "','"
            StrSql = StrSql & AccountNo & "" & "','"
            StrSql = StrSql & StrMCBNonMCB & "" & "','"
            StrSql = StrSql & Position & "" & "','"
            StrSql = StrSql & Rdr("JobCode") & "" & "',"
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & "," ' basic salary
            StrSql = StrSql & 0 & "," 'EPZ ee
            StrSql = StrSql & 0 & "," 'EPZ er

            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & "," ' OT30Amt
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","

            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & "," ' locked
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & LngBonus & "," ' IFS Bonus
            StrSql = StrSql & 0 & "," ' travel allowance

            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & "," ' nsf

            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & SngTravel_Exepmt & ","
            StrSql = StrSql & 0 & "," 'HouseBenefit

            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & "," 'SLtaken

            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & Math.Round(LngEdf, 2) & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & "," ' TravelTrips

            StrSql = StrSql & LngPaye & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & Rdr("TotEdf") & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & "," 'loan repay
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ",'"
            StrSql = StrSql & StrCat & "','"
            StrSql = StrSql & StrCatMNS & "','"
            StrSql = StrSql & Rdr("Gender") & "',"
            StrSql = StrSql & 0 & ","
            StrSql = StrSql & 0 & ",'"
            StrSql = StrSql & StrStatus & "')"
            CmdSal.CommandText = StrSql

            Try
                CmdSal.ExecuteNonQuery()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End While
        Rdr.Close()
    End Sub

    Protected Sub ButAuthorised_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButAuthorised.Click
        Dim CmdAth As New OleDb.OleDbCommand
        CmdAth.Connection = Con

        LblAuthorised.Visible = False
        LblAuthorised.Text = ""

        If TxtEmpID.Text <> "ALL" Then
            CmdAth.CommandText = "update Employee set Recruit = 1 where EmpID = '" & Trim(TxtEmpID.Text) & "'"
        Else
            If CmbOutlet.Text = "ALL" Then
                'CmdAth.CommandText = "update Employee set Recruit = 1"
                LblAuthorised.Visible = True
                LblAuthorised.Text = "Please Select Outlet."
                Exit Sub
            Else
                CmdAth.CommandText = "update Employee set Recruit = 1 where OutletName = '" & Trim(CmbOutlet.Text) & "'"
            End If
        End If
        CmdAth.ExecuteNonQuery()
        LblAuthorised.Visible = True
        LblAuthorised.Text = "Employee Records Authorised."

        Dim StrAudit As String
        StrAudit = ""
        StrAudit = "ALL Records Authorised Salary " & Trim(CmbMonth.Text) & " " & TxtYear.Text
        CmdAth.CommandText = "Insert Into SalMasterAudit values('" & Session("UserID") & "','Authorised Record','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
        CmdAth.ExecuteNonQuery()
        Con.Close()
        Call Page_Load(Me, e)
    End Sub
End Class