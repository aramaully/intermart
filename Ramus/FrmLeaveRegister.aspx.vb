﻿Public Partial Class FrmLeaveRegister
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            TxtFrom.Text = "01/01/" & Date.Today.Year
            TxtTo.Text = "31/12/" & Date.Today.Year
            TxtYear.Text = Date.Today.Year
            TxtEmpID.Text = "ALL"
            Cmd.Connection = Con
            Cmd.CommandText = "Select OutletName from Outlet Order by OutletCode"
            Rdr = Cmd.ExecuteReader
            CmbDept.Items.Add("ALL")
            While Rdr.Read
                CmbDept.Items.Add(Rdr("OutletName"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ClearAll()
        TxtFirst.Text = ""
        TxtLast.Text = ""
        TxtClockID.Text = ""
        TxtCategory.Text = ""
        TxtTitle.Text = ""
        TxtGender.Text = ""
        ImgPhoto.ImageUrl = Nothing
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ev As New System.EventArgs
        If e.Item.CommandName = "New" Then
            ClearAll()
            TxtEmpID.Text = "ALL"
        End If
        If e.Item.CommandName = "Print" Then
            Session("FieldCount") = 0
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Dim StrSelFormula As String
            Dim StrDateFrom As String
            Dim StrDateTo As String
            StrDateFrom = Format(TxtFrom.Text, "yyyy,mm,dd")
            StrDateTo = Format(TxtTo.Text, "yyyy,mm,dd")
            If ChkNew.Checked Then
                Session("ReportFile") = "RNewEmployee.rpt"
                StrSelFormula = "{Employee.HireDate} >= Date(" & Format(CDate(TxtFrom.Text), "yyyy,MM,dd") & ") "
                StrSelFormula = StrSelFormula & " and {Employee.OutletName} = '" & Trim(Session("Level")) & "' and {Employee.HireDate} <= Date(" & Format(CDate(TxtTo.Text), "yyyy,MM,dd") & ") "
            ElseIf ChkLeft.Checked Then
                Session("ReportFile") = "RLeftEmployee.rpt"
                StrSelFormula = "{Employee.LastDay} >= Date(" & Format(CDate(TxtFrom.Text), "yyyy,MM,dd") & ") "
                StrSelFormula = StrSelFormula & " and {Employee.OutletName} = '" & Trim(Session("Level")) & "' and {Employee.LastDay} <= Date(" & Format(CDate(TxtTo.Text), "yyyy,MM,dd") & ") "
            Else
                If ChkLeaveSummary.Checked Then
                    Session("ReportFile") = "RLeaveSummary.rpt"
                Else
                    Session("ReportFile") = "RLEAVE.rpt"
                End If
                StrSelFormula = "{Leave.Date} >= Date(" & Format(CDate(TxtFrom.Text), "yyyy,MM,dd") & ") "
                StrSelFormula = StrSelFormula & " And {Leave.Date} <= Date(" & Format(CDate(TxtTo.Text), "yyyy,MM,dd") & ") "
                If TxtEmpID.Text <> "ALL" Then
                    StrSelFormula = StrSelFormula & "And {Leave.EmpID} = '" & TxtEmpID.Text & "'"
                End If
                If CmbDept.Text <> "ALL" Then
                    StrSelFormula = StrSelFormula & "And {Employee.OutletName} = '" & CmbDept.Text & "'"
                End If
            End If
            Session("SelFormula") = StrSelFormula
            Session("RepHead") = "Leave Report From " & TxtFrom.Text & " to " & TxtTo.Text
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        End If
            If e.Item.CommandName = "Delete" Then
                Try
                    Cmd.Connection = Con
                    Cmd.CommandText = "DELETE FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
                    Cmd.ExecuteNonQuery()
                    TxtEmpID_TextChanged(Me, ev)
                Catch ex As Exception
                    ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
                End Try
            End If
            If e.Item.CommandName = "Next" Then
                Try
                    Cmd.Connection = Con
                    Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID > '" & TxtEmpID.Text & "'"
                    TxtEmpID.Text = Cmd.ExecuteScalar
                    TxtEmpID_TextChanged(Me, ev)
                Catch ex As Exception

                End Try

            End If
            If e.Item.CommandName = "Previous" Then
                Try
                    Cmd.Connection = Con
                    Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID < '" & TxtEmpID.Text & "' ORDER BY EmpID DESC"
                    TxtEmpID.Text = Cmd.ExecuteScalar
                    TxtEmpID_TextChanged(Me, ev)
                Catch ex As Exception

                End Try
            End If
            If e.Item.CommandName = "Find" Then
                Session.Add("PstrCode", TxtEmpID.Text)
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
            End If
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        If TxtEmpID.Text = "ALL" Then Exit Sub
        If Rdr.Read Then
            On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
            TxtTitle.Text = Rdr("Title")
            TxtCategory.Text = Rdr("Category")
            TxtGender.Text = Rdr("Gender")
            TxtCategory.Text = Rdr("Category")
            ChkClocked.Checked = IIf(Rdr("Clocked"), True, False)
            TxtClockID.Text = Rdr("BatchNo")
            Session.Add("EmpID", Rdr("EmpID"))
            ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & Rdr("EmpID")
            'Personal Details
        Else
            Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(1))
            ToolBar1_ItemClick(Me, ev)
            'ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('Invalid Employee ID','Error');</script>", False)
        End If
        Rdr.Close()
    End Sub

    Protected Sub ChkNew_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ChkNew.CheckedChanged
        If ChkNew.Checked Then
            ChkLeft.Checked = False
        End If
    End Sub

    Protected Sub ChkCrossTab_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ChkLeft.CheckedChanged
        If ChkLeft.Checked Then
            ChkNew.Checked = False
        End If
    End Sub
End Class