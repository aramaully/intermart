﻿Public Partial Class FrmDenom
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            FillGrid()
        End If
    End Sub

    Protected Sub FillGrid()
        Cmd.Connection = Con
        StrSql = "SELECT * FROM Denomination"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvLoan.DataSource = Rdr
        GdvLoan.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "New" Then
            TxtDenomination.Text = ""
            TxtDenomination.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Dim Trans As OleDb.OleDbTransaction
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
            Cmd.Connection = Con
            Cmd.Transaction = Trans
            Cmd.CommandText = "DELETE FROM Denomination WHERE Denomination = " & Val(TxtDenomination.Text)
            Try
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO Denomination (Denomination) VALUES("
                StrSql = StrSql & Val(TxtDenomination.Text) & ")"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                Trans.Commit()
                TxtDenomination.Text = ""
                FillGrid()
            Catch ex As Exception
                Trans.Rollback()
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Denomination WHERE Denomination =" & Val(TxtDenomination.Text)
            Try
                Cmd.ExecuteNonQuery()
                TxtDenomination.Text = ""
                FillGrid()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
    End Sub

    Private Sub GdvLoan_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvLoan.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvLoan, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvLoan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvLoan.SelectedIndexChanged
        TxtDenomination.Text = GdvLoan.SelectedRow.Cells(0).Text
    End Sub

    Private Sub GdvLoan_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvLoan.SelectedIndexChanging
        GdvLoan.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvLoan.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvLoan.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub
End Class