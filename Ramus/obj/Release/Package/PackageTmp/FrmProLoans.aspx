﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmProLoans.aspx.vb" Inherits="Ramus.FrmProLoans" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<html>
<head id="Head1" runat="server">
    <title>Process Employee Loans</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.
            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Process Employee Loans'; //Set the name
            w.Width = 775; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 500;            
            w.Top = 10;
            w.Left = 10;
            w.Show(); //Show the window
        };
        function ShowFind() {
            var FindWindow = window.open("FindEmployee.aspx", "FindEmployee", "width=565,height=435,top=200, left=575");
        };
        function FunctionConfirm(Tlb, TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Save") {
                var validated = Page_ClientValidate('Group1');
                if (validated) {
                    Index = '2';
                    w.Confirm("Sure to Save", "Confirm", null, SaveFunction);
                }
            }
            else if (TlbCmd == "Find") {
                __doPostBack('ToolBar1', '1');
            }
            else if (TlbCmd == "New") {
                __doPostBack('ToolBar1', '0');
            }
            else if (TlbCmd == "Next") {
                __doPostBack('ToolBar1', '5');
            }
            else if (TlbCmd == "Previous") {
                __doPostBack('ToolBar1', '4');
            }
        }
        function SaveFunction(Sender, RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        }
        function FunPic() {
            var myArgs = 'nothing';
            myargs = window.showModalDialog('Snap.aspx', myArgs, "dialogHeight:350px;dialogWidth:650px");
            if (myArgs == 'nothing') {
                PageMethods.AjaxSetSession("True");
            }
        }
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</head>
<body onload="init();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px;
                height: 32px; width: 32px;" runat="server" BorderStyle="None" Visible="True">
                <div id="divImage" style="display: none">
                    <asp:Image ID="img1" runat="server" ImageUrl="/Images/Wait.gif" />
                </div>
            </asp:Panel>
            <div id="toolbar" style="width: 100%;">
                <eo:ToolBar ID="ToolBar1" runat="server" BackgroundImage="00100103" ClientSideOnItemClick="FunctionConfirm"
                    BackgroundImageLeft="00100101" BackgroundImageRight="00100102" SeparatorImage="00100104"
                    Width="100%">
                    <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 1px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: #335ea8 1px solid; background-color: #99afd4; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <Items>
                        <eo:ToolBarItem CommandName="New" ImageUrl="00101001" ToolTip="New">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Find" ImageUrl="~/images/findHS.png" ToolTip="Find Employee">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Save" ImageUrl="00101003" ToolTip="Save">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Previous" ImageUrl="~/images/NavBack.png" ToolTip="Previous Record">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Next" ImageUrl="~/images/NavForward.png" ToolTip="Next Record">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Help" ImageUrl="~/images/help.png" ToolTip="Help">
                        </eo:ToolBarItem>
                    </Items>
                    <ItemTemplates>
                        <eo:ToolBarItem Type="Custom">
                            <NormalStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <HoverStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <DownStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="DropDownMenu">
                            <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                            <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; background-image: url(00100106); background-position-x: right;" />
                            <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: none; background-color:transparent; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                        </eo:ToolBarItem>
                    </ItemTemplates>
                    <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                </eo:ToolBar>
            </div>
            <div id="Content" style="width: 100%; height: 100%; padding: 2px;
                border-right: solid 1px #cddaea;">                
            <asp:Panel ID="Panel10" runat="server" BorderStyle="Inset" BorderWidth="1">
                <table class="style1">
                    <tr>
                        <td>
                            Emp ID
                        </td>
                        <td>
                            <asp:TextBox ID="TxtEmpID" runat="server" AutoPostBack="True"></asp:TextBox>
                        </td>
                        <td>
                            Title
                        </td>
                        <td>
                            <asp:TextBox ID="TxtTitle" runat="server" Width="50px" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            Gender
                        </td>
                        <td>
                            <asp:TextBox ID="TxtGender" runat="server" Width="50px" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td align="right" rowspan="4" valign="top">
                            <asp:Image ID="ImgPhoto" runat="server" Height="97px" Width="87px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Clocked</td>
                        <td>
                            <asp:CheckBox ID="ChkClocked" runat="server" Text="Yes" Enabled="False" />
                        </td>
                        <td>
                            Clock ID</td>
                        <td>
                            <asp:TextBox ID="TxtClockID" runat="server" Width="100px"></asp:TextBox>
                        </td>
                        <td>
                            Category</td>
                        <td>
                            <asp:TextBox ID="TxtCategory" runat="server" Width="100px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Last Name</td>
                        <td colspan="3">
                            <asp:TextBox ID="TxtLast" runat="server" Width="400px" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            Basic</td>
                        <td>
                            <asp:TextBox ID="TxtBasic" runat="server" Width="100px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            First Name</td>
                        <td colspan="4">
                            <asp:TextBox ID="TxtFirst" runat="server" Width="400px" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                </asp:Panel>                
                <asp:Panel ID="PnlContent" runat="server" BorderStyle="Inset" BorderWidth="1">
                    <table class="style1">
                        <tr>
                            <td colspan="6">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Small" 
                                    ForeColor="#0033CC" Text="Loan Details"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Loan ID</td>
                            <td colspan="2">
                                <asp:TextBox ID="TxtLoanID" runat="server" AutoPostBack="True"></asp:TextBox>
                            </td>
                            <td>
                                Type</td>
                            <td colspan="2">
                                <asp:DropDownList ID="CmbType" runat="server" Width="200px" 
                                    ValidationGroup="Group1">
                                </asp:DropDownList>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                    ControlToValidate="CmbType" ErrorMessage="Select Type of Loan" 
                                    Operator="NotEqual" ValidationGroup="Group1" ValueToCompare="Select..."></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Date Taken</td>
                            <td>
                                <asp:TextBox ID="TxtDate" runat="server"></asp:TextBox>
                                <cc1:CalendarExtender ID="TxtDate_CalendarExtender" runat="server" 
                                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtDate">
                                </cc1:CalendarExtender>
                            </td>
                            <td colspan="3">
                                Description</td>
                            <td>
                                <asp:TextBox ID="TxtDescription" runat="server" Width="300px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Amount</td>
                            <td>
                                <asp:TextBox ID="TxtAmount" runat="server"></asp:TextBox>
                            </td>
                            <td colspan="3">
                                Duration</td>
                            <td valign="middle">
                                <asp:TextBox ID="TxtDuration" runat="server"></asp:TextBox>
                                Months</td>
                        </tr>
                        <tr>
                            <td>
                                Rate of Interest</td>
                            <td>
                                <asp:TextBox ID="TxtInterest" runat="server"></asp:TextBox>
                            </td>
                            <td colspan="3">
                                Mode of Payment</td>
                            <td>
                                <asp:DropDownList ID="CmbModePay" runat="server" ValidationGroup="Group1" 
                                    Width="150px">
                                    <asp:ListItem>Select...</asp:ListItem>
                                    <asp:ListItem>Monthly</asp:ListItem>
                                    <asp:ListItem>Quarterly</asp:ListItem>
                                    <asp:ListItem>Yearly</asp:ListItem>
                                </asp:DropDownList>
                                <asp:CompareValidator ID="CompareValidator2" runat="server" 
                                    ControlToValidate="CmbModePay" ErrorMessage="Select Mode of Payment" 
                                    Operator="NotEqual" ValidationGroup="Group1" ValueToCompare="Select..."></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Paid Todate</td>
                            <td colspan="2">
                                <asp:TextBox ID="TxtPaidTodate" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                Amount</td>
                            <td colspan="2">
                                <asp:TextBox ID="TxtAmtPaid" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Installment</td>
                            <td colspan="2">
                                <asp:TextBox ID="TxtInstallment" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                No of Payments</td>
                            <td colspan="2">
                                <asp:TextBox ID="TxtNoOfPayments" runat="server"></asp:TextBox>
                                <asp:Button ID="ButCalc" runat="server" Text="Calculate Installment" />
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="PnlGrid" runat="server" Width="100%" Height="150px" BorderStyle="Inset" BorderWidth="1">
                    
                        <asp:GridView ID="GdvLoan" runat="server" AutoGenerateColumns="False" 
                            Width="100%">
                            <Columns>
                                <asp:BoundField DataField="LoanID" HeaderText="Loan ID" />
                                <asp:BoundField DataField="DateTaken" HeaderText="Date" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField DataField="LoanDesc" HeaderText="Description" />
                                <asp:BoundField DataField="LoanAmount" HeaderText="Amount" />
                            </Columns>
                            <SelectedRowStyle BackColor="#CCCC00" />
                            <HeaderStyle BackColor="#000099" ForeColor="White" Font-Size="Smaller" />
                            <AlternatingRowStyle BackColor="#CCFFFF" />
                        </asp:GridView>
                    
                    </asp:Panel>                   
                </asp:Panel> 
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_InitializeRequest);
        prm.add_endRequest(prm_EndRequest);

        function prm_InitializeRequest(sender, args) {
            var panelProg = $get('divImage');
            if (args._postBackElement.id != "Timer1") {
                panelProg.style.display = '';
            }
        };

        function prm_EndRequest(sender, args) {
            var panelProg = $get('divImage');
            panelProg.style.display = 'none';
        };
    </script>

</body>
</html>

