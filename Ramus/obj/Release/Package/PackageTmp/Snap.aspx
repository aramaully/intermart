﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Snap.aspx.vb" Inherits="Ramus.Snap1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Web Cam Capture</title>    
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.

            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Take Photo by Web Cam'; //Set the name

            w.Width = 575; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 320;
            w.Top = 10;
            w.Left = 10;

            w.CenterScreen();
            w.Resizeable = false; // Disable resize
            w.Minimizeable = true; // Disable minimize
            w.Maximizeable = false; // Disable maximize
            w.ShowDialog(); // Show the window as a dialog
        };
        function Done() {
            PageMethods.AjaxSetSession("True");
            window.returnValue = 'nothing';
        };
    </script>
    
    	<script type="text/javascript" src="js/swfobject.js"></script>
		<script type="text/javascript">
		swfobject.registerObject("myId", "9.0.0", "expressInstall.swf");
		</script>
</head>
<body onunload="Done();">
    <form id="form1" runat="server" >
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
		<div>	
			<object id="myId" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="700" height="340">
				<param name="movie" value="swf/webCamCapture.swf" />
        		<!--[if !IE]>-->
				<object type="application/x-shockwave-flash" data="swf/webCamCapture.swf" width="700" height="340">
				<!--<![endif]-->
				<div>
					<h1>Alternative content</h1>

					<p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>
				</div>
				<!--[if !IE]>-->
				</object>
				<!--<![endif]-->
			</object>
		</div>
    <asp:Label ID="lblReturn" runat="server" Text=""></asp:Label>
    </form>
    <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
    try {
        var pageTracker = _gat._getTracker("UA-2504048-34");
        pageTracker._trackPageview();
    } catch (err) { }</script>
</body>
</html>

