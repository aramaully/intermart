﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmEmpDailyInput.aspx.vb" Inherits="Ramus.EmpDailyInput" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<html>
<head id="Head1" runat="server">
    <title>Daily Input</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.
            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Daily Input'; //Set the name
            w.Width = 775; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 300;
            w.Top = 10;
            w.Left = 10;
            w.Show(); //Show the window
        };
        function ShowFind() {
            var FindWindow = window.open("FindEmployee.aspx", "FindEmployee", "width=565,height=435,top=200, left=575");
        };
        function FunctionConfirm(Tlb, TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Find") {
                 __doPostBack('ToolBar1', '1');
            }
        }
        function SaveFunction(Sender, RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        }
        function FunPic() {
            var myArgs = 'nothing';
            myargs = window.showModalDialog('Snap.aspx', myArgs, "dialogHeight:350px;dialogWidth:650px");
            if (myArgs == 'nothing') {
                PageMethods.AjaxSetSession("True");
            }
        }
        function ShowReport() {
            wfind = window.frameElement.IDCWindow;
            wfind.CreateWindow('FrmRpt.aspx', w);
            return false;
        };
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 268435376px;
        }
        .style3
        {
            width: 856px;
        }
        .style5
        {
            width: 80px;
        }
        </style>
</head>
<body onload="init();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px;
                height: 32px; width: 32px;" runat="server" BorderStyle="None" Visible="True">
                <div id="divImage" style="display: none">
                    <asp:Image ID="img1" runat="server" ImageUrl="/Images/Wait.gif" />
                </div>
            </asp:Panel>
            <div id="toolbar" style="width: 100%;">
                <eo:ToolBar ID="ToolBar1" runat="server" BackgroundImage="00100103" ClientSideOnItemClick="FunctionConfirm"
                    BackgroundImageLeft="00100101" BackgroundImageRight="00100102" SeparatorImage="00100104"
                    Width="100%">
                    <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 1px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: #335ea8 1px solid; background-color: #99afd4; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <Items>

                    </Items>
                    <ItemTemplates>
                        <eo:ToolBarItem Type="Custom">
                            <NormalStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <HoverStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <DownStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="DropDownMenu">
                            <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                            <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; background-image: url(00100106); background-position-x: right;" />
                            <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: none; background-color:transparent; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                        </eo:ToolBarItem>
                    </ItemTemplates>
                    <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                </eo:ToolBar>
            </div>
            <div id="Content" style="width: 100%; height: 100%; padding: 2px;
                border-right: solid 1px #cddaea;">                
            <asp:Panel ID="Panel10" runat="server" BorderStyle="Inset" BorderWidth="1">
                <table class="style1">
                    <tr>
                        <td style="white-space:nowrap">
                            Emp ID
                        </td>
                        <td class="style3">
                            <asp:TextBox ID="TxtEmpID" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:Button ID="TxtFind" runat="server" Text="Find" />
                        </td>
                        <td>
                            <asp:TextBox ID="TxtYear" runat="server" visible="false" AutoPostBack="True" 
                                Width="44px"></asp:TextBox></td>
                        </tr>
                    <tr>
                        <td style="white-space:nowrap">
                            Last Name</td>
                        <td colspan="2">
                            <asp:TextBox ID="TxtLast" runat="server" Width="400px" ReadOnly="True"></asp:TextBox>
                        </td>
                         <td class="style5">
                             Local Balance</td>
                            <td colspan="2" class="style2">
                            <asp:TextBox ID="TxtLocalBalance" runat="server" Width="50px" ReadOnly="True"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="white-space:nowrap">
                            First Name</td>
                        <td colspan="2">
                            <asp:TextBox ID="TxtFirst" runat="server" Width="400px" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            Sick Balance</td>
                                                    <td colspan="2" class="style2">
                            <asp:TextBox ID="TxtSickBalance" runat="server" Width="50px" ReadOnly="True"></asp:TextBox>
                    </tr>
                    <tr>
                        <td>
                            From Date</td>
                        <td colspan="2">
                            <asp:TextBox ID="TxtFromDate" runat="server" Height="22px" Width="86px"></asp:TextBox>
                            <cc1:CalendarExtender ID="TxtFromDate_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MMM/yyyy" TargetControlID="TxtFromDate">
                            </cc1:CalendarExtender>
                        </td>
                        <td>
                            <asp:Label ID="LblMsg" runat="server" width = "100px" ForeColor="Red"></asp:Label>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            To Date</td>
                        <td colspan="2">
                            <asp:TextBox ID="TxtToDate" runat="server" Height = "22px" Width ="86px"></asp:TextBox>
                            <cc1:CalendarExtender ID="TxtToDate_CalendarExtender1" runat="server" 
                                Enabled="True" Format="dd/MMM/yyyy" TargetControlID="TxtToDate">
                            </cc1:CalendarExtender>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td colspan="2">
                            <asp:Button ID="BtnRefresh" runat="server" Text="Refresh" Width="85px" />
                        </td>
                        <td>
                            <asp:Button ID="BtnSave" runat="server" Text="Saved" Width="85px" />
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
                </asp:Panel>                
                <asp:Panel ID="PnlContent" runat="server" BorderStyle="Inset" Height = "330px" ScrollBars ="Auto" BorderWidth="1">
                    <table class="style1">
                <tr>
                    <td colspan="8">
                    <asp:Panel runat="server" ID="PnlTravelList" width="100%" Height="1500px" ScrollBars="Both">
                        <asp:GridView ID="GdvTVList" runat="server" AutoGenerateColumns="False" width="99%">
                        <Columns>
                        <asp:TemplateField HeaderText="Date" ><ItemTemplate>
                        <asp:TextBox runat="server" ID="TxtDate" Enabled="false" Width ="50%">
                        </asp:TextBox></ItemTemplate>
                            <ControlStyle Font-Bold="False" Width="130px" />
                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Size="Smaller" 
                                HorizontalAlign="Left" Width="50px" />
                        </asp:TemplateField>    
                         <asp:TemplateField HeaderText="OT1.0"><ItemTemplate> 
                        <asp:TextBox runat="server" ID="TxtOT10" Width = "50px" Enabled="True">
                        </asp:TextBox></ItemTemplate>
                            <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" Width="50px" />
                        </asp:TemplateField>         
                        <asp:TemplateField HeaderText="OT1.5"><ItemTemplate> 
                        <asp:TextBox runat="server" ID="TxtOT15" Width = "50px" Enabled="True">
                        </asp:TextBox></ItemTemplate>
                            <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" Width="50px" />
                        </asp:TemplateField>         
                        
                        <asp:TemplateField HeaderText="OT2.0"><ItemTemplate> 
                        <asp:TextBox runat="server" ID="TxtOT20" Width = "50px" Enabled="True">
                        </asp:TextBox></ItemTemplate>
                            <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" Width="50px" />
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderText="OT3.0"><ItemTemplate> 
                        <asp:TextBox runat="server" ID="TxtOT30" Width = "50px" Enabled="True">
                        </asp:TextBox></ItemTemplate>
                            <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Leave Type"><ItemTemplate> 
                        <asp:DropDownList runat="server" Width="130px" ID="CmbLeaveType" Enabled ="True"  >
                            <asp:ListItem> </asp:ListItem>
                        </asp:DropDownList></ItemTemplate>
                            <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" Width="130px" />
                            </asp:TemplateField> 
                        <asp:TemplateField HeaderText="Day"><ItemTemplate> 
                        <asp:TextBox runat="server" ID="TxtDay" Width = "50px" Enabled="True">
                        </asp:TextBox></ItemTemplate>
                            <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText=""><ItemTemplate> 
                        <asp:TextBox runat="server" ID="TxtDTravel" Width = "100px" Enabled="True">
                        </asp:TextBox></ItemTemplate>
                            <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lateness"><ItemTemplate> 
                        <asp:TextBox runat="server" ID="TxtLateness" Width = "50px" Enabled="True">
                        </asp:TextBox></ItemTemplate>
                            <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" Width="50px" />
                        </asp:TemplateField>
                        </Columns>
                    </asp:GridView>                                            
                    </asp:Panel>
                    </td>
                  </tr>

                    </table>
                </asp:Panel> 
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_InitializeRequest);
        prm.add_endRequest(prm_EndRequest);

        function prm_InitializeRequest(sender, args) {
            var panelProg = $get('divImage');
            if (args._postBackElement.id != "Timer1") {
                panelProg.style.display = '';
            }
        };

        function prm_EndRequest(sender, args) {
            var panelProg = $get('divImage');
            panelProg.style.display = 'none';
        };
    </script>

</body>
</html>


