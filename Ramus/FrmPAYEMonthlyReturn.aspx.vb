﻿Imports System.IO
Imports System.Data.OleDb
Partial Public Class FrmPAYEMonthlyReturn
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim CmdTrans As New OleDb.OleDbCommand
    Dim RdrTrans As OleDb.OleDbDataReader
    Dim CmdEmp As New OleDb.OleDbCommand
    Dim RdrEmp As OleDb.OleDbDataReader
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            TxtYear.Text = Date.Today.Year
            CmbMonth.SelectedIndex = Date.Today.Month - 1
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim StrSql As String
        Dim StrSalaryPeriod As String
        Dim DtePayDate As Date
        Dim StrLine As String
        Dim LngMaxSalNPF As Long
        Dim LngSal As Long
        Dim StrSSNo As String
        Dim strData As String = ""
        Dim StrFlag As String
        Dim StrLast As String
        Dim StrFirst As String
        Dim StrAddress As String
        Dim StrCity As String
        Dim StrCountry As String
        Dim StrM As String = "M"
        Dim LngNSF As Long
        Dim LngSalNPS As Single
        Dim stringWriter As New StringWriter
        Dim wrt As New IO.StringWriter()
        Dim StrCompanyName As String
        Dim StrType As String
        Dim StrTAN As String
        Dim tab As String = ""
        StrCompanyName = ""
        Dim SngLevy As Single = 0
        Dim SngPAYE As Single = 0

        Dim StrCompanyRegNum As String
        Dim StrCompanyBRN As String
        Dim StrCompanyPhone As String
        Dim StrCompanyDeclarent As String
        Dim StrCompanyEmail As String
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim StrSpace As String = ""
        If CmbMonth.Text = "Bonus" Then
            DtePayDate = CDate("20-December" + "-" + TxtYear.Text)
        Else
            DtePayDate = CDate("28-" + CmbMonth.Text + "-" + TxtYear.Text)
        End If
        If ChkSPAS.Checked Then
            Response.Clear()
            Response.ContentType = "text/csv"
            Response.AppendHeader("Content-Disposition", String.Format("attachment; filename=SPAS" & Format(DateTime.Today, "MMyy") & ".csv"))

            StrSql = "Select *From [System]"
            CmdTrans.Connection = Con
            CmdTrans.CommandText = StrSql
            RdrTrans = CmdTrans.ExecuteReader
            While RdrTrans.Read
                StrCompanyRegNum = RdrTrans("PAYERegNo") & ""
                StrCompanyBRN = RdrTrans("BRN") & ""
                StrCompanyName = RdrTrans("Name") & ""
                StrSalaryPeriod = Format(DtePayDate, "yyMM")
                StrCompanyPhone = RdrTrans("Phone") & ""
                'StrCompanyPhone = ""
                StrCompanyDeclarent = RdrTrans("PAYEDeclarant") & ""
                StrCompanyEmail = RdrTrans("Email") & ""
            End While
            RdrTrans.Close()

            Dim Dt As DataTable
            Dim Dr As DataRow
            Dim Dc As DataColumn
            Dt = New DataTable()
            Dc = New DataColumn("Col1", Type.GetType("System.String"))
            Dt.Columns.Add(Dc)
            Dc = New DataColumn("Col2", Type.GetType("System.String"))
            Dt.Columns.Add(Dc)
            Dc = New DataColumn("Col3", Type.GetType("System.String"))
            Dt.Columns.Add(Dc)
            Dc = New DataColumn("Col4", Type.GetType("System.String"))
            Dt.Columns.Add(Dc)
            Dc = New DataColumn("Col5", Type.GetType("System.String"))
            Dt.Columns.Add(Dc)
            Dc = New DataColumn("Col6", Type.GetType("System.String"))
            Dt.Columns.Add(Dc)

            Dim LngSumBasic As Long
            Dim LngIVTB As Long
            LngSumBasic = 0
            LngIVTB = 0

            If e.Item.CommandName = "Print" Then
                Try
                    Dim StrFileName As String = "SPAS" & Format(DtePayDate, "MM") & Format(DtePayDate, "yy")

                    Dim DtHeader1 As DataTable
                    Dim DrHeader1 As DataRow
                    Dim DcHeader1 As DataColumn

                    DtHeader1 = New DataTable()
                    DcHeader1 = New DataColumn("First1", Type.GetType("System.String"))
                    DtHeader1.Columns.Add(DcHeader1)
                    DcHeader1 = New DataColumn("First2", Type.GetType("System.String"))
                    DtHeader1.Columns.Add(DcHeader1)
                    DcHeader1 = New DataColumn("First3", Type.GetType("System.String"))
                    DtHeader1.Columns.Add(DcHeader1)
                    DrHeader1 = DtHeader1.NewRow
                    DrHeader1.Item("First1") = "MRA"
                    DrHeader1.Item("First2") = "SPAS"
                    DrHeader1.Item("First3") = "V1.0"
                    DtHeader1.Rows.Add(DrHeader1)
                    For Each DrHeader1 In DtHeader1.Rows
                        tab = ""
                        For i = 0 To DtHeader1.Columns.Count - 1
                            Response.Write(tab & DrHeader1(i).ToString())
                            tab = ","
                        Next
                        Response.Write(vbCrLf)
                    Next

                    Dim DtHeader2 As DataTable
                    Dim DrHeader2 As DataRow
                    Dim DcHeader2 As DataColumn

                    DtHeader2 = New DataTable()
                    DcHeader2 = New DataColumn("Second1", Type.GetType("System.String"))
                    DtHeader2.Columns.Add(DcHeader2)
                    DcHeader2 = New DataColumn("Second2", Type.GetType("System.String"))
                    DtHeader2.Columns.Add(DcHeader2)
                    DcHeader2 = New DataColumn("Second3", Type.GetType("System.String"))
                    DtHeader2.Columns.Add(DcHeader2)
                    DcHeader2 = New DataColumn("Second4", Type.GetType("System.String"))
                    DtHeader2.Columns.Add(DcHeader2)
                    DcHeader2 = New DataColumn("Second5", Type.GetType("System.String"))
                    DtHeader2.Columns.Add(DcHeader2)
                    DcHeader2 = New DataColumn("Second6", Type.GetType("System.String"))
                    DtHeader2.Columns.Add(DcHeader2)

                    DrHeader2 = DtHeader2.NewRow
                    DrHeader2.Item("Second1") = "Employer Registration Number(ERN)"
                    DrHeader2.Item("Second2") = "Employer Name"
                    DrHeader2.Item("Second3") = "Period"
                    DrHeader2.Item("Second4") = "Telphone Number"
                    DrHeader2.Item("Second5") = "Mobile Number"
                    DrHeader2.Item("Second6") = "Email Address"
                    DtHeader2.Rows.Add(DrHeader2)
                    For Each DrHeader2 In DtHeader2.Rows
                        tab = ""
                        For i = 0 To DtHeader2.Columns.Count - 1
                            Response.Write(tab & DrHeader2(i).ToString())
                            tab = ","
                        Next
                        Response.Write(vbCrLf)
                    Next

                    Dr = Dt.NewRow
                    Dr.Item("Col1") = "'" & StrCompanyRegNum
                    Dr.Item("Col2") = StrCompanyName
                    Dr.Item("Col3") = StrSalaryPeriod
                    Dr.Item("Col4") = StrCompanyPhone
                    Dr.Item("Col5") = "52513487"
                    Dr.Item("Col6") = StrCompanyEmail
                    Dt.Rows.Add(Dr)

                    For Each Dr In Dt.Rows
                        tab = ""
                        For i = 0 To Dt.Columns.Count - 1
                            Response.Write(tab & Dr(i).ToString())
                            tab = ","
                        Next
                        Response.Write(vbCrLf)
                    Next

                    Dim DtHeader3 As DataTable
                    Dim DrHeader3 As DataRow
                    Dim DcHeader3 As DataColumn

                    DtHeader3 = New DataTable()
                    DcHeader3 = New DataColumn("Empe1", Type.GetType("System.String"))
                    DtHeader3.Columns.Add(DcHeader3)
                    DcHeader3 = New DataColumn("Empe2", Type.GetType("System.String"))
                    DtHeader3.Columns.Add(DcHeader3)
                    DcHeader3 = New DataColumn("Empe3", Type.GetType("System.String"))
                    DtHeader3.Columns.Add(DcHeader3)
                    DcHeader3 = New DataColumn("Empe4", Type.GetType("System.String"))
                    DtHeader3.Columns.Add(DcHeader3)
                    DcHeader3 = New DataColumn("Empe5", Type.GetType("System.String"))
                    DtHeader3.Columns.Add(DcHeader3)
                    DcHeader3 = New DataColumn("Empe6", Type.GetType("System.String"))
                    DtHeader3.Columns.Add(DcHeader3)
                    DcHeader3 = New DataColumn("Empe7", Type.GetType("System.String"))
                    DtHeader3.Columns.Add(DcHeader3)
                    DcHeader3 = New DataColumn("Empe8", Type.GetType("System.String"))
                    DtHeader3.Columns.Add(DcHeader3)

                    DrHeader3 = DtHeader3.NewRow
                    DrHeader3.Item("Empe1") = "Employee NID"
                    DrHeader3.Item("Empe2") = "Surname of Employee"
                    DrHeader3.Item("Empe3") = "Other Name of Employee"
                    DrHeader3.Item("Empe4") = "Mauritian Citizen (Y/N)"
                    DrHeader3.Item("Empe5") = "Employment Type : Part Time / Full Time ?"
                    DrHeader3.Item("Empe6") = "Bank Code"
                    DrHeader3.Item("Empe7") = "Bank Account Number"
                    DrHeader3.Item("Empe8") = "Basic Salary"
                    DtHeader3.Rows.Add(DrHeader3)
                    For Each DrHeader3 In DtHeader3.Rows
                        tab = ""
                        For i = 0 To DtHeader3.Columns.Count - 1
                            Response.Write(tab & DrHeader3(i).ToString())
                            tab = ","
                        Next
                        Response.Write(vbCrLf)
                    Next

                    Dim Dt3 As DataTable
                    Dim Dr3 As DataRow
                    Dim Dc3 As DataColumn

                    Dt3 = New DataTable()
                    Dc3 = New DataColumn("Col9", Type.GetType("System.String"))
                    Dt3.Columns.Add(Dc3)
                    Dc3 = New DataColumn("Col10", Type.GetType("System.String"))
                    Dt3.Columns.Add(Dc3)
                    Dc3 = New DataColumn("Col11", Type.GetType("System.String"))
                    Dt3.Columns.Add(Dc3)
                    Dc3 = New DataColumn("Col12", Type.GetType("System.String"))
                    Dt3.Columns.Add(Dc3)
                    Dc3 = New DataColumn("Col13", Type.GetType("System.String"))
                    Dt3.Columns.Add(Dc3)
                    Dc3 = New DataColumn("Col14", Type.GetType("System.String"))
                    Dt3.Columns.Add(Dc3)
                    Dc3 = New DataColumn("Col15", Type.GetType("System.String"))
                    Dt3.Columns.Add(Dc3)
                    Dc3 = New DataColumn("Col16", Type.GetType("System.String"))
                    Dt3.Columns.Add(Dc3)

                    StrSql = "SELECT * FROM Salary where [Month] = '" & CmbMonth.Text & "' AND Year='" & TxtYear.Text & "' And EmpBasic <= 9900 and ModePay = 'Bank'  order by EmpID"
                    CmdTrans.Connection = Con
                    CmdTrans.CommandText = StrSql
                    RdrTrans = CmdTrans.ExecuteReader
                    While RdrTrans.Read
                        CmdEmp.Connection = Con
                        CmdEmp.CommandText = "Select * From Employee where EmpID = '" & RdrTrans("EmpID") & "'"
                        RdrEmp = CmdEmp.ExecuteReader
                        StrLast = ""
                        StrFirst = ""
                        StrSSNo = ""
                        Dim SngTotal As Single = 0
                        Dim SngBonusPAYE As Single = 0
                        If RdrEmp.Read Then
                            StrSSNo = RdrEmp("SSNo") & ""
                        End If
                        RdrEmp.Close()
                        'Get MNS code and Format
                        StrSql = "SELECT * FROM Bank where BankID = '" & RdrTrans("BankID") & "'"
                        Cmd.Connection = Con
                        Cmd.CommandText = StrSql
                        Dim StrMNSCode As String
                        Dim StrFormat As String
                        Rdr = Cmd.ExecuteReader
                        If Rdr.Read Then
                            StrMNSCode = Rdr("MNSCode") & ""
                            StrFormat = Rdr("Format") & ""
                        End If
                        Rdr.Close()

                        Dr3 = Dt3.NewRow
                        Dr3.Item("Col9") = StrSSNo
                        Dr3.Item("Col10") = UCase(RdrTrans("LastName"))
                        Dr3.Item("Col11") = UCase(RdrTrans("FirstName"))
                        Dr3.Item("Col12") = "Y"
                        Dr3.Item("Col13") = "F"
                        Dr3.Item("Col14") = StrMNSCode

                        Dr3.Item("Col15") = "'" & RdrTrans("AccountNo") & ""
                        Dr3.Item("Col16") = RdrTrans("EmpBasic")
                        Dt3.Rows.Add(Dr3)
                    End While
                    RdrTrans.Close()

                    For Each Dr3 In Dt3.Rows
                        tab = ""
                        For i = 0 To Dt3.Columns.Count - 1
                            Response.Write(tab & Dr3(i).ToString())
                            tab = ","
                        Next
                        Response.Write(vbCrLf)
                    Next

                    Response.End()
                    Response.[End]()
                Catch ex As Exception
                    '' MsgBox("")
                End Try

            End If
        Else
            Response.Clear()
            Response.ContentType = "text/csv"
            Response.AppendHeader("Content-Disposition", String.Format("attachment; filename=PAYE" & Format(DateTime.Today, "MMyy") & ".csv"))

            StrSql = "Select *From [System]"
            CmdTrans.Connection = Con
            CmdTrans.CommandText = StrSql
            RdrTrans = CmdTrans.ExecuteReader
            While RdrTrans.Read
                StrCompanyRegNum = RdrTrans("PAYERegNo") & ""
                StrCompanyBRN = RdrTrans("BRN") & ""
                StrCompanyName = RdrTrans("Name") & ""
                StrSalaryPeriod = Format(DtePayDate, "yyMM")
                StrCompanyPhone = RdrTrans("Phone") & ""
                'StrCompanyPhone = ""
                StrCompanyDeclarent = RdrTrans("PAYEDeclarant") & ""
                StrCompanyEmail = RdrTrans("Email") & ""
            End While
            RdrTrans.Close()

            Dim Dt As DataTable
            Dim Dr As DataRow
            Dim Dc As DataColumn
            Dt = New DataTable()
            Dc = New DataColumn("Col1", Type.GetType("System.String"))
            Dt.Columns.Add(Dc)
            Dc = New DataColumn("Col2", Type.GetType("System.String"))
            Dt.Columns.Add(Dc)
            Dc = New DataColumn("Col3", Type.GetType("System.String"))
            Dt.Columns.Add(Dc)
            Dc = New DataColumn("Col4", Type.GetType("System.String"))
            Dt.Columns.Add(Dc)
            Dc = New DataColumn("Col5", Type.GetType("System.String"))
            Dt.Columns.Add(Dc)
            Dc = New DataColumn("Col6", Type.GetType("System.String"))
            Dt.Columns.Add(Dc)
            Dc = New DataColumn("Col7", Type.GetType("System.String"))
            Dt.Columns.Add(Dc)
            Dc = New DataColumn("Col8", Type.GetType("System.String"))
            Dt.Columns.Add(Dc)

            Dim LngSumBasic As Long
            Dim LngIVTB As Long
            LngSumBasic = 0
            LngIVTB = 0

            If e.Item.CommandName = "Print" Then
                Try
                    Dim StrFileName As String = "PAYE" & Format(DtePayDate, "MM") & Format(DtePayDate, "yy")

                    Dim DtHeader1 As DataTable
                    Dim DrHeader1 As DataRow
                    Dim DcHeader1 As DataColumn

                    DtHeader1 = New DataTable()
                    DcHeader1 = New DataColumn("First1", Type.GetType("System.String"))
                    DtHeader1.Columns.Add(DcHeader1)
                    DcHeader1 = New DataColumn("First2", Type.GetType("System.String"))
                    DtHeader1.Columns.Add(DcHeader1)
                    DcHeader1 = New DataColumn("First3", Type.GetType("System.String"))
                    DtHeader1.Columns.Add(DcHeader1)
                    DrHeader1 = DtHeader1.NewRow
                    DrHeader1.Item("First1") = "MNS"
                    DrHeader1.Item("First2") = "PAYE"
                    DrHeader1.Item("First3") = "V1.0"
                    DtHeader1.Rows.Add(DrHeader1)
                    For Each DrHeader1 In DtHeader1.Rows
                        tab = ""
                        For i = 0 To DtHeader1.Columns.Count - 1
                            Response.Write(tab & DrHeader1(i).ToString())
                            tab = ","
                        Next
                        Response.Write(vbCrLf)
                    Next

                    Dim DtHeader2 As DataTable
                    Dim DrHeader2 As DataRow
                    Dim DcHeader2 As DataColumn

                    DtHeader2 = New DataTable()
                    DcHeader2 = New DataColumn("Second1", Type.GetType("System.String"))
                    DtHeader2.Columns.Add(DcHeader2)
                    DcHeader2 = New DataColumn("Second2", Type.GetType("System.String"))
                    DtHeader2.Columns.Add(DcHeader2)
                    DcHeader2 = New DataColumn("Second3", Type.GetType("System.String"))
                    DtHeader2.Columns.Add(DcHeader2)
                    DcHeader2 = New DataColumn("Second4", Type.GetType("System.String"))
                    DtHeader2.Columns.Add(DcHeader2)
                    DcHeader2 = New DataColumn("Second5", Type.GetType("System.String"))
                    DtHeader2.Columns.Add(DcHeader2)
                    DcHeader2 = New DataColumn("Second6", Type.GetType("System.String"))
                    DtHeader2.Columns.Add(DcHeader2)
                    DcHeader2 = New DataColumn("Second7", Type.GetType("System.String"))
                    DtHeader2.Columns.Add(DcHeader2)
                    DcHeader2 = New DataColumn("Second8", Type.GetType("System.String"))
                    DtHeader2.Columns.Add(DcHeader2)

                    DrHeader2 = DtHeader2.NewRow
                    DrHeader2.Item("Second1") = "Employer Registration Number"
                    DrHeader2.Item("Second2") = "Employer Business Registration Number"
                    DrHeader2.Item("Second3") = "Employer Name"
                    DrHeader2.Item("Second4") = "Tax Period"
                    DrHeader2.Item("Second5") = "Telephone Number"
                    DrHeader2.Item("Second6") = "Mobile Number"
                    DrHeader2.Item("Second7") = "Name of Declarant"
                    DrHeader2.Item("Second8") = "Email Address"
                    DtHeader2.Rows.Add(DrHeader2)
                    For Each DrHeader2 In DtHeader2.Rows
                        tab = ""
                        For i = 0 To DtHeader2.Columns.Count - 1
                            Response.Write(tab & DrHeader2(i).ToString())
                            tab = ","
                        Next
                        Response.Write(vbCrLf)
                    Next

                    Dr = Dt.NewRow
                    Dr.Item("Col1") = "'" & StrCompanyRegNum
                    Dr.Item("Col2") = StrCompanyBRN
                    Dr.Item("Col3") = StrCompanyName
                    Dr.Item("Col4") = StrSalaryPeriod
                    Dr.Item("Col5") = StrCompanyPhone
                    Dr.Item("Col6") = ""
                    Dr.Item("Col7") = StrCompanyDeclarent
                    Dr.Item("Col8") = StrCompanyEmail
                    Dt.Rows.Add(Dr)

                    For Each Dr In Dt.Rows
                        tab = ""
                        For i = 0 To Dt.Columns.Count - 1
                            Response.Write(tab & Dr(i).ToString())
                            tab = ","
                        Next
                        Response.Write(vbCrLf)
                    Next

                    Dim DtHeader3 As DataTable
                    Dim DrHeader3 As DataRow
                    Dim DcHeader3 As DataColumn

                    DtHeader3 = New DataTable()
                    DcHeader3 = New DataColumn("Empe1", Type.GetType("System.String"))
                    DtHeader3.Columns.Add(DcHeader3)
                    DcHeader3 = New DataColumn("Empe2", Type.GetType("System.String"))
                    DtHeader3.Columns.Add(DcHeader3)
                    DcHeader3 = New DataColumn("Empe3", Type.GetType("System.String"))
                    DtHeader3.Columns.Add(DcHeader3)
                    DcHeader3 = New DataColumn("Empe4", Type.GetType("System.String"))
                    DtHeader3.Columns.Add(DcHeader3)
                    DcHeader3 = New DataColumn("Empe5", Type.GetType("System.String"))
                    DtHeader3.Columns.Add(DcHeader3)
                    DcHeader3 = New DataColumn("Empe6", Type.GetType("System.String"))
                    DtHeader3.Columns.Add(DcHeader3)

                    DrHeader3 = DtHeader3.NewRow
                    DrHeader3.Item("Empe1") = "Employee NID"
                    DrHeader3.Item("Empe2") = "Surname of Employee"
                    DrHeader3.Item("Empe3") = "Other Name of Employee"
                    DrHeader3.Item("Empe4") = "Salary Payable excluding travel and Bonus"
                    DrHeader3.Item("Empe5") = "PAYE Amount"
                    DrHeader3.Item("Empe6") = "Full Time Employment"
                    DtHeader3.Rows.Add(DrHeader3)
                    For Each DrHeader3 In DtHeader3.Rows
                        tab = ""
                        For i = 0 To DtHeader3.Columns.Count - 1
                            Response.Write(tab & DrHeader3(i).ToString())
                            tab = ","
                        Next
                        Response.Write(vbCrLf)
                    Next

                    Dim Dt3 As DataTable
                    Dim Dr3 As DataRow
                    Dim Dc3 As DataColumn

                    Dt3 = New DataTable()
                    Dc3 = New DataColumn("Col9", Type.GetType("System.String"))
                    Dt3.Columns.Add(Dc3)
                    Dc3 = New DataColumn("Col10", Type.GetType("System.String"))
                    Dt3.Columns.Add(Dc3)
                    Dc3 = New DataColumn("Col11", Type.GetType("System.String"))
                    Dt3.Columns.Add(Dc3)
                    Dc3 = New DataColumn("Col12", Type.GetType("System.String"))
                    Dt3.Columns.Add(Dc3)
                    Dc3 = New DataColumn("Col13", Type.GetType("System.String"))
                    Dt3.Columns.Add(Dc3)
                    Dc3 = New DataColumn("Col14", Type.GetType("System.String"))
                    Dt3.Columns.Add(Dc3)

                    StrSql = "SELECT * FROM Salary where [Month] = '" & CmbMonth.Text & "' AND Year='" & TxtYear.Text & "' And PAYE >= 0 order by EmpID"
                    CmdTrans.Connection = Con
                    CmdTrans.CommandText = StrSql
                    RdrTrans = CmdTrans.ExecuteReader
                    While RdrTrans.Read
                        CmdEmp.Connection = Con
                        CmdEmp.CommandText = "Select * From Employee where EmpID = '" & RdrTrans("EmpID") & "'"
                        RdrEmp = CmdEmp.ExecuteReader
                        StrLast = ""
                        StrFirst = ""
                        StrSSNo = ""
                        Dim SngTotal As Single = 0
                        Dim SngBonusPAYE As Single = 0
                        If RdrEmp.Read Then
                            If CmbMonth.Text = "December" Then
                                StrSql = "SELECT * FROM Salary where [Month] = 'Bonus' AND Year='" & TxtYear.Text & "' And EmpID = '" & RdrEmp("EmpID") & "'"
                                Cmd.Connection = Con
                                Cmd.CommandText = StrSql
                                Rdr = Cmd.ExecuteReader
                                If Rdr.Read Then
                                    SngBonusPAYE = Rdr("PAYE")
                                End If
                                Rdr.Close()
                            End If
                            StrSSNo = RdrEmp("SSNo") & ""
                            StrLast = UCase(RdrEmp("Last"))
                            StrFirst = UCase(RdrEmp("First"))
                            ''get total salary
                            SngTotal = 0
                            SngTotal = RdrTrans("BasicSalary")
                            SngTotal = SngTotal + RdrTrans("Overtime")
                            SngTotal = SngTotal + RdrTrans("Arrears")
                            SngTotal = SngTotal + RdrTrans("PerfAllow")
                            SngTotal = SngTotal + RdrTrans("MaternityAllow")
                            SngTotal = SngTotal + RdrTrans("RespAllow")
                            SngTotal = SngTotal + RdrTrans("MealAllow")
                            SngTotal = SngTotal + RdrTrans("PensionEmp")
                            SngTotal = SngTotal + RdrTrans("PassageBenefit")
                            SngTotal = SngTotal + RdrTrans("OtherAllow")
                            SngTotal = SngTotal + RdrTrans("SpecialBenefit")
                            SngTotal = SngTotal + Math.Round((RdrTrans("LocalRefund") + RdrTrans("SickRefund")) * (RdrTrans("DailyRate")), 0)
                            SngPAYE = RdrTrans("PAYE")
                        End If
                        RdrEmp.Close()

                        Dr3 = Dt3.NewRow
                        Dr3.Item("Col9") = StrSSNo
                        Dr3.Item("Col10") = StrLast
                        Dr3.Item("Col11") = StrFirst
                        Dr3.Item("Col12") = Math.Round(SngTotal, 0)
                        Dr3.Item("Col13") = SngPAYE + SngBonusPAYE
                        Dr3.Item("Col14") = "Y"
                        Dt3.Rows.Add(Dr3)
                    End While
                    RdrTrans.Close()

                    For Each Dr3 In Dt3.Rows
                        tab = ""
                        For i = 0 To Dt3.Columns.Count - 1
                            Response.Write(tab & Dr3(i).ToString())
                            tab = ","
                        Next
                        Response.Write(vbCrLf)
                    Next

                    Response.End()
                    Response.[End]()
                Catch ex As Exception
                    '' MsgBox("")
                End Try
            End If
        End If
    End Sub

End Class