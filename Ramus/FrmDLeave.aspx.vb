﻿Public Partial Class FrmDLeave
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrAudit As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT Name FROM LeaveMaster"
            Rdr = Cmd.ExecuteReader
            CmbType.Items.Add("Select...")
            While Rdr.Read
                CmbType.Items.Add(Rdr("Name"))
            End While
            Rdr.Close()
            TxtYear.Text = Year(Date.Today)
        End If
    End Sub

    Protected Sub ClearAll()
        TxtFirst.Text = ""
        TxtLast.Text = ""
        TxtClockID.Text = ""
        TxtCategory.Text = ""
        TxtTitle.Text = ""
        TxtDateFrom.Text = ""
        TxtDateTo.Text = ""
        ImgPhoto.ImageUrl = Nothing
        GdvLeave.Dispose()
        GdvLeave.DataSource = ""
        GdvLeave.DataBind()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ev As New System.EventArgs
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If e.Item.CommandName = "New" Then
            ClearAll()
            TxtEmpID.Text = ""
            TxtEmpID.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Cmd.Connection = Con
            Try
                Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                Dim DteFrom As Date
                Dim DteTo As Date
                DteFrom = Format(CDate(TxtDateFrom.Text), "dd/MM/yyyy")
                DteTo = Format(CDate(TxtDateTo.Text), "dd/MM/yyyy")
                Do While DteFrom <= DteTo
                    StrSql = "DELETE FROM Leave WHERE EmpID = '" & TxtEmpID.Text & "' "
                    StrSql = StrSql & "AND [Date] = '" & Format(DteFrom, "dd-MMM-yyyy") & "' "
                    StrSql = StrSql & "AND Type = '" & CmbType.Text & "'"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                    StrSql = "INSERT INTO Leave (EmpID,[Date],Type,Year,Leave) VALUES('"
                    StrSql = StrSql & TxtEmpID.Text & "','"
                    StrSql = StrSql & Format(DteFrom, "dd-MMM-yyyy") & "','"
                    StrSql = StrSql & CmbType.Text & "',"
                    StrSql = StrSql & Val(TxtYear.Text) & ","
                    StrSql = StrSql & Val(TxtDays.Text) & ")"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                    DteFrom = DteFrom.AddDays(1)
                Loop
                FillGrid()
                TxtFirst.Text = ""
                TxtLast.Text = ""
                TxtClockID.Text = ""
                TxtCategory.Text = ""
                TxtTitle.Text = ""
                CmbType.SelectedIndex = 0
                TxtDateFrom.Text = ""
                TxtDateTo.Text = ""
                ImgPhoto.ImageUrl = Nothing
                GdvLeave.Dispose()
                GdvLeave.DataSource = ""
                GdvLeave.DataBind()

                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Leave Record for " & TxtEmpID.Text & " Created / Edited. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Create / Edit Leave','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Try
                Cmd.Connection = Con
                StrSql = "DELETE FROM Leave WHERE EmpID = '" & TxtEmpID.Text & "' "
                StrSql = StrSql & "AND [Date] = '" & Format(CDate(TxtDateFrom.Text), "dd-MMM-yyyy") & "' "
                StrSql = StrSql & "AND Type = '" & CmbType.Text & "'"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()

                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Leave Record for " & TxtEmpID.Text & " Deleted. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Delete Leave','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()

                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Next" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID > '" & TxtEmpID.Text & "'"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try
        End If
        If e.Item.CommandName = "Previous" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID < '" & TxtEmpID.Text & "' ORDER BY EmpID DESC"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try
        End If
        If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtEmpID.Text)
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
        End If
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        If Rdr.Read Then
            On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
            TxtTitle.Text = Rdr("Title")
            TxtCategory.Text = Rdr("Category")
            TxtCategory.Text = Rdr("Category")
            ChkClocked.Checked = IIf(Rdr("Clocked"), True, False)
            TxtLocal.Text = Rdr("Local")
            TxtSick.Text = Rdr("Sick")
            TxtClockID.Text = Rdr("BatchNo")
            Session.Add("EmpID", Rdr("EmpID"))
            ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & Rdr("EmpID")
        Else
            Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(1))
            ToolBar1_ItemClick(Me, ev)
            'ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('Invalid Employee ID','Error');</script>", False)
        End If
        Rdr.Close()
        FillGrid()
    End Sub

    Private Sub FillGrid()
        'Leave Details
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Cmd.Connection = Con
        StrSql = "SELECT * FROM Leave WHERE Leave.EmpID = '" & TxtEmpID.Text & "' AND Year(Date) = " & TxtYear.Text
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvLeave.DataSource = Rdr
        GdvLeave.DataBind()
        Rdr.Close()
        TxtBalLocal.Text = ""
        TxtBalSick.Text = ""
        For Each Row As GridViewRow In GdvLeave.Rows
            If Row.Cells(1).Text = "Local Leave" Then
                TxtBalLocal.Text = Val(TxtBalLocal.Text) + Val(Row.Cells(2).Text)
            ElseIf Row.Cells(1).Text = "Sick Leave" Then
                TxtBalSick.Text = Val(TxtBalSick.Text) + Val(Row.Cells(2).Text)
            End If
        Next
        TxtBalSick.Text = Val(TxtSick.Text) - Val(TxtBalSick.Text)
        TxtBalLocal.Text = Val(TxtLocal.Text) - Val(TxtBalLocal.Text)
    End Sub

   
    'Protected Sub CmbType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbType.SelectedIndexChanged

    '    Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
    '    If Val(TxtDays.Text) = 0 Then
    '        ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'>  w.Alert('Plese Enter No of Days','Error');</script>", False)
    '        TxtDays.Focus()
    '        Exit Sub
    '    End If
    '    If CmbType.Text = "Sick Leave" Then
    '        If Val(TxtDays.Text) > Val(TxtBalSick.Text) Then
    '            TxtDays.Text = ""
    '            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('Sick Leave is Exceeded','Error');</script>", False)
    '        End If
    '    End If
    '    If CmbType.Text = "Local Leave" Then
    '        If Val(TxtDays.Text) > Val(TxtBalLocal.Text) Then
    '            TxtDays.Text = ""
    '            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('Local Leave is Exceeded','Error');</script>", False)
    '        End If
    '    End If
    'End Sub

    Private Sub GdvLeave_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvLeave.RowDataBound
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvLeave, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Protected Sub GdvLeave_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GdvLeave.SelectedIndexChanged
        'Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        'On Error Resume Next
        TxtDateFrom.Text = GdvLeave.SelectedRow.Cells(0).Text
        CmbType.Text = GdvLeave.SelectedRow.Cells(1).Text
        TxtDays.Text = GdvLeave.SelectedRow.Cells(2).Text
    End Sub

    Private Sub GdvLeave_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvLeave.SelectedIndexChanging
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        GdvLeave.SelectedIndex = e.NewSelectedIndex
    End Sub
    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        For i = 0 To Me.GdvLeave.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvLeave.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub
End Class