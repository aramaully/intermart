﻿Public Partial Class FrmFringe
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT DeptName FROM Dept"
            Rdr = Cmd.ExecuteReader
            CmbDept.Items.Add("ALL")
            While Rdr.Read
                CmbDept.Items.Add(Rdr("DeptName"))
            End While
            Rdr.Close()
            CmbMonth.SelectedIndex = Month(Date.Today) - 1
            TxtYear.Text = Year(Date.Today)
        End If
    End Sub

    Protected Sub CmbDept_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbDept.SelectedIndexChanged
        FillGrid()
    End Sub

    Private Sub FillGrid()
        If TxtYear.Text = "" Then TxtYear.Text = Year(Date.Today)
        Dim DteDate As String = "28-" & CmbMonth.Text & "-" & TxtYear.Text
        StrSql = "SELECT Employee.EmpID, SalData.* " 
        StrSql = StrSql & "FROM Employee LEFT OUTER JOIN SalData ON SalData.EmpID = Employee.EmpID "
        StrSql = StrSql & "AND SalData.[Date] = '" & DteDate & "' "
        If CmbDept.Text <> "ALL" Then
            StrSql = StrSql & "WHERE Employee.DeptCode = '" & CmbDept.Text & "'"
        End If
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvFringe.DataSource = Rdr
        GdvFringe.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "New" Then
            FillGrid()
        End If
        If e.Item.CommandName = "Save" Then
            Dim Trans As OleDb.OleDbTransaction
            Dim DteDate As String = "28-" & CmbMonth.Text & "-" & TxtYear.Text
            Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
            Cmd.Connection = Con
            Cmd.Transaction = Trans
            Dim IntInterest As Integer
            Dim IntTips As Integer
            Dim IntAccom As Integer
            Dim IntWriteOff As Integer
            Dim IntExpenses As Integer
            Dim IntTaxPaid As Integer
            Try
                For Each Row As GridViewRow In GdvFringe.Rows
                    StrSql = "DELETE FROM SalData WHERE [Date] = '" & DteDate & "' AND EmpID = '" & Row.Cells(0).Text & "'"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                    IntInterest = Val(DirectCast(Row.FindControl("TxtInterest"), TextBox).Text)
                    IntTips = Val(DirectCast(Row.FindControl("TxtTips"), TextBox).Text)
                    IntAccom = Val(DirectCast(Row.FindControl("TxtAccommodation"), TextBox).Text)
                    IntWriteOff = Val(DirectCast(Row.FindControl("TxtWriteOff"), TextBox).Text)
                    IntExpenses = Val(DirectCast(Row.FindControl("TxtExpenses"), TextBox).Text)
                    IntTaxPaid = Val(DirectCast(Row.FindControl("TxtTaxPaid"), TextBox).Text)
                    If IntInterest + IntTips + IntAccom + IntWriteOff + IntExpenses + IntTaxPaid > 0 Then
                        StrSql = "INSERT INTO SalData (EmpID,[Date],Interest,Tips,Accommodation,WriteOff,Expenses,TaxPaid) VALUES('"
                        StrSql = StrSql & Row.Cells(0).Text & "','"
                        StrSql = StrSql & DteDate & "',"
                        StrSql = StrSql & IntInterest & ","
                        StrSql = StrSql & IntTips & ","
                        StrSql = StrSql & IntAccom & ","
                        StrSql = StrSql & IntWriteOff & ","
                        StrSql = StrSql & IntExpenses & ","
                        StrSql = StrSql & IntTaxPaid & ")"
                        Cmd.CommandText = StrSql
                        Cmd.ExecuteNonQuery()
                    End If
                Next
                Trans.Commit()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
                Trans.Rollback()
            End Try
        End If
    End Sub

    Private Sub GdvFringe_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFringe.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Cells(0).Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFringe, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFringe_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFringe.SelectedIndexChanged
        TxtEmpID.Text = GdvFringe.SelectedRow.Cells(0).Text
        TxtEmpID_TextChanged(Me, e)
    End Sub

    Private Sub GdvFringe_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFringe.SelectedIndexChanging
        GdvFringe.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvFringe.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFringe.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        If Rdr.Read Then
            On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
            TxtTitle.Text = Rdr("Title")
            TxtCategory.Text = Rdr("Category")
            TxtGender.Text = Rdr("Gender")
            TxtCategory.Text = Rdr("Category")
            ChkClocked.Checked = IIf(Rdr("Clocked"), True, False)
            TxtClockID.Text = Rdr("BatchNo")
            Session.Add("EmpID", Rdr("EmpID"))
            ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & Rdr("EmpID")
        End If
        Rdr.Close()
    End Sub

    Private Sub ClearAll()
        TxtFirst.Text = ""
        TxtLast.Text = ""
        TxtTitle.Text = ""
        TxtCategory.Text = ""
        TxtGender.Text = ""
        TxtCategory.Text = ""
        ChkClocked.Checked = True
        TxtClockID.Text = ""
        ImgPhoto.ImageUrl = Nothing
    End Sub

    Protected Sub CmbMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbMonth.SelectedIndexChanged
        FillGrid()
    End Sub
End Class