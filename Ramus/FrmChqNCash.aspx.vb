﻿Public Partial Class FrmChqNCash
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim StrSql As String
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            CmbMonth.SelectedIndex = Date.Today.Month - 1
            TxtYear.Text = Date.Today.Year
            'Outlet
            Cmd.Connection = Con
            Cmd.CommandText = "Select OutletName from Outlet Order by OutletCode"
            CmbOutlet.Items.Add("ALL")
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                CmbOutlet.Items.Add(Rdr("OutletName"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            Dim StrSelFormula As String
            If CmbMonth.Text = "Bonus" Then
                If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
                    StrSelFormula = "{Salary.Month} = 'Bonus' And {Salary.ModePay} <> 'Bank' And {Salary.Year} = " & TxtYear.Text & ""
                Else
                    StrSelFormula = "{Salary.Month} = 'Bonus' And {Salary.ModePay} <> 'Bank' And {Salary.OutletName} = '" & Session("Level") & "' And {Salary.Year} = " & TxtYear.Text & ""
                End If
            Else
                Dim DtePayDate As Date = CDate("28-" + CmbMonth.Text + "-" + TxtYear.Text)
                Dim StrDate As String = Format(DtePayDate, "yyyy,MM,dd")

                If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
                    StrSelFormula = "{Salary.Date}=date(" & StrDate & ") And {Salary.ModePay} <> 'Bank'"
                Else
                    StrSelFormula = "{Salary.Date}=date(" & StrDate & ") And {Salary.ModePay} <> 'Bank' And {Salary.OutletName} = '" & Session("Level") & "'"
                End If
            End If
            If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
                If CmbOutlet.Text <> "ALL" Then
                    StrSelFormula = StrSelFormula & " And {Salary.OutletName} = '" & Trim(CmbOutlet.Text) & "'"
                End If
            End If
            Session("ReportFile") = "RSALLIST.RPT"
            Session("SelFormula") = StrSelFormula
            Session("RepHead") = "Salary List (Cheque & Cash only) for the month of " & CmbMonth.Text & ", " & TxtYear.Text
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        End If
        'End If
    End Sub
End Class