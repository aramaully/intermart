﻿Public Partial Class FrmLockSalary
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrAudit As String

    Dim TSCon As New OleDb.OleDbConnection
    Dim TSCmd As New OleDb.OleDbCommand
    Dim TSRdr As OleDb.OleDbDataReader
    Dim TSStrSql As String
    Dim CmdBackup As New OleDb.OleDbCommand

    Dim CmdUpdate As New OleDb.OleDbCommand
    Dim RdrUpdate As OleDb.OleDbDataReader
    Dim UPStrSql As String
    Dim CmdEmp As New OleDb.OleDbCommand

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            LblWarning.Text = "When this process is run Salary for this month " _
            & "will be marked paid, all loan repayment adjusted and then the File " _
            & "will be locked. That means you will not be able to make any " _
            & "further modification. Therefore before you run this module make " _
            & "sure that all the salary and loan repayment calculations are accurate."
            If Date.Today.Month = 1 Then
                CmbMonth.SelectedIndex = 11
            Else
                CmbMonth.SelectedIndex = Date.Today.Month - 2
            End If

            TxtYear.Text = Date.Today.Year
        End If
    End Sub

    Private Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Lock" Then
            'Back Up Databse
            'CmdBackup.Connection = Con
            'CmdBackup.CommandText = "Backup Database Intermart to disk = 'C:\RamusDBBackup\RamusPayroll" & Format(Today, "ddMMyyyy") & ".bak'"
            'CmdBackup.CommandText = "Backup Database Intermart to disk = 'C:\RamusBackupDatabase\RamusPayroll.bak'"
            'CmdBackup.ExecuteNonQuery()

            LblHead.Text = "Warning !!!"
            'DteSalDate = CDate("28-" + CmbMonth.Text + "-" + TxtYear.Text)
            StrSql = "SELECT COUNT(*) FROM Salary WHERE [Year] = " & TxtYear.Text
            StrSql = StrSql & " AND [Month] = '" & CmbMonth.Text & "'"
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                LblResults.Text = "Salary for " & CmbMonth.Text & ", " & TxtYear.Text & " is not processed!"
                Exit Sub
            End If
            'Lock Salary
            StrSql = "UPDATE Salary SET Locked = 1 WHERE [Year] = " & TxtYear.Text
            StrSql = StrSql & " AND [Month] = '" & CmbMonth.Text & "'"
            Cmd.CommandText = StrSql
            'Lock Monthly Input Table
            TSCmd.Connection = Con
            TSStrSql = "Update MonthlyInput set Locked = 1 where [Year] = " & TxtYear.Text & " and [Month] = '" & CmbMonth.Text & "'"
            TSCmd.CommandText = TSStrSql

            'Update Employee Basic with YOS amount
            CmdEmp.Connection = Con
            CmdEmp.CommandText = "update Employee set BasicSalary = BasicSalary+YOSAmt,YOSAmt = 0 where Clocked=1 and Outletname <> 'Management'"
            CmdEmp.ExecuteNonQuery()

            Try
                Cmd.ExecuteNonQuery()
                TSCmd.ExecuteNonQuery()
                LblResults.Text = "All Salary / Monthly Records for the month of " & CmbMonth.Text & ", " & TxtYear.Text & " Locked!"
                'Audit
                CmdAudit.Connection = Con
                StrAudit = "All Salary Records for the month of " & CmbMonth.Text & ", " & TxtYear.Text & " Locked and Database Backup!"
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Salary Record Locked','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()

            Catch ex As Exception
                LblResults.Text = "Process Failed " & ex.Message
            End Try
        End If
    End Sub
End Class