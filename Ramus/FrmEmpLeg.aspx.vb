﻿Public Partial Class FrmEmpLeg
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT * FROM Bank ORDER BY Name"
            CmbBank.Items.Add("Select...")
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                CmbBank.Items.Add(Rdr("Name"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ClearAll()
        TxtFirst.Text = ""
        TxtLast.Text = ""
        TxtClockID.Text = ""
        TxtCategory.Text = ""
        TxtTitle.Text = ""
        TxtGender.Text = ""
        ImgPhoto.ImageUrl = Nothing
        TxtTaxAcc.Text = ""
        CmbBank.SelectedIndex = 0
        TxtBankAcc.Text = ""
        TxtReviewDate.Text = ""
        TxtNIC.Text = ""
        TxtPassportNo.Text = ""
        OptNPSY.Checked = True
        OptNPSN.Checked = False
        TxtReportTo.Text = ""
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ev As New System.EventArgs
        If e.Item.CommandName = "New" Then
            ClearAll()
            TxtEmpID.Text = ""
            TxtEmpID.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT BankID FROM Bank WHERE Name = '" & CmbBank.Text & "'"
            Dim StrBankID As String = ""
            Try
                StrBankID = Cmd.ExecuteScalar
            Catch ex As Exception

            End Try
            Try
                StrSql = "UPDATE Employee SET "
                StrSql = StrSql & "TaxAccNo ='" & TxtTaxAcc.Text & "',"
                StrSql = StrSql & "BankID ='" & StrBankID & "',"
                StrSql = StrSql & "AccountNo ='" & TxtBankAcc.Text & "',"
                StrSql = StrSql & "ReportTo ='" & TxtReportTo.Text & "',"
                If IsDate(TxtReviewDate.Text) Then
                    StrSql = StrSql & "ReviewDate ='" & Format(CDate(TxtReviewDate.Text), "dd-MMM-yyyy") & "',"
                Else
                    StrSql = StrSql & "ReviewDate = Null,"
                End If
                StrSql = StrSql & "SSNo ='" & TxtNIC.Text & "',"
                StrSql = StrSql & "PassPort ='" & TxtPassportNo.Text & "',"

                If OptNPSY.Checked Then
                    StrSql = StrSql & "NPSPaid = 1"
                Else
                    StrSql = StrSql & "NPSPaid = 0"
                End If
                StrSql = StrSql & "WHERE EmpID = '" & TxtEmpID.Text & "'"
                Cmd.Connection = Con
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                TxtFirst.Text = ""
                TxtLast.Text = ""
                TxtClockID.Text = ""
                TxtCategory.Text = ""
                TxtTitle.Text = ""
                TxtGender.Text = ""
                ImgPhoto.ImageUrl = Nothing
                TxtTaxAcc.Text = ""
                CmbBank.SelectedIndex = 0
                TxtBankAcc.Text = ""
                TxtReviewDate.Text = ""
                TxtNIC.Text = ""
                TxtPassportNo.Text = ""
                OptNPSY.Checked = True
                OptNPSN.Checked = False
                TxtReportTo.Text = ""
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
            If e.Item.CommandName = "Delete" Then
                Try
                    Cmd.Connection = Con
                    Cmd.CommandText = "DELETE FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
                    Cmd.ExecuteNonQuery()
                    TxtEmpID_TextChanged(Me, ev)
                Catch ex As Exception
                    ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
                End Try
            End If
            If e.Item.CommandName = "Next" Then
                Try
                    Cmd.Connection = Con
                    Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID > '" & TxtEmpID.Text & "'"
                    TxtEmpID.Text = Cmd.ExecuteScalar
                    TxtEmpID_TextChanged(Me, ev)
                Catch ex As Exception

                End Try

            End If
            If e.Item.CommandName = "Previous" Then
                Try
                    Cmd.Connection = Con
                    Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID < '" & TxtEmpID.Text & "' ORDER BY EmpID DESC"
                    TxtEmpID.Text = Cmd.ExecuteScalar
                    TxtEmpID_TextChanged(Me, ev)
                Catch ex As Exception

                End Try
            End If
            If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtEmpID.Text)
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
            End If
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        Dim StrBankID As String = ""
        If Rdr.Read Then
            On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
            TxtTitle.Text = Rdr("Title")
            TxtCategory.Text = Rdr("Category")
            TxtGender.Text = Rdr("Gender")
            TxtCategory.Text = Rdr("Category")
            ChkClocked.Checked = IIf(Rdr("Clocked"), True, False)
            TxtClockID.Text = Rdr("BatchNo")
            Session.Add("EmpID", Rdr("EmpID"))
            ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & Rdr("EmpID")
            'Personal Details
            TxtTaxAcc.Text = Rdr("TaxAccNo").ToString
            StrBankID = Rdr("BankID").ToString
            TxtBankAcc.Text = Rdr("AccountNo").ToString
            TxtReviewDate.Text = Format(Rdr("ReviewDate"), "dd/MM/yyyy")
            TxtNIC.Text = Rdr("SSNo").ToString
            TxtPassportNo.Text = Rdr("PassPort").ToString
            TxtReportTo.Text = Rdr("ReportTo")
            If Rdr("NPSPaid") Then
                OptNPSY.Checked = True
                OptNPSN.Checked = False
            Else
                OptNPSN.Checked = True
                OptNPSY.Checked = False
            End If

        Else
            Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(1))
            ToolBar1_ItemClick(Me, ev)
            'ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('Invalid Employee ID','Error');</script>", False)
        End If
        Rdr.Close()
        If StrBankID <> "" Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT Name FROM Bank WHERE BankID = '" & StrBankID & "'"
            CmbBank.Text = Cmd.ExecuteScalar
        End If
    End Sub

End Class


