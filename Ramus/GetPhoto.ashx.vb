﻿Imports System
Imports System.Configuration
Imports System.Web
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient

Public Class GetPhoto
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim StrEmpID As String
        On Error Resume Next
        context.Response.ContentType = "image/jpeg"
        If Not context.Request.QueryString("id") Is Nothing Then
            StrEmpID = context.Request.QueryString("id")
        Else
            Throw New ArgumentException("No parameter specified")
        End If
        Dim strm As Stream = ShowEmpImage(StrEmpID)
        Dim buffer As Byte() = New Byte(4095) {}
        Dim byteSeq As Integer = strm.Read(buffer, 0, 4096)
        Do While byteSeq > 0
            context.Response.OutputStream.Write(buffer, 0, byteSeq)
            byteSeq = strm.Read(buffer, 0, 4096)
        Loop
    End Sub

    Public Function ShowEmpImage(ByVal EmpID As String) As Stream

        'Dim conn As String = "Data Source=ROBIN-HP\SQLEXPRESS;Initial Catalog=TNTWEB;User Id=admin;Password=swathi*;"
        Dim conn As String = "Data Source=BULLDOG;Initial Catalog=TNTWEB;User Id=admin;Password=swathi*;"
        Dim connection As SqlConnection = New SqlConnection(conn)
        Dim sql As String = "SELECT PhotoOle FROM Employee WHERE Empid = @ID"
        Dim cmd As SqlCommand = New SqlCommand(sql, connection)
        cmd.CommandType = CommandType.Text
        cmd.Parameters.AddWithValue("@ID", EmpID)
        connection.Open()
        Dim img As Object = cmd.ExecuteScalar()
        Try
            Return New MemoryStream(CType(img, Byte()))
        Catch
            Return Nothing
        Finally
            connection.Close()
        End Try
    End Function

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class