﻿Imports System.IO
Partial Public Class FrmSBMFile
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim CmdTrans As New OleDb.OleDbCommand
    Dim RdrTrans As OleDb.OleDbDataReader
    Dim CmdEmp As New OleDb.OleDbCommand
    Dim RdrEmp As OleDb.OleDbDataReader
    Dim CmdBank As New OleDb.OleDbCommand
    Dim RdrBank As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            TxtYear.Text = Date.Today.Year
            CmbMonth.SelectedIndex = Date.Today.Month - 1
            'Outlet
            Cmd.Connection = Con
            Cmd.CommandText = "Select OutletName from Outlet Order by OutletCode"
            CmbOutlet.Items.Add("ALL")
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                CmbOutlet.Items.Add(Rdr("OutletName"))
            End While
            Rdr.Close()

            If Session("Level") = "Administrator" Then
                CmbOutlet.Visible = True
            ElseIf Session("Level") = "Beau Bassin" Then
                CmbOutlet.Visible = False
                CmbOutlet.Text = "ALL"
            ElseIf Session("Level") = "Ebene" Then
                CmbOutlet.Visible = False
                CmbOutlet.Text = "ALL"
            ElseIf Session("Level") = "Grand Baie" Then
                CmbOutlet.Visible = False
                CmbOutlet.Text = "ALL"
            End If

        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim StrSql As String
        Dim StrSalaryPeriod As String
        Dim StrCompName As String = ""
        Dim StrCompAccNo As String
        Dim SngTotal As Single = 0
        Dim LngTAmount As Long = 0
        Dim DtePayDate As Date
        Dim IntCounter As Integer = 0
        Dim StrLine As String
        Dim StrEmpName As String = 40
        Dim StrBOMCode As String
        Dim StrNetPay As String
        Dim StrAcNo As String
        Dim strData As String = ""
        Dim path As String
        Dim StrFlag As String = "N"
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If CmbMonth.Text = "Bonus" Then
            DtePayDate = CDate("20-December" + "-" + TxtYear.Text)
            StrSalaryPeriod = "Bonus " & Format(DtePayDate, "MMMdd")
        Else
            DtePayDate = CDate("28-" + CmbMonth.Text + "-" + TxtYear.Text)
            'StrSalaryPeriod = "SALARY " & Format(DtePayDate, "MMMdd")
            StrSalaryPeriod = "SALARY " & CmbMonth.Text & "" & TxtYear.Text
        End If
        If e.Item.CommandName = "Print" Then
            Dim stringWriter As New StringWriter
            Try
                If CmbMonth.Text = "Bonus" Then
                    DtePayDate = CDate("20/" & Format(CmbMonth.SelectedIndex, "00") & "/" & TxtYear.Text)
                End If
                Dim wrt As New IO.StringWriter()
                StrSql = "Select *From [System]"
                CmdTrans.Connection = Con
                CmdTrans.CommandText = StrSql
                RdrTrans = CmdTrans.ExecuteReader
                While RdrTrans.Read
                    StrCompName = RdrTrans("Name")
                    StrCompAccNo = Microsoft.VisualBasic.Right("000000000000" + RdrTrans("AccountNo"), 12)
                End While
                RdrTrans.Close()
                If CmbMonth.SelectedValue = "Bonus" Then
                    If CmbOutlet.Text = "ALL" Then
                        StrSql = "SELECT *From Salary Where Month = 'Bonus' and IfsBonus > 0 And Year = '" & TxtYear.Text & "' And ModePay = 'Bank'"
                    Else
                        StrSql = "SELECT *From Salary Where Month = 'Bonus' And IfsBonus > 0 and Year = '" & TxtYear.Text & "' And OutletName = '" & Trim(CmbOutlet.Text) & "' And ModePay = 'Bank'"
                    End If
                Else
                    If CmbOutlet.Text = "ALL" Then
                        StrSql = "SELECT *From Salary Where Date = '" & Format(DtePayDate, "dd-MMM-yyyy") & "' And ModePay = 'Bank'"
                    Else
                        StrSql = "SELECT *From Salary Where Date = '" & Format(DtePayDate, "dd-MMM-yyyy") & "' And OutletName = '" & Trim(CmbOutlet.Text) & "' and ModePay = 'Bank'"
                    End If
                End If
                CmdTrans.Connection = Con
                CmdTrans.CommandText = StrSql
                RdrTrans = CmdTrans.ExecuteReader
                While RdrTrans.Read
                    CmdEmp.Connection = Con
                    CmdEmp.CommandText = "Select *From Employee Where EmpID = '" & RdrTrans("EmpID") & "'"
                    RdrEmp = CmdEmp.ExecuteReader
                    If RdrEmp.Read Then
                        StrEmpName = Trim(Trim(UCase(RdrEmp("Last"))) & " " & Trim(UCase(RdrEmp("First"))))
                    End If
                    RdrEmp.Close()

                    SngTotal = RdrTrans("BasicSalary")
                    SngTotal = SngTotal + RdrTrans("Overtime")
                    SngTotal = SngTotal + RdrTrans("IfsBonus")
                    SngTotal = SngTotal + RdrTrans("Arrears")
                    SngTotal = SngTotal + RdrTrans("PerfAllow")
                    SngTotal = SngTotal + RdrTrans("TravelAllowance")
                    SngTotal = SngTotal + RdrTrans("MaternityAllow")
                    SngTotal = SngTotal + RdrTrans("RespAllow")
                    SngTotal = SngTotal + RdrTrans("MealAllow")
                    SngTotal = SngTotal + RdrTrans("PensionEmp")
                    SngTotal = SngTotal + RdrTrans("PassageBenefit")
                    SngTotal = SngTotal + RdrTrans("OtherAllow")
                    SngTotal = SngTotal + RdrTrans("SpecialBenefit")

                    SngTotal = SngTotal + Math.Round((RdrTrans("LocalRefund") + RdrTrans("SickRefund")) * (RdrTrans("DailyRate")), 2)

                    SngTotal = SngTotal - (RdrTrans("NPFEmployee") + RdrTrans("PAYE") + +RdrTrans("Others") + RdrTrans("LoanRepay") + RdrTrans("OtherDeduct"))
                    SngTotal = SngTotal - (RdrTrans("EPZEE") + RdrTrans("Short") + RdrTrans("CreditCard") + RdrTrans("LoanRepay1") + RdrTrans("NSFEmployee"))

                    LngTAmount = Math.Round((SngTotal * 100), 2, MidpointRounding.AwayFromZero)

                    StrNetPay = LngTAmount
                    StrAcNo = RdrTrans("AccountNo")
                    CmdBank.Connection = Con
                    CmdBank.CommandText = "Select *From Bank Where BankID = '" & RdrTrans("BankID") & "'"
                    RdrBank = CmdBank.ExecuteReader
                    If RdrBank.Read Then
                        StrBOMCode = RdrBank("BankCode")
                    End If
                    RdrBank.Close()
                    Dim pt As Char = " "
                    StrLine = StrAcNo.PadRight(32 - 16, pt)
                    StrLine = StrLine + StrNetPay.PadLeft(32 - 16, pt)
                    StrLine = StrLine + StrEmpName.PadRight(80 - 40, pt)
                    StrLine = StrLine + StrBOMCode.PadLeft(2 - 2, pt)
                    StrLine = StrLine + StrCompName.PadRight(80 - 40, pt)
                    StrLine = StrLine + StrSalaryPeriod.PadRight(60 - 30, pt)
                    StrLine = StrLine + StrFlag.PadRight(1 - 1, pt)
                    'Microsoft.VisualBasic.Right(StrBOMCode, 2) + Microsoft.VisualBasic.Right(StrCompName, 40) + StrCompName + "|" + "N"
                    strData = StrLine
                    wrt.WriteLine(StrLine)
                End While
                Dim StrFileName As String = "PRL" & Format(DtePayDate, "MM") & Format(DtePayDate, "yy")
                Response.Clear()
                Response.ContentType = "text/plain"
                Response.AddHeader("content-disposition", "attachment;filename=" & StrFileName & "")
                Response.Write(wrt.ToString())
                Response.[End]()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try

        End If
    End Sub
End Class