﻿Public Partial Class FrmWarningLetter
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            FillGrid()
        End If
    End Sub

    Protected Sub FillGrid()
        Cmd.Connection = Con
        StrSql = "SELECT * FROM Warning order by EmpID"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvWarn.DataSource = Rdr
        GdvWarn.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "New" Then
            TxtEmpID.Text = ""
            TxtLast.Text = ""
            TxtFirst.Text = ""
            TxtDate.Text = ""
            TxtWarning.Text = ""
            TxtType.Text = ""
            TxtReason.Text = ""
            TxtAction.Text = ""
            TxtEmpID.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Dim Trans As OleDb.OleDbTransaction
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
            Cmd.Connection = Con
            Cmd.Transaction = Trans
            Cmd.CommandText = "Select * FROM Warning"
            Try
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO Warning (EmpID,Last,First,Date,Warning,TypeWarning,Reason,ActionTaken) VALUES('"
                StrSql = StrSql & TxtEmpID.Text & "','"
                StrSql = StrSql & TxtLast.Text & "','"
                StrSql = StrSql & TxtFirst.Text & "','"
                StrSql = StrSql & Format(CDate(TxtDate.Text), "dd-MMM-yyyy") & "','"
                StrSql = StrSql & TxtWarning.Text & "','"
                StrSql = StrSql & TxtType.Text & "','"
                StrSql = StrSql & TxtReason.Text & "','"
                StrSql = StrSql & TxtAction.Text & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                Trans.Commit()

                TxtEmpID.Text = ""
                TxtLast.Text = ""
                TxtFirst.Text = ""
                TxtDate.Text = ""
                TxtWarning.Text = ""
                TxtType.Text = ""
                TxtReason.Text = ""
                TxtAction.Text = ""
                TxtEmpID.Focus()
                FillGrid()
            Catch ex As Exception
                Trans.Rollback()
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Cmd.Connection = Con
            'Cmd.CommandText = "DELETE FROM Warning WHERE [Date] = '" & Format(CDate(TxtDate.Text), "yyyy-MM-dd") & "' And EmpID = '" & TxtEmpID.Text & "'"
            Cmd.CommandText = "Delete FROM Warning WHERE RecNo = " & Val(TxtRecNo.Text) & ""
            Try
                Cmd.ExecuteNonQuery()
                TxtEmpID.Text = ""
                TxtLast.Text = ""
                TxtFirst.Text = ""
                TxtDate.Text = ""
                TxtWarning.Text = ""
                TxtType.Text = ""
                TxtReason.Text = ""
                TxtAction.Text = ""
                TxtEmpID.Focus()
                FillGrid()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtEmpID.Text)
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
        End If
    End Sub

    Private Sub GdvExp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvWarn.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvWarn, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvWarn_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvWarn.SelectedIndexChanged
        TxtEmpID.Text = GdvWarn.SelectedRow.Cells(0).Text.Replace("&nbsp;", "")
        TxtLast.Text = GdvWarn.SelectedRow.Cells(1).Text.Replace("&nbsp;", "")
        TxtFirst.Text = GdvWarn.SelectedRow.Cells(2).Text.Replace("&nbsp;", "")
        TxtDate.Text = GdvWarn.SelectedRow.Cells(3).Text.Replace("&nbsp;", "")
        TxtWarning.Text = GdvWarn.SelectedRow.Cells(4).Text.Replace("&nbsp;", "")
        TxtType.Text = GdvWarn.SelectedRow.Cells(5).Text.Replace("&nbsp;", "")
        TxtReason.Text = GdvWarn.SelectedRow.Cells(6).Text.Replace("&nbsp;", "")
        TxtAction.Text = GdvWarn.SelectedRow.Cells(7).Text.Replace("&nbsp;", "")
        TxtRecNo.Text = GdvWarn.SelectedRow.Cells(8).Text.Replace("&nbsp;", "")
    End Sub

    Private Sub GdvWarn_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvWarn.SelectedIndexChanging
        GdvWarn.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvWarn.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvWarn.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
        Else
            Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(1))
            ToolBar1_ItemClick(Me, ev)
        End If
        Rdr.Close()
        FillGrid()
    End Sub

    Protected Sub BtnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnView.Click
        Dim StrSelFormula As String
        Session("ReportFile") = "RWarning.rpt"

        Dim DtePayDate As Date = CDate(TxtDate.Text)
        Dim StrDate As String = Format(DtePayDate, "yyyy,MM,dd")
        StrSelFormula = "{Warning.Date}=Date(" & StrDate & ") And {Warning.EmpID} = '" & TxtEmpID.Text & "'"

        Session("SelFormula") = StrSelFormula
        Session("RepHead") = "Warning Letter For " & TxtLast.Text & " " & TxtFirst.Text
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)

    End Sub

    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnDelete.Click
        Cmd.Connection = Con
        'Cmd.CommandText = "DELETE FROM Warning WHERE [Date] = '" & Format(CDate(TxtDate.Text), "yyyy-MM-dd") & "' And EmpID = '" & TxtEmpID.Text & "'"
        Cmd.CommandText = "Delete FROM Warning WHERE RecNo = " & Val(TxtRecNo.Text) & ""
        Try
            Cmd.ExecuteNonQuery()
            TxtEmpID.Text = ""
            TxtLast.Text = ""
            TxtFirst.Text = ""
            TxtDate.Text = ""
            TxtWarning.Text = ""
            TxtType.Text = ""
            TxtReason.Text = ""
            TxtAction.Text = ""
            TxtEmpID.Focus()
            FillGrid()
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
        End Try
    End Sub
End Class