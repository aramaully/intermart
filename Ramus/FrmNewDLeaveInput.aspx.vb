﻿Public Partial Class FrmNewDLeaveInput
    Inherits System.Web.UI.Page
    Dim TimexCon As New OleDb.OleDbConnection
    Dim TimexCmd As New OleDb.OleDbCommand
    Dim RosterCmd As New OleDb.OleDbCommand
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdEmp As New OleDb.OleDbCommand
    Dim RdrEmp As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdLeave As New OleDb.OleDbCommand
    Dim CmdLeaveBal As New OleDb.OleDbCommand
    Dim RdrLeaveBal As OleDb.OleDbDataReader
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrAudit As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        TimexCon.ConnectionString = Session("ConnString")
        TimexCon.Open()
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT Name FROM LeaveMaster"
            Rdr = Cmd.ExecuteReader
            CmbApproved.Items.Add("Select...")
            While Rdr.Read
                CmbApproved.Items.Add(Rdr("Name"))
            End While
            Rdr.Close()
            TxtYear.Text = Year(Date.Today)
        End If
    End Sub

    Private Sub FillGrid()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Cmd.Connection = Con
        StrSql = "SELECT [Date],Type, Leave FROM Leave WHERE Leave.EmpID = '" & TxtEmpID.Text & "' AND Year(Date) = " & TxtYear.Text
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvLeave.DataSource = Rdr
        GdvLeave.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ev As New System.EventArgs
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If e.Item.CommandName = "Save" Then
            Cmd.Connection = Con
            Try
                Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                Dim DteFrom As Date
                Dim DteTo As Date
                DteFrom = Format(CDate(TxtFrom.Text), "dd/MM/yyyy")
                DteTo = Format(CDate(TxtTo.Text), "dd/MM/yyyy")
                Do While DteFrom <= DteTo
                    StrSql = "DELETE FROM Leave WHERE EmpID = '" & TxtEmpID.Text & "' "
                    StrSql = StrSql & "AND [Date] = '" & Format(DteFrom, "dd-MMM-yyyy") & "' "
                    StrSql = StrSql & "AND Type = '" & CmbApproved.Text & "'"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                    StrSql = "INSERT INTO Leave (EmpID,[Date],Type,Year,Leave) VALUES('"
                    StrSql = StrSql & TxtEmpID.Text & "','"
                    StrSql = StrSql & Format(DteFrom, "dd-MMM-yyyy") & "','"
                    StrSql = StrSql & CmbApproved.Text & "',"
                    StrSql = StrSql & Val(TxtYear.Text) & ","
                    StrSql = StrSql & Val(TxtLeave.Text) & ")"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                    'Audit
                    Dim TimexCon As New OleDb.OleDbConnection
                    TimexCon.ConnectionString = Session("ConnString") 'Session("DesktopTNT") ' Database of desktop Payroll TNT
                    TimexCon.Open()
                    CmdAudit.Connection = TimexCon
                    StrAudit = "Leave Record for " & TxtEmpID.Text & " Added " & CmbApproved.Text & " "
                    CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Leave Record Added','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                    CmdAudit.ExecuteNonQuery()

                    DteFrom = DteFrom.AddDays(1)
                Loop
                FillGrid()
                'TxtFirst.Text = ""
                'TxtLast.Text = ""
                'TxtClockID.Text = ""
                'TxtCategory.Text = ""
                'TxtTitle.Text = ""
                'CmbApproved.SelectedIndex = 0
                'TxtFrom.Text = ""
                'TxtTo.Text = ""
                'ImgPhoto.ImageUrl = Nothing
                'GdvLeave.Dispose()
                'GdvLeave.DataSource = ""
                'GdvLeave.DataBind()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Try
                Cmd.Connection = Con
                StrSql = "DELETE FROM Leave WHERE EmpID = '" & TxtEmpID.Text & "' "
                StrSql = StrSql & "AND [Date] = '" & Format(CDate(TxtFrom.Text), "dd-MMM-yyyy") & "' "
                StrSql = StrSql & "AND Type = '" & CmbApproved.Text & "'"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Leave Record for " & TxtEmpID.Text & " Deleted " & CmbApproved.Text & " "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Leave Record Deleted','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()

                CmbApproved.SelectedIndex = 0
                TxtFrom.Text = ""
                TxtLeave.Text = "1"
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Next" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID > '" & TxtEmpID.Text & "'"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try
        End If
        If e.Item.CommandName = "Previous" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID < '" & TxtEmpID.Text & "' ORDER BY EmpID DESC"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try
        End If
        If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtEmpID.Text)
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
        End If
    End Sub

    Private Sub GdvLeave_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvLeave.RowDataBound
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvLeave, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvLoan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvLeave.SelectedIndexChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        On Error Resume Next
        TxtFrom.Text = GdvLeave.SelectedRow.Cells(0).Text
        CmbApproved.Text = GdvLeave.SelectedRow.Cells(1).Text
        TxtLeave.Text = GdvLeave.SelectedRow.Cells(2).Text
        TxtEmpID_TextChanged(Me, e)
    End Sub

    Private Sub GdvLeave_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvLeave.SelectedIndexChanging
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        GdvLeave.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        For i = 0 To Me.GdvLeave.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvLeave.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub


    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        'ClearAll()
        Dim IntLocal As Integer = 0
        Dim IntSick As Integer = 0
        If Rdr.Read Then
            On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
            TxtTitle.Text = Rdr("Title")
            TxtCategory.Text = Rdr("Category")
            TxtGender.Text = Rdr("Gender")
            TxtCategory.Text = Rdr("Category")
            ChkClocked.Checked = IIf(Rdr("Clocked"), True, False)
            TxtClockID.Text = Rdr("BatchNo")
            IntLocal = Rdr("Local")
            IntSick = Rdr("Sick")
            Session.Add("EmpID", Rdr("EmpID"))
            ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & Rdr("EmpID")
        End If
        Rdr.Close()
        Dim SngCasual As Single = 0
        Dim SngSick As Single = 0
        CmdLeaveBal.Connection = Con
        StrSql = "SELECT * FROM Leave WHERE Leave.EmpID = '" & TxtEmpID.Text & "'"
        StrSql = StrSql + " AND Year(Date) = " & TxtYear.Text
        CmdLeaveBal.CommandText = StrSql
        RdrLeaveBal = CmdLeaveBal.ExecuteReader
        While RdrLeaveBal.Read
            If RdrLeaveBal("Type") = "Local Leave" Then
                SngCasual = SngCasual + RdrLeaveBal("Leave")
            End If
            If RdrLeaveBal("Type") = "Sick Leave" Then
                SngSick = SngSick + RdrLeaveBal("Leave")
            End If
        End While
        TxtLocal.Text = IntLocal - SngCasual
        TxtSick.Text = IntSick - SngSick
        FillGrid()
    End Sub
End Class