﻿Public Partial Class FrmBank
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrAudit As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            FillGrid()
        End If
    End Sub

    Protected Sub FillGrid()
        Cmd.Connection = Con
        StrSql = "SELECT * FROM Bank"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvBank.DataSource = Rdr
        GdvBank.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim Ev As New System.EventArgs

        If e.Item.CommandName = "New" Then
            TxtBankID.Text = ""
            TxtName.Text = ""
            TxtAddress.Text = ""
            TxtBankCode.Text = ""
            TxtTelephone.Text = ""
            TxtFax.Text = ""
            TxtMSNCode.Text = ""
            TxtFormat.Text = ""
            TxtBankID.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Dim Trans As OleDb.OleDbTransaction
            Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
            Cmd.Connection = Con
            Cmd.Transaction = Trans
            Cmd.CommandText = "DELETE FROM Bank WHERE BankID = '" & TxtBankID.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO Bank (BankID,Name,Address,Telephone,Fax,MNSCode,Format,BankCode) VALUES('"
                StrSql = StrSql & TxtBankID.Text & "','"
                StrSql = StrSql & TxtName.Text & "','"
                StrSql = StrSql & TxtAddress.Text & "','"
                StrSql = StrSql & TxtTelephone.Text & "','"
                StrSql = StrSql & TxtFax.Text & "','"
                StrSql = StrSql & TxtMSNCode.Text & "','"
                StrSql = StrSql & TxtFormat.Text & "','"
                StrSql = StrSql & TxtBankCode.Text & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                Trans.Commit()
                TxtBankID.Text = ""
                TxtName.Text = ""
                TxtAddress.Text = ""
                TxtBankCode.Text = ""
                TxtTelephone.Text = ""
                TxtFax.Text = ""
                TxtMSNCode.Text = ""
                TxtFormat.Text = ""
                TxtBankID.Focus()
                FillGrid()
                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Bank ID " & TxtBankID.Text & " Created / Edited. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Create / Edit Bank','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                Trans.Rollback()
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Bank WHERE BankID = '" & TxtBankID.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                TxtBankID.Text = ""
                TxtName.Text = ""
                TxtAddress.Text = ""
                TxtBankCode.Text = ""
                TxtTelephone.Text = ""
                TxtFax.Text = ""
                TxtMSNCode.Text = ""
                TxtFormat.Text = ""
                TxtBankID.Focus()
                FillGrid()
                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Bank ID " & TxtBankID.Text & " Deleted. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Deleted Bank','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
    End Sub

    Private Sub GdvLoan_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvBank.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvBank, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvBank_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvBank.SelectedIndexChanged
        TxtBankID.Text = GdvBank.SelectedRow.Cells(0).Text
        TxtName.Text = GdvBank.SelectedRow.Cells(1).Text
        TxtBankCode.Text = GdvBank.SelectedRow.Cells(2).Text
        TxtAddress.Text = GdvBank.SelectedRow.Cells(3).Text
        TxtTelephone.Text = GdvBank.SelectedRow.Cells(4).Text
        TxtFax.Text = GdvBank.SelectedRow.Cells(5).Text
        TxtMSNCode.Text = GdvBank.SelectedRow.Cells(6).Text
        TxtFormat.Text = GdvBank.SelectedRow.Cells(7).Text
    End Sub

    Private Sub GdvBank_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvBank.SelectedIndexChanging
        GdvBank.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvBank.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvBank.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub
End Class