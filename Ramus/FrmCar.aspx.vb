﻿Public Partial Class FrmCar
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrAudit As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            FillGrid()
        End If
    End Sub

    Protected Sub FillGrid()
        Cmd.Connection = Con
        StrSql = "SELECT * FROM Car"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvUseCar.DataSource = Rdr
        GdvUseCar.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim Ev As New System.EventArgs

        If e.Item.CommandName = "New" Then
            TxtUseCar.Text = ""
            TxtFrom1.Text = ""
            TxtTo1.Text = ""
            TxtAmount1.Text = ""
            TxtFrom2.Text = ""
            TxtTo2.Text = ""
            TxtAmount2.Text = ""
            TxtFrom3.Text = ""
            TxtTo3.Text = ""
            TxtAmount3.Text = ""
            TxtFrom4.Text = ""
            TxtTo4.Text = ""
            TxtAmount4.Text = ""
            TxtFrom5.Text = ""
            TxtTo5.Text = ""
            TxtAmount5.Text = ""
            TxtUseCar.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Dim Trans As OleDb.OleDbTransaction
            Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
            Cmd.Connection = Con
            Cmd.Transaction = Trans
            Cmd.CommandText = "DELETE FROM Car WHERE UseCar = '" & TxtUseCar.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO Car (UseCar,[From-1],[To-1],[Amount-1],[From-2],[To-2],[Amount-2] "
                StrSql = StrSql & ",[From-3],[To-3],[Amount-3],[From-4],[To-4],[Amount-4],[From-5],[To-5],[Amount-5]) VALUES('"
                StrSql = StrSql & TxtUseCar.Text & "',"
                StrSql = StrSql & Val(TxtFrom1.Text) & ","
                StrSql = StrSql & Val(TxtTo1.Text) & ","
                StrSql = StrSql & Val(TxtAmount1.Text) & ","
                StrSql = StrSql & Val(TxtFrom2.Text) & ","
                StrSql = StrSql & Val(TxtTo2.Text) & ","
                StrSql = StrSql & Val(TxtAmount2.Text) & ","
                StrSql = StrSql & Val(TxtFrom3.Text) & ","
                StrSql = StrSql & Val(TxtTo3.Text) & ","
                StrSql = StrSql & Val(TxtAmount3.Text) & ","
                StrSql = StrSql & Val(TxtFrom4.Text) & ","
                StrSql = StrSql & Val(TxtTo4.Text) & ","
                StrSql = StrSql & Val(TxtAmount4.Text) & ","
                StrSql = StrSql & Val(TxtFrom5.Text) & ","
                StrSql = StrSql & Val(TxtTo5.Text) & ","
                StrSql = StrSql & Val(TxtAmount5.Text) & ")"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                Trans.Commit()
                TxtUseCar.Text = ""
                TxtFrom1.Text = ""
                TxtTo1.Text = ""
                TxtAmount1.Text = ""
                TxtFrom2.Text = ""
                TxtTo2.Text = ""
                TxtAmount2.Text = ""
                TxtFrom3.Text = ""
                TxtTo3.Text = ""
                TxtAmount3.Text = ""
                TxtFrom4.Text = ""
                TxtTo4.Text = ""
                TxtAmount4.Text = ""
                TxtFrom5.Text = ""
                TxtTo5.Text = ""
                TxtAmount5.Text = ""
                TxtUseCar.Focus()
                FillGrid()
                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Car Benefit Created / Edited."
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Car Benefit Created / Edited','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                Trans.Rollback()
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Car WHERE UseCar = '" & TxtUseCar.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                TxtUseCar.Text = ""
                TxtFrom1.Text = ""
                TxtTo1.Text = ""
                TxtAmount1.Text = ""
                TxtFrom2.Text = ""
                TxtTo2.Text = ""
                TxtAmount2.Text = ""
                TxtFrom3.Text = ""
                TxtTo3.Text = ""
                TxtAmount3.Text = ""
                TxtFrom4.Text = ""
                TxtTo4.Text = ""
                TxtAmount4.Text = ""
                TxtFrom5.Text = ""
                TxtTo5.Text = ""
                TxtAmount5.Text = ""
                TxtUseCar.Focus()
                FillGrid()
                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Car Benefit Deleted."
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Car Benefit Deleted.','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & ex.Message & "','Error');</script>", False)
            End Try
        End If
    End Sub

    Private Sub GdvLoan_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvUseCar.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvUseCar, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvBank_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvUseCar.SelectedIndexChanged
        TxtUseCar.Text = GdvUseCar.SelectedRow.Cells(0).Text
        TxtFrom1.Text = GdvUseCar.SelectedRow.Cells(1).Text
        TxtTo1.Text = GdvUseCar.SelectedRow.Cells(2).Text
        TxtAmount1.Text = GdvUseCar.SelectedRow.Cells(3).Text
        TxtFrom2.Text = GdvUseCar.SelectedRow.Cells(4).Text
        TxtTo2.Text = GdvUseCar.SelectedRow.Cells(5).Text
        TxtAmount2.Text = GdvUseCar.SelectedRow.Cells(6).Text
        TxtFrom3.Text = GdvUseCar.SelectedRow.Cells(7).Text
        TxtTo3.Text = GdvUseCar.SelectedRow.Cells(8).Text
        TxtAmount3.Text = GdvUseCar.SelectedRow.Cells(9).Text
        TxtFrom4.Text = GdvUseCar.SelectedRow.Cells(10).Text
        TxtTo4.Text = GdvUseCar.SelectedRow.Cells(11).Text
        TxtAmount4.Text = GdvUseCar.SelectedRow.Cells(12).Text
        TxtFrom5.Text = GdvUseCar.SelectedRow.Cells(13).Text
        TxtTo5.Text = GdvUseCar.SelectedRow.Cells(14).Text
        TxtAmount5.Text = GdvUseCar.SelectedRow.Cells(15).Text
    End Sub

    Private Sub GdvBank_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvUseCar.SelectedIndexChanging
        GdvUseCar.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvUseCar.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvUseCar.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub
End Class