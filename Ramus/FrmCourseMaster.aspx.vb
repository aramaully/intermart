﻿Public Partial Class FrmCourseMaster
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrAudit As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            'Qty
            Dim I As Integer
            For I = 1 To 100
                CmbNo.Items.Add(I)
            Next I
            FillGrid()
        End If
    End Sub

    Protected Sub FillGrid()
        Cmd.Connection = Con
        StrSql = "SELECT * FROM CourseMaster"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvCourseMaster.DataSource = Rdr
        GdvCourseMaster.DataBind()
        Rdr.Close()
    End Sub
    Protected Sub ClearAll()
        TxtCourseCode.Text = ""
        TxtName.Text = ""
        CmbNo.SelectedIndex = 0
        CmbDesc.SelectedIndex = 0
        TxtRequirement.Text = ""
        CmbDelivery.SelectedIndex = 0
        CmbTraining.SelectedIndex = 0
        CmbSatus.SelectedIndex = 0
        TxtAwardBody.Text = ""
        TxtFees.Text = ""
        TxtTrainer.Text = ""
        TxtVenue.Text = ""
        TxtCLevel.Text = ""
    End Sub
    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim Ev As New System.EventArgs

        If e.Item.CommandName = "New" Then
            ClearAll()
            TxtCourseCode.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Dim Trans As OleDb.OleDbTransaction
            Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
            Cmd.Connection = Con
            Cmd.Transaction = Trans
            Cmd.CommandText = "DELETE FROM CourseMaster WHERE RecNo = " & TxtRecNo.Text & ""
            Try
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO CourseMaster (CourseCode,Name,TimeNo,TimeName,Requirement,Delivery,InternalExternal,Status,AwardBody,Fees,Trainer,Venue,CLevel) VALUES('"
                StrSql = StrSql & TxtCourseCode.Text & "','"
                StrSql = StrSql & TxtName.Text & "',"
                StrSql = StrSql & CmbNo.Text & ",'"
                StrSql = StrSql & CmbDesc.Text & "','"
                StrSql = StrSql & TxtRequirement.Text & "','"
                StrSql = StrSql & CmbDelivery.Text & "','"
                StrSql = StrSql & CmbTraining.Text & "','"
                StrSql = StrSql & CmbSatus.Text & "','"
                StrSql = StrSql & TxtAwardBody.Text & "','"
                StrSql = StrSql & TxtFees.Text & "','"
                StrSql = StrSql & TxtTrainer.Text & "','"
                StrSql = StrSql & TxtVenue.Text & "','"
                StrSql = StrSql & TxtCLevel.Text & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                Trans.Commit()
                ClearAll()
                TxtCourseCode.Focus()
                FillGrid()

                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Course Code " & TxtCourseCode.Text & " Created / Edited. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Create / Edit Course','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                Trans.Rollback()
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM CourseMaster WHERE RecNo = " & TxtRecNo.Text & ""
            Try
                Cmd.ExecuteNonQuery()
                ClearAll()
                TxtCourseCode.Focus()
                FillGrid()

                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Course Code " & TxtCourseCode.Text & " Deleted. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Delete Course','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
    End Sub

    Private Sub GdvLoan_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvCourseMaster.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvCourseMaster, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvBank_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvCourseMaster.SelectedIndexChanged
        Try
            TxtCourseCode.Text = GdvCourseMaster.SelectedRow.Cells(0).Text
            TxtName.Text = GdvCourseMaster.SelectedRow.Cells(1).Text
            CmbNo.Text = GdvCourseMaster.SelectedRow.Cells(2).Text
            CmbDesc.Text = GdvCourseMaster.SelectedRow.Cells(3).Text
            TxtRequirement.Text = GdvCourseMaster.SelectedRow.Cells(4).Text
            CmbDelivery.Text = GdvCourseMaster.SelectedRow.Cells(5).Text
            CmbTraining.Text = GdvCourseMaster.SelectedRow.Cells(6).Text
            CmbSatus.Text = GdvCourseMaster.SelectedRow.Cells(7).Text
            TxtAwardBody.Text = GdvCourseMaster.SelectedRow.Cells(8).Text
            TxtFees.Text = GdvCourseMaster.SelectedRow.Cells(9).Text
            TxtTrainer.Text = GdvCourseMaster.SelectedRow.Cells(10).Text
            TxtVenue.Text = GdvCourseMaster.SelectedRow.Cells(11).Text
            TxtCLevel.Text = GdvCourseMaster.SelectedRow.Cells(12).Text
            TxtRecNo.Text = GdvCourseMaster.SelectedRow.Cells(13).Text
        Catch

        End Try
    End Sub

    Private Sub GdvBank_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvCourseMaster.SelectedIndexChanging
        GdvCourseMaster.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvCourseMaster.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvCourseMaster.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub
End Class