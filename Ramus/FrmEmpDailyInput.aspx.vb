﻿Public Partial Class EmpDailyInput

    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim CmdTemp As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdLM As New OleDb.OleDbCommand
    Dim RdrLM As OleDb.OleDbDataReader
    Dim CmdHoliday As New OleDb.OleDbCommand
    Dim RdrHoliday As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim SngYear As Single

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            TxtEmpID.Text = ""
            TxtYear.Text = Year(Date.Today)
        End If
    End Sub

    Protected Sub ClearAll()
        TxtFirst.Text = ""
        TxtLast.Text = ""
    End Sub


    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
            StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Else
            StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "' And OutletName = '" & Session("Level") & "'"
        End If
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        If TxtEmpID.Text = "ALL" Then Exit Sub
        If Rdr.Read Then
            On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
        Else
            Call TxtFind_Click(Me, sender)
        End If
        Rdr.Close()
        'Get Local and Sick Balance
        Dim SngLocal As Single = 0
        Dim SngSick As Single = 0
        Dim SngTotLocal As Single = 0
        Dim SngTotSick As Single = 0
        Dim CmdEmpLeave As New OleDb.OleDbCommand
        Dim RdrEmpLeave As OleDb.OleDbDataReader
        CmdEmpLeave.Connection = Con
        'Get Employee Local and Sick for the year
        CmdEmpLeave.CommandText = "Select Local,Sick from Employee where EmpID = '" & Trim(TxtEmpID.Text) & "'"
        RdrEmpLeave = CmdEmpLeave.ExecuteReader
        While RdrEmpLeave.Read
            SngLocal = Val(RdrEmpLeave("Local") & "")
            SngSick = Val(RdrEmpLeave("Sick") & "")
        End While
        RdrEmpLeave.Close()
        'Get Total Local and Sick
        StrSql = "SELECT Leave.Type,Leave.Leave,LeaveMaster.Paid"
        StrSql = StrSql & " FROM Leave,LeaveMaster "
        StrSql = StrSql & "WHERE Leave.Type = LeaveMaster.Name "
        StrSql = StrSql & "And Year = " & Val(TxtYear.Text) & ""
        StrSql = StrSql & " AND EmpID = '" & Trim(TxtEmpID.Text) & "'  ORDER BY [Date]"
        CmdEmpLeave.CommandText = StrSql
        RdrEmpLeave = CmdEmpLeave.ExecuteReader
        While RdrEmpLeave.Read
            If RdrEmpLeave(0) = "Local Leave" Then
                SngTotLocal = SngTotLocal + RdrEmpLeave("Leave")
            End If
            If RdrEmpLeave(0) = "Sick Leave" Then
                SngTotSick = SngTotSick + RdrEmpLeave("Leave")
            End If
        End While
        RdrEmpLeave.Close()
        TxtLocalBalance.Text = SngLocal - SngTotLocal
        TxtSickBalance.Text = SngSick - SngTotSick
    End Sub

    Protected Sub TxtFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles TxtFind.Click
        Session.Add("PstrCode", TxtEmpID.Text)
        ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
    End Sub

    Protected Sub BtnRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnRefresh.Click
        Dim Dt As New DataTable
        Dim Dc As New DataColumn
        Dim Dr As DataRow
        Dim Count As Integer
        Dim I As Integer = 0
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")

        LblMsg.Visible = False
        LblMsg.Text = ""

        If TxtFromDate.Text = "" Then
            LblMsg.Visible = True
            LblMsg.Text = "Please Enter Date From!!!"
            Exit Sub
        End If
        If TxtToDate.Text = "" Then
            LblMsg.Visible = True
            LblMsg.Text = "Please Enter Date To !!!"
            Exit Sub
        End If
        Dim DteFromDate As Date = CDate(TxtFromDate.Text).ToString("dd-MMM-yyyy")
        Dim DteToDate As Date = CDate(TxtToDate.Text).ToString("dd-MMM-yyyy")
        Dim DteLeaveFrom As Date = CDate(TxtFromDate.Text).ToString("dd-MMM-yyyy")
        Dim DteLeaveTo As Date = CDate(TxtToDate.Text).ToString("dd-MMM-yyyy")

        'Format(CDate(DteLeaveFrom), "dd/MMM/yyyy")
        'Format(CDate(DteLeaveTo), "dd/MMM/yyyy")

        Count = Math.Round((DateDiff(DateInterval.Day, DteFromDate, DteToDate)), 2)

        For I = 0 To Count
            Dr = Dt.NewRow
            Dt.Rows.Add(Dr)
        Next
        GdvTVList.DataSource = Dt
        GdvTVList.DataBind()
        I = 0

        DirectCast(GdvTVList.Rows(I).FindControl("TxtDate"), TextBox).Text = CDate(DateAdd("d", -1, CDate(TxtFromDate.Text).ToString("dd-MMM-yyyy"))).ToString("dd-MMM-yyyy")
        While DteLeaveFrom <= DteLeaveTo
            DirectCast(GdvTVList.Rows(I).FindControl("TxtDate"), TextBox).Text = CDate(DateAdd("d", I, CDate(TxtFromDate.Text).ToString("dd-MMM-yyyy"))).ToString("dd-MMM-yyyy")
            CmdLM.Connection = Con
            CmdLM.CommandText = "Select * from LeaveMaster"
            RdrLM = CmdLM.ExecuteReader
            While RdrLM.Read
                DirectCast(GdvTVList.Rows(I).FindControl("CmbLeaveType"), DropDownList).Items.Add(RdrLM("Name"))
            End While
            RdrLM.Close()
            I = I + 1
            DteLeaveFrom = DteLeaveFrom.AddDays(1)
        End While
        I = 0
        While CDate(DteFromDate) <= CDate(DteToDate)
            StrSql = "SELECT * FROM Leave where EmpID = '" & TxtEmpID.Text & "' And Date = '" & DteFromDate.ToString("yyyy-MMM-dd") & "'"
            CmdLM.CommandText = StrSql
            RdrLM = CmdLM.ExecuteReader
            While RdrLM.Read
                DirectCast(GdvTVList.Rows(I).FindControl("CmbLeaveType"), DropDownList).Text = RdrLM("Type") & ""
                DirectCast(GdvTVList.Rows(I).FindControl("TxtDay"), TextBox).Text = Val(RdrLM("Leave") & "")
            End While
            RdrLM.Close()

            StrSql = "SELECT * FROM DailyInput where EmpID = '" & TxtEmpID.Text & "' And Date = '" & DteFromDate.ToString("yyyy-MM-dd") & "'"
            CmdLM.CommandText = StrSql
            RdrLM = CmdLM.ExecuteReader
            While RdrLM.Read
                DirectCast(GdvTVList.Rows(I).FindControl("TxtOT10"), TextBox).Text = Val(RdrLM("OT10") & "")
                DirectCast(GdvTVList.Rows(I).FindControl("TxtOT15"), TextBox).Text = Val(RdrLM("OT15") & "")
                DirectCast(GdvTVList.Rows(I).FindControl("TxtOT20"), TextBox).Text = Val(RdrLM("OT20") & "")
                DirectCast(GdvTVList.Rows(I).FindControl("TxtOT30"), TextBox).Text = Val(RdrLM("OT30") & "")
                DirectCast(GdvTVList.Rows(I).FindControl("TxtLateness"), TextBox).Text = Val(RdrLM("Lateness") & "")
            End While
            RdrLM.Close()

            If Weekday(DteFromDate) = 1 Then
                DirectCast(GdvTVList.Rows(I).FindControl("TxtDTravel"), TextBox).Text = "SU"
            ElseIf Weekday(DteFromDate) > 1 And Weekday(DteFromDate) < 7 Then
                DirectCast(GdvTVList.Rows(I).FindControl("TxtDTravel"), TextBox).Text = "WD"
            ElseIf Weekday(DteFromDate) = 7 Then
                DirectCast(GdvTVList.Rows(I).FindControl("TxtDTravel"), TextBox).Text = "WE"
            End If

            CmdHoliday.Connection = Con
            StrSql = "SELECT * From Holiday where Date = '" & Format(DteFromDate, "yyyy-MM-dd") & "' Order by [Date]"
            CmdHoliday.CommandText = StrSql
            RdrHoliday = CmdHoliday.ExecuteReader
            While RdrHoliday.Read
                DirectCast(GdvTVList.Rows(I).FindControl("TxtDTravel"), TextBox).Text = "PH"
            End While
            RdrHoliday.Close()

            I = I + 1
            DteFromDate = DteFromDate.AddDays(1)
        End While
    End Sub
    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnSave.Click
        LblMsg.Visible = False
        If TxtFromDate.Text = "" Then
            LblMsg.Visible = True
            LblMsg.Text = "Please Enter Date From!!!"
            Exit Sub
        End If
        If TxtToDate.Text = "" Then
            LblMsg.Visible = True
            LblMsg.Text = "Please Enter Date To !!!"
            Exit Sub
        End If
        Dim Trans As OleDb.OleDbTransaction
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Transaction = Trans

        Dim DteFromDate As Date = CDate(TxtFromDate.Text).ToString("yyyy-MM-dd")
        Dim DteToDate As Date =CDate(TxtToDate.Text).ToString("yyyy-MM-dd")
        'Cmd.Connection = Con
        'Cmd.CommandText = "Delete FROM DailyInput where EmpID = '" & TxtEmpID.Text & "' And Date >= '" & DteFromDate.ToString("yyyy-MMM-dd") & "' and  Date <= '" & DteToDate.ToString("yyyy-MMM-dd") & "'"
        Try
            'Cmd.ExecuteNonQuery()
            For Each Row As GridViewRow In GdvTVList.Rows
                If Trim(DirectCast(Row.FindControl("TxtDate"), TextBox).Text) <> "" Then

                    Cmd.Connection = Con
                    'Cmd.CommandText = "Delete FROM DailyInput where EmpID = '" & TxtEmpID.Text & "' And Date >= '" & DteFromDate.ToString("yyyy-MMM-dd") & "' and  Date <= '" & DteToDate.ToString("yyyy-MMM-dd") & "'"
                    Cmd.CommandText = "Delete FROM DailyInput where EmpID = '" & TxtEmpID.Text & "' And Date = '" & Trim(DirectCast(Row.FindControl("TxtDate"), TextBox).Text) & "'"
                    Cmd.ExecuteNonQuery()

                    StrSql = "Insert Into DailyInput Values('" & Trim(TxtEmpID.Text) & "','"
                    StrSql = StrSql & Trim(DirectCast(Row.FindControl("TxtDate"), TextBox).Text) & "',"
                    StrSql = StrSql & Val(Trim(DirectCast(Row.FindControl("TxtOT15"), TextBox).Text)) & ","
                    StrSql = StrSql & Val(Trim(DirectCast(Row.FindControl("TxtOT20"), TextBox).Text)) & ","
                    StrSql = StrSql & Val(Trim(DirectCast(Row.FindControl("TxtOT30"), TextBox).Text)) & ",'"
                    StrSql = StrSql & Trim(DirectCast(Row.FindControl("CmbLeaveType"), DropDownList).Text) & "',"
                    StrSql = StrSql & Val(Trim(DirectCast(Row.FindControl("TxtDay"), TextBox).Text)) & ","
                    StrSql = StrSql & Val(Trim(DirectCast(Row.FindControl("TxtOT10"), TextBox).Text)) & ",'"
                    StrSql = StrSql & (Trim(DirectCast(Row.FindControl("TxtDTravel"), TextBox).Text)) & "',"
                    StrSql = StrSql & Val(Trim(DirectCast(Row.FindControl("TxtLateness"), TextBox).Text)) & ")"
                    Cmd.Connection = Con
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                End If
            Next
            Trans.Commit()
            LblMsg.Visible = True
            LblMsg.Text = "Data has been Saved!!!"
        Catch ex As Exception
            Trans.Rollback()
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
        End Try
    End Sub
    ' ''Private Sub GdvTVList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GdvTVList.RowEditing
    ' ''    Dim CmdEmpLeave As New OleDb.OleDbCommand       TxtDay  TxtDTravel TxtOT10
    ' ''    Dim RdrEmpLeave As OleDb.OleDbDataReader
    ' ''    Dim SngLocal As Single = 0
    ' ''    Dim SngSick As Single = 0
    ' ''    Dim SngTotLocal As Single = 0
    ' ''    Dim SngTotSick As Single = 0

    ' ''    For Each Row As GridViewRow In GdvTVList.Rows
    ' ''        If Trim(DirectCast(Row.FindControl("TxtDay"), TextBox).Text) > 0 Then
    ' ''            'Get Employee Local and Sick for the year
    ' ''            CmdEmpLeave.Connection = Con
    ' ''            CmdEmpLeave.CommandText = "Select Local,Sick from Employee where EmpID = '" & Trim(TxtEmpID.Text) & "'"
    ' ''            RdrEmpLeave = CmdEmpLeave.ExecuteReader
    ' ''            While RdrEmpLeave.Read
    ' ''                SngLocal = Val(RdrEmpLeave("Local") & "")
    ' ''                SngSick = Val(RdrEmpLeave("Sick") & "")
    ' ''            End While
    ' ''            RdrEmpLeave.Close()
    ' ''            'Get Total Local and Sick
    ' ''            StrSql = "SELECT Leave.Type,Leave.Leave,LeaveMaster.Paid"
    ' ''            StrSql = StrSql & " FROM Leave,LeaveMaster "
    ' ''            StrSql = StrSql & "WHERE Leave.Type = LeaveMaster.Name "
    ' ''            StrSql = StrSql & "And Year = " & SngYear & ""
    ' ''            StrSql = StrSql & " AND EmpID = '" & Trim(TxtEmpID.Text) & "'  ORDER BY [Date]"
    ' ''            CmdEmpLeave.CommandText = StrSql
    ' ''            RdrEmpLeave = CmdEmpLeave.ExecuteReader
    ' ''            While RdrEmpLeave.Read
    ' ''                If RdrEmpLeave(0) = "Local Leave" Then
    ' ''                    SngTotLocal = SngTotLocal + RdrEmpLeave("Leave")
    ' ''                End If
    ' ''                If RdrEmpLeave(0) = "Sick Leave" Then
    ' ''                    SngTotSick = SngTotSick + RdrEmpLeave("Leave")
    ' ''                End If
    ' ''            End While
    ' ''            RdrEmpLeave.Close()

    ' ''            If Trim(DirectCast(Row.FindControl("CmbLeaveType"), DropDownList).Text) = "Local Leave" Then


    ' ''            End If
    ' ''            If Trim(DirectCast(Row.FindControl("CmbLeaveType"), DropDownList).Text) = "Sick Leave" Then

    ' ''            End If
    ' ''        End If
    ' ''    Next
    ' ''End Sub
End Class