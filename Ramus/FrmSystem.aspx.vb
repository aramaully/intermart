﻿Public Partial Class FrmSystem
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            UpdateFields()
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ev As New System.EventArgs
        If e.Item.CommandName = "Save" Then
            StrSql = "UPDATE [System] SET "
            StrSql = StrSql & "Name = '" & TxtName.Text & "',"
            StrSql = StrSql & "Address = '" & TxtAddress.Text & "',"
            StrSql = StrSql & "Business = '" & TxtBusiness.Text & "',"
            StrSql = StrSql & "Phone = '" & TxtTelephone.Text & "',"
            StrSql = StrSql & "Fax = '" & TxtTeleFax.Text & "',"
            StrSql = StrSql & "EMail = '" & TxtEMail.Text & "',"
            StrSql = StrSql & "NPFEmployee = " & Val(TxtNPF.Text) & ","
            StrSql = StrSql & "NPFEmployeeMax = " & Val(TxtNPFMax.Text) & ","
            StrSql = StrSql & "NPFEmployer = " & Val(TxtNPFEmp.Text) & ","
            StrSql = StrSql & "NPFEmployerMax = " & Val(TxtNPFEmpMax.Text) & ","
            StrSql = StrSql & "NSF = " & Val(TxtNSF.Text) & ","
            StrSql = StrSql & "NSFMax = " & Val(TxtNSFMax.Text) & ","
            StrSql = StrSql & "IVTB = " & Val(TxtIVTB.Text) & ","
            StrSql = StrSql & "MinPAYE = " & Val(TxtMinPAYE.Text) & ","
            StrSql = StrSql & "Bank = '" & TxtBank.Text & "',"
            StrSql = StrSql & "BnkAddress = '" & TxtBankAddress.Text & "',"
            StrSql = StrSql & "Attention = '" & TxtAttention.Text & "',"
            StrSql = StrSql & "AccountNo = '" & TxtAccountNo.Text & "',"
            StrSql = StrSql & "PAYERegNo = '" & TxtPAYERegNo.Text & "',"
            StrSql = StrSql & "TaxExempt = " & Val(TxtTaxExempt.Text) & ","
            StrSql = StrSql & "TransportExcess = " & Val(TxtMaxTrans.Text) & ","
            StrSql = StrSql & "Local = " & Val(TxtLocal.Text) & ","
            StrSql = StrSql & "Sick = " & Val(TxtSick.Text) & ","
            StrSql = StrSql & "WorkingDays = " & Val(TxtDays.Text) & ","
            StrSql = StrSql & "WDHrs = " & Val(TxtWDHrs.Text) & ","
            StrSql = StrSql & "WEHrs = " & Val(TxtWEHrs.Text) & ","
            StrSql = StrSql & "BRN = '" & TxtBRN.Text & "',"
            StrSql = StrSql & "TAN = '" & TxtTAN.Text & "',"
            StrSql = StrSql & "PAYEDeclarant = '" & TxtDecName.Text & "',"
            StrSql = StrSql & "Attendance = " & Val(TxtAttendAllow.Text) & ","
            StrSql = StrSql & "Meal = " & Val(TxtMealAllow.Text) & ","
            StrSql = StrSql & "BreakTime = " & Val(TxtBrkTime.Text) & ","
            StrSql = StrSql & "NSFEmployee = " & Val(TxtNSFEmployee.Text) & ","
            StrSql = StrSql & "NSFEmployeeMax = " & Val(TxtNSFEmployeeMax.Text) & ","
            StrSql = StrSql & "EPZEE = " & Val(TxtEPZee.Text) & ","
            StrSql = StrSql & "EPZER = " & Val(TxtEPZer.Text) & ","
            StrSql = StrSql & "PensionEmp = " & Val(TxtPensionEmp.Text) & ","
            StrSql = StrSql & "Name1 = '" & TxtName1.Text & "',"
            StrSql = StrSql & "Deg1 = '" & TxtDeg1.Text & "',"
            StrSql = StrSql & "Name2 = '" & TxtName2.Text & "',"
            StrSql = StrSql & "Deg2 = '" & TxtDeg2.Text & "',"
            StrSql = StrSql & "MaxSalNPS = " & Val(TxtMaxSalNPF.Text) & ""
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Try
                Cmd.ExecuteNonQuery()
                'TxtName.Text = ""
                'TxtAddress.Text = ""
                'TxtBusiness.Text = ""
                'TxtTelephone.Text = ""
                'TxtTeleFax.Text = ""
                'TxtEMail.Text = ""
                'TxtNPF.Text = ""
                'TxtNPFMax.Text = ""
                'TxtNPFEmp.Text = ""
                'TxtNPFEmpMax.Text = ""
                'TxtNSF.Text = ""
                'TxtNSFMax.Text = ""
                'TxtIVTB.Text = ""
                'TxtMinPAYE.Text = ""
                'TxtBank.Text = ""
                'TxtBankAddress.Text = ""
                'TxtAttention.Text = ""
                'TxtAccountNo.Text = ""
                'TxtPAYERegNo.Text = ""
                'TxtTaxExempt.Text = ""
                'TxtMaxTrans.Text = ""
                'TxtLocal.Text = ""
                'TxtSick.Text = ""
                'TxtDays.Text = ""
                'TxtWDHrs.Text = ""
                'TxtWEHrs.Text = ""
                'TxtBRN.Text = ""
                'TxtTAN.Text = ""
                'TxtDecName.Text = ""
                'TxtAttendAllow.Text = ""
                'TxtMealAllow.Text = ""
                'TxtBrkTime.Text = ""
                'TxtNSFEmployee.Text = ""
                'TxtNSFEmployeeMax.Text = ""
                'TxtMaxSalNPF.Text = ""
                'TxtBrkTime.Text = ""
                'TxtName1.Text = ""
                'TxtName2.Text = ""
                'TxtDeg1.Text = ""
                'TxtDeg2.Text = ""
                'TxtPensionEmp.Text = ""
                'TxtEPZee.Text = ""
                'TxtEPZer.Text = ""
            Catch ex As Exception

            End Try
        End If
        If e.Item.CommandName = "Refresh" Then
            UpdateFields()
            'TxtName.Text = ""
            'TxtAddress.Text = ""
            'TxtBusiness.Text = ""
            'TxtTelephone.Text = ""
            'TxtTeleFax.Text = ""
            'TxtEMail.Text = ""
            'TxtNPF.Text = ""
            'TxtNPFMax.Text = ""
            'TxtNPFEmp.Text = ""
            'TxtNPFEmpMax.Text = ""
            'TxtNSF.Text = ""
            'TxtNSFMax.Text = ""
            'TxtIVTB.Text = ""
            'TxtMinPAYE.Text = ""
            'TxtBank.Text = ""
            'TxtBankAddress.Text = ""
            'TxtAttention.Text = ""
            'TxtAccountNo.Text = ""
            'TxtPAYERegNo.Text = ""
            'TxtTaxExempt.Text = ""
            'TxtMaxTrans.Text = ""
            'TxtLocal.Text = ""
            'TxtSick.Text = ""
            'TxtDays.Text = ""
            'TxtWDHrs.Text = ""
            'TxtWEHrs.Text = ""
            'TxtBRN.Text = ""
            'TxtTAN.Text = ""
            'TxtDecName.Text = ""
            'TxtAttendAllow.Text = ""
            'TxtMealAllow.Text = ""
            'TxtBrkTime.Text = ""
            'TxtNSFEmployee.Text = ""
            'TxtNSFEmployeeMax.Text = ""
            'TxtMaxSalNPF.Text = ""
            'TxtBrkTime.Text = ""
            'TxtName1.Text = ""
            'TxtName2.Text = ""
            'TxtDeg1.Text = ""
            'TxtDeg2.Text = ""
            'TxtPensionEmp.Text = ""
            'TxtEPZee.Text = ""
            'TxtEPZer.Text = ""
        End If
    End Sub

    Private Sub UpdateFields()
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT * FROM [System]"
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            TxtName.Text = Rdr("Name").ToString
            TxtAddress.Text = Rdr("Address").ToString
            TxtBusiness.Text = Rdr("Business").ToString
            TxtTelephone.Text = Rdr("Phone").ToString
            TxtTeleFax.Text = Rdr("Fax").ToString
            TxtEMail.Text = Rdr("EMail").ToString
            TxtNPF.Text = Rdr("NPFEmployee").ToString
            TxtNPFMax.Text = Rdr("NPFEmployeeMax").ToString
            TxtNPFEmp.Text = Rdr("NPFEmployer").ToString
            TxtNPFEmpMax.Text = Rdr("NPFEmployerMax").ToString
            TxtNSF.Text = Rdr("NSF").ToString
            TxtNSFMax.Text = Rdr("NSFMax").ToString
            TxtIVTB.Text = Rdr("IVTB").ToString
            TxtMinPAYE.Text = Rdr("MinPAYE").ToString
            TxtBank.Text = Rdr("Bank").ToString
            TxtBankAddress.Text = Rdr("BnkAddress").ToString
            TxtAttention.Text = Rdr("Attention").ToString
            TxtAccountNo.Text = Rdr("AccountNo").ToString
            TxtPAYERegNo.Text = Rdr("PAYERegNo").ToString
            TxtTaxExempt.Text = Rdr("TaxExempt").ToString
            TxtMaxTrans.Text = Rdr("TransportExcess").ToString
            TxtLocal.Text = Rdr("Local").ToString
            TxtSick.Text = Rdr("Sick").ToString
            TxtDays.Text = Rdr("WorkingDays").ToString
            TxtWDHrs.Text = Rdr("WDHrs").ToString
            TxtWEHrs.Text = Rdr("WEHrs").ToString
            TxtBRN.Text = Rdr("BRN").ToString
            TxtTAN.Text = Rdr("TAN").ToString
            TxtDecName.Text = Rdr("PAYEDeclarant").ToString
            TxtMaxSalNPF.Text = Rdr("MaxSalNPS").ToString
            TxtNSFEmployee.Text = Rdr("NSFEmployee").ToString
            TxtNSFEmployeeMax.Text = Rdr("NSFEmployeeMax").ToString
            TxtAttendAllow.Text = Rdr("Attendance").ToString
            TxtMealAllow.Text = Rdr("Meal").ToString
            TxtBrkTime.Text = Rdr("BreakTime").ToString
            TxtName1.Text = Rdr("Name1").ToString
            TxtDeg1.Text = Rdr("Deg1").ToString
            TxtName2.Text = Rdr("Name2").ToString
            TxtDeg2.Text = Rdr("Deg2").ToString
            TxtPensionEmp.Text = Rdr("PensionEmp").ToString
            TxtEPZee.Text = Rdr("EPZEE").ToString
            TxtEPZer.Text = Rdr("EPZER").ToString
        End If
        Rdr.Close()
    End Sub
End Class