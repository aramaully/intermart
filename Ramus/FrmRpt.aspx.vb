﻿Public Partial Class FrmRpt
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim report As New CrystalDecisions.Web.CrystalReportSource
        report.Report.FileName = Session("reportfile")
        report.ReportDocument.Close()

        RepHead.Value = Session("rephead")
        report.ReportDocument.Refresh()
        CrystalViewer.ReportSource = report

        CrystalViewer.DisplayToolbar = True
        CrystalViewer.HasExportButton = True
        CrystalViewer.HasPrintButton = False
        CrystalViewer.HasSearchButton = True
        CrystalViewer.HasGotoPageButton = True
        CrystalViewer.HasPageNavigationButtons = True
        CrystalViewer.HasRefreshButton = True

        If report.Report.FileName <> "RBankSummary.rpt" Then
            Dim i As Integer = 0
            For i = 1 To Session("fieldcount")
                report.ReportDocument.DataDefinition.FormulaFields(Session("field" & i)).Text = "'" & Session("fieldval" & i) & "'"
            Next
        End If


        CrystalViewer.SelectionFormula = Session("selformula")

        'SSL Machines
        'CrystalViewer.LogOnInfo(0).ConnectionInfo.ServerName = "(local)"
        'CrystalViewer.LogOnInfo(0).ConnectionInfo.ServerName = "SAJEED"
        'CrystalViewer.LogOnInfo(0).ConnectionInfo.DatabaseName = "InterMart"

        'Intermart Server
        CrystalViewer.LogOnInfo(0).ConnectionInfo.ServerName = "payrollbb\sqlexpress"
        CrystalViewer.LogOnInfo(0).ConnectionInfo.DatabaseName = "InterMart"

        'Intermart Server(Test)
        'CrystalViewer.LogOnInfo(0).ConnectionInfo.ServerName = "payrollbb\sqlexpress"
        'CrystalViewer.LogOnInfo(0).ConnectionInfo.DatabaseName = "InterMartTest"

        CrystalViewer.LogOnInfo(0).ConnectionInfo.UserID = "admin"
        CrystalViewer.LogOnInfo(0).ConnectionInfo.Password = "swathi*"

        CrystalViewer.DataBind()
        CrystalViewer.RefreshReport()
    End Sub
End Class