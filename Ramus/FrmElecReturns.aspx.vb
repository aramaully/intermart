﻿Imports System.IO
Imports System.Data.OleDb
Partial Public Class FrmElecReturns
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim CmdTrans As New OleDb.OleDbCommand
    Dim RdrTrans As OleDb.OleDbDataReader
    Dim CmdEmp As New OleDb.OleDbCommand
    Dim RdrEmp As OleDb.OleDbDataReader
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            'TxtYear.Text = Date.Today.Year
            'CmbMonth.SelectedIndex = Date.Today.Month - 1
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        'Protected Sub BtnReturn_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnReturn.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim ev As New System.EventArgs

        LblPath.Visible = False
        If TxtDateFrom.Text = "" Then
            LblPath.Visible = True
            LblPath.Text = "Please Enter Date From!!!"
            Exit Sub
        End If
        If TextDateTo.Text = "" Then
            LblPath.Visible = True
            LblPath.Text = "Please Enter Date To !!!"
            Exit Sub
        End If
        Dim StrSql As String
        Dim StrSalaryPeriod As String
        Dim DtePayDate As Date

        Dim LngSal As Long
        Dim StrSSNo As String
        Dim strData As String = ""
        Dim StrLast As String
        Dim StrFirst As String
        Dim StrM As String = "M"
        Dim stringWriter As New StringWriter
        Dim wrt As New IO.StringWriter()
        Dim StrCompanyName As String
        Dim StrType As String
        Dim tab As String = ""
        StrCompanyName = ""
        Dim SngLevy As Single = 0
        Dim StrROE As String = ""
        Dim StrPAYEReg As String = ""
        Dim StrBRN As String = ""
        Dim StrTAN As String = ""
        Dim SngSysTotEarning As Single = 0
        Dim StrSpace As String = ""
        Dim StrDateFrom As String
        Dim StrDateTo As String
        Dim DteFromDate As Date
        Dim DteToDate As Date
        Dim StrPhone As String = ""
        Dim StrDeclarant As String = ""
        Dim strEMail As String = ""

        DteFromDate = CDate(TxtDateFrom.Text)
        DteToDate = CDate(TextDateTo.Text)
        StrDateFrom = Format(DteFromDate, "yyyy,MM,dd")
        StrDateTo = Format(DteToDate, "yyyy,MM,dd")

        StrSalaryPeriod = Format(DtePayDate, "yyyyMM")

        Response.Clear()
        Response.ContentType = "text/csv"
        Response.AppendHeader("Content-Disposition", String.Format("attachment; filename=ROE" & Format(DateTime.Today, "MMyy") & ".csv"))

        StrSql = "Select *From [System]"
        CmdTrans.Connection = Con
        CmdTrans.CommandText = StrSql
        RdrTrans = CmdTrans.ExecuteReader

        While RdrTrans.Read
            StrPAYEReg = RdrTrans("PAYERegNo")
            StrCompanyName = RdrTrans("Name")
            StrBRN = RdrTrans("BRN")
            StrTAN = RdrTrans("TAN")
            StrPhone = RdrTrans("Phone")
            StrDeclarant = RdrTrans("PAYEDeclarant")
            strEMail = RdrTrans("Email")
            SngSysTotEarning = Val(RdrTrans("TotEarning"))
        End While
        RdrTrans.Close()

        Dim Dt As DataTable
        Dim Dr As DataRow
        Dim Dc As DataColumn
        Dt = New DataTable()
        Dc = New DataColumn("Col1", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col2", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col3", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col4", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col5", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col6", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col7", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col8", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col9", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col10", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col11", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col12", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col13", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col14", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col15", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col16", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col17", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col18", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col19", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col20", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)

        'Fisrt Line
        Dim DtHeaderFirst As DataTable
        Dim DrHeaderFirst As DataRow
        Dim DcHeaderFirst As DataColumn
        DtHeaderFirst = New DataTable()
        DcHeaderFirst = New DataColumn("Header1", Type.GetType("System.String"))
        DtHeaderFirst.Columns.Add(DcHeaderFirst)
        DcHeaderFirst = New DataColumn("Header2", Type.GetType("System.String"))
        DtHeaderFirst.Columns.Add(DcHeaderFirst)

        'second line
        Dim DtHeaderSecond As DataTable
        Dim DrHeaderSecond As DataRow
        Dim DcHeaderSecond As DataColumn
        DtHeaderSecond = New DataTable()
        DcHeaderSecond = New DataColumn("Header1", Type.GetType("System.String"))
        DtHeaderSecond.Columns.Add(DcHeaderSecond)
        DcHeaderSecond = New DataColumn("Header2", Type.GetType("System.String"))
        DtHeaderSecond.Columns.Add(DcHeaderSecond)
        DcHeaderSecond = New DataColumn("Header3", Type.GetType("System.String"))
        DtHeaderSecond.Columns.Add(DcHeaderSecond)
        DcHeaderSecond = New DataColumn("Header4", Type.GetType("System.String"))
        DtHeaderSecond.Columns.Add(DcHeaderSecond)
        DcHeaderSecond = New DataColumn("Header5", Type.GetType("System.String"))
        DtHeaderSecond.Columns.Add(DcHeaderSecond)
        DcHeaderSecond = New DataColumn("Header6", Type.GetType("System.String"))
        DtHeaderSecond.Columns.Add(DcHeaderSecond)
        DcHeaderSecond = New DataColumn("Header7", Type.GetType("System.String"))
        DtHeaderSecond.Columns.Add(DcHeaderSecond)
        DcHeaderSecond = New DataColumn("Header8", Type.GetType("System.String"))
        DtHeaderSecond.Columns.Add(DcHeaderSecond)
        DcHeaderSecond = New DataColumn("Header9", Type.GetType("System.String"))
        DtHeaderSecond.Columns.Add(DcHeaderSecond)

        'third line
        Dim DtHeaderthird As DataTable
        Dim DrHeaderthird As DataRow
        Dim DcHeaderthird As DataColumn
        DtHeaderthird = New DataTable()
        DcHeaderthird = New DataColumn("Header1", Type.GetType("System.String"))
        DtHeaderthird.Columns.Add(DcHeaderthird)
        DcHeaderthird = New DataColumn("Header2", Type.GetType("System.String"))
        DtHeaderthird.Columns.Add(DcHeaderthird)
        DcHeaderthird = New DataColumn("Header3", Type.GetType("System.String"))
        DtHeaderthird.Columns.Add(DcHeaderthird)
        DcHeaderthird = New DataColumn("Header4", Type.GetType("System.String"))
        DtHeaderthird.Columns.Add(DcHeaderthird)
        DcHeaderthird = New DataColumn("Header5", Type.GetType("System.String"))
        DtHeaderthird.Columns.Add(DcHeaderthird)
        DcHeaderthird = New DataColumn("Header6", Type.GetType("System.String"))
        DtHeaderthird.Columns.Add(DcHeaderthird)
        DcHeaderthird = New DataColumn("Header7", Type.GetType("System.String"))
        DtHeaderthird.Columns.Add(DcHeaderthird)
        DcHeaderthird = New DataColumn("Header8", Type.GetType("System.String"))
        DtHeaderthird.Columns.Add(DcHeaderthird)
        DcHeaderthird = New DataColumn("Header9", Type.GetType("System.String"))
        DtHeaderthird.Columns.Add(DcHeaderthird)
        DcHeaderthird = New DataColumn("Header10", Type.GetType("System.String"))
        DtHeaderthird.Columns.Add(DcHeaderthird)
        DcHeaderthird = New DataColumn("Header11", Type.GetType("System.String"))
        DtHeaderthird.Columns.Add(DcHeaderthird)
        DcHeaderthird = New DataColumn("Header12", Type.GetType("System.String"))
        DtHeaderthird.Columns.Add(DcHeaderthird)
        DcHeaderthird = New DataColumn("Header13", Type.GetType("System.String"))
        DtHeaderthird.Columns.Add(DcHeaderthird)
        DcHeaderthird = New DataColumn("Header14", Type.GetType("System.String"))
        DtHeaderthird.Columns.Add(DcHeaderthird)
        DcHeaderthird = New DataColumn("Header15", Type.GetType("System.String"))
        DtHeaderthird.Columns.Add(DcHeaderthird)
        DcHeaderthird = New DataColumn("Header16", Type.GetType("System.String"))
        DtHeaderthird.Columns.Add(DcHeaderthird)
        DcHeaderthird = New DataColumn("Header17", Type.GetType("System.String"))
        DtHeaderthird.Columns.Add(DcHeaderthird)
        DcHeaderthird = New DataColumn("Header18", Type.GetType("System.String"))
        DtHeaderthird.Columns.Add(DcHeaderthird)
        DcHeaderthird = New DataColumn("Header19", Type.GetType("System.String"))
        DtHeaderthird.Columns.Add(DcHeaderthird)
        DcHeaderthird = New DataColumn("Header20", Type.GetType("System.String"))
        DtHeaderthird.Columns.Add(DcHeaderthird)

        Dim DtHeader As DataTable
        Dim DrHeader As DataRow
        Dim DcHeader As DataColumn
        DtHeader = New DataTable()
        DcHeader = New DataColumn("Header1", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)
        DcHeader = New DataColumn("Header2", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)
        DcHeader = New DataColumn("Header3", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)
        DcHeader = New DataColumn("Header4", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)
        DcHeader = New DataColumn("Header5", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)
        DcHeader = New DataColumn("Header6", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)
        DcHeader = New DataColumn("Header7", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)
        DcHeader = New DataColumn("Header8", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)
        DcHeader = New DataColumn("Header9", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)

        Dim LngSumBasic As Long
        Dim LngIVTB As Long
        Dim LngTotPaye As Single
        LngSumBasic = 0
        LngIVTB = 0
        LngTotPaye = 0

        Dim HireDate As Date
        HireDate = CDate("21/06/" & Year(DteToDate))
        Dim StrEmpTAN As String = ""
        Dim StrEmpNIC As String = ""
        Dim SngSum As Single = 0
        Dim SngTravelExempt As Single = 0

        If e.Item.CommandName = "Print" Then
            Try
                CmdEmp.Connection = Con
                CmdEmp.CommandText = "Select * From Employee Where working = 1 and hiredate < '" & HireDate.ToString("yyyy-MM-dd") & "' or (working = 0 and (lastday >='" & DteFromDate.ToString("yyyy-MM-dd") & "' And lastday <= '" & DteToDate.ToString("yyyy-MM-dd") & "'))"
                RdrEmp = CmdEmp.ExecuteReader
                StrLast = ""
                StrFirst = ""
                StrSSNo = ""
                While RdrEmp.Read
                    StrEmpTAN = RdrEmp("TaxAccNo")
                    StrEmpNIC = RdrEmp("SSNo")
                    StrLast = RdrEmp("Last")
                    StrFirst = RdrEmp("First")
                    StrEmpTAN = RdrEmp("EmpID")
                    If StrEmpTAN = "665" Then
                        StrEmpTAN = RdrEmp("EmpID")
                    End If
                    StrSql = "Select sum(BasicSalary)as Basic , sum(Overtime) As OT , Max(TotEdf) as Edf"
                    StrSql = StrSql & ",sum(IfsBonus) as Bonus"
                    StrSql = StrSql & ",sum(TravelAllowance - TravelExempt) as TExempt"
                    StrSql = StrSql & ",sum(PassageBenefit - ExemptPassageBenefit) as PassageExempt"
                    StrSql = StrSql & ",Sum(Paye) as TotPaye"
                    StrSql = StrSql & ",Sum(OtherAllow) as Allow"
                    StrSql = StrSql & ",Sum(TravelAllowance+PassageBenefit) as TAllow,Sum(Arrears) as Reimbursement"
                    StrSql = StrSql & ",Sum(PerfAllow) as PAll"
                    StrSql = StrSql & ",Sum(CarBenefit) as Car"
                    StrSql = StrSql & ",Sum(SpecialBenefit) as ExGratia"
                    StrSql = StrSql & ",Sum(MaternityAllow) as Mater,Sum(RespAllow) as Resp,Sum(PensionEmp) as PenEmp"
                    StrSql = StrSql & ",Sum(LocalRefund * Dailyrate) as Lfund from Salary where empid = '" & RdrEmp("Empid") & "' "
                    StrSql = StrSql & " And Date >= '" & DteFromDate.ToString("yyyy-MM-dd") & " ' "
                    StrSql = StrSql & " And Date <= '" & DteToDate.ToString("yyyy-MM-dd") & " ' "
                    CmdTrans.Connection = Con
                    CmdTrans.CommandText = StrSql
                    RdrTrans = CmdTrans.ExecuteReader
                    If RdrTrans.Read Then
                        Try
                            SngSum = 0
                            SngSum = Math.Round(IIf(IsDBNull(RdrTrans("Basic")), 0, Val(RdrTrans("Basic"))), 2)
                            SngSum = SngSum + Math.Round(IIf(IsDBNull(RdrTrans("OT")), 0, Val(RdrTrans("OT"))), 2)
                            SngSum = SngSum + Math.Round(IIf(IsDBNull(RdrTrans("Bonus")), 0, Val(RdrTrans("Bonus"))), 2)
                            SngSum = SngSum + Math.Round(IIf(IsDBNull(RdrTrans("PAll")), 0, Val(RdrTrans("PAll"))), 2)
                            'SngSum = SngSum + Math.Round(IIf(IsDBNull(RdrTrans("Fringe")), 0, Val(RdrTrans("Fringe"))), 2)
                            SngSum = SngSum + Math.Round(IIf(IsDBNull(RdrTrans("Lfund")), 0, Val(RdrTrans("Lfund"))), 2)
                            SngSum = SngSum + Math.Round(IIf(IsDBNull(RdrTrans("Allow")), 0, Val(RdrTrans("Allow"))), 2)
                            SngSum = SngSum + Math.Round(IIf(IsDBNull(RdrTrans("Mater")), 0, Val(RdrTrans("Mater"))), 2)
                            SngSum = SngSum + Math.Round(IIf(IsDBNull(RdrTrans("ExGratia")), 0, Val(RdrTrans("ExGratia"))), 2)
                            SngSum = SngSum + Math.Round(IIf(IsDBNull(RdrTrans("Resp")), 0, Val(RdrTrans("Resp"))), 2)
                            'SngSum = SngSum + Math.Round(IIf(IsDBNull(RdrTrans("Reimbursement")), 0, Val(RdrTrans("Reimbursement"))), 2)
                            SngSum = SngSum + Math.Round(IIf(IsDBNull(RdrTrans("PenEmp")), 0, Val(RdrTrans("PenEmp"))), 2)

                            'Increment Excel rows
                            If (Val(RdrTrans("TotPaye")) >= 0 And SngSum >= SngSysTotEarning) Or Val(RdrTrans("TotPaye")) >= 0 Then
                                'Check Exempt
                                SngTravelExempt = 0
                                Dim SngPassageExempt As Single = 0
                                Cmd.Connection = Con
                                StrSql = "Select * from salary where empid = '" & RdrEmp("Empid") & "' And Date >= ' " & DteFromDate.ToString("yyyy-MM-dd") & " ' And Date <= '" & DteToDate.ToString("yyyy-MM-dd") & " '"
                                Cmd.CommandText = StrSql
                                Rdr = Cmd.ExecuteReader
                                While Rdr.Read
                                    If ((Rdr("TravelAllowance") + Rdr("PassageBenefit")) - (Rdr("TravelExempt") + Rdr("ExemptPassageBenefit"))) > 0 Then
                                        SngTravelExempt = (SngTravelExempt + Rdr("ExemptPassageBenefit") + Rdr("TravelExempt"))
                                    Else
                                        SngTravelExempt = SngTravelExempt + Rdr("PassageBenefit") + Rdr("TravelAllowance")
                                    End If
                                    If (Rdr("PassageBenefit") - Rdr("ExemptPassageBenefit")) > 0 Then
                                        SngPassageExempt = 0 '(SngPassageExempt + Rdr("ExemptPassageBenefit"))
                                    Else
                                        SngPassageExempt = 0 'SngPassageExempt + Rdr("PassageBenefit")
                                    End If
                                End While
                                Rdr.Close()


                                Dim pt As Char = " "
                                Dr = Dt.NewRow
                                Dr.Item("Col1") = StrEmpTAN
                                Dr.Item("Col2") = StrEmpNIC
                                Dr.Item("Col3") = StrLast
                                Dr.Item("Col4") = StrFirst
                                Dr.Item("Col5") = SngSum
                                Dr.Item("Col6") = "0"
                                Dr.Item("Col7") = Math.Round(Val(RdrTrans("TAllow") & ""), 2)
                                Dr.Item("Col8") = Math.Round(Val(RdrTrans("Reimbursement") & ""), 2)
                                Dr.Item("Col9") = Math.Round(Val(RdrTrans("Car") & ""), 2)
                                Dr.Item("Col10") = "0"
                                Dr.Item("Col11") = "0"
                                Dr.Item("Col12") = "0"
                                Dr.Item("Col13") = "0"
                                Dr.Item("Col14") = "0"

                                SngSum = 0
                                SngSum = Math.Round(Val(SngTravelExempt & ""), 0) + Math.Round(Val(SngPassageExempt & ""), 0)
                                If SngSum < 0 Then
                                    SngSum = Math.Round(Val(RdrTrans("tAllow") & ""), 0)
                                End If
                                Dr.Item("Col15") = SngSum

                                Dr.Item("Col16") = Math.Round(Val(RdrTrans("Edf") & ""), 2)
                                Dr.Item("Col17") = "0"
                                Dr.Item("Col18") = "0"
                                Dr.Item("Col19") = "0"
                                Dr.Item("Col20") = Math.Round(Val(RdrTrans("TotPaye") & ""), 2)
                                LngTotPaye = LngTotPaye + Math.Round(Val(RdrTrans("TotPaye") & ""), 2)
                                Dt.Rows.Add(Dr)
                            End If
                        Catch ex As Exception
                            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
                        End Try
                    End If
                    RdrTrans.Close()
                End While
                RdrEmp.Close()

                Dim StrFileName As String = "ROE" & Format(DtePayDate, "MM") & Format(DtePayDate, "yy")

                DrHeaderFirst = DtHeaderFirst.NewRow
                DrHeaderFirst.Item("Header1") = "MNS"
                DrHeaderFirst.Item("Header2") = "V1.0"

                Dim I As Integer = 0
                DtHeaderFirst.Rows.Add(DrHeaderFirst)
                For Each DrHeaderFirst In DtHeaderFirst.Rows
                    tab = ""
                    For I = 0 To DtHeaderFirst.Columns.Count - 1
                        Response.Write(tab & DrHeaderFirst(I).ToString())
                        tab = ","
                    Next
                    Response.Write(vbCrLf)
                Next

                DrHeaderSecond = DtHeaderSecond.NewRow
                DrHeaderSecond.Item("Header1") = "Employer Registration Number"
                DrHeaderSecond.Item("Header2") = "Employer BRN"
                DrHeaderSecond.Item("Header3") = "Employer Name"
                DrHeaderSecond.Item("Header4") = "Income Year"
                DrHeaderSecond.Item("Header5") = "Total PAYE"
                DrHeaderSecond.Item("Header6") = "Telephone"
                DrHeaderSecond.Item("Header7") = "Mobile"
                DrHeaderSecond.Item("Header8") = "Name of  Declarant"
                DrHeaderSecond.Item("Header9") = "Email"

                I = 0
                DtHeaderSecond.Rows.Add(DrHeaderSecond)
                For Each DrHeaderSecond In DtHeaderSecond.Rows
                    tab = ""
                    For I = 0 To DtHeaderSecond.Columns.Count - 1
                        Response.Write(tab & DrHeaderSecond(I).ToString())
                        tab = ","
                    Next
                    Response.Write(vbCrLf)
                Next

                DrHeader = DtHeader.NewRow
                DrHeader.Item("Header1") = StrPAYEReg
                DrHeader.Item("Header2") = StrBRN
                DrHeader.Item("Header3") = StrCompanyName
                DrHeader.Item("Header4") = Year(DteToDate)
                DrHeader.Item("Header5") = LngTotPaye
                DrHeader.Item("Header6") = StrPhone
                DrHeader.Item("Header7") = ""
                DrHeader.Item("Header8") = StrDeclarant
                DrHeader.Item("Header9") = strEMail
                'DrHeader.Item("Header6") = SngSysTotEarning

                I = 0
                DtHeader.Rows.Add(DrHeader)
                For Each DrHeader In DtHeader.Rows
                    tab = ""
                    For I = 0 To DtHeader.Columns.Count - 1
                        Response.Write(tab & DrHeader(I).ToString())
                        tab = ","
                    Next
                    Response.Write(vbCrLf)
                Next

                DrHeaderthird = DtHeaderthird.NewRow
                DrHeaderthird.Item("Header1") = "Employee TAN"
                DrHeaderthird.Item("Header2") = "Employee ID"
                DrHeaderthird.Item("Header3") = "Surname of Employee"
                DrHeaderthird.Item("Header4") = "Other Names"
                DrHeaderthird.Item("Header5") = "Salary"
                DrHeaderthird.Item("Header6") = "Entertainment"
                DrHeaderthird.Item("Header7") = "Transport"
                DrHeaderthird.Item("Header8") = "Reimbursement"
                DrHeaderthird.Item("Header9") = "Car Benefits"
                DrHeaderthird.Item("Header10") = "House Benefits"
                DrHeaderthird.Item("Header11") = "Tax Benefits"
                DrHeaderthird.Item("Header12") = "Other Benefits"
                DrHeaderthird.Item("Header13") = "Lump Sum"
                DrHeaderthird.Item("Header14") = "Retirement"
                DrHeaderthird.Item("Header15") = "Exempt Emoluments"
                DrHeaderthird.Item("Header16") = "EDF Declaration"
                DrHeaderthird.Item("Header17") = "Additional Exemption for Children"
                DrHeaderthird.Item("Header18") = "Interest Relief on Secured Housing Loan"
                DrHeaderthird.Item("Header19") = "Relief for Medical insurance premium or contr. for approved provident"
                DrHeaderthird.Item("Header20") = "PAYE Withheld"
                I = 0
                DtHeaderthird.Rows.Add(DrHeaderthird)
                For Each DrHeaderthird In DtHeaderthird.Rows
                    tab = ""
                    For I = 0 To DtHeaderthird.Columns.Count - 1
                        Response.Write(tab & DrHeaderthird(I).ToString())
                        tab = ","
                    Next
                    Response.Write(vbCrLf)
                Next

                'Populate employee and salary details
                For Each Dr In Dt.Rows
                    tab = ""
                    For I = 0 To Dt.Columns.Count - 1
                        Response.Write(tab & Dr(I).ToString())
                        tab = ","
                    Next
                    Response.Write(vbCrLf)
                Next

                Response.End()
                Response.[End]()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
                'MsgBox("")
            End Try
        End If
    End Sub
End Class