﻿Public Partial Class FrmEmpPer
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        ''If Not Page.IsPostBack Then
        ''    Dim Cmd As New OleDb.OleDbCommand
        ''    Dim Rdr As OleDb.OleDbDataReader
        ''    Cmd.Connection = Con
        ''    Cmd.CommandText = "SELECT RouteNo FROM RouteMaster"
        ''    Rdr = Cmd.ExecuteReader
        ''    While Rdr.Read
        ''        CmbRoute.Items.Add(Rdr("RouteNo"))
        ''    End While
        ''    Rdr.Close()
        ''End If
    End Sub

    Protected Sub ClearAll()
        TxtFirst.Text = ""
        TxtLast.Text = ""
        TxtClockID.Text = ""
        TxtCategory.Text = ""
        TxtTitle.Text = ""
        TxtGender.Text = ""
        ImgPhoto.ImageUrl = Nothing
        TxtAddress.Text = ""
        TxtCity.Text = ""
        TxtCountry.Text = ""
        TxtPhone.Text = ""
        TxtFax.Text = ""
        TxtMobile.Text = ""
        TxtEMail.Text = ""
        TxtEContact.Text = ""
        CmbCivilStatus.SelectedIndex = 0
        'CmbRoute.SelectedIndex = 0
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ev As New System.EventArgs
        If e.Item.CommandName = "New" Then
            ClearAll()
            TxtEmpID.Text = ""
            TxtEmpID.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = "UPDATE Employee SET "
            StrSql = StrSql & "Address ='" & TxtAddress.Text & "',"
            StrSql = StrSql & "City ='" & TxtCity.Text & "',"
            StrSql = StrSql & "Country ='" & TxtCountry.Text & "',"
            StrSql = StrSql & "HomePhone ='" & TxtPhone.Text & "',"
            StrSql = StrSql & "HomeFax ='" & TxtFax.Text & "',"
            StrSql = StrSql & "Mobile ='" & TxtMobile.Text & "',"
            StrSql = StrSql & "EMail ='" & TxtEMail.Text & "',"
            StrSql = StrSql & "Emergency ='" & TxtEContact.Text & "',"
            StrSql = StrSql & "CivilStatus ='" & CmbCivilStatus.Text & "'"
            StrSql = StrSql & "WHERE EmpID = '" & TxtEmpID.Text & "'"
            Try
                Cmd.Connection = Con
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                TxtFirst.Text = ""
                TxtLast.Text = ""
                TxtClockID.Text = ""
                TxtCategory.Text = ""
                TxtTitle.Text = ""
                TxtGender.Text = ""
                ImgPhoto.ImageUrl = Nothing
                TxtAddress.Text = ""
                TxtCity.Text = ""
                TxtCountry.Text = ""
                TxtPhone.Text = ""
                TxtFax.Text = ""
                TxtMobile.Text = ""
                TxtEMail.Text = ""
                TxtEContact.Text = ""
                CmbCivilStatus.SelectedIndex = 0
                ' CmbRoute.SelectedIndex = 0
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "DELETE FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
                Cmd.ExecuteNonQuery()
                TxtEmpID.Text = ""
                TxtFirst.Text = ""
                TxtLast.Text = ""
                TxtClockID.Text = ""
                TxtCategory.Text = ""
                TxtTitle.Text = ""
                TxtGender.Text = ""
                ImgPhoto.ImageUrl = Nothing
                TxtAddress.Text = ""
                TxtCity.Text = ""
                TxtCountry.Text = ""
                TxtPhone.Text = ""
                TxtFax.Text = ""
                TxtMobile.Text = ""
                TxtEMail.Text = ""
                TxtEContact.Text = ""
                CmbCivilStatus.SelectedIndex = 0
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Next" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID > '" & TxtEmpID.Text & "'"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try

        End If
        If e.Item.CommandName = "Previous" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID < '" & TxtEmpID.Text & "' ORDER BY EmpID DESC"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try
        End If
        If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtEmpID.Text)
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
        End If
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        If Rdr.Read Then
            On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
            TxtTitle.Text = Rdr("Title")
            TxtCategory.Text = Rdr("Category")
            TxtGender.Text = Rdr("Gender")
            TxtCategory.Text = Rdr("Category")
            ChkClocked.Checked = IIf(Rdr("Clocked"), True, False)
            TxtClockID.Text = Rdr("BatchNo")
            Session.Add("EmpID", Rdr("EmpID"))
            ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & Rdr("EmpID")
            'Personal Details
            TxtAddress.Text = Rdr("Address").ToString
            TxtCity.Text = Rdr("City").ToString
            TxtCountry.Text = Rdr("Country").ToString
            TxtPhone.Text = Rdr("HomePhone").ToString
            TxtFax.Text = Rdr("HomeFax").ToString
            TxtMobile.Text = Rdr("Mobile").ToString
            TxtEMail.Text = Rdr("EMail").ToString
            TxtEContact.Text = Rdr("Emergency").ToString
            CmbCivilStatus.Text = Rdr("CivilStatus").ToString
        Else
            Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(1))
            ToolBar1_ItemClick(Me, ev)
            'ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('Invalid Employee ID','Error');</script>", False)
        End If
        Rdr.Close()
    End Sub
End Class