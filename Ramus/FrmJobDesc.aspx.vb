﻿Public Partial Class FrmJobDesc
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrAudit As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            FillGrid()
        End If
    End Sub

    Protected Sub FillGrid()
        Cmd.Connection = Con
        StrSql = "SELECT * FROM JobDesc"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvJobDesc.DataSource = Rdr
        GdvJobDesc.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim Ev As New System.EventArgs

        If e.Item.CommandName = "New" Then
            TxtCode.Text = ""
            TxtPosition.Text = ""
            TxtCode.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Dim Trans As OleDb.OleDbTransaction
            Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
            Cmd.Connection = Con
            Cmd.Transaction = Trans
            Cmd.CommandText = "DELETE FROM JobDesc WHERE Code = '" & TxtCode.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO JobDesc (Code,Position) VALUES('"
                StrSql = StrSql & TxtCode.Text & "','"
                StrSql = StrSql & TxtPosition.Text & "')"
                
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                Trans.Commit()
                TxtCode.Text = ""
                TxtPosition.Text = ""
                TxtCode.Focus()
                FillGrid()

                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Position  " & TxtPosition.Text & " Created / Edited. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Create / Edit Position','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                Trans.Rollback()
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM JobDesc WHERE Code = '" & TxtCode.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                TxtCode.Text = ""
                TxtPosition.Text = ""
                TxtCode.Focus()
                FillGrid()

                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Position " & TxtPosition.Text & " Deleted. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Delete Position','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
    End Sub

    Private Sub GdvJobDesc_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvJobDesc.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvJobDesc, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvJobDesc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvJobDesc.SelectedIndexChanged
        TxtCode.Text = GdvJobDesc.SelectedRow.Cells(0).Text
        TxtPosition.Text = GdvJobDesc.SelectedRow.Cells(1).Text

    End Sub

    Private Sub GdvJobDesc_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvJobDesc.SelectedIndexChanging
        GdvJobDesc.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvJobDesc.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvJobDesc.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    'Protected Sub BtnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnView.Click
    '    Dim StrSelFormula As String
    '    Session("ReportFile") = "RJobDes.rpt"
    '    StrSelFormula = "{JobDesc.Code} = '" & TxtCode.Text & "'"

    '    Session("SelFormula") = StrSelFormula
    '    Session("RepHead") = "Job Description"
    '    ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
    'End Sub
End Class