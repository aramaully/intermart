﻿Public Partial Class FrmUpDateEmp
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim TimexCon As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim TimexCmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        TimexCon.ConnectionString = Session("TimexConn")
        TimexCon.Open()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Update" Then
            Cmd.Connection = Con
            StrSql = "SELECT EmpID,[Last],[First],Title,Position,BatchNo,Local,Sick,DeptCode,Working,LastDay,Clocked "
            StrSql = StrSql & "FROM Employee"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            TimexCmd.Connection = TimexCon
            Dim Trans As OleDb.OleDbTransaction
            Trans = TimexCon.BeginTransaction(IsolationLevel.Chaos)
            TimexCmd.Transaction = Trans
            Try
                While Rdr.Read
                    TimexCmd.CommandText = "DELETE FROM Employee WHERE EmpID = '" & Rdr("EmpID") & "'"
                    TimexCmd.ExecuteNonQuery()
                    StrSql = "INSERT INTO Employee (EmpID,[Last],[First],Title,Position,IdCardNo,Local,Sick,DeptCode,Working,LastDay,Clocked) VALUES('"
                    StrSql = StrSql & Rdr("EmpID") & "','"
                    StrSql = StrSql & Rdr("Last") & "','"
                    StrSql = StrSql & Rdr("First") & "','"
                    StrSql = StrSql & Rdr("Title") & "','"
                    StrSql = StrSql & Rdr("Position") & "','"
                    StrSql = StrSql & Rdr("BatchNo") & "',"
                    StrSql = StrSql & Rdr("Local") & ","
                    StrSql = StrSql & Rdr("Sick") & ",'"
                    StrSql = StrSql & Rdr("DeptCode") & "',"
                    StrSql = StrSql & IIf(Rdr("Working"), 1, 0) & ",'"
                    StrSql = StrSql & Rdr("LastDay") & "',"
                    StrSql = StrSql & IIf(Rdr("Clocked"), 1, 0) & ")"
                    TimexCmd.CommandText = StrSql
                    TimexCmd.ExecuteNonQuery()
                End While
                Rdr.Close()
                Trans.Commit()
                LblResults.Text = "Timex Employee File Updated"
            Catch ex As Exception
                Trans.Rollback()
                LblResults.Text = "Error While updating... " & ex.Message
            End Try

        End If
    End Sub
End Class