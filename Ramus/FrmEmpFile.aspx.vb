﻿Public Partial Class FrmEmpFile
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            TxtEmpID.Text = "ALL"
        End If
    End Sub

    Protected Sub ClearAll()
        TxtFirst.Text = ""
        TxtLast.Text = ""
        TxtClockID.Text = ""
        TxtCategory.Text = ""
        TxtTitle.Text = ""
        TxtGender.Text = ""
        ImgPhoto.ImageUrl = Nothing
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ev As New System.EventArgs
        If e.Item.CommandName = "New" Then
            ClearAll()
            TxtEmpID.Text = "ALL"
        End If
        If e.Item.CommandName = "Print" Then
            Dim StrSelFormula As String
            Session("ReportFile") = "RptEmployee.rpt"
            If (TxtEmpID.Text <> "") Or (TxtEmpID.Text <> "ALL") Then
                StrSelFormula = "{Employee.EmpID}= '" & TxtEmpID.Text & "'"
                Session("SelFormula") = StrSelFormula
            Else
                'StrSelFormula = ""
            End If

            Session("RepHead") = "Employee Details"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        End If
        If e.Item.CommandName = "Delete" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "DELETE FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
                Cmd.ExecuteNonQuery()
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Next" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID > '" & TxtEmpID.Text & "'"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try

        End If
        If e.Item.CommandName = "Previous" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID < '" & TxtEmpID.Text & "' ORDER BY EmpID DESC"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try
        End If
        If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtEmpID.Text)
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
        End If
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
            StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Else
            StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "' And OutletName = '" & Session("Level") & "'"
        End If
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        If TxtEmpID.Text = "ALL" Then Exit Sub
        If Rdr.Read Then
            On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
            TxtTitle.Text = Rdr("Title")
            TxtCategory.Text = Rdr("Category")
            TxtGender.Text = Rdr("Gender")
            TxtCategory.Text = Rdr("Category")
            ChkClocked.Checked = IIf(Rdr("Clocked"), True, False)
            TxtClockID.Text = Rdr("BatchNo")
            Session.Add("EmpID", Rdr("EmpID"))
            ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & Rdr("EmpID")
            'Personal Details
        Else
            Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(1))
            ToolBar1_ItemClick(Me, ev)
            'ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('Invalid Employee ID','Error');</script>", False)
        End If
        Rdr.Close()
    End Sub

End Class