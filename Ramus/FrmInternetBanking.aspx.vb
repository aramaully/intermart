﻿Imports System.IO
Imports System.Data.OleDb
Partial Public Class FrmInternetBanking
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim CmdTrans As New OleDb.OleDbCommand
    Dim RdrTrans As OleDb.OleDbDataReader
    Dim CmdEmp As New OleDb.OleDbCommand
    Dim RdrEmp As OleDb.OleDbDataReader
    Dim CmdSystem As New OleDb.OleDbCommand
    Dim RdrSystem As OleDb.OleDbDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            TxtYear.Text = Date.Today.Year
            CmbMonth.SelectedIndex = Date.Today.Month - 1
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim StrSql As String
        Dim StrSalaryPeriod As String
        Dim DtePayDate As Date
        Dim StrLine As String
        Dim LngMaxSalNPF As Long
        Dim LngSal As Long
        Dim StrSSNo As String
        Dim strData As String = ""
        Dim StrFlag As String
        Dim StrLast As String
        Dim StrFirst As String
        Dim StrAddress As String
        Dim StrCity As String
        Dim StrCountry As String
        Dim StrM As String = "M"
        Dim LngNSF As Long
        Dim LngSalNPS As Single
        Dim stringWriter As New StringWriter
        Dim wrt As New IO.StringWriter()
        Dim StrCompanyName As String
        Dim StrType As String
        Dim tab As String = ""
        StrCompanyName = ""
        Dim SngTotal As Single

        Dim StrSpace As String = ""
        If CmbMonth.Text = "Bonus" Then
            DtePayDate = CDate("20-12" + "-" + TxtYear.Text)
        Else
            DtePayDate = CDate("28-" + CmbMonth.Text + "-" + TxtYear.Text)
        End If


        StrSalaryPeriod = Format(DtePayDate, "yyyyMM")

        Response.Clear()
        Response.ContentType = "text/csv"
        Response.AppendHeader("Content-Disposition", String.Format("attachment; filename=PRL" & Format(DateTime.Today, "MMyy") & ".csv"))

        CmdSystem.Connection = Con
        CmdSystem.CommandText = "SELECT * FROM [System]"
        RdrSystem = CmdSystem.ExecuteReader
        If RdrSystem.Read Then
            StrCompanyName = RdrSystem("Name") & ""
        End If
        RdrSystem.Close()

        Dim Dt As DataTable
        Dim Dr As DataRow
        Dim Dc As DataColumn
        Dt = New DataTable()
        Dc = New DataColumn("Col1", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col2", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col3", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col4", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col5", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col6", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col7", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)


        Dim DtHeader As DataTable
        Dim DrHeader As DataRow
        Dim DcHeader As DataColumn

        DtHeader = New DataTable()
        DcHeader = New DataColumn("Header1", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)
        DcHeader = New DataColumn("Header2", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)
        DcHeader = New DataColumn("Header3", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)
        DcHeader = New DataColumn("Header4", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)
        DcHeader = New DataColumn("Header5", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)
        DcHeader = New DataColumn("Header6", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)
        DcHeader = New DataColumn("Header7", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)

        'Excel Header
        DrHeader = DtHeader.NewRow
        DrHeader.Item("Header1") = "AccountNo"
        DrHeader.Item("Header2") = "Amount"
        DrHeader.Item("Header3") = "Name"
        DrHeader.Item("Header4") = "BankCode"
        DrHeader.Item("Header5") = "Description"
        DrHeader.Item("Header6") = "Month"
        DrHeader.Item("Header7") = "Flag"

        DtHeader.Rows.Add(DrHeader)
        For Each DrHeader In DtHeader.Rows
            tab = ""
            For i = 0 To DtHeader.Columns.Count - 1
                Response.Write(tab & DrHeader(i).ToString())
                tab = ","
            Next
            Response.Write(vbCrLf)
        Next

        If e.Item.CommandName = "Print" Then
            Try
                StrSql = "SELECT * from Salary WHERE [Month] = '" & CmbMonth.Text & "' AND Year='" & TxtYear.Text & "' And ModePay = 'Bank' ORDER BY EmpID"
                CmdTrans.Connection = Con
                CmdTrans.CommandText = StrSql
                RdrTrans = CmdTrans.ExecuteReader
                While RdrTrans.Read
                    Dim pt As Char = " "
                    Dr = Dt.NewRow
                    If RdrTrans("BankID") = "SBM" Then
                        Dr.Item("Col1") = Microsoft.VisualBasic.Right("00000000000000" + RdrTrans("AccountNo"), 14)
                    Else
                        Dr.Item("Col1") = Microsoft.VisualBasic.Right("0000000000000000" + RdrTrans("AccountNo"), 16)
                    End If
                    Dr.Item("Col2") = RdrTrans("LastName") + " " + RdrTrans("FirstName")

                    SngTotal = 0
                    SngTotal = RdrTrans("BasicSalary")
                    SngTotal = SngTotal + RdrTrans("Overtime")
                    SngTotal = SngTotal + RdrTrans("IfsBonus")
                    SngTotal = SngTotal + RdrTrans("Arrears")
                    SngTotal = SngTotal + RdrTrans("PerfAllow")
                    SngTotal = SngTotal + RdrTrans("TravelAllowance")
                    SngTotal = SngTotal + RdrTrans("MaternityAllow")
                    SngTotal = SngTotal + RdrTrans("RespAllow")
                    SngTotal = SngTotal + RdrTrans("MealAllow")
                    SngTotal = SngTotal + RdrTrans("PensionEmp")
                    SngTotal = SngTotal + RdrTrans("PassageBenefit")
                    SngTotal = SngTotal + RdrTrans("OtherAllow")
                    SngTotal = SngTotal + Math.Round((RdrTrans("LocalRefund") + RdrTrans("SickRefund")) * (RdrTrans("DailyRate")), 2)

                    SngTotal = SngTotal - (RdrTrans("NPFEmployee") + RdrTrans("PAYE") + +RdrTrans("Others") + RdrTrans("LoanRepay"))
                    SngTotal = SngTotal - (RdrTrans("EPZEE") + RdrTrans("Short") + RdrTrans("CreditCard") + RdrTrans("LoanRepay1") + RdrTrans("NSFEmployee"))
                    Dr.Item("Col3") = SngTotal

                    Dr.Item("Col4") = Trim(RdrTrans("BankCode"))
                    Dr.Item("Col5") = StrCompanyName
                    Dr.Item("Col6") = "Salary" & Format(DtePayDate, "MMM") & Format(DtePayDate, "yy")
                    Dr.Item("Col7") = "N"
                    Dt.Rows.Add(Dr)
                End While
                Dim StrFileName As String = "PRL" & Format(DtePayDate, "MM") & Format(DtePayDate, "yy")

                For Each Dr In Dt.Rows
                    tab = ""
                    For i = 0 To Dt.Columns.Count - 1
                        Response.Write(tab & Dr(i).ToString())
                        tab = ","
                    Next
                    Response.Write(vbCrLf)
                Next
                Response.End()
                Response.[End]()
            Catch ex As Exception

            End Try

        End If
    End Sub
End Class