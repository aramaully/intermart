﻿Public Partial Class FrmGeneralAudit
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim CmdShift As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            Session("FieldCount") = 0
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Dim StrSelFormula As String
            Dim DteFrom As Date = Format(CDate(TxtDateFrom.Text), "dd/MM/yyyy")
            Dim StrFrom As String = Format(DteFrom, "yyyy,MM,dd")
            Dim DteTo As Date = Format(CDate(TxtDateTo.Text), "dd/MM/yyyy")
            Dim StrTo As String = Format(DteTo, "yyyy,MM,dd")
            If ChkEmpAudit.Checked Then
                StrSelFormula = "{EmpMasterAudit.ADate}>=date(" & StrFrom & ") And {EmpMasterAudit.ADate}<=date(" & StrTo & ")"
                Session("ReportFile") = "RptEmpMasterAudit.rpt"
                Session("SelFormula") = StrSelFormula
                Session("RepHead") = "Employee Master Audit"
            End If
            If ChkSalAudit.Checked Then
                StrSelFormula = "{SalMasterAudit.ADate}>=date(" & StrFrom & ") And {SalMasterAudit.ADate}<=date(" & StrTo & ")"
                Session("ReportFile") = "RptSalMasterAudit.rpt"
                Session("SelFormula") = StrSelFormula
                Session("RepHead") = "Salary / Modify Audit"
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        End If
    End Sub

End Class