﻿Public Partial Class LeaveMaster
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrAudit As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            FillGrid()
        End If
    End Sub

    Protected Sub FillGrid()
        Cmd.Connection = Con
        StrSql = "SELECT * FROM LeaveMaster"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvLeave.DataSource = Rdr
        GdvLeave.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim Ev As New System.EventArgs

        If e.Item.CommandName = "New" Then
            TxtLeaveID.Text = ""
            TxtName.Text = ""
            TxtLeaveID.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Dim Trans As OleDb.OleDbTransaction
            Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
            Cmd.Connection = Con
            Cmd.Transaction = Trans
            Cmd.CommandText = "DELETE FROM LeaveMaster WHERE LeaveID = '" & TxtLeaveID.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO LeaveMaster (LeaveID,Name,Paid) VALUES('"
                StrSql = StrSql & TxtLeaveID.Text & "','"
                StrSql = StrSql & TxtName.Text & "',"
                StrSql = StrSql & IIf(OptYes.Checked, 1, 0) & ")"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                Trans.Commit()
                TxtLeaveID.Text = ""
                TxtName.Text = ""
                TxtLeaveID.Focus()
                FillGrid()

                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Leave ID " & TxtLeaveID.Text & " Created / Edited. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Create / Edit Leave','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                Trans.Rollback()
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM LeaveMaster WHERE LeaveID = '" & TxtLeaveID.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                TxtLeaveID.Text = ""
                TxtName.Text = ""
                TxtLeaveID.Focus()
                FillGrid()
                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Leave ID " & TxtLeaveID.Text & " Deleted. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Delete leave Master','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
    End Sub

    Private Sub GdvLoan_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvLeave.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvLeave, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvLoan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvLeave.SelectedIndexChanged
        TxtLeaveID.Text = GdvLeave.SelectedRow.Cells(0).Text
        TxtName.Text = GdvLeave.SelectedRow.Cells(1).Text
        OptYes.Checked = DirectCast(GdvLeave.SelectedRow.Cells(2).Controls(0), CheckBox).Checked
        OptNo.Checked = Not DirectCast(GdvLeave.SelectedRow.Cells(2).Controls(0), CheckBox).Checked
    End Sub

    Private Sub GdvLoan_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvLeave.SelectedIndexChanging
        GdvLeave.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvLeave.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvLeave.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub
End Class