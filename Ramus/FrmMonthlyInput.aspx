﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmMonthlyInput.aspx.vb" Inherits="Ramus.FrmMonthlyInput" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<html>
<head id="Head1" runat="server">
    <title>Monthly Input</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.
            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Monthly Input'; //Set the name
            w.Width = 1250; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 545;            
            w.Top = 10;
            w.Left = 10;
            w.Show(); //Show the window
        };
        function ShowFind() {
            var FindWindow = window.open("FindEmployee.aspx", "FindEmployee", "width=565,height=435,top=200, left=575");
        };
        function FunctionConfirm(Tlb, TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Save") {
                    Index = '1';
                    w.Confirm("Sure to Save", "Confirm", null, SaveFunction);
            }
            else if (TlbCmd == "Refresh") {
                __doPostBack('ToolBar1', '0');
            }
            else if (TlbCmd == "Lock") {
                Index = '3';
                w.Confirm("Sure to Lock All Timesheet?", "Confirm", null, SaveFunction);
            }
            else if (TlbCmd == "Unlock") {
                Index = '4';
                w.Confirm("Sure to Unlock Timesheet?", "Confirm", null, SaveFunction);
            }
        }
        function SaveFunction(Sender, RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        }
        function FunPic() {
            var myArgs = 'nothing';
            myargs = window.showModalDialog('Snap.aspx', myArgs, "dialogHeight:350px;dialogWidth:650px");
            if (myArgs == 'nothing') {
                PageMethods.AjaxSetSession("True");
            }
        }
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        </style>

    </head>
<body onload="init();">
    <form id="form2" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <script type="text/javascript">
        var xPos, yPos;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            xPos = $get('PnlGrid').scrollLeft;
            yPos = $get('PnlGrid').scrollTop;
        }
        function EndRequestHandler(sender, args) {
            $get('PnlGrid').scrollLeft = xPos;
            $get('PnlGrid').scrollTop = yPos;
        };
            function ShowReport() {
            wfind = window.frameElement.IDCWindow;
            wfind.CreateWindow('FrmRpt.aspx', w);
            return false;
        };
</script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px;
                height: 32px; width: 32px;" runat="server" BorderStyle="None" Visible="True">
                <div id="divImage" style="display: none">
                    <asp:Image ID="img1" runat="server" ImageUrl="/Images/Wait.gif" />
                </div>
            </asp:Panel>
            <div id="toolbar" style="width: 100%;">
                <eo:ToolBar ID="ToolBar1" runat="server" BackgroundImage="00100103" ClientSideOnItemClick="FunctionConfirm"
                    BackgroundImageLeft="00100101" BackgroundImageRight="00100102" SeparatorImage="00100104"
                    Width="100%">
                    <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 1px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: #335ea8 1px solid; background-color: #99afd4; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <Items>
                        <eo:ToolBarItem CommandName="Refresh" ImageUrl="~/images/RefreshDocViewHS.png" ToolTip="Refresh" Text="Refresh">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Help" ImageUrl="~/images/help.png" ToolTip="Help" Text="Help">
                        </eo:ToolBarItem>
                    </Items>
                    <ItemTemplates>
                        <eo:ToolBarItem Type="Custom">
                            <NormalStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <HoverStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <DownStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="DropDownMenu">
                            <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                            <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; background-image: url(00100106); background-position-x: right;" />
                            <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: none; background-color:transparent; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                        </eo:ToolBarItem>
                    </ItemTemplates>
                    <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                </eo:ToolBar>
            </div>
            <div id="Content" style="width: 100%; height: 100%; padding: 2px; border-right: solid 1px #cddaea;">
                <table class="style1">
                    <tr>
                        <td>
                            Outlet</td>
                        <td colspan="3">
                            <asp:DropDownList ID="CmbDept" runat="server" Width="300px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Month</td>
                        <td>
                            <asp:DropDownList ID="CmbMonth" runat="server" AutoPostBack="True">
                                    <asp:ListItem>January</asp:ListItem>
                                    <asp:ListItem>February</asp:ListItem>
                                    <asp:ListItem>March</asp:ListItem>
                                    <asp:ListItem>April</asp:ListItem>
                                    <asp:ListItem>May</asp:ListItem>
                                    <asp:ListItem>June</asp:ListItem>
                                    <asp:ListItem>July</asp:ListItem>
                                    <asp:ListItem>August</asp:ListItem>
                                    <asp:ListItem>September</asp:ListItem>
                                    <asp:ListItem>October</asp:ListItem>
                                    <asp:ListItem>November</asp:ListItem>
                                    <asp:ListItem>December</asp:ListItem>
                                    <asp:ListItem>Bonus</asp:ListItem>
                                </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="BtnSave" runat="server" Text="SAVE" Height="29px" 
                                Width="84px" />
                        </td>
                        <td>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Year</td>
                        <td>
                            <asp:TextBox ID="TxtYear" runat="server" AutoPostBack="True" Width="75px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="BtnView" runat="server" Text="YOS Report" Height="29px" 
                                Width="129px" />
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
                
                <br />
                <asp:Label ID="Lblerror" runat="server" Font-Bold="False" ForeColor="#FF3300"></asp:Label>
                <asp:Panel ID="PnlGrid" runat="server" ScrollBars="Vertical" BorderStyle="Inset" 
                    BorderWidth="1px" Height="90%">
                <tr>
                    <td colspan="8">
                    <asp:Panel runat="server" ID="PnlTravelList" width="100%" Height="90%" ScrollBars="Horizontal">
                        <asp:GridView ID="GdvTVList" runat="server" AutoGenerateColumns="False" width="99%" EnableModelValidation="True" GridLines="Horizontal" Height="100%" PageSize="25" ShowFooter="True" HorizontalAlign="Left">
                        <Columns>
                        <asp:TemplateField HeaderText="EmpID" ><ItemTemplate>
                        <asp:TextBox runat="server" ID="TxtEmpID" Enabled="false" Width ="50%">
                        </asp:TextBox></ItemTemplate>
                            <ControlStyle Font-Bold="False" Width="50px" />
                            <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Size="Smaller" 
                                HorizontalAlign="Left" Width="50px" />
                            <ItemStyle Font-Bold="True" />
                            </asp:TemplateField>           
                        <asp:TemplateField HeaderText="Name"><ItemTemplate> 
                        <asp:TextBox runat="server" ID="TxtName" Enabled = "false" Width = "200px">
                        </asp:TextBox></ItemTemplate>
                            <ControlStyle Font-Overline="False" Width="300px" />
                            <HeaderStyle Font-Bold="True" Font-Size="Smaller" HorizontalAlign="Left" 
                                Width="100px" Font-Underline="False" />
                            <ItemStyle Wrap="True" />
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderText="BasicSalary"><ItemTemplate> 
                        <asp:TextBox runat="server" Enabled = "false" width="70px"  ID="TxtBasic">
                        </asp:TextBox></ItemTemplate>
                            <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" Width="70px" />
                            </asp:TemplateField> 
                        <asp:TemplateField HeaderText="Position"><ItemTemplate> 
                        <asp:TextBox runat="server" Enabled = "false" width="150px"  ID="TxtPosition">
                        </asp:TextBox></ItemTemplate>
                            <ControlStyle Width="200px" />
                            <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" Width="70px" />
                            </asp:TemplateField> 
                                         
                        <asp:TemplateField HeaderText="Other Allowances"><ItemTemplate> 
                        <asp:TextBox runat="server" width="70px"  ID="TxtOtherAllow">
                        </asp:TextBox></ItemTemplate>
                            <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" Width="70px" />
                            </asp:TemplateField>     

                        <asp:TemplateField HeaderText="Travel"><ItemTemplate> 
                        <asp:TextBox runat="server" ID="TxtTravel" Width = "50px" Enabled="True">
                        </asp:TextBox></ItemTemplate>
                            <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" Width="50px" />
                        </asp:TemplateField>   
                 
                        <asp:TemplateField HeaderText="Short"><ItemTemplate> 
                        <asp:TextBox runat="server" ID="txtShort" Width = "50px" Enabled="True">
                        </asp:TextBox></ItemTemplate>
                            <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" Width="50px" />
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderText="Credit Card"><ItemTemplate> 
                        <asp:TextBox runat="server" ID="TxtCCard" Width = "50px" Enabled="True">
                        </asp:TextBox></ItemTemplate>
                            <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" Width="50px" />
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderText="YOS"><ItemTemplate> 
                        <asp:TextBox runat="server" ID="TxtYOS" Width = "50px" Enabled="False">
                        </asp:TextBox></ItemTemplate>
                        <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="YOS Amount"><ItemTemplate> 
                        <asp:TextBox runat="server" ID="TxtYOSAmt" Width = "50px" Enabled="True">
                        </asp:TextBox></ItemTemplate>
                            <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ex Gratia"><ItemTemplate> 
                            <asp:TextBox runat="server" ID="TxtExGratia" Width = "70px" Enabled="True">
                            </asp:TextBox></ItemTemplate>
                            <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" Width="70px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Other Deduction"><ItemTemplate> 
                            <asp:TextBox runat="server" ID="TxtOthDeduction" Width = "70px" Enabled="True">
                            </asp:TextBox></ItemTemplate>
                            <HeaderStyle Font-Size="Smaller" HorizontalAlign="Left" Width="70px" />
                        </asp:TemplateField>
                        </Columns>
                            <AlternatingRowStyle BackColor="#666699" />
                            <PagerSettings PageButtonCount="50" />
                    </asp:GridView>                                            
                    </asp:Panel>
                    </td>
                  </tr>
                  
                  <tr>

                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_InitializeRequest);
        prm.add_endRequest(prm_EndRequest);

        function prm_InitializeRequest(sender, args) {
            var panelProg = $get('divImage');
            if (args._postBackElement.id != "Timer1") {
                panelProg.style.display = '';
            }
        };

        function prm_EndRequest(sender, args) {
            var panelProg = $get('divImage');
            panelProg.style.display = 'none';
        };
    </script>

</body>
</html>

