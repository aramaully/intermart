﻿Public Partial Class FrmImpClocking
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim TimexCon As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim TimexCmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim ChkCmd As New OleDb.OleDbCommand
    Dim CmdEmp As New OleDb.OleDbCommand
    Dim RdrEmp As OleDb.OleDbDataReader
    Dim StrSqlEmp As String
    Dim StrSql As String
    Dim StrEmpID As String
    Dim StrTimeIn As String
    Dim StrBatchNo As String
    Dim DteDate As Date
    Dim StrTimeOut As String
    Dim DteTimeIn As Date
    Dim DteTimeOut As Date
    Dim SngHrs As Single
    Dim StrDay As String
    Dim DteStart As Date
    Dim DteEnd As Date
    Dim CmdDel As New OleDb.OleDbCommand
    Dim CmdPH As New OleDb.OleDbCommand
    Dim SngHours As Single

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        TimexCon.ConnectionString = Session("TimexConn")
        TimexCon.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            TxtEmpID.Text = "ALL"
        End If
    End Sub

    Protected Sub ClearAll()
        TxtLastName.Text = ""
        TxtFirstName.Text = ""
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If e.Item.CommandName = "Update" Then
            LblResults.Text = "Analysing and Updating Timesheet"
            Try
                If TxtEmpID.Text = "ALL" Then
                    StrSqlEmp = "Select *From Employee Where Working = 1"
                Else
                    StrSqlEmp = "Select *From Employee Where Working = 1 And EmpID = '" & TxtEmpID.Text & "'"
                End If
                CmdEmp.Connection = Con
                CmdEmp.CommandText = StrSqlEmp
                RdrEmp = CmdEmp.ExecuteReader
                While RdrEmp.Read
                    Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                    DteStart = Format(CDate(TxtDateFrom.Text), "dd-MMM-yyyy")
                    DteEnd = Format(CDate(TxtDateTo.Text), "dd-MMM-yyyy")
                    While DteStart <= DteEnd
                        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                        StrTimeIn = ""
                        StrTimeOut = ""
                        CmdDel.Connection = Con
                        CmdDel.CommandText = "Delete From TimeSheet Where EmpID = '" & RdrEmp("EmpID") & "' And [Date] = '" & Format(DteStart, "dd-MMM-yyyy") & "'"
                        CmdDel.ExecuteNonQuery()
                        SngHours = 0
                        StrSql = "Select Min(CheckTime) as MinTime, Max(CheckTime) as MaxTime From CheckInOut Where "
                        StrSql = StrSql & "CheckTime >= '" & Format(DteStart, "MM/dd/yyyy 05:30:00") & "' And "
                        StrSql = StrSql & "CheckTime <= '" & Format(DteStart.AddDays(1), "MM/dd/yyyy 05:29:59") & "' "
                        StrSql = StrSql & "And UserID = '" & RdrEmp("BadgeNo") & "'"
                        TimexCmd.Connection = TimexCon
                        TimexCmd.CommandText = StrSql
                        Rdr = TimexCmd.ExecuteReader
                        If Rdr.Read Then
                            If Not IsDBNull(Rdr("MinTime")) Then
                                DteTimeIn = Rdr("MinTime")
                                StrTimeIn = DteTimeIn
                            End If
                            If Not IsDBNull(Rdr("MaxTime")) Then
                                DteTimeOut = Rdr("MaxTime")
                                StrTimeOut = Rdr("MaxTime")
                            End If
                            If Not IsDBNull(Rdr("MinTime")) And Not IsDBNull(Rdr("MaxTime")) Then
                                If CDate(Rdr("MinTime")) = CDate(Rdr("MaxTime")) Then
                                    StrTimeOut = ""
                                    SngHours = 0
                                Else
                                    SngHours = DateDiff(DateInterval.Second, CDate(DteTimeIn), CDate(DteTimeOut)) / 60 / 60
                                End If
                            End If
                        End If
                        Rdr.Close()
                        Select Case Weekday(DteStart)
                            Case 1
                                StrDay = "SU"
                            Case 7
                                StrDay = "WE"
                            Case Else
                                StrDay = "WD"
                        End Select
                        CmdPH.Connection = Con
                        CmdPH.CommandText = "SELECT COUNT(*) FROM Holiday WHERE [Date] = '" & Format(DteStart, "dd-MMM-yyyy") & "'"
                        If CmdPH.ExecuteScalar > 0 Then
                            StrDay = "PH"
                        End If
                        'Now start building the INSERT QUERY
                        StrSql = "INSERT INTO TimeSheet (EmpID,[Date],TimeIn,TimeOut,[Normal],[Day]"
                        StrSql = StrSql & ") VALUES('" & RdrEmp("EmpID") & "','"
                        StrSql = StrSql & Format(DteStart, "dd-MMM-yyyy") & "','"
                        StrSql = StrSql & Mid(StrTimeIn, 12, 5) & "','"
                        StrSql = StrSql & Mid(StrTimeOut, 12, 5) & "',"
                        StrSql = StrSql & Math.Round(SngHours, 2, MidpointRounding.AwayFromZero) & ",'"
                        StrSql = StrSql & StrDay & "')"
                        ChkCmd.Connection = Con
                        ChkCmd.CommandText = StrSql
                        Try
                            ChkCmd.ExecuteNonQuery()
                        Catch ex As Exception
                            LblResults.Text = "Error"
                        End Try
                        DteStart = DteStart.AddDays(1)
                    End While
                End While
                RdrEmp.Close()
            Catch ex As Exception
                LblResults.Text = "Error Processing Clockings.. " & ex.Message
            End Try
            LblResults.Text = "Clocking Processed Successfully"
        End If
        If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtEmpID.Text)
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
        End If
    End Sub

    Protected Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtEmpID.TextChanged
        StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        If TxtEmpID.Text = "ALL" Then Exit Sub
        If Rdr.Read Then
            On Error Resume Next
            TxtFirstName.Text = Rdr("First")
            TxtLastName.Text = Rdr("Last")
            Session.Add("EmpID", Rdr("EmpID"))
        Else
            Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(1))
            ToolBar1_ItemClick(Me, ev)
            'ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('Invalid Employee ID','Error');</script>", False)
        End If
        Rdr.Close()
    End Sub
End Class