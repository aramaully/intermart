﻿Public Partial Class FrmPaySlip
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim CmdTemp As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            CmbMonth.SelectedIndex = Date.Today.Month - 1
            TxtYear.Text = Date.Today.Year
            TxtDate.Text = CDate("28-" & CmbMonth.Text & "-" & TxtYear.Text)
            TxtEmpID.Text = "ALL"

            'Outlet
            Cmd.Connection = Con
            Cmd.CommandText = "Select OutletName from Outlet Order by OutletCode"
            CmbOutlet.Items.Add("ALL")
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                CmbOutlet.Items.Add(Rdr("OutletName"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ClearAll()
        TxtFirst.Text = ""
        TxtLast.Text = ""
        TxtClockID.Text = ""
        TxtCategory.Text = ""
        TxtTitle.Text = ""
        TxtGender.Text = ""
        ImgPhoto.ImageUrl = Nothing
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ev As New System.EventArgs
        If e.Item.CommandName = "New" Then
            ClearAll()
            TxtEmpID.Text = "ALL"
        End If
        If e.Item.CommandName = "Print" Then
            Session("FieldCount") = 0
            Dim StrSelFormula As String
            Session("ReportFile") = "PaySlip.rpt"

            If CmbMonth.Text = "Bonus" Then
                If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
                    StrSelFormula = "{Salary.Month} = 'Bonus' And {Salary.Year} = " & TxtYear.Text & ""
                Else
                    StrSelFormula = "{Salary.Month} = 'Bonus' And {Salary.OutletName} = '" & Session("Level") & "' and {Salary.Year} = " & TxtYear.Text & ""
                End If
            Else
                Dim DtePayDate As Date = CDate(TxtDate.Text)
                Dim StrDate As String = Format(DtePayDate, "yyyy,MM,dd")
                If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
                    'StrSelFormula = "{Salary.Date}=date(" & StrDate & ")"
                    StrSelFormula = "{Salary.Month}= '" & Trim(CmbMonth.Text) & "' And {Salary.Year} = " & TxtYear.Text & " "
                    If CmbOutlet.Text <> "ALL" Then
                        StrSelFormula = StrSelFormula & " And {Salary.OutletName} = '" & Trim(CmbOutlet.Text) & "'"
                    End If
                Else
                    StrSelFormula = "{Salary.Month}= '" & Trim(CmbMonth.Text) & "' And {Salary.Year} = " & TxtYear.Text & " And {Salary.OutletName} = '" & Session("Level") & "' "
                    'StrSelFormula = "{Salary.Date}=date(" & StrDate & ") And {Salary.OutletName} = '" & Session("Level") & "'"
                End If

            End If

            If TxtLast.Text <> "" Then
                StrSelFormula = StrSelFormula & " And {Salary.EmpID} = '" & TxtEmpID.Text & "'"
            End If

            Session("SelFormula") = StrSelFormula
            Session("RepHead") = "Payslip for " & CmbMonth.Text & ", " & TxtYear.Text
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
            'End If
        End If
        If e.Item.CommandName = "Delete" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "DELETE FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
                Cmd.ExecuteNonQuery()
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Next" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID > '" & TxtEmpID.Text & "'"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try

        End If
        If e.Item.CommandName = "Previous" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID < '" & TxtEmpID.Text & "' ORDER BY EmpID DESC"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try
        End If
        If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtEmpID.Text)
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
        End If
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
            StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Else
            StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "' And OutletName = '" & Session("Level") & "'"
        End If

        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        If TxtEmpID.Text = "ALL" Then Exit Sub
        If Rdr.Read Then
            On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
            TxtTitle.Text = Rdr("Title")
            TxtCategory.Text = Rdr("Category")
            TxtGender.Text = Rdr("Gender")
            TxtCategory.Text = Rdr("Category")
            ChkClocked.Checked = IIf(Rdr("Clocked"), True, False)
            TxtClockID.Text = Rdr("BatchNo")
            Session.Add("EmpID", Rdr("EmpID"))
            ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & Rdr("EmpID")
            'Personal Details
        Else
            Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(1))
            ToolBar1_ItemClick(Me, ev)
            'ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('Invalid Employee ID','Error');</script>", False)
        End If
        Rdr.Close()
    End Sub

    Protected Sub CmbCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbCategory.SelectedIndexChanged
        If CmbCategory.SelectedIndex = 0 Then
            LblDate.Text = "Month"
            CmbMonth.Visible = True
            TxtDate.Visible = False
            TxtDate.Text = CDate("28-" & CmbMonth.Text & "-" & TxtYear.Text)
        ElseIf CmbCategory.SelectedIndex = 1 Then
            LblDate.Text = "Week Ending"
            TxtDate.Text = Date.Today
            TxtDate.Visible = True
            CmbMonth.Visible = False
        Else
            LblDate.Text = "FortNight Ending"
            TxtDate.Text = Date.Today
            TxtDate.Visible = True
            CmbMonth.Visible = False
        End If
    End Sub

    Protected Sub CmbMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbMonth.SelectedIndexChanged
        If CmbCategory.Text = "Monthly" Then
            If CmbMonth.Text = "Bonus" Then
                TxtDate.Text = CDate("30-December" & "-" & TxtYear.Text)
            Else
                TxtDate.Text = CDate("28-" & CmbMonth.Text & "-" & TxtYear.Text)
            End If

        End If
    End Sub
End Class