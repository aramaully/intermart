﻿Public Partial Class FrmMPaysheetSum
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim StrSql As String
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            CmbMonth.SelectedIndex = Date.Today.Month - 1
            TxtYear.Text = Date.Today.Year
            TxtDate.Text = CDate("28-" & CmbMonth.Text & "-" & TxtYear.Text)
        End If
    End Sub

    Protected Sub CmbCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbCategory.SelectedIndexChanged
        If CmbCategory.Text = "Monthly" Then
            TxtDate.Text = CDate("28-" & CmbMonth.Text & "-" & TxtYear.Text)
            CmbMonth.Visible = True
            TxtDate.Visible = False
            LblDate.Text = "Month"
        ElseIf CmbCategory.Text = "Weekly" Then
            CmbMonth.Visible = False
            TxtDate.Text = Date.Today
            TxtDate.Visible = True
            LblDate.Text = "Week Ending"
        Else
            CmbMonth.Visible = False
            TxtDate.Text = Date.Today
            TxtDate.Visible = True
            LblDate.Text = "Fortnight Ending"
        End If
    End Sub

    Protected Sub CmbMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbMonth.SelectedIndexChanged
        If CmbMonth.Text = "Bonus" Then
            TxtDate.Text = CDate("30-December" & "-" & TxtYear.Text)
        Else
            TxtDate.Text = CDate("28-" & CmbMonth.Text & "-" & TxtYear.Text)
        End If

    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            Session("FieldCount") = 0
            Dim StrSelFormula As String
            If ChkALL.Checked Then
                Session("ReportFile") = "RptPaySheetA4HODSum.rpt"
                If CmbMonth.Text = "Bonus" Then
                    StrSelFormula = "{Salary.Month} = 'Bonus' And {Salary.Year} = '" & TxtYear.Text & "' "
                Else
                    Dim DtePayDate As Date = CDate("28-" + CmbMonth.Text + "-" + TxtYear.Text)
                    Dim StrDate As String = Format(DtePayDate, "yyyy,MM,dd")
                    StrSelFormula = "{Salary.Date}=date(" & StrDate & ")"
                End If
                StrSelFormula = StrSelFormula & ""
                Session("SelFormula") = StrSelFormula
                Session("RepHead") = "Pay Sheet Summary for the month of " & CmbMonth.Text & ", " & TxtYear.Text
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
                Exit Sub
            End If
            If ChkStaff.Checked Then
                Session("ReportFile") = "RptPaySheetA4HODSum.rpt"
                If CmbMonth.Text = "Bonus" Then
                    StrSelFormula = "{Salary.Month} = 'Bonus' And {Salary.Year} = '" & TxtYear.Text & "' "
                Else
                    Dim DtePayDate As Date = CDate("28-" + CmbMonth.Text + "-" + TxtYear.Text)
                    Dim StrDate As String = Format(DtePayDate, "yyyy,MM,dd")
                    StrSelFormula = "{Salary.Date}=date(" & StrDate & ")"
                End If
                StrSelFormula = StrSelFormula & " And {Employee.Staff} = False"
                Session("SelFormula") = StrSelFormula
                Session("RepHead") = "Pay Sheet Summary for the month of " & CmbMonth.Text & ", " & TxtYear.Text
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
                Exit Sub
            End If
            Dim DteSalDate As Date = CDate("28-" + CmbMonth.Text + "-" + TxtYear.Text)
            StrSql = "Select Count(*) From Access Where UserId = '" & Session("UserID") & "' And MenuName = 'ProHOD'"
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar > 0 Then
                Session("ReportFile") = "RptPaySheetA4HODSum.rpt"
                If CmbMonth.Text = "Bonus" Then
                    StrSelFormula = "{Salary.Month} = 'Bonus' And {Salary.Year} = '" & TxtYear.Text & "' "
                Else
                    Dim DtePayDate As Date = CDate("28-" + CmbMonth.Text + "-" + TxtYear.Text)
                    Dim StrDate As String = Format(DtePayDate, "yyyy,MM,dd")
                    StrSelFormula = "{Salary.Date}=date(" & StrDate & ")"
                End If
                StrSelFormula = StrSelFormula & " And {Employee.Staff} = True"
                Session("SelFormula") = StrSelFormula
                Session("RepHead") = "Pay Sheet Summary for the month of " & CmbMonth.Text & ", " & TxtYear.Text
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
            Else
                Session("ReportFile") = "RptPaySheetA4Sum.rpt"
                If CmbMonth.Text = "Bonus" Then
                    StrSelFormula = "{Salary.Month} = 'Bonus' And {Salary.Year} = '" & TxtYear.Text & "' "
                Else
                    Dim DtePayDate As Date = CDate("28-" + CmbMonth.Text + "-" + TxtYear.Text)
                    Dim StrDate As String = Format(DtePayDate, "yyyy,MM,dd")
                    StrSelFormula = "{Salary.Date}=date(" & StrDate & ")"
                End If
                StrSelFormula = StrSelFormula & " And {Employee.Staff} = False"
                Session("SelFormula") = StrSelFormula
                Session("RepHead") = "Pay Sheet Summary for the month of " & CmbMonth.Text & ", " & TxtYear.Text
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
            End If
        End If
    End Sub

    Protected Sub ChkALL_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ChkALL.CheckedChanged
        If ChkALL.Checked Then
            ChkStaff.Checked = False
        End If
    End Sub

    Protected Sub ChkStaff_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ChkStaff.CheckedChanged
        If ChkStaff.Checked Then
            ChkALL.Checked = False
        End If
    End Sub
End Class