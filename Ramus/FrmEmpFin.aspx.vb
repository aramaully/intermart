﻿Imports System.Data.SqlClient
Imports System.Web.Services
Imports System.IO

Imports System.Configuration
Imports System.Data
Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq
Imports System.Data.OleDb
Imports System.Text
Imports Ramus.ClsPRGF

Partial Public Class FrmEmpFin
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdAccess As New OleDb.OleDbCommand
    Dim CmdCheckEmp As New OleDb.OleDbCommand
    Dim RdrCheckEmp As OleDb.OleDbDataReader
    Dim DteProbation As Date
    Dim StrSql As String
    Dim CmdSubDept As New OleDb.OleDbCommand
    Dim RdrSubDept As OleDb.OleDbDataReader
    Dim CmdDept As New OleDb.OleDbCommand
    Dim RdrDept As OleDb.OleDbDataReader
    'Audit
    Dim StrBasicSalary As String = ""
    Dim StrPosition As String = ""
    Dim StrJobDesc As String = ""
    Dim StrLocation As String = ""
    Dim StrTotEDF As String = ""
    Dim StrTravelAllow As String = ""
    Dim StrFringe As String = ""
    Dim StrProd As String = ""
    Dim StrMaternityAllow As String = ""
    Dim StrMealAllow As String = ""
    Dim StrRespAllow As String = ""
    Dim StrWorking As String = ""
    Dim StrLastDay As String = ""
    Dim StrAttBonus As String = ""
    Dim StrOTPaid As String = ""
    Dim StrEmpPassageBebefit As String = ""
    Dim CmdGenAudit As New OleDb.OleDbCommand


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'BtnOpen.Attributes.Add("OnClick", "return getFile();")
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then

            'If Session("Level") = "FacUser" Then
            '    TabPanel4.Visible = False
            '    TabPanel5.Visible = False
            '    TabPanel7.Visible = False
            '    TabPanel8.Visible = False
            '    TabPanel9.Visible = False
            '    TabPanel12.Visible = False
            'End If
            'If Session("Level") = "DataEntry" Then
            '    TabPanel2.Visible = False
            '    TabPanel3.Visible = False
            'End If

            'Populate Combo boxes

            Cmd.Connection = Con
            Cmd.CommandText = "SELECT * FROM Bank ORDER BY Name"
            CmbBank.Items.Add("Select...")
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                CmbBank.Items.Add(Rdr("Name"))
            End While
            Rdr.Close()

            Cmd.Connection = Con
            Cmd.CommandText = "SELECT Usecar FROM Car"
            CmbComCar.Items.Add("Select...")
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                CmbComCar.Items.Add(Rdr("UseCar"))
            End While
            Rdr.Close()

            Cmd.Connection = Con
            Cmd.CommandText = "SELECT Position FROM JobDesc"
            CmbJobDesc.Items.Add("Select...")
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                CmbJobDesc.Items.Add(Rdr("Position"))
            End While
            Rdr.Close()

            CmbTMode.Items.Add("Select...")
            CmbTMode.Items.Add("Fixed")
            CmbTMode.Items.Add("Daily")
            CmbMOP.Items.Add("Select...")
            CmbMOP.Items.Add("Bank")
            CmbMOP.Items.Add("Cheque")
            CmbMOP.Items.Add("Cash")

            Cmd.CommandText = "SELECT * FROM Paye ORDER BY PAYEScheme"
            CmbPScheme.Items.Add("Select...")
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                CmbPScheme.Items.Add(Rdr("PAYEScheme"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ClearAll()
        TxtFirst.Text = ""
        TxtLast.Text = ""
        TxtClockID.Text = ""
        TxtCategory.Text = ""
        TxtTitle.Text = ""
        TxtGender.Text = ""
        CmbStatus.SelectedIndex = 0
        ImgPhoto.ImageUrl = Nothing
        TxtBSalary.Text = ""
        TxtPosition.Text = ""
        CmbPScheme.SelectedIndex = 0
        CmbJobDesc.SelectedIndex = 0
        TxtPPaid.Text = ""
        TxtLLeave.Text = ""
        TxtSLeave.Text = ""
        TxtEDFFactor.Text = ""
        CmbTMode.SelectedIndex = 0
        OptWorking.Checked = True
        OptNotWorking.Checked = False
        ChkAttendance.Checked = False
        ChkOTime.Checked = False
        TxtTAllowance.Text = ""
        CmbMOP.SelectedIndex = 0
        TxtLWD.Text = ""
        TxtTotEdf.Text = ""
        TxtMedical.Text = ""
        TxtPerBonus.Text = ""
        TxtMaternityAllow.Text = ""
        TxtRespAllow.Text = ""
        TxtMealAllow.Text = ""
        TxtTaxAcc.Text = ""
        CmbBank.SelectedIndex = 0
        TxtBankAcc.Text = ""
        TxtReviewDate.Text = ""
        TxtNIC.Text = ""
        TxtPassportNo.Text = ""
        OptNPSY.Checked = True
        OptNPSN.Checked = False
        TxtReportTo.Text = ""
        TxtAddress.Text = ""
        TxtCity.Text = ""
        TxtCountry.Text = ""
        TxtPhone.Text = ""
        TxtFax.Text = ""
        TxtMobile.Text = ""
        TxtEMail.Text = ""
        CmbCivilStatus.SelectedIndex = 0
        CmbComCar.SelectedIndex = 0
        TxtCapCar.Text = ""
        TxtAmount.Text = ""
        TxtEmpContributionC.Text = ""
        CmbPropType.SelectedIndex = 0
        CmbTOH.SelectedIndex = 0
        TxtPercentage.Text = ""
        TxtEmpContributionH.Text = ""
        TxtAccBenefit.Text = ""
        TxtExtention.Text = ""
        TxtPerEmail.Text = ""
        ChkPension.Checked = False
        chkExpat.Checked = False

        'Loan
        ChkLoan1.Checked = False
        ChkLoan2.Checked = False
        TxtFrom1.Text = ""
        TxtFrom2.Text = ""
        TxtTo1.Text = ""
        TxtTo2.Text = ""
        TxtLoanAmt1.Text = ""
        TxtLoanAmt2.Text = ""
        'Emergency Details
        txtcontact1first.Text = ""
        Txtcontact1last.Text = ""
        txtcontact1home.Text = ""
        txtcontact1mobile.Text = ""
        txtcontact1rel.Text = ""
        txtcontact1work.Text = ""
        txtcontact2first.Text = ""
        Txtcontact2last.Text = ""
        txtcontact2home.Text = ""
        txtcontact2mobile.Text = ""
        txtcontact2rel.Text = ""
        txtcontact2work.Text = ""
        txttakento.Text = ""
        TxtEmpPassBenefit.Text = ""
        'Qualification grid
        GdvQua.DataSource = Nothing
        GdvQua.DataBind()
        'Reference grid
        GdvRef.DataSource = Nothing
        GdvRef.DataBind()
        'Experience grid
        GdvExp.DataSource = Nothing
        GdvExp.DataBind()
        'File Path
        GdvSavePath.DataSource = Nothing
        GdvSavePath.DataBind()

    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ev As New System.EventArgs
        'If e.Item.CommandName = "New" Then
        '    ClearAll()
        '    TxtEmpID.Text = ""
        '    TxtEmpID.Focus()
        'End If
        If e.Item.CommandName = "Save" Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT BankID FROM Bank WHERE Name = '" & CmbBank.Text & "'"
            Dim StrBankID As String = ""
            Try
                StrBankID = Cmd.ExecuteScalar
            Catch ex As Exception
            End Try
            LblAccess.Visible = False

            'Travel Mode
            If CmbTMode.Text = "" Or CmbTMode.Text = "Select..." Then
                LblAccess.Visible = True
                LblAccess.Text = "Please Enter Travel Mode!!!"
                Exit Sub
            End If


            'Mode of Pay
            If CmbMOP.Text = "" Or CmbMOP.Text = "Select..." Then
                LblAccess.Visible = True
                LblAccess.Text = "Please Enter Mode Of Pay!!!"
                Exit Sub
            End If
            'Basic Salary
            If TxtBSalary.Text = "" Then
                LblAccess.Visible = True
                LblAccess.Text = "Please Enter Basic Salary!!!"
                Exit Sub
            End If
            'Paye scheme
            If CmbPScheme.Text = "" Or CmbPScheme.Text = "Select..." Then
                LblAccess.Visible = True
                LblAccess.Text = "Please Select Monthly for PAYE Scheme!!!"
                Exit Sub
            End If

            StrSql = "SELECT COUNT(*) FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
            Cmd.CommandText = StrSql
            Cmd.Connection = Con
            If Cmd.ExecuteScalar > 0 Then
                'Audit
                If Request.Cookies("LastFinDetail") IsNot Nothing Then
                    Dim lvGen As HttpCookie = Request.Cookies("LastFinDetail")
                    StrBasicSalary = Server.HtmlDecode(lvGen.Values("BasicSalary"))
                    StrPosition = Server.HtmlDecode(lvGen.Values("Position"))
                    StrJobDesc = Server.HtmlDecode(lvGen.Values("JobDesc"))
                    StrLocation = Server.HtmlDecode(lvGen.Values("Location"))
                    StrTotEDF = Server.HtmlDecode(lvGen.Values("TotEdf"))
                    StrTravelAllow = Server.HtmlDecode(lvGen.Values("TravelAllowance"))
                    StrFringe = Server.HtmlDecode(lvGen.Values("MedicalContrib"))
                    StrProd = Server.HtmlDecode(lvGen.Values("PerfAllow"))
                    StrMaternityAllow = Server.HtmlDecode(lvGen.Values("MaternityAllow"))
                    StrRespAllow = Server.HtmlDecode(lvGen.Values("RespAllow"))
                    StrMealAllow = Server.HtmlDecode(lvGen.Values("MealAllow"))
                    StrEmpPassageBebefit = Server.HtmlDecode(lvGen.Values("PassageBenefit"))
                    If Server.HtmlDecode(lvGen.Values("Working")) = "" Then
                        StrWorking = True
                    Else
                        StrWorking = False
                    End If
                    StrLastDay = Server.HtmlDecode(lvGen.Values("LastDay"))
                    If Server.HtmlDecode(lvGen.Values("AttBonus")) = "" Then
                        StrAttBonus = True
                    Else
                        StrAttBonus = False
                    End If
                    If Server.HtmlDecode(lvGen.Values("OTPaid")) = "" Then
                        StrOTPaid = True
                    Else
                        StrOTPaid = False
                    End If
                End If
                Try
                    StrSql = "UPDATE Employee SET "
                    StrSql = StrSql & "EmpBasic ='" & Val(TxtBSalary.Text) & "',"
                    StrSql = StrSql & "BasicSalary ='" & Val(TxtBSalary.Text) & "',"
                    'Audit
                    If StrBasicSalary <> TxtBSalary.Text Then
                        CmdGenAudit.Connection = Con
                        CmdGenAudit.CommandText = "Insert Into EmpMasterAudit Values('" & TxtEmpID.Text & "','Basic Salary change from ','" & StrBasicSalary & "','" & TxtBSalary.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                        CmdGenAudit.ExecuteNonQuery()
                    End If
                    StrSql = StrSql & "Position ='" & IIf(CmbJobDesc.Text = "Select...", "", CmbJobDesc.Text) & "',"
                    If StrPosition <> TxtPosition.Text Then
                        CmdGenAudit.Connection = Con
                        CmdGenAudit.CommandText = "Insert Into EmpMasterAudit Values('" & TxtEmpID.Text & "','Position change from ','" & StrPosition & "','" & TxtPosition.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                        CmdGenAudit.ExecuteNonQuery()
                    End If
                    StrSql = StrSql & "JobDesc ='" & IIf(CmbJobDesc.Text = "Select...", "", CmbJobDesc.Text) & "',"
                    If StrJobDesc <> CmbJobDesc.Text Then
                        CmdGenAudit.Connection = Con
                        CmdGenAudit.CommandText = "Insert Into EmpMasterAudit Values('" & TxtEmpID.Text & "','Job Description change from ','" & StrJobDesc & "','" & CmbJobDesc.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                        CmdGenAudit.ExecuteNonQuery()
                    End If
                    StrSql = StrSql & "PAYEScheme ='" & IIf(CmbPScheme.Text = "Select...", "", CmbPScheme.Text) & "',"
                    StrSql = StrSql & "PayePercent ='" & Val(TxtPPaid.Text) & "',"
                    StrSql = StrSql & "Local ='" & Val(TxtLLeave.Text) & "',"
                    StrSql = StrSql & "Sick ='" & Val(TxtSLeave.Text) & "',"
                    StrSql = StrSql & "EdfFactor ='" & Val(TxtEDFFactor.Text) & "',"
                    StrSql = StrSql & "TotEdf ='" & Val(TxtTotEdf.Text) & "',"
                    If StrTotEDF <> TxtTotEdf.Text Then
                        CmdGenAudit.Connection = Con
                        CmdGenAudit.CommandText = "Insert Into EmpMasterAudit Values('" & TxtEmpID.Text & "','EDF Amount change from ','" & StrTotEDF & "','" & TxtTotEdf.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                        CmdGenAudit.ExecuteNonQuery()
                    End If
                    StrSql = StrSql & "TravelMode='" & IIf(CmbTMode.Text = "Select...", "", CmbTMode.Text) & "',"
                    StrSql = StrSql & "TravelAllowance='" & Val(TxtTAllowance.Text) & "',"
                    If StrTravelAllow <> TxtTAllowance.Text Then
                        CmdGenAudit.Connection = Con
                        CmdGenAudit.CommandText = "Insert Into EmpMasterAudit Values('" & TxtEmpID.Text & "','Transport change from ','" & StrTravelAllow & "','" & TxtTAllowance.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                        CmdGenAudit.ExecuteNonQuery()
                    End If
                    StrSql = StrSql & "MedicalContrib='" & Val(TxtMedical.Text) & "',"
                    If StrFringe <> TxtMedical.Text Then
                        CmdGenAudit.Connection = Con
                        CmdGenAudit.CommandText = "Insert Into EmpMasterAudit Values('" & TxtEmpID.Text & "','Medical Amount change from ','" & StrFringe & "','" & TxtMedical.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                        CmdGenAudit.ExecuteNonQuery()
                    End If
                    StrSql = StrSql & "PerfAllow='" & Val(TxtPerBonus.Text) & "',"
                    If StrProd <> TxtPerBonus.Text Then
                        CmdGenAudit.Connection = Con
                        CmdGenAudit.CommandText = "Insert Into EmpMasterAudit Values('" & TxtEmpID.Text & "','Performance Allow Amount change from ','" & StrProd & "','" & TxtPerBonus.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                        CmdGenAudit.ExecuteNonQuery()
                    End If
                    StrSql = StrSql & "MaternityAllow='" & Val(TxtMaternityAllow.Text) & "',"
                    If StrMaternityAllow <> TxtMaternityAllow.Text Then
                        CmdGenAudit.Connection = Con
                        CmdGenAudit.CommandText = "Insert Into EmpMasterAudit Values('" & TxtEmpID.Text & "','Maternity Allow Amount change from ','" & StrMaternityAllow & "','" & TxtMaternityAllow.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                        CmdGenAudit.ExecuteNonQuery()
                    End If
                    StrSql = StrSql & "MealAllow='" & Val(TxtMealAllow.Text) & "',"
                    If StrMealAllow <> TxtMealAllow.Text Then
                        CmdGenAudit.Connection = Con
                        CmdGenAudit.CommandText = "Insert Into EmpMasterAudit Values('" & TxtEmpID.Text & "','Meal Allow Amount change from ','" & StrMealAllow & "','" & TxtMealAllow.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                        CmdGenAudit.ExecuteNonQuery()
                    End If
                    'Passage Benefit
                    StrSql = StrSql & "PassageBenefit='" & Val(TxtEmpPassBenefit.Text) & "',"
                    If StrEmpPassageBebefit <> TxtEmpPassBenefit.Text Then
                        CmdGenAudit.Connection = Con
                        CmdGenAudit.CommandText = "Insert Into EmpMasterAudit Values('" & TxtEmpID.Text & "','Passage Benefit Amount change from ','" & StrEmpPassageBebefit & "','" & TxtEmpPassBenefit.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                        CmdGenAudit.ExecuteNonQuery()
                    End If

                    StrSql = StrSql & "RespAllow='" & Val(TxtRespAllow.Text) & "',"
                    If StrRespAllow <> TxtRespAllow.Text Then
                        CmdGenAudit.Connection = Con
                        CmdGenAudit.CommandText = "Insert Into EmpMasterAudit Values('" & TxtEmpID.Text & "','Resp. Allow Amount change from ','" & StrRespAllow & "','" & TxtRespAllow.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                        CmdGenAudit.ExecuteNonQuery()
                    End If
                    StrSql = StrSql & "ModePay='" & IIf(CmbMOP.Text = "Select...", "", CmbMOP.Text) & "',"

                    StrSql = StrSql & "Status='" & IIf(CmbStatus.Text = "Select...", "", CmbStatus.Text) & "',"

                    If OptWorking.Checked Then
                        StrSql = StrSql & "Working = 1,"
                        StrSql = StrSql & "LastDay = Null,"
                    Else
                        Dim dtelastday As Date
                        dtelastday = CDate(TxtLWD.Text).ToString("dd-MMM-yyyy")
                        StrSql = StrSql & "Working = 0,"
                        'StrSql = StrSql & "LastDay = '" & Format(CDate(TxtLWD.Text), "dd-MMM-yyyy") & "',"   'TxtLWD.Text.ToString("yyyy-MM-dd")
                        StrSql = StrSql & "LastDay = '" & dtelastday.ToString("yyyy-MM-dd") & "',"
                    End If
                    If StrWorking <> OptWorking.Checked Then
                        CmdGenAudit.Connection = Con
                        CmdGenAudit.CommandText = "Insert Into EmpMasterAudit Values('" & TxtEmpID.Text & "','Working Status change from ','" & StrWorking & "','" & OptWorking.Checked & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                        CmdGenAudit.ExecuteNonQuery()
                    End If
                    'loan 
                    If ChkLoan1.Checked Then
                        Dim DteLoanFrom As Date
                        Dim DteLoanTo As Date
                        DteLoanFrom = CDate(TxtFrom1.Text).ToString("dd-MMM-yyyy")
                        DteLoanTo = CDate(TxtTo1.Text).ToString("dd-MMM-yyyy")
                        StrSql = StrSql & "Loan1 = 1,"
                        StrSql = StrSql & "AmtTaken1 ='" & Val(TxtLoanAmt1.Text) & "',"
                        StrSql = StrSql & "LoanFrom1 = '" & DteLoanFrom.ToString("yyyy-MM-dd") & "',"
                        StrSql = StrSql & "LoanTo1 = '" & DteLoanTo.ToString("yyyy-MM-dd") & "',"

                    Else
                        StrSql = StrSql & "Loan1 = 0,"
                        StrSql = StrSql & "AmtTaken1 ='0',"
                        StrSql = StrSql & "LoanFrom1 = Null,"
                        StrSql = StrSql & "LoanTo1 = Null,"
                    End If
                    If ChkLoan2.Checked Then
                        Dim DteLoanFrom1 As Date
                        Dim DteLoanTo1 As Date
                        DteLoanFrom1 = CDate(TxtFrom2.Text).ToString("dd-MMM-yyyy")
                        DteLoanTo1 = CDate(TxtTo2.Text).ToString("dd-MMM-yyyy")
                        StrSql = StrSql & "Loan2 = 1,"
                        StrSql = StrSql & "AmtTaken2 ='" & Val(TxtLoanAmt2.Text) & "',"
                        StrSql = StrSql & "LoanFrom2 = '" & DteLoanFrom1.ToString("yyyy-MM-dd") & "',"
                        StrSql = StrSql & "LoanTo2 = '" & DteLoanTo1.ToString("yyyy-MM-dd") & "',"
                    Else
                        StrSql = StrSql & "Loan2 = 0,"
                        StrSql = StrSql & "AmtTaken2 ='0',"
                        StrSql = StrSql & "LoanFrom2 = Null,"
                        StrSql = StrSql & "LoanTo2 = Null,"
                    End If
                    'End
                    If StrLastDay <> TxtLWD.Text Then
                        CmdGenAudit.Connection = Con
                        CmdGenAudit.CommandText = "Insert Into EmpMasterAudit Values('" & TxtEmpID.Text & "','Last Day Working on ','" & StrLastDay & "','" & TxtLWD.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                        CmdGenAudit.ExecuteNonQuery()
                    End If
                    If ChkAttendance.Checked Then
                        StrSql = StrSql & "AttBonus = 1,"
                    Else
                        StrSql = StrSql & "AttBonus = 0,"
                    End If
                    If StrAttBonus <> ChkAttendance.Checked Then
                        CmdGenAudit.Connection = Con
                        CmdGenAudit.CommandText = "Insert Into EmpMasterAudit Values('" & TxtEmpID.Text & "','Att. Bonus from ','" & StrAttBonus & "','" & ChkAttendance.Checked & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                        CmdGenAudit.ExecuteNonQuery()
                    End If
                    If ChkOTime.Checked Then
                        StrSql = StrSql & "OTPaid = 1,"
                    Else
                        StrSql = StrSql & "OTPaid = 0,"
                    End If
                    If StrOTPaid <> ChkOTime.Checked Then
                        CmdGenAudit.Connection = Con
                        CmdGenAudit.CommandText = "Insert Into EmpMasterAudit Values('" & TxtEmpID.Text & "','Overtime Paid from ','" & StrOTPaid & "','" & ChkOTime.Checked & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                        CmdGenAudit.ExecuteNonQuery()
                    End If
                    'Personal
                    StrSql = StrSql & "Address ='" & TxtAddress.Text & "',"
                    StrSql = StrSql & "City ='" & TxtCity.Text & "',"
                    StrSql = StrSql & "Country ='" & TxtCountry.Text & "',"
                    StrSql = StrSql & "HomePhone ='" & TxtPhone.Text & "',"
                    StrSql = StrSql & "HomeFax ='" & TxtFax.Text & "',"
                    StrSql = StrSql & "Mobile ='" & TxtMobile.Text & "',"
                    StrSql = StrSql & "EMail ='" & TxtEMail.Text & "',"
                    StrSql = StrSql & "CivilStatus ='" & CmbCivilStatus.Text & "',"

                    'Expatriate
                    If chkExpat.Checked Then
                        StrSql = StrSql & "Expatriate = 1,"
                    Else
                        StrSql = StrSql & "Expatriate = 0,"
                    End If

                    'Legal
                    StrSql = StrSql & "TaxAccNo ='" & TxtTaxAcc.Text & "',"
                    StrSql = StrSql & "BankID ='" & StrBankID & "',"
                    StrSql = StrSql & "AccountNo ='" & TxtBankAcc.Text & "',"
                    StrSql = StrSql & "ReportTo ='" & TxtReportTo.Text & "',"
                    If IsDate(TxtReviewDate.Text) Then
                        StrSql = StrSql & "ReviewDate ='" & Format(CDate(TxtReviewDate.Text), "dd-MMM-yyyy") & "',"
                    Else
                        StrSql = StrSql & "ReviewDate = Null,"
                    End If
                    StrSql = StrSql & "SSNo ='" & TxtNIC.Text & "',"
                    StrSql = StrSql & "PassPort ='" & TxtPassportNo.Text & "',"

                    'Recruited set to 0
                    StrSql = StrSql & "Recruit = 0,"

                    If OptNPSY.Checked Then
                        StrSql = StrSql & "NPSPaid = 1,"
                    Else
                        StrSql = StrSql & "NPSPaid = 0,"
                    End If
                    'Employee Pension
                    If ChkPension.Checked Then
                        StrSql = StrSql & "PensionEmpPaid = 1,"
                    Else
                        StrSql = StrSql & "PensionEmpPaid = 0,"
                    End If
                    StrSql = StrSql & "Extention ='" & TxtExtention.Text & "',"
                    StrSql = StrSql & "PerMail ='" & TxtPerEmail.Text & "',"

                    'Emergency Details
                    StrSql = StrSql & "contact1last ='" & Txtcontact1last.Text & "',"
                    StrSql = StrSql & "contact2last ='" & Txtcontact2last.Text & "',"
                    StrSql = StrSql & "contact1first ='" & txtcontact1first.Text & "',"
                    StrSql = StrSql & "contact2first ='" & txtcontact2first.Text & "',"
                    StrSql = StrSql & "contact1rel ='" & txtcontact1rel.Text & "',"
                    StrSql = StrSql & "contact2rel ='" & txtcontact2rel.Text & "',"
                    StrSql = StrSql & "contact1work ='" & txtcontact1work.Text & "',"
                    StrSql = StrSql & "contact2work ='" & txtcontact2work.Text & "',"
                    StrSql = StrSql & "contact1home ='" & txtcontact1home.Text & "',"
                    StrSql = StrSql & "contact2home ='" & txtcontact2home.Text & "',"
                    StrSql = StrSql & "contact1mobile ='" & txtcontact1mobile.Text & "',"
                    StrSql = StrSql & "contact2mobile ='" & txtcontact2mobile.Text & "',"
                    StrSql = StrSql & "takento ='" & txttakento.Text & "',"

                    StrSql = StrSql & "CarUse ='" & IIf(CmbComCar.Text = "Select...", "", CmbComCar.Text) & "',"
                    StrSql = StrSql & "CarCapacity ='" & TxtCapCar.Text & "',"
                    StrSql = StrSql & "CarAmt ='" & TxtAmount.Text & "',"
                    StrSql = StrSql & "CarEmpCont ='" & TxtEmpContributionC.Text & "',"
                    StrSql = StrSql & "PropertyType ='" & IIf(CmbPropType.Text = "Select...", "", CmbPropType.Text) & "',"
                    StrSql = StrSql & "HouseType ='" & IIf(CmbTOH.Text = "Select...", "", CmbTOH.Text) & "',"
                    StrSql = StrSql & "HousePercent ='" & TxtPercentage.Text & "',"
                    StrSql = StrSql & "HouseEmpCont ='" & TxtEmpContributionH.Text & "'"

                    StrSql = StrSql & " WHERE EmpID = '" & TxtEmpID.Text & "'"

                    Cmd.Connection = Con
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()

                    LblAccess.Visible = True
                    LblAccess.Text = "Data Saved!!!"

                    StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
                    Cmd.Connection = Con
                    Cmd.CommandText = StrSql
                    Rdr = Cmd.ExecuteReader
                    If Rdr.Read Then
                        If Rdr("Recruit") Then
                            TxtAuthorised.Text = "Athorised"
                        Else
                            TxtAuthorised.Text = "Not Athorised"
                        End If
                    End If
                    Rdr.Close()

                    'ClearAll()

                Catch ex As Exception
                    ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
                End Try

            Else
                LblAccess.Visible = True
                LblAccess.Text = "Invalid EmpID, cannot update details!!!"
                Exit Sub
            End If

        End If
        If e.Item.CommandName = "Delete" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "DELETE FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
                Cmd.ExecuteNonQuery()
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Next" Then
            Try
                LblAccess.Text = ""
                LblAccess.Visible = False
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID > '" & TxtEmpID.Text & "'"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try

        End If
        If e.Item.CommandName = "Previous" Then
            Try
                LblAccess.Text = ""
                LblAccess.Visible = False
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID < '" & TxtEmpID.Text & "' ORDER BY EmpID DESC"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try
        End If
        If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtEmpID.Text)
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
        End If
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged

        'set access level for each user  'Middle management, general admin and factory employees.
        Dim StrBankID As String = ""
        ButAuthorised.Visible = False
        'If Session("Level") = "Beau Bassin" Then
        '    StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "' And OutletName = 'Beau Bassin'"
        'ElseIf Session("Level") = "Ebene" Then
        '    StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "' And OutletName = 'Ebene'"
        'ElseIf Session("Level") = "Grand Baie" Then
        '    StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "' and OutletName = 'Grand Baie'"
        'End If
        If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
            StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
            ButAuthorised.Visible = True
        Else
            StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "' And OutletName = '" & Session("Level") & "'"
        End If

        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader

        LblAccess.Text = ""
        LblAccess.Visible = False

        ClearAll()
        If Rdr.Read Then
            On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
            TxtTitle.Text = Rdr("Title")
            TxtCategory.Text = Rdr("Category")
            TxtGender.Text = Rdr("Gender")
            TxtCategory.Text = Rdr("Category")
            ChkClocked.Checked = IIf(Rdr("Clocked"), True, False)
            chkExpat.Checked = IIf(Rdr("Expatriate"), True, False)
            TxtClockID.Text = Rdr("BatchNo")
            Session.Add("EmpID", Rdr("EmpID"))
            ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & Rdr("EmpID")
            TxtDOB.Text = Rdr("DOB")
            TxtHire.Text = Rdr("HireDate")
            TxtAge.Text = Rdr("Age")
            TxtFPid.Text = Rdr("FPid")
            CmbStatus.Text = Rdr("Status").ToString

            'Personal Details
            TxtBSalary.Text = Rdr("BasicSalary").ToString
            TxtPosition.Text = Rdr("Position").ToString
            CmbJobDesc.Text = Rdr("JobDesc").ToString
            CmbPScheme.Text = Rdr("PAYEScheme").ToString
            TxtPPaid.Text = Rdr("PayePercent").ToString
            TxtLLeave.Text = Rdr("Local").ToString
            TxtSLeave.Text = Rdr("Sick").ToString
            TxtEDFFactor.Text = Rdr("EdfFactor").ToString
            TxtTotEdf.Text = Rdr("TotEdf").ToString
            CmbTMode.Text = Rdr("TravelMode").ToString
            TxtTAllowance.Text = Rdr("TravelAllowance").ToString
            CmbMOP.Text = Rdr("ModePay").ToString
            TxtLWD.Text = Rdr("LastDay").ToString  'Format(Rdr("LastDay"), "dd/MM/yyyy")
            TxtMedical.Text = Rdr("MedicalContrib").ToString

            'loan
            TxtLoanAmt1.Text = Rdr("AmtTaken1").ToString
            TxtLoanAmt2.Text = Rdr("AmtTaken2").ToString
            TxtFrom1.Text = Rdr("LoanFrom1").ToString
            TxtFrom2.Text = Rdr("LoanFrom2").ToString
            TxtTo1.Text = Rdr("LoanTo1").ToString
            TxtTo2.Text = Rdr("LoanTo2").ToString
            If Rdr("Loan1") Then
                ChkLoan1.Checked = True
            Else
                ChkLoan1.Checked = False
            End If
            If Rdr("Loan2") Then
                ChkLoan2.Checked = True
            Else
                ChkLoan2.Checked = False
            End If

            TxtPerBonus.Text = Rdr("PerfAllow").ToString
            TxtMaternityAllow.Text = Rdr("MaternityAllow").ToString
            TxtRespAllow.Text = Rdr("RespAllow").ToString
            TxtEmpPassBenefit.Text = Rdr("PassageBenefit").ToString
            TxtMealAllow.Text = Rdr("MealAllow").ToString
            If Rdr("Working") Then
                OptWorking.Checked = True
                OptNotWorking.Checked = False
            Else
                OptNotWorking.Checked = True
                OptWorking.Checked = False
            End If
            If Rdr("AttBonus") Then
                ChkAttendance.Checked = True
            Else
                ChkAttendance.Checked = False
            End If
            If Rdr("OTPaid") Then
                ChkOTime.Checked = True
            Else
                ChkOTime.Checked = False
            End If
            If Rdr("PensionEmpPaid") Then
                ChkPension.Checked = True
            Else
                ChkPension.Checked = False
            End If
            If Rdr("Recruit") Then
                TxtAuthorised.Text = "Athorised"
            Else
                TxtAuthorised.Text = "Not Athorised"
            End If
            TxtAddress.Text = Rdr("Address").ToString
            TxtCity.Text = Rdr("City").ToString
            TxtCountry.Text = Rdr("Country").ToString
            TxtPhone.Text = Rdr("HomePhone").ToString
            TxtFax.Text = Rdr("HomeFax").ToString
            TxtMobile.Text = Rdr("Mobile").ToString
            TxtEMail.Text = Rdr("EMail").ToString
            CmbCivilStatus.Text = Rdr("CivilStatus").ToString
            TxtTaxAcc.Text = Rdr("TaxAccNo").ToString
            StrBankID = Rdr("BankID").ToString
            TxtBankAcc.Text = Rdr("AccountNo").ToString
            TxtReviewDate.Text = Rdr("ReviewDate").ToString  'Format(Rdr("ReviewDate"), "dd/MM/yyyy")
            TxtNIC.Text = Rdr("SSNo").ToString
            TxtPassportNo.Text = Rdr("PassPort").ToString
            TxtReportTo.Text = Rdr("ReportTo")
            If Rdr("NPSPaid") Then
                OptNPSY.Checked = True
                OptNPSN.Checked = False
            Else
                OptNPSN.Checked = True
                OptNPSY.Checked = False
            End If
            CmbComCar.Text = Rdr("CarUse").ToString
            TxtCapCar.Text = Rdr("CarCapacity").ToString
            TxtAmount.Text = Rdr("CarAmt").ToString
            TxtEmpContributionC.Text = Rdr("CarEmpCont").ToString
            CmbPropType.Text = Rdr("PropertyType").ToString
            CmbTOH.Text = Rdr("HouseType").ToString
            TxtPercentage.Text = Rdr("HousePercent").ToString
            TxtEmpContributionH.Text = Rdr("HouseEmpCont").ToString

            TxtExtention.Text = Rdr("Extention").ToString
            TxtPerEmail.Text = Rdr("PerMail").ToString

            'Emergency Details
            txtcontact1first.Text = Rdr("contact1first").ToString
            Txtcontact1last.Text = Rdr("contact1last").ToString
            txtcontact1home.Text = Rdr("contact1home").ToString
            txtcontact1mobile.Text = Rdr("contact1mobile").ToString
            txtcontact1rel.Text = Rdr("contact1rel").ToString
            txtcontact1work.Text = Rdr("contact1work").ToString
            txtcontact2first.Text = Rdr("contact2first").ToString
            Txtcontact2last.Text = Rdr("contact2last").ToString
            txtcontact2home.Text = Rdr("contact2home").ToString
            txtcontact2mobile.Text = Rdr("contact2mobile").ToString
            txtcontact2rel.Text = Rdr("contact2rel").ToString
            txtcontact2work.Text = Rdr("contact2work").ToString
            txttakento.Text = Rdr("takento").ToString

            FillGridQua()
            FillGridRef()
            FillGridExp()
            FillGridFilePath()

            Dim LastFinDetail As New HttpCookie("LastFinDetail")
            LastFinDetail.Values("LastTime") = Date.Now.ToShortTimeString()
            LastFinDetail.Values("JobDesc") = Rdr("JobDesc").ToString
            LastFinDetail.Values("Location") = Rdr("Location").ToString
            LastFinDetail.Values("BasicSalary") = Val(Rdr("BasicSalary"))
            LastFinDetail.Values("TravelAllowance") = Val(Rdr("TravelAllowance"))
            LastFinDetail.Values("Fringe") = Val(Rdr("Fringe"))
            LastFinDetail.Values("TravelMode") = Rdr("TravelMode").ToString
            LastFinDetail.Values("ModePay") = Rdr("ModePay").ToString
            LastFinDetail.Values("BankID") = Rdr("BankID").ToString
            LastFinDetail.Expires = Date.Now.AddDays(1)
            Response.Cookies.Add(LastFinDetail)

        Else
            Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(1))
            ToolBar1_ItemClick(Me, ev)
            'ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('Invalid Employee ID','Error');</script>", False)
        End If
        Rdr.Close()
        If StrBankID <> "" Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT Name FROM Bank WHERE BankID = '" & StrBankID & "'"
            CmbBank.Text = Cmd.ExecuteScalar
        End If
    End Sub
    Protected Sub FillGridFilePath()
        Dim ConPath As New OleDb.OleDbConnection
        Dim CmdPath As New OleDb.OleDbCommand
        Dim RdrPath As OleDb.OleDbDataReader
        CmdPath.Connection = Con
        StrSql = "SELECT * FROM UploadPath where EmpID = '" & TxtEmpID.Text & "'"
        CmdPath.CommandText = StrSql
        RdrPath = CmdPath.ExecuteReader
        GdvSavePath.DataSource = RdrPath
        GdvSavePath.DataBind()
        RdrPath.Close()
    End Sub


    Protected Sub FillGridExp()
        Dim ConExp As New OleDb.OleDbConnection
        Dim CmdExp As New OleDb.OleDbCommand
        Dim RdrExp As OleDb.OleDbDataReader
        CmdExp.Connection = Con
        StrSql = "SELECT * FROM Experience where EmpID = '" & TxtEmpID.Text & "' order by ExpFromDate"
        CmdExp.CommandText = StrSql
        RdrExp = CmdExp.ExecuteReader
        GdvExp.DataSource = RdrExp
        GdvExp.DataBind()
        RdrExp.Close()
    End Sub
    Protected Sub FillGridRef()
        Dim ConRef As New OleDb.OleDbConnection
        Dim CmdRef As New OleDb.OleDbCommand
        Dim RdrRef As OleDb.OleDbDataReader
        CmdRef.Connection = Con
        StrSql = "SELECT * FROM Reference where EmpID = '" & TxtEmpID.Text & "' order by RefDate"
        CmdRef.CommandText = StrSql
        RdrRef = CmdRef.ExecuteReader
        GdvRef.DataSource = RdrRef
        GdvRef.DataBind()
        RdrRef.Close()
    End Sub

    Protected Sub FillGridQua()
        Dim ConQua As New OleDb.OleDbConnection
        Dim CmdQau As New OleDb.OleDbCommand
        Dim RdrQua As OleDb.OleDbDataReader
        CmdQau.Connection = Con
        StrSql = "SELECT * FROM Qualifications where EmpID = '" & TxtEmpID.Text & "' order by QYrStart"
        CmdQau.CommandText = StrSql
        RdrQua = CmdQau.ExecuteReader
        GdvQua.DataSource = RdrQua
        GdvQua.DataBind()
        RdrQua.Close()
    End Sub
    Private Sub TxtCapCar_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtCapCar.TextChanged
        StrSql = "SELECT *From Car Where UseCar = '" & CmbComCar.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            If Val(TxtCapCar.Text) > Val(Rdr("From-1")) And Val(TxtCapCar.Text) <= Val(Rdr("To-1")) Then
                TxtAmount.Text = Rdr("Amount-1")
            ElseIf Val(TxtCapCar.Text) > Val(Rdr("From-2")) And Val(TxtCapCar.Text) <= Val(Rdr("To-2")) Then
                TxtAmount.Text = Rdr("Amount-2")
            ElseIf Val(TxtCapCar.Text) > Val(Rdr("From-3")) And Val(TxtCapCar.Text) <= Val(Rdr("To-3")) Then
                TxtAmount.Text = Rdr("Amount-3")
            ElseIf Val(TxtCapCar.Text) > Val(Rdr("From-4")) And Val(TxtCapCar.Text) <= Val(Rdr("To-4")) Then
                TxtAmount.Text = Rdr("Amount-4")
            ElseIf Val(TxtCapCar.Text) > Val(Rdr("From-5")) And Val(TxtCapCar.Text) <= Val(Rdr("To-5")) Then
                TxtAmount.Text = Rdr("Amount-5")
            End If
        End If
        Rdr.Close()
    End Sub


    ' ''    ''If FileUpload1.HasFile Then
    ' ''    ''    Dim tempFileName As String = (Guid.NewGuid().ToString("N") & "_") + FileUpload1.FileName
    ' ''    ''    Dim saveLocation As String = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings("FileUploadLocation"))
    ' ''    ''    Dim SavePath As String = IO.Path.Combine(saveLocation, tempFileName)
    ' ''    ''    Dim filetoread As String
    ' ''    ''    Try
    ' ''    ''        FileUpload1.SaveAs(SavePath)
    ' ''    ''        filetoread = IO.Path.Combine(saveLocation, tempFileName)
    ' ''    ''    Catch ex As Exception
    ' ''    ''    End Try
    ' ''    ''End If

    Protected Sub BtnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnView.Click
        Response.Clear()
        Try

            Dim file As New System.IO.FileInfo(TxtView.Text)
            Response.AddHeader("Content-Disposition", "attachment;filename=""" & TxtView.Text & """")
            Response.AddHeader("Content-Length", file.Length.ToString())
            Response.ContentType = file.Extension.ToLower()
            Response.WriteFile(file.FullName)
            Response.[End]()
        Catch

        End Try
    End Sub

    Protected Sub ButUpLoad_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButUpLoad.Click
        'If FileUpload1.HasFile Then
        'Dim tempFileName As String = (Guid.NewGuid().ToString("N") & "_") + FileUpload1.FileName
        Dim tempFileName As String = FileUpload1.FileName
        Dim saveLocation As String = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings("FileUploadLocation"))
        Dim SavePath As String = IO.Path.Combine(saveLocation, tempFileName)
        Dim filetoread As String
        Try
            'FileUpload1.SaveAs(SavePath)
            'filetoread = IO.Path.Combine(saveLocation, tempFileName)

            filetoread = Server.MapPath("~/TEST/" + tempFileName)
            FileUpload1.PostedFile.SaveAs(filetoread)
            Dim stream As FileStream = File.Open(filetoread, FileMode.Open, FileAccess.Read)

            Cmd.Connection = Con
            StrSql = "INSERT INTO UploadPath (EmpID,UploadPath) VALUES('"
            StrSql = StrSql & Trim(TxtEmpID.Text) & "','"
            StrSql = StrSql & filetoread & "')"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            FillGridFilePath()
        Catch ex As Exception
            LblAccess.Text = "error : " + ex.Message
            Exit Sub
        End Try
        'End If
    End Sub
    Private Sub GdvSavePath_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvSavePath.SelectedIndexChanged
        TxtView.Text = GdvSavePath.SelectedRow.Cells(1).Text.Replace("&nbsp;", "")
    End Sub

    Private Sub GdvSavePath_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvSavePath.SelectedIndexChanging
        GdvSavePath.SelectedIndex = e.NewSelectedIndex
    End Sub
    Private Sub GdvSavePath_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvSavePath.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvSavePath, "Select$" & e.Row.RowIndex)
        End If
    End Sub
    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvSavePath.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvSavePath.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub GdvSavePath_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GdvSavePath.RowEditing
        Dim strEmpID As Integer = GdvSavePath.Rows(e.NewEditIndex).Cells(2).Text
        Cmd.Connection = Con
        Cmd.CommandText = "Delete from UploadPath where RecNo = " & strEmpID & ""
        Cmd.ExecuteNonQuery()
        FillGridFilePath()
    End Sub

    Private Sub GdvExp_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GdvExp.RowEditing
        Dim strEmpID As String = TxtEmpID.Text

        Dim StrSelFormula As String
        Session("ReportFile") = "RExperience.rpt"
        StrSelFormula = "{Experience.EmpID}= '" & strEmpID & "'"

        Session("SelFormula") = StrSelFormula
        Session("RepHead") = "Experience Report for Employee " & strEmpID & ""
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
    End Sub
    Private Sub GdvRef_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GdvRef.RowEditing
        Dim strEmpID As String = TxtEmpID.Text

        Dim StrSelFormula As String
        Session("ReportFile") = "RReference.rpt"
        StrSelFormula = "{Reference.EmpID}= '" & strEmpID & "'"

        Session("SelFormula") = StrSelFormula
        Session("RepHead") = "Reference Report for Employee " & strEmpID & ""
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
    End Sub
    Private Sub GdvQua_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GdvQua.RowEditing
        Dim strEmpID As String = TxtEmpID.Text

        Dim StrSelFormula As String
        Session("ReportFile") = "RQualifications.rpt"
        StrSelFormula = "{Qualifications.EmpID}= '" & strEmpID & "'"

        Session("SelFormula") = StrSelFormula
        Session("RepHead") = "Qualification Report for Employee " & strEmpID & ""
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
    End Sub

    Protected Sub ButAuthorised_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButAuthorised.Click
        Dim CmdAth As New OleDb.OleDbCommand
        CmdAth.Connection = Con
        CmdAth.CommandText = "update Employee set Recruit = 1 where EmpID = '" & Trim(TxtEmpID.Text) & "'"
        CmdAth.ExecuteNonQuery()
        LblAccess.Visible = True
        LblAccess.Text = "Record has been Authorised."

        StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            If Rdr("Recruit") Then
                TxtAuthorised.Text = "Athorised"
            Else
                TxtAuthorised.Text = "Not Athorised"
            End If
        End If
        Rdr.Close()

        Dim StrAudit As String
        StrAudit = ""
        CmdAth.Connection = Con
        StrAudit = "Record Authorised for EmpID " & Trim(TxtEmpID.Text)
        CmdAth.CommandText = "Insert Into SalMasterAudit values('" & Session("UserID") & "','Authorised Record','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
        CmdAth.ExecuteNonQuery()
    End Sub

    Protected Sub ButList_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButList.Click
        Dim StrSelFormula As String
        Session("FieldCount") = 0
        Session("ReportFile") = "RUnauthorisedList.rpt"

        If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
            StrSelFormula = "{Employee.Recruit} = false"
        Else
            StrSelFormula = "{Employee.Recruit} = false and {Employee.OutletName} = '" & Trim(Session("Level")) & "'"
        End If

        Session("SelFormula") = StrSelFormula
        Session("RepHead") = "Unauthorised List of Employee"
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
    End Sub
End Class