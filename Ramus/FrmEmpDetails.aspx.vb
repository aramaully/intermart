﻿Public Partial Class FrmEmpDetails
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            CmbCategory.SelectedIndex = 0
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            Session("FieldCount") = 0
            Dim StrSelFormula As String
            StrSelFormula = "{Employee.Working} = True "
            If CmbCategory.Text <> "ALL" Then
                StrSelFormula = StrSelFormula & " And {Employee.Category} = '" & Microsoft.VisualBasic.Left(CmbCategory.Text, 1) & "'"
            End If
            Session("ReportFile") = "RptEmployeeDetails.rpt"
            Session("SelFormula") = StrSelFormula
            Session("RepHead") = "Employee Details"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        End If
    End Sub
End Class