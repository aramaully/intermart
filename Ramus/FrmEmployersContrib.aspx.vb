﻿Public Partial Class FrmEmployersContrib
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            TxtYear.Text = Date.Today.Year
            CmbMonth.SelectedIndex = Date.Today.Month - 1
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            Dim StrSelFormula As String
            Dim DtePayDate As Date = CDate("28-" + CmbMonth.Text + "-" + TxtYear.Text)
            Dim StrDate As String = Format(DtePayDate, "yyyy,MM,dd")

            If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
                StrSelFormula = "{Salary.Date}=date(" & StrDate & ")"
            Else
                StrSelFormula = "{Salary.Date}=date(" & StrDate & ") And {Salary.OutletName} = '" & Session("Level") & "'"
            End If


            'StrSelFormula = "{Salary.Date}=date(" & StrDate & ") "
            Session("ReportFile") = "RptEmployersContrib.rpt"
            Session("SelFormula") = StrSelFormula
            Session("RepHead") = "Employers Report for the month of " & CmbMonth.Text & ", " & TxtYear.Text
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        End If
    End Sub

End Class