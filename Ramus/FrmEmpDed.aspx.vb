﻿Public Partial Class FrmEmpDed
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            Cmd.CommandText = "Select *From ProvidentFund Order By ID"
            CmbProvidentFund.Items.Add("Select...")
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                CmbProvidentFund.Items.Add(Rdr("ID") & ", " & Rdr("Name"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ClearAll()
        TxtFirst.Text = ""
        TxtLast.Text = ""
        TxtClockID.Text = ""
        TxtCategory.Text = ""
        TxtTitle.Text = ""
        TxtGender.Text = ""
        ImgPhoto.ImageUrl = Nothing
        TxtCUSC.Text = ""
        TxtHPWH.Text = ""
        TxtMELD.Text = ""
        TxtMEID.Text = ""
        TxtMHCCD.Text = ""
        TxtMWWU.Text = ""
        ChkIBL.Checked = False
        TxtPEEU.Text = ""
        ChkIBL.Checked = False
        CmbProvidentFund.SelectedIndex = 0
        OptPFundY.Checked = True
        OptPFundN.Checked = False
        TxtCUR.Text = ""
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ev As New System.EventArgs
        If e.Item.CommandName = "New" Then
            ClearAll()
            TxtEmpID.Text = ""
            TxtEmpID.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = "UPDATE Employee SET "
            StrSql = StrSql & "CUSCont =" & Val(TxtCUSC.Text) & ","
            StrSql = StrSql & "HPWHouse =" & Val(TxtHPWH.Text) & ","
            StrSql = StrSql & "MELDeduct =" & Val(TxtMELD.Text) & ","
            StrSql = StrSql & "MEIDeduct =" & Val(TxtMEID.Text) & ","
            StrSql = StrSql & "MHCDeduct =" & Val(TxtMHCCD.Text) & ","
            StrSql = StrSql & "MtiusDeduct =" & Val(TxtMWWU.Text) & ","
            StrSql = StrSql & "PrivateEnt =" & Val(TxtPEEU.Text) & ","
            If CmbProvidentFund.Text = "Select..." Then
                StrSql = StrSql & "ProvidentFund ='',"
            Else
                StrSql = StrSql & "ProvidentFund ='" & Mid(CmbProvidentFund.Text, 1, InStr(CmbProvidentFund.Text, ",") - 1) & "',"
            End If
            StrSql = StrSql & "CreditUnionRef =" & Val(TxtCUR.Text) & ","

            If OptPFundY.Checked Then
                StrSql = StrSql & "PFYesNo = 1,"
            Else
                StrSql = StrSql & "PFYesNo = 0,"
            End If
            If ChkTEWF.Checked Then
                StrSql = StrSql & "TEWF = 1,"
            Else
                StrSql = StrSql & "TEWF = 0,"
            End If
            If ChkIBL.Checked Then
                StrSql = StrSql & "PCAScheme = 1"
            Else
                StrSql = StrSql & "PCAScheme = 0"
            End If
            StrSql = StrSql & "WHERE EmpID = '" & TxtEmpID.Text & "'"
            Try
                Cmd.Connection = Con
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "DELETE FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
                Cmd.ExecuteNonQuery()
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Next" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID > '" & TxtEmpID.Text & "'"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try

        End If
        If e.Item.CommandName = "Previous" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID < '" & TxtEmpID.Text & "' ORDER BY EmpID DESC"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try
        End If
        If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtEmpID.Text)
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
        End If
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        If Rdr.Read Then
            On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
            TxtTitle.Text = Rdr("Title")
            TxtCategory.Text = Rdr("Category")
            TxtGender.Text = Rdr("Gender")
            TxtCategory.Text = Rdr("Category")
            ChkClocked.Checked = IIf(Rdr("Clocked"), True, False)
            TxtClockID.Text = Rdr("BatchNo")
            Session.Add("EmpID", Rdr("EmpID"))
            ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & Rdr("EmpID")
            'Personal Details
            TxtCUSC.Text = Rdr("CUSCont").ToString
            TxtHPWH.Text = Rdr("HPWHouse").ToString
            TxtMELD.Text = Rdr("MELDeduct").ToString
            TxtMEID.Text = Rdr("MEIDeduct").ToString
            TxtMHCCD.Text = Rdr("MHCDeduct").ToString
            TxtMWWU.Text = Rdr("MtiusDeduct").ToString
            TxtPEEU.Text = Rdr("PrivateEnt").ToString
            CmbProvidentFund.Text = Rdr("ProvidentFund").ToString
            If Rdr("PFYesNo") Then
                OptPFundY.Checked = True
                OptPFundN.Checked = False
            Else
                OptPFundN.Checked = True
                OptPFundY.Checked = False
            End If
            If Rdr("TEWF") Then
                ChkTEWF.Checked = True
            Else
                ChkTEWF.Checked = False
            End If
            If Rdr("PCAScheme") Then
                ChkIBL.Checked = True
            Else
                ChkIBL.Checked = False
            End If
            TxtCUR.Text = Rdr("CreditUnionRef").ToString
        Else
            Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(1))
            ToolBar1_ItemClick(Me, ev)
            'ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('Invalid Employee ID','Error');</script>", False)
        End If
        Rdr.Close()
    End Sub

End Class