﻿Public Partial Class FrmAddUser
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            FillGrid()
            'CmbLevel.Items.Clear()
            'CmbLevel.Items.Add("Select...")
            'CmbLevel.Items.Add("Administrator")
            'CmbLevel.Items.Add("Beau Bassin")
            'CmbLevel.Items.Add("Ebene")
            'CmbLevel.Items.Add("Grand Baie")
        End If
        'Department
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT DeptName FROM Dept"
        CmbDepartment.Items.Add("ALL")
        Rdr = Cmd.ExecuteReader
        While Rdr.Read
            CmbDepartment.Items.Add(Rdr("DeptName"))
        End While
        Rdr.Close()
        'Outlet
        Cmd.CommandText = "SELECT OutletName FROM Outlet"
        CmbLevel.Items.Add("Administrator")
        Rdr = Cmd.ExecuteReader
        While Rdr.Read
            CmbLevel.Items.Add(Rdr("OutletName"))
        End While
        Rdr.Close()
    End Sub

    Protected Sub FillGrid()
        Cmd.Connection = Con
        StrSql = "SELECT * FROM [User]"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvUser.DataSource = Rdr
        GdvUser.DataBind()
        Rdr.Close()
        GdvUser.Columns(2).Visible = True
        GdvUser.Columns(3).Visible = True
        GdvUser.Columns(4).Visible = True
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim Ev As New System.EventArgs

        If e.Item.CommandName = "New" Then
            TxtUserID.Text = ""
            CmbLevel.SelectedIndex = -1
            TxtEmpID.Text = ""
            CmbDepartment.SelectedIndex = 0
            TxtPassword.Text = ""
            TxtReEnter.Text = ""
            TxtEmpName.Text = ""
            TxtUserID.Focus()
        End If
        If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtEmpID.Text)
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
        End If
        If e.Item.CommandName = "Save" Then
            Dim Trans As OleDb.OleDbTransaction
            Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
            Cmd.Connection = Con
            Cmd.Transaction = Trans
            Cmd.CommandText = "DELETE FROM [User] WHERE UserID = '" & TxtUserID.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO [User] (UserID,Name,Level,EmpID,Password,DeptCode) VALUES('"
                StrSql = StrSql & TxtUserID.Text & "','"
                StrSql = StrSql & TxtEmpName.Text & "','"
                StrSql = StrSql & CmbLevel.Text & "','"
                StrSql = StrSql & TxtEmpID.Text & "','"
                StrSql = StrSql & TxtPassword.Text & "','"
                StrSql = StrSql & CmbDepartment.Text & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                Trans.Commit()
                FillGrid()
                'Audit
                Dim CmdAudit As New OleDb.OleDbCommand
                Dim StrAudit As String
                CmdAudit.Connection = Con
                StrAudit = "New User " & TxtUserID.Text & " Created "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','New User','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()

                TxtUserID.Text = ""
                CmbLevel.SelectedIndex = -1
                TxtEmpID.Text = ""
                TxtEmpName.Text = ""
                CmbDepartment.SelectedIndex = 0
                TxtPassword.Text = ""
                TxtReEnter.Text = ""
                TxtUserID.Focus()
            Catch ex As Exception
                Trans.Rollback()
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM [User] WHERE UserID = '" & TxtUserID.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                FillGrid()
                TxtUserID.Text = ""
                CmbLevel.SelectedIndex = -1
                TxtEmpID.Text = ""
                CmbDepartment.SelectedIndex = 0
                TxtPassword.Text = ""
                TxtReEnter.Text = ""
                TxtEmpName.Text = ""
                TxtUserID.Focus()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
    End Sub

    Private Sub GdvUser_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvUser.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvUser, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvUser_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvUser.SelectedIndexChanged
        TxtUserID.Text = GdvUser.SelectedRow.Cells(0).Text
        CmbLevel.Text = GdvUser.SelectedRow.Cells(4).Text
        TxtPassword.Attributes.Add("value", GdvUser.SelectedRow.Cells(2).Text)
        TxtReEnter.Attributes.Add("value", GdvUser.SelectedRow.Cells(2).Text)
        TxtEmpID.Text = GdvUser.SelectedRow.Cells(3).Text
        If GdvUser.SelectedRow.Cells(4).Text <> "&nbsp;" Then
            CmbLevel.Text = GdvUser.SelectedRow.Cells(4).Text
        End If
        Dim ev As New System.EventArgs
        TxtEmpID_TextChanged(Me, ev)
    End Sub

    Private Sub GdvUser_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvUser.SelectedIndexChanging
        GdvUser.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvUser.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvUser.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Protected Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtEmpID.TextChanged
        StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            On Error Resume Next
            TxtEmpName.Text = Rdr("Last") & " " & Rdr("First")
        End If
        Rdr.Close()
    End Sub
End Class