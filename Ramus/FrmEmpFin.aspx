﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmEmpFin.aspx.vb" Inherits="Ramus.FrmEmpFin" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Edit Employee Financial Data</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>    
    <script language="javascript" type="text/javascript">
        function getFile() {
            document.getElementById("file1").click();

            var file = "";
            document.getElementById("TxtView").value = ""
            if (document.getElementById("TxtView").value == "")
                file = document.getElementById("file1").value;
            else
                file = document.getElementById("TxtView").value + "\r\n" + document.getElementById("file1").value;
            document.getElementById("TxtView").value = file;
        }
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.
            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Edit Employee Financial Data'; //Set the name
            w.Width = 1200; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 560;            
            w.Top = 10;
            w.Left = 10;
            w.Show(); //Show the window
        };
        function ShowFind() {
            var FindWindow = window.open("FindEmployee.aspx", "FindEmployee", "width=565,height=435,top=200, left=575");
        };
        function FunctionConfirm(Tlb, TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Save") {
                Index = '2';
                w.Confirm("Sure to Save", "Confirm", null, SaveFunction);
            }
            else if (TlbCmd == "Find") {
                __doPostBack('ToolBar1', '1');
            }
            else if (TlbCmd == "New") {
                __doPostBack('ToolBar1', '0');
            }
            else if (TlbCmd == "Next") {
                __doPostBack('ToolBar1', '5');
            }
            else if (TlbCmd == "Previous") {
                __doPostBack('ToolBar1', '4');
            }
        }
        function SaveFunction(Sender, RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        }
        function FunPic() {
            var myArgs = 'nothing';
            myargs = window.showModalDialog('Snap.aspx', myArgs, "dialogHeight:350px;dialogWidth:650px");
            if (myArgs == 'nothing') {
                PageMethods.AjaxSetSession("True");
            }
        }
        function ShowReport() {
            wfind = window.frameElement.IDCWindow;
            wfind.CreateWindow('FrmRpt.aspx', w);
            return false;
        };
    </script>

    <style type="text/css">
        .style1
        {
            width: 99%;
            height: 112px;
        }
        .style22
        {
            width: 111px;
        }
        .style23
        {
            height: 26px;
        }
        </style>
</head>
<body onload="init();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="conditional">
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="BtnView" />
<asp:PostBackTrigger ControlID="ButUpload"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ButUpload"></asp:PostBackTrigger>
                                    <asp:PostBackTrigger ControlID="ButUpload" />
                                    <asp:PostBackTrigger ControlID="ButUpload" />
                                    <asp:PostBackTrigger ControlID="ButUpload" />
<asp:PostBackTrigger ControlID="ButUpload"></asp:PostBackTrigger>
                                </Triggers>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="ButUpload" />
                                </Triggers> 
                                <ContentTemplate>
                                <td class="style24">
                                    </td>
                                    <td class="style25">
                                    <asp:Button ID="BtnOpen" runat="server" Text="Open" Width="69px" Visible="False"  />
                                    </td>
                                    <td class="style26">
                                    <input id="file1" width: 1px; visibility : hidden; type="file" />
                                    </td>
                                    <td class="style27">
                                    <asp:Button ID="BtnView" runat="server" Text="View" Width="69px"  />
                                    </td>
                                    <asp:FileUpload ID="FileUpload1" runat="server" BorderStyle="Inset" 
                                        BorderWidth="1px" ToolTip="Select the File to Upload" Width="228px" 
                                        Height="21px" />
                                    <asp:Button ID="ButUpLoad" runat="server" Text="Upload" Width="78px" />
                                  
                                </ContentTemplate>
                            </asp:UpdatePanel>                                             
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

        <ContentTemplate>
            <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px;
                height: 32px; width: 32px;" runat="server" BorderStyle="None" Visible="True">
                <div id="divImage" style="display: none">
                    <asp:Image ID="img1" runat="server" ImageUrl="/Images/Wait.gif" />
                </div>
            </asp:Panel>
            <div id="toolbar" style="width: 100%;">
                <eo:ToolBar ID="ToolBar1" runat="server" BackgroundImage="00100103" ClientSideOnItemClick="FunctionConfirm"
                    BackgroundImageLeft="00100101" BackgroundImageRight="00100102" SeparatorImage="00100104"
                    Width="100%">
                    <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 1px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: #335ea8 1px solid; background-color: #99afd4; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <Items>
                        <eo:ToolBarItem CommandName="New" ImageUrl="00101001" ToolTip="New">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Find" ImageUrl="~/images/findHS.png" ToolTip="Find Employee">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Save" ImageUrl="00101003" ToolTip="Save">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Previous" ImageUrl="~/images/NavBack.png" ToolTip="Previous Record">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Next" ImageUrl="~/images/NavForward.png" ToolTip="Next Record">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Help" ImageUrl="~/images/help.png" ToolTip="Help">
                        </eo:ToolBarItem>
                    </Items>
                    <ItemTemplates>
                        <eo:ToolBarItem Type="Custom">
                            <NormalStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <HoverStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <DownStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="DropDownMenu">
                            <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                            <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; background-image: url(00100106); background-position-x: right;" />
                            <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: none; background-color:transparent; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                        </eo:ToolBarItem>
                    </ItemTemplates>
                    <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                </eo:ToolBar>
            </div>
            <div id="Content" style="width: 100%; height: 550px; padding: 2px;
                border-right: solid 1px #cddaea;">                
            <asp:Panel ID="Panel1" runat="server" BorderStyle="Inset" BorderWidth="1" 
                    Height="554px">
                <table class="style1">
                    <tr>
                        <td>
                            Emp ID
                        </td>
                        <td>
                            <asp:TextBox ID="TxtEmpID" runat="server" AutoPostBack="True"></asp:TextBox>
                        </td>
                        <td>
                            Title
                        </td>
                        <td>
                            <asp:TextBox ID="TxtTitle" runat="server" Width="50px" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td class="style22">
                            Gender
                        </td>
                        <td>
                            <asp:TextBox ID="TxtGender" runat="server" Width="50px" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td align="right" rowspan="7" valign="top">
                            <asp:Image ID="ImgPhoto" runat="server" Height="97px" Width="87px" />
                        </td>
                    </tr>
                    <tr>
                        <td>Clocked</td>
                        <td>
                            <asp:CheckBox ID="ChkClocked" runat="server" Text="Yes" Enabled="False" />
                        </td>
                        <td>
                            Clock ID</td>
                        <td>
                            <asp:TextBox ID="TxtClockID" runat="server" Width="100px"></asp:TextBox>
                        </td>
                        <td class="style22">
                            Category</td>
                        <td>
                            <asp:TextBox ID="TxtCategory" runat="server" Width="100px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            DOB</td>
                        <td>
                            <asp:TextBox ID="TxtDOB" runat="server" Height="25px" ReadOnly="True" 
                                Width="126px"></asp:TextBox>
                        </td>
                        <td>
                            FP ID</td>
                        <td>
                            <asp:TextBox ID="TxtFPid" runat="server" Height="23px" ReadOnly="True" 
                                Width="99px"></asp:TextBox>
                        </td>
                        <td class="style22">
                            Marital Status</td>
                        <td>
                            <asp:DropDownList ID="CmbStatus" runat="server" Height="16px" Width="100px">
                                <asp:ListItem>Select...</asp:ListItem>
                                <asp:ListItem>Single</asp:ListItem>
                                <asp:ListItem>Married</asp:ListItem>
                                <asp:ListItem>Divorced</asp:ListItem>
                                <asp:ListItem>Widowed</asp:ListItem>
                                <asp:ListItem>Other</asp:ListItem>
                            </asp:DropDownList>
                            </td>
                     </tr>
                    <tr>
                        <td>Hire Date</td>
                        <td>
                            <asp:TextBox ID="TxtHire" runat="server" Height="24px" ReadOnly="True" 
                                Width="125px"></asp:TextBox>
                        </td>
                        <td>Age</td>
                        <td>
                            <asp:TextBox ID="TxtAge" runat="server" ReadOnly="True" Width="50px"></asp:TextBox>
                        </td>
                        <td class="style22">
                            <asp:Button ID="ButAuthorised" runat="server" Text="Authorised" visible = "False" Width="78px" />
                        </td>
                        <td>
                            <asp:TextBox ID="TxtAuthorised" runat="server" Enabled = "False" Width="150px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style23">Last Name</td>
                        <td colspan="4" class="style23">
                            <asp:TextBox ID="TxtLast" runat="server" Width="400px" ReadOnly="True"></asp:TextBox>
                            &nbsp;
                        </td>
                        <td class="style23">
                            &nbsp;
                            <asp:Button ID="ButList" runat="server" Text="UnAuthorised List" 
                                Width="140px" />
                        </td>                        
                    </tr>
                    <tr>
                        <td>First Name</td>
                        <td>
                            <asp:TextBox ID="TxtFirst" runat="server" Width="400px" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                            <asp:Label ID="LblAccess" runat="server" ForeColor="Red" Visible="False"></asp:Label>
                        </td>s
                        <td>&nbsp;</td>
                        <td>Expatriate</td>
                        <td><asp:CheckBox ID="chkExpat" runat="server" Text="Yes" /></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="4">
                            <asp:TextBox ID="TxtView" runat="server" AutoPostBack="True" Height="23px" 
                                Width="398px"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="2" 
                    Height="308px" Width="100%" style="margin-top: 0px">
                    <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="General Information"><HeaderTemplate>Personal</HeaderTemplate><ContentTemplate><asp:Panel ID="PnlContent" runat="server" BorderStyle="Inset" BorderWidth="1px" 
                                Height="323px"><table class="style1"><tr><td colspan="2"><asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Small" 
                                    ForeColor="#0033CC" Text="Personnel Details"></asp:Label></td></tr><tr><td class="style24">Address<td class="style2" colspan="3"><asp:TextBox ID="TxtAddress" runat="server" Width="305px" Height="22px"></asp:TextBox></td><td class="style35">&#160;</td><td class="style28">&#160;</td><td </td=""></td></td></tr><tr><td class="style29">City</td><td class="style25"><asp:TextBox ID="TxtCity" runat="server" Width="305px"></asp:TextBox></td><td class="style23">Country</td><td class="style30"><asp:TextBox ID="TxtCountry" runat="server" Width="185px"></asp:TextBox></td><td class="style36">Position</td><td class="style23"><asp:TextBox ID="TxtPosition" runat="server" Width="210px"></asp:TextBox></td><td class="style23"></td></tr><tr><td class="style24">Phone</td><td class="style25"><asp:TextBox ID="TxtPhone" runat="server" Width="200px" Height="24px"></asp:TextBox></td><td class="style31">Fax</td><td class="style32"><asp:TextBox ID="TxtFax" runat="server" Width="185px"></asp:TextBox></td><td class="style35">Report To</td><td class="style34"><asp:TextBox ID="TxtReportTo" runat="server" Width="210px"></asp:TextBox></td><td class="style33"></td></tr><tr><td class="style24">Mobile</td><td class="style25"><asp:TextBox ID="TxtMobile" runat="server" Width="200px"></asp:TextBox></td><td class="style37">Work Email</td><td class="style32"><asp:TextBox ID="TxtEMail" runat="server" Width="185px" Height="21px"></asp:TextBox></td><td class="style36">&#160;</td><td class="style39">&#160;</td><td class="style23"></td></tr><tr><td class="style24">Phone Extension</td><td class="style25"><asp:TextBox ID="TxtExtention" runat="server" Width="200px"></asp:TextBox></td><td class="style27">Personal Email</td><td class="style30"><asp:TextBox ID="TxtPerEmail" runat="server" Width="185px"></asp:TextBox></td><td class="style35">Job Desc.</td><td><asp:DropDownList ID="CmbJobDesc" runat="server" AutoPostBack="True" 
                                    Height="22px" Width="142px"></asp:DropDownList></td></tr><tr><td class="style24">&#160;</td><td class="style25">&#160;</td><td>&#160;</td><td class="style32">&#160;</td><td class="style35"></td><td></td><tr><td class="style24">Marital Status</td><td class="style25"><asp:DropDownList ID="CmbCivilStatus" runat="server" Height="18px" 
                                        ValidationGroup="Group1" Width="200px"><asp:ListItem>Select...</asp:ListItem><asp:ListItem>Married</asp:ListItem><asp:ListItem>Single</asp:ListItem><asp:ListItem>Widow</asp:ListItem><asp:ListItem>Divorced</asp:ListItem><asp:ListItem>Others</asp:ListItem></asp:DropDownList></td><td>&#160;</td><td class="style32">&#160;</td><td class="style35"></td><td></td></tr></tr></table></asp:Panel></ContentTemplate></cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="Financial"><HeaderTemplate>Financial</HeaderTemplate><ContentTemplate><table class="style1"><tr><td class="style7">Basic Salary</td><td class="style9"><asp:TextBox ID="TxtDCode" runat="server" Visible="False" Width="25px">13</asp:TextBox></td><td class="style3"><asp:TextBox ID="TxtBSalary" runat="server" AutoPostBack="True"></asp:TextBox></td><td class="style13">PAYE Scheme</td><td class="style5"><asp:DropDownList ID="CmbPScheme" runat="server"></asp:DropDownList></td><td class="style23">PAYE Paid</td><td><asp:TextBox ID="TxtPPaid" runat="server" AutoPostBack="True"></asp:TextBox>%</td></tr><tr><td class="style7">Local Leave</td><td class="style9">&nbsp;</td><td class="style3"><asp:TextBox ID="TxtLLeave" runat="server" AutoPostBack="True" 
                            Width="50px"></asp:TextBox>Per Year</td><td class="style2">Sick Leave</td><td class="style5"><asp:TextBox ID="TxtSLeave" runat="server" AutoPostBack="True" Width="50px"></asp:TextBox>Per Year</td><td class="style23">Travel Allowance</td><td><asp:TextBox ID="TxtTAllowance" runat="server" AutoPostBack="True"></asp:TextBox></td></tr><tr><td class="style7">No of Month/EDF Value</td><td class="style9"><asp:TextBox ID="TxtEDFFactor" runat="server" Width="25px">13</asp:TextBox></td><td class="style3"><asp:TextBox ID="TxtTotEdf" runat="server" Width="127px" 
                            style="margin-left: 0px"></asp:TextBox></td><td class="style2"><asp:Label ID="Label281" runat="server" ForeColor="#FF3300" Text="Medical"></asp:Label></td><td class="style5"><asp:TextBox ID="TxtMedical" runat="server" 
                            AutoPostBack="True" Width="50px"></asp:TextBox></td><td 
                    class="style23">Travel Mode</td><td><asp:DropDownList ID="CmbTMode" runat="server" Width="100px"></asp:DropDownList></td></tr><tr><td class="style7">Attendance Allowance</td><td class="style10">&nbsp;</td><td class="style3"><asp:CheckBox ID="ChkAttendance" runat="server" Text="Yes" /></td><td class="style13">Performance Allow</td><td class="style5"><asp:TextBox ID="TxtPerBonus" runat="server" AutoPostBack="True" 
                            Width="50px"></asp:TextBox></td><td class="style23">Mode of Payment</td><td><asp:DropDownList ID="CmbMOP" runat="server" Width="100px"></asp:DropDownList></td></tr><tr><td class="style7">Overtime Paid</td><td class="style9">&#160;</td><td class="style3"><asp:CheckBox ID="ChkOTime" 
                                runat="server" Text="Paid" /></td><td class="style2">Maternity Allow</td><td class="style5"><asp:TextBox ID="TxtMaternityAllow" runat="server" 
                            AutoPostBack="True" Width="50px"></asp:TextBox></td><td class="style23">Working</td><td><asp:RadioButton ID="OptWorking" runat="server" Checked="True" 
                            GroupName="Working" Text="Yes" /><asp:RadioButton ID="OptNotWorking" runat="server" GroupName="Working" 
                            Text="No" /></td></tr><tr><td class="style7">Pension</td><td class="style3"><asp:CheckBox ID="ChkPension" runat="server" Text="Paid" /></td><td class="style5"><asp:TextBox ID="TxtMealAllow" runat="server" 
                            AutoPostBack="True" Width="50px"></asp:TextBox>Meal Allow</td><td class="style13">Resp. Allow</td><td class="style5"><asp:TextBox ID="TxtRespAllow" runat="server" 
                            AutoPostBack="True" Width="50px"></asp:TextBox></td><td class="style23">Last Working Day</td><td><asp:TextBox ID="TxtLWD" runat="server"></asp:TextBox><cc1:CalendarExtender ID="TxtLWD_CalendarExtender" runat="server" 
                            Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtLWD"></cc1:CalendarExtender></td></tr><tr><td class="style7">Loan 1</td><td class="style9"><asp:CheckBox ID="ChkLoan1" runat="server" Text="Paid" /></td><td class="style3">From 1</td><td class="style13"><asp:TextBox ID="TxtFrom1" runat="server" Width="80px"></asp:TextBox><cc1:CalendarExtender ID="TxtFrom1_CalendarExtender" runat="server" 
                            Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtFrom1"></cc1:CalendarExtender></td><td class="style5">To 1</td><td class="style23"><asp:TextBox ID="TxtTo1" runat="server" Width="80px"></asp:TextBox><cc1:CalendarExtender ID="TxtTo1_CalendarExtender" runat="server" 
                            Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtTo1"></cc1:CalendarExtender></td><td><asp:TextBox ID="TxtLoanAmt1" runat="server" AutoPostBack="True" Width="70px"></asp:TextBox></td></tr><tr><td class="style7">Loan 2</td><td class="style9"><asp:CheckBox ID="ChkLoan2" runat="server" Text="Paid" /></td><td class="style3">From 2</td><td class="style13"><asp:TextBox ID="TxtFrom2" runat="server" Width="80px"></asp:TextBox><cc1:CalendarExtender ID="TxtFrom2_CalendarExtender" runat="server" 
                            Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtFrom2"></cc1:CalendarExtender></td><td class="style5">To 2</td><td class="style23"><asp:TextBox ID="TxtTo2" runat="server" Width="80px"></asp:TextBox><cc1:CalendarExtender ID="TxtTo2_CalendarExtender" runat="server" 
                            Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtTo2"></cc1:CalendarExtender></td><td><asp:TextBox ID="TxtLoanAmt2" runat="server" AutoPostBack="True" Width="70px"></asp:TextBox></td></tr></table></ContentTemplate></cc1:TabPanel><cc1:TabPanel ID="TabPanel3" runat="server" HeaderText="Leave / Working Hours"><HeaderTemplate>Legal</HeaderTemplate><ContentTemplate><asp:Panel ID="Panel2" runat="server" BorderStyle="Inset" BorderWidth="1px"><table class="style1" cellpadding="0"><tr><td class="style5">Bank&#160; </td><td colspan="3"><asp:DropDownList ID="CmbBank" runat="server" Width="70%" Height="48px"></asp:DropDownList></td></tr><tr><td colspan="4"><asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="#0033CC" Text="Legal Details"></asp:Label></td></tr><tr><td class="style5">Tax Account No.</td><td class="style3"><asp:TextBox ID="TxtTaxAcc" runat="server" style="margin-right: 34px" Width="211px"></asp:TextBox></td><td class="style4">Review Date</td><td><asp:TextBox ID="TxtReviewDate" runat="server" Width="211px"></asp:TextBox><cc1:CalendarExtender ID="TxtReviewDate_CalendarExtender" runat="server" 
                                    DaysModeTitleFormat="dd/MM/yyyy" Enabled="True" TargetControlID="TxtReviewDate"></cc1:CalendarExtender></td></tr><tr><td class="style5">Bank A/c No.</td><td class="style3"><asp:TextBox ID="TxtBankAcc" runat="server" Width="211px"></asp:TextBox></td><td class="style4">&#160;</td><td>&#160;</td></tr><tr><td class="style4">NIC No.</td><td><asp:TextBox ID="TxtNIC" runat="server" Width="211px"></asp:TextBox></td></tr><tr><td class="style5">&#160;</td><td class="style3"><asp:TextBox ID="TxtPassportNo" runat="server" Width="211px" Visible="False"></asp:TextBox></td><td class="style4">NPS</td><td><asp:RadioButton ID="OptNPSY" runat="server" Checked="True" GroupName="NPS" Text="Paid" /><asp:RadioButton ID="OptNPSN" runat="server" GroupName="NPS" Text="Not Paid" /></td></tr><tr><td class="style5"></td></tr><caption>&#160;<tr><td class="style3">Passage Benefit</td><td class="style4"><asp:TextBox ID="TxtEmpPassBenefit" runat="server" style="margin-right: 34px" Width="211px"></asp:TextBox></td><td>&#160;</td></tr></caption></table></asp:Panel></ContentTemplate></cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel4" runat="server" HeaderText="Payroll Bank Account"><HeaderTemplate>Fringe Benefits</HeaderTemplate><ContentTemplate><asp:Panel ID="Panel3" runat="server" BorderStyle="Inset" BorderWidth="1px"><table class="style1" cellpadding="0"><tr><td colspan="2"><asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="#0033CC" Text="Fringe Benefits Details"></asp:Label></td></tr><tr><td colspan="2">&#160;</td></tr><tr><td><asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Size="X-Small" Text="Car Benefits"></asp:Label></td><td><asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Overline="False" Font-Size="X-Small" ForeColor="Black" Text="Housing Benefits"></asp:Label></td></tr><tr><td><table class="style1"><tr><td>Company Car</td><td><asp:DropDownList ID="CmbComCar" runat="server"></asp:DropDownList></td></tr><tr><td>Capacity Of Car</td><td><asp:TextBox ID="TxtCapCar" runat="server" AutoPostBack="True"></asp:TextBox></td></tr><tr><td>Amount</td><td><asp:TextBox ID="TxtAmount" runat="server"></asp:TextBox></td></tr><tr><td>Employee Contribution</td><td><asp:TextBox ID="TxtEmpContributionC" runat="server"></asp:TextBox></td></tr></table></td><td><table class="style1"><tr><td>Property Type</td><td><asp:DropDownList ID="CmbPropType" runat="server"><asp:ListItem Text="Select..."></asp:ListItem><asp:ListItem Text="Owned by Employer"></asp:ListItem><asp:ListItem Text="Rented by Employer"></asp:ListItem></asp:DropDownList></td></tr><tr><td>Type Of Housing</td><td><asp:DropDownList ID="CmbTOH" runat="server"><asp:ListItem Text="Select..."></asp:ListItem><asp:ListItem Text="Furnished"></asp:ListItem><asp:ListItem Text="Un Furnished"></asp:ListItem></asp:DropDownList></td></tr><tr><td>Percentage</td><td><asp:TextBox ID="TxtPercentage" runat="server"></asp:TextBox>%</td></tr><tr><td>Employee Contribution</td><td><asp:TextBox ID="TxtEmpContributionH" runat="server"></asp:TextBox></td></tr><tr><td>&#160;</td><td><asp:TextBox ID="TxtAccBenefit" runat="server" Visible="False"></asp:TextBox></td></tr></table></td></tr></table></asp:Panel></ContentTemplate></cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel5" runat="server" HeaderText="General Information"><HeaderTemplate>Emergency Details</HeaderTemplate><ContentTemplate><asp:Panel ID="Panel4" runat="server" BorderStyle="Inset" BorderWidth="1px"><table class="style1"><tr><td colspan="2"><asp:Label ID="Label6" runat="server" Font-Bold="True" Font-Size="Small" 
                                    ForeColor="#0033CC" Text="1st Contact"></asp:Label></td><td><asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Size="Small" 
                                    ForeColor="#0033CC" Text="2nd Contact"></asp:Label></td></tr><tr><td class="style3">Last Name</td><td class="style24"><asp:TextBox ID="Txtcontact1last" runat="server" Width="200px"></asp:TextBox></td><td><asp:TextBox ID="Txtcontact2last" runat="server" Width="200px"></asp:TextBox></td></tr><tr><td class="style3">First Name</td><td class="style24"><asp:TextBox ID="txtcontact1first" runat="server" Width="200px"></asp:TextBox></td><td><asp:TextBox ID="txtcontact2first" runat="server" Width="200px"></asp:TextBox></td></tr><tr><td class="style3">Relationship</td><td class="style24"><asp:TextBox ID="txtcontact1rel" runat="server" Width="200px"></asp:TextBox></td><td><asp:TextBox ID="txtcontact2rel" runat="server" Width="200px"></asp:TextBox></td></tr><tr><td class="style3">Phone: Work</td><td class="style24"><asp:TextBox ID="txtcontact1work" runat="server" Width="200px"></asp:TextBox></td><td><asp:TextBox ID="txtcontact2work" runat="server" Width="200px"></asp:TextBox></td></tr><tr><td class="style8">Home</td><td class="style24"><asp:TextBox ID="txtcontact1home" runat="server" Width="200px"></asp:TextBox></td><td><asp:TextBox ID="txtcontact2home" runat="server" Width="200px"></asp:TextBox></td></tr><tr><td class="style8">Mobile</td><td class="style24"><asp:TextBox ID="txtcontact1mobile" runat="server" Width="200px"></asp:TextBox></td><td><asp:TextBox ID="txtcontact2mobile" runat="server" Width="200px"></asp:TextBox></td></tr><tr><td class="style8">Taken To</td><td class="style24"><asp:TextBox ID="txttakento" runat="server" Width="200px"></asp:TextBox></td><td>&#160;</td></tr></table></asp:Panel></ContentTemplate></cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel7" runat="server" HeaderText="QualificationSkills"><HeaderTemplate>Qualification / Skills</HeaderTemplate><ContentTemplate><asp:Panel ID="Panel6" runat="server" BorderStyle="Inset" BorderWidth="1px"><asp:GridView ID="GdvQua" runat="server" Width="100%" AutoGenerateColumns="False"><Columns><asp:BoundField DataField="EmpID" HeaderText="EmpID" ><HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" /><ItemStyle Wrap="False" /></asp:BoundField><asp:BoundField DataField="QYrStart" HeaderText="Year Start" ><HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" /><ItemStyle Wrap="False" /></asp:BoundField><asp:BoundField DataField="QYrEnd" HeaderText="Year End"><HeaderStyle Font-Size="Smaller" ForeColor="White" /></asp:BoundField><asp:BoundField DataField="QLevel" HeaderText="Level"><HeaderStyle Font-Size="Smaller" ForeColor="White" /></asp:BoundField><asp:BoundField DataField="QCourse" HeaderText="Courses" ><HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" /><ItemStyle Wrap="False" /></asp:BoundField><asp:BoundField DataField="QInstitution" HeaderText="Institutions" ><HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" /><ItemStyle Wrap="False" /></asp:BoundField><asp:BoundField DataField="QModule" HeaderText="Modules"><HeaderStyle Font-Size="Smaller" ForeColor="White" /></asp:BoundField><asp:BoundField DataField="QRemarks" HeaderText="Remarks"><HeaderStyle Font-Size="Smaller" ForeColor="White" /></asp:BoundField><asp:CommandField HeaderText="View" ShowEditButton="True" EditText="View" /></Columns><HeaderStyle BackColor="#0080C0" /><AlternatingRowStyle BackColor="#CAE4FF" /></asp:GridView></asp:Panel></ContentTemplate></cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel8" runat="server" HeaderText="General Information"><HeaderTemplate>References</HeaderTemplate><ContentTemplate><asp:Panel ID="Panel7" runat="server" BorderStyle="Inset" BorderWidth="1px"><asp:GridView ID="GdvRef" runat="server" Width="100%" AutoGenerateColumns="False"><Columns><asp:BoundField DataField="EmpID" HeaderText="EmpID" ><HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" /><ItemStyle Wrap="False" /></asp:BoundField><asp:BoundField DataField="RefDate" HeaderText="Date" DataFormatString="{0:dd/MM/yyyy}" ><HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" /><ItemStyle Wrap="False" /></asp:BoundField><asp:BoundField DataField="RefName" HeaderText="Reference Name"><HeaderStyle Font-Size="Smaller" ForeColor="White" /></asp:BoundField><asp:BoundField DataField="RefCompany" HeaderText="Institution/Company"><HeaderStyle Font-Size="Smaller" ForeColor="White" /></asp:BoundField><asp:BoundField DataField="RefPosition" HeaderText="Position" ><HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" /><ItemStyle Wrap="False" /></asp:BoundField><asp:BoundField DataField="RefContact" HeaderText="Contact Name" ><HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" /><ItemStyle Wrap="False" /></asp:BoundField><asp:CommandField HeaderText="View" ShowEditButton="True" EditText="View" /></Columns><HeaderStyle BackColor="#0080C0" /><AlternatingRowStyle BackColor="#CAE4FF" /></asp:GridView></asp:Panel></ContentTemplate></cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel9" runat="server" HeaderText="Experience"><HeaderTemplate>Experience</HeaderTemplate><ContentTemplate><asp:Panel ID="Panel8" runat="server" BorderStyle="Inset" BorderWidth="1px"><asp:GridView ID="GdvExp" runat="server" Width="100%" AutoGenerateColumns="False"><AlternatingRowStyle BackColor="#CAE4FF" /><Columns><asp:BoundField DataField="EmpID" HeaderText="EmpID" DataFormatString="{0:dd/MM/yyyy}" ><HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" /><ItemStyle Wrap="False" /></asp:BoundField><asp:BoundField DataField="ExpFromDate" HeaderText="From Date"  ><HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" /><ItemStyle Wrap="False" /></asp:BoundField><asp:BoundField DataField="ExpToDate" HeaderText="To Date"><HeaderStyle Font-Size="Smaller" ForeColor="White" /></asp:BoundField><asp:BoundField DataField="ExpCompany" HeaderText="Institution/Company"><HeaderStyle Font-Size="Smaller" ForeColor="White" /></asp:BoundField><asp:BoundField DataField="ExpRole" HeaderText="Role" ><HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" /><ItemStyle Wrap="False" /></asp:BoundField><asp:BoundField DataField="ExpResp" HeaderText="Responsibility" ><HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" /><ItemStyle Wrap="False" /></asp:BoundField><asp:BoundField DataField="ExpCondition" HeaderText="Working Condition" ><HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" /><ItemStyle Wrap="False" /></asp:BoundField><asp:CommandField HeaderText="View" ShowEditButton="True" EditText="View" /></Columns><HeaderStyle BackColor="#0080C0" /></asp:GridView></asp:Panel></ContentTemplate></cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel12" runat="server" HeaderText="Warning"><HeaderTemplate>Save File Path</HeaderTemplate><ContentTemplate><asp:Panel ID="Panel12" runat="server" BorderStyle="Inset" BorderWidth="1px"><asp:GridView ID="GdvSavePath" runat="server" Width="100%" AutoGenerateColumns="False"><Columns><asp:BoundField DataField="EmpID" HeaderText="EmpID"><HeaderStyle Font-Size="Smaller" ForeColor="White" /></asp:BoundField><asp:BoundField DataField="UploadPath" HeaderText="File Path" ><HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" /><ItemStyle Wrap="False" /></asp:BoundField><asp:BoundField DataField="RecNo" HeaderText="No"><HeaderStyle Font-Size="Smaller" ForeColor="White" /></asp:BoundField><asp:CommandField HeaderText="Delete" ShowEditButton="True" EditText="Delete" /></Columns><HeaderStyle BackColor="#0080C0" /><AlternatingRowStyle BackColor="#CAE4FF" /></asp:GridView></asp:Panel></ContentTemplate></cc1:TabPanel>
                </cc1:TabContainer>
                </asp:Panel>                
            </div>
        </ContentTemplate>
        
    </asp:UpdatePanel>


    <triggers>
        <asp:postbacktrigger ControlID = "ButUpload" />
    </triggers>

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_InitializeRequest);
        prm.add_endRequest(prm_EndRequest);

        function prm_InitializeRequest(sender, args) {
            var panelProg = $get('divImage');
            if (args._postBackElement.id != "Timer1") {
                panelProg.style.display = '';
            }
        };

        function prm_EndRequest(sender, args) {
            var panelProg = $get('divImage');
            panelProg.style.display = 'none';
        };
    </script>


    </form>
    
</body>
</html>

