﻿Imports System.IO
Imports System.Data.OleDb
Partial Public Class FrmMNS
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim CmdTrans As New OleDb.OleDbCommand
    Dim RdrTrans As OleDb.OleDbDataReader
    Dim CmdEmp As New OleDb.OleDbCommand
    Dim RdrEmp As OleDb.OleDbDataReader
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            TxtYear.Text = Date.Today.Year
            CmbMonth.SelectedIndex = Date.Today.Month - 1
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim StrSql As String
        Dim StrSalaryPeriod As String
        Dim DtePayDate As Date
        Dim StrLine As String
        Dim LngMaxSalNPF As Long
        Dim LngSal As Long
        Dim StrSSNo As String
        Dim strData As String = ""
        Dim StrFlag As String
        Dim StrLast As String
        Dim StrFirst As String
        Dim StrAddress As String
        Dim StrCity As String
        Dim StrCountry As String
        Dim StrM As String = "M"
        Dim LngNSF As Long
        Dim LngSalNPS As Single
        Dim stringWriter As New StringWriter
        Dim wrt As New IO.StringWriter()
        Dim StrCompanyName As String
        Dim StrType As String
        Dim tab As String = ""
        StrCompanyName = ""
        Dim SngLevy As Single = 0

        Dim StrSpace As String = ""
        DtePayDate = CDate("28-" + CmbMonth.Text + "-" + TxtYear.Text)

        StrSalaryPeriod = Format(DtePayDate, "yyyyMM")

        Response.Clear()
        Response.ContentType = "text/csv"
        Response.AppendHeader("Content-Disposition", String.Format("attachment; filename=CNP" & Format(DateTime.Today, "MMyy") & ".csv"))

        StrSql = "Select *From [System]"
        CmdTrans.Connection = Con
        CmdTrans.CommandText = StrSql
        RdrTrans = CmdTrans.ExecuteReader
        While RdrTrans.Read
            LngMaxSalNPF = RdrTrans("MaxSalNPS")
            StrCompanyName = RdrTrans("Name")
            SngLevy = RdrTrans("IVTB")
        End While
        RdrTrans.Close()

        Dim Dt As DataTable
        Dim Dr As DataRow
        Dim Dc As DataColumn
        Dt = New DataTable()
        Dc = New DataColumn("Col1", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col2", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col3", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col4", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col5", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col6", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col7", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col8", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)
        Dc = New DataColumn("Col9", Type.GetType("System.String"))
        Dt.Columns.Add(Dc)

        Dim DtHeader As DataTable
        Dim DrHeader As DataRow
        Dim DcHeader As DataColumn

        DtHeader = New DataTable()
        DcHeader = New DataColumn("Header1", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)
        DcHeader = New DataColumn("Header2", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)
        DcHeader = New DataColumn("Header3", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)
        DcHeader = New DataColumn("Header4", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)
        DcHeader = New DataColumn("Header5", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)
        DcHeader = New DataColumn("Header6", Type.GetType("System.String"))
        DtHeader.Columns.Add(DcHeader)

        Dim LngSumBasic As Long
        Dim LngIVTB As Long
        LngSumBasic = 0
        LngIVTB = 0

        If e.Item.CommandName = "Print" Then
            Try
                CmdEmp.Connection = Con
                CmdEmp.CommandText = "Select * From Employee where NPSPaid = 1 order by EmpID" ' and EmpID = '" & RdrTrans("EmpID") & "'"
                RdrEmp = CmdEmp.ExecuteReader
                StrLast = ""
                StrFirst = ""
                StrSSNo = ""
                While RdrEmp.Read
                    StrLast = UCase(RdrEmp("Last"))
                    StrFirst = UCase(RdrEmp("First"))
                    StrSSNo = RdrEmp("SSNo") & ""

                    StrSql = "SELECT * FROM Salary where EmpID = '" & RdrEmp("EmpID") & "' and [Month] = '" & CmbMonth.Text & "' AND Year='" & TxtYear.Text & "' And IVTB >= 0 order by EmpID"
                    CmdTrans.Connection = Con
                    CmdTrans.CommandText = StrSql
                    RdrTrans = CmdTrans.ExecuteReader
                    While RdrTrans.Read
                        Dim LEVY As String
                        LEVY = "N"
                        If RdrTrans("IVTB") > 0 Then
                            LngSumBasic = LngSumBasic + Val(RdrTrans("BasicSalary"))
                            LEVY = "Y"
                        End If
                        If ((RdrTrans("NSF") + RdrTrans("NPFEmployee") + RdrTrans("NSFEmployee")) = 0) And RdrTrans("NPFEmployer") >= 0 Then
                            StrType = "T2"
                        Else
                            StrType = "S2"
                        End If
                        If Val(RdrTrans("BasicSalary")) > LngMaxSalNPF Then
                            'LngSal = LngMaxSalNPF
                            LngSal = Val(RdrTrans("BasicSalary"))
                        Else
                            LngSal = Val(RdrTrans("BasicSalary"))
                        End If
                        If (Val(RdrTrans("NSF")) + Val(RdrTrans("NSFEmployee"))) = 0 Then
                            'LngSalNPS = 0
                            LngSal = Val(RdrTrans("BasicSalary"))
                        Else
                            'LngSalNPS = LngSal
                            LngSal = Val(RdrTrans("BasicSalary"))
                        End If
                        Dim pt As Char = " "

                        Dr = Dt.NewRow
                        Dr.Item("Col1") = StrSSNo
                        Dr.Item("Col2") = StrLast
                        Dr.Item("Col3") = StrFirst
                        Dr.Item("Col4") = Val(RdrTrans("BasicSalary")) 'LngSal
                        Dr.Item("Col5") = Val(RdrTrans("BasicSalary")) 'LngSalNPS
                        Dr.Item("Col6") = StrType
                        Dr.Item("Col7") = "M"
                        Dr.Item("Col8") = "1"
                        Dr.Item("Col9") = LEVY
                        Dt.Rows.Add(Dr)
                    End While
                    RdrTrans.Close()
                End While
                RdrEmp.Close()

                Dim StrFileName As String = "CNP" & Format(DtePayDate, "MM") & Format(DtePayDate, "yy")

                DrHeader = DtHeader.NewRow
                DrHeader.Item("Header1") = "NPFINFO"
                DrHeader.Item("Header2") = "01106325"
                DrHeader.Item("Header3") = StrCompanyName
                DrHeader.Item("Header4") = StrSalaryPeriod
                DrHeader.Item("Header5") = LngSumBasic
                DrHeader.Item("Header6") = Math.Round((SngLevy * LngSumBasic) / 100, 0)

                DtHeader.Rows.Add(DrHeader)
                For Each DrHeader In DtHeader.Rows
                    tab = ""
                    For i = 0 To DtHeader.Columns.Count - 1
                        Response.Write(tab & DrHeader(i).ToString())
                        tab = ","
                    Next
                    Response.Write(vbCrLf)
                Next

                For Each Dr In Dt.Rows
                    tab = ""
                    For i = 0 To Dt.Columns.Count - 1
                        Response.Write(tab & Dr(i).ToString())
                        tab = ","
                    Next
                    Response.Write(vbCrLf)
                Next

                Response.End()
                Response.[End]()
            Catch ex As Exception
                '' MsgBox("")
            End Try

        End If
    End Sub
   
End Class