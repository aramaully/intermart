﻿Imports System.Data.SqlClient
Imports System.Web.Services
Imports System.Web.UI.WebControls

Partial Public Class FrmEmpGen
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim StrSqlUser As String
    Dim CmdUser As New OleDb.OleDbCommand
    Dim StrPassword As String
    Dim StrSqlAccess As String
    Dim CmdAccess As OleDb.OleDbCommand
    Dim RdrUser As OleDb.OleDbDataReader
    Dim StrDepartment As String = ""
    Dim StrHireDate As String = ""
    Dim StrOutlet As String = ""
    Dim CmdGenAudit As New OleDb.OleDbCommand
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrAudit As String

    <WebMethod()> _
    Public Shared Function AjaxSetSession(ByVal SessionValue As String)
        Try
            HttpContext.Current.Session("Done") = SessionValue
        Catch ex As Exception
        End Try
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            'Department
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT * FROM Dept ORDER By DeptCode"
            CmbDepartment.Items.Add("Select...")
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                CmbDepartment.Items.Add(Rdr("DeptName"))
            End While
            Rdr.Close()
            'Outlet
            Cmd.Connection = Con
            Cmd.CommandText = "Select OutletName from Outlet Order by OutletCode"
            CmbOutlet.Items.Add("Select...")
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                CmbOutlet.Items.Add(Rdr("OutletName"))
            End While
            Rdr.Close()
            CmbCategory.SelectedIndex = 3
        End If
        If Session("Done") = "True" Then
            ScriptManager.RegisterStartupScript(Button2, Me.GetType, "Camera", "<script language='javascript'> __doPostBack('Button2', ''); </script>", False)
        End If
    End Sub

    Protected Sub ClearAll()
        TxtFirst.Text = ""
        TxtLast.Text = ""
        TxtDOB.Text = ""
        TxtHireDate.Text = ""
        TxtClockID.Text = ""
        CmbCategory.SelectedIndex = 0
        CmbOutlet.SelectedIndex = 0
        CmbTitle.SelectedIndex = 0
        OptMale.Checked = True
        OptFemale.Checked = False
        OptYes.Checked = True
        OptNo.Checked = False
        ImgPhoto.ImageUrl = Nothing
        ChkEPZPaid.Checked = False
        TxtFPid.Text = ""
        CmbDepartment.SelectedIndex = 0
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ev As New System.EventArgs
        If e.Item.CommandName = "New" Then
            lblMessage.Visible = False
            If TxtLast.Text = "" Then
                lblMessage.Visible = True
                lblMessage.Text = "Please Enter Last Name before pressing on New Button!!!"
                Exit Sub
            End If
            Dim StrLastName As String
            If (Mid(Trim(TxtLast.Text), 3, 1)) = " " Then
                StrLastName = UCase(Mid(Trim(TxtLast.Text), 1, 2)) + "" + UCase(Mid(Trim(TxtLast.Text), 4, 1))
            Else
                StrLastName = UCase(Mid(Trim(TxtLast.Text), 1, 3))
            End If

            StrSql = "SELECT count(*) From Employee WHERE EmpID LIKE '%" & StrLastName & "%'"
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader

            If Rdr.Read Then
                TxtEmpID.Text = StrLastName + "" + Format((Int(Rdr(0)) + 1), "00")
            Else
                TxtEmpID.Text = StrLastName + "" + Format((Int(Rdr(0)) + 1), "00")
            End If
            Rdr.Close()
        End If
        If e.Item.CommandName = "Save" Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = "SELECT COUNT(*) FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
            Cmd.CommandText = StrSql
            Cmd.Connection = Con
            CmdUser.Connection = Con
            Dim img As FileUpload = CType(FileUpload1, FileUpload)
            lblMessage.Visible = False
            If TxtDOB.Text = "" Then
                lblMessage.Visible = True
                lblMessage.Text = "Please Enter Date of Birth!!!"
                Exit Sub
            End If
            If TxtEmpID.Text = "" Then
                lblMessage.Visible = True
                lblMessage.Text = "Please Enter Employee ID!!!"
                Exit Sub
            End If
            If CmbCategory.Text = "" Then
                lblMessage.Visible = True
                lblMessage.Text = "Please Enter Category!!!"
                Exit Sub
            End If
            If TxtHireDate.Text = "" Then
                lblMessage.Visible = True
                lblMessage.Text = "Please Enter Hire Date!!!"
                Exit Sub
            End If
            'department
            If CmbDepartment.Text = "" Or CmbDepartment.Text = "Select..." Then
                lblMessage.Visible = True
                lblMessage.Text = "Please Enter Department Code!!!"
                Exit Sub
            End If
            If CmbOutlet.Text = "" Or CmbOutlet.Text = "Select..." Then
                lblMessage.Visible = True
                lblMessage.Text = "Please Enter Outlet Name!!!"
                Exit Sub
            End If
            If Cmd.ExecuteScalar > 0 Then
                If Request.Cookies("LastGenDetail") IsNot Nothing Then
                    Dim lvGen As HttpCookie = Request.Cookies("LastGenDetail")
                    StrDepartment = Server.HtmlDecode(lvGen.Values("DeptCode"))
                    'StrOutlet = Server.HtmlDecode(lvGen.Values("OutletName"))
                    StrHireDate = Server.HtmlDecode(lvGen.Values("HireDate"))
                End If

                StrSql = "UPDATE Employee SET "
                StrSql = StrSql & "[Last] = '" & TxtLast.Text & "',"
                StrSql = StrSql & "[First] = '" & TxtFirst.Text & "',"
                StrSql = StrSql & "DOB = '" & Format(CDate(TxtDOB.Text), "dd-MMM-yyyy") & "',"

                StrSql = StrSql & "HireDate = '" & Format(CDate(TxtHireDate.Text), "dd-MMM-yyyy") & "',"
                If StrHireDate <> "" Then
                    If Format(CDate(StrHireDate), "dd-MMM-yyyy") <> TxtHireDate.Text Then
                        CmdGenAudit.Connection = Con
                        CmdGenAudit.CommandText = "Insert Into EmpMasterAudit Values('" & TxtEmpID.Text & "','Hire Date change from ','" & StrHireDate & "','" & TxtHireDate.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                        CmdGenAudit.ExecuteNonQuery()
                    End If
                End If
                StrSql = StrSql & "Clocked = " & IIf(OptYes.Checked, 1, 0) & ","
                StrSql = StrSql & "Category = '" & CmbCategory.Text & "',"

                StrSql = StrSql & "DeptCode ='" & CmbDepartment.Text & "',"
                If StrDepartment <> CmbDepartment.Text Then
                    CmdGenAudit.Connection = Con
                    CmdGenAudit.CommandText = "Insert Into EmpMasterAudit Values('" & TxtEmpID.Text & "','Department Code change from','" & StrDepartment & "','" & CmbDepartment.Text & "','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString()), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString() & "')"
                    CmdGenAudit.ExecuteNonQuery()
                End If
                StrSql = StrSql & "OutletName ='" & CmbOutlet.Text & "',"
                StrSql = StrSql & "Title = '" & CmbTitle.Text & "',"
                StrSql = StrSql & "BatchNo = '" & TxtClockID.Text & "',"
                StrSql = StrSql & "FPid = '" & TxtFPid.Text & "',"
                StrSql = StrSql & "Recruit = 0,"

                ' ''StrSql = StrSql & "BasicSalary = 0,"
                ' ''StrSql = StrSql & "ProdBonus = 0,"
                ' ''StrSql = StrSql & "MedicalContribEr = 0,"
                ' ''StrSql = StrSql & "MedicalContrib = 0,"
                ' ''StrSql = StrSql & "Fringe = 0,"
                ' ''StrSql = StrSql & "TravelAllowance = 0,"
                ' ''StrSql = StrSql & "HousePercent = 0,"
                ' ''StrSql = StrSql & "TotEDF = 0,"
                ' ''StrSql = StrSql & "EDFFactor = 13,"
                ' ''StrSql = StrSql & "AdvInst = 0,"

                If ChkEPZPaid.Checked Then
                    StrSql = StrSql & "EPZPaid = 1,"
                Else
                    StrSql = StrSql & "EPZPaid = 0,"
                End If
                StrSql = StrSql & "Gender = '" & IIf(OptMale.Checked, "Male", "Female") & "' "
                StrSql = StrSql & "WHERE EmpID = '" & TxtEmpID.Text & "'"
            Else
                StrSql = "INSERT INTO Employee(EmpID,[First],[Last],DOB,HireDate,Clocked,Category,DeptCode,OutletName,Title,BatchNo,FPid,EPZPaid,Working,NPSPaid,AttBonus,BasicSalary,ProdBonus,MedicalContribEr,MedicalContrib,Fringe,TravelAllowance,HousePercent,TotEDF,EDFFactor,OTPaid,Recruit,Gender) "
                StrSql = StrSql & "VALUES('"
                StrSql = StrSql & TxtEmpID.Text & "','"
                StrSql = StrSql & TxtFirst.Text & "','"
                StrSql = StrSql & TxtLast.Text & "','"
                StrSql = StrSql & Format(CDate(TxtDOB.Text), "yyyy-MM-dd") & "','"
                StrSql = StrSql & Format(CDate(TxtHireDate.Text), "yyyy-MM-dd") & "',"
                StrSql = StrSql & IIf(OptYes.Checked, 1, 0) & ",'"
                StrSql = StrSql & CmbCategory.Text & "','"
                StrSql = StrSql & CmbDepartment.Text & "','"
                StrSql = StrSql & CmbOutlet.Text & "','"
                StrSql = StrSql & CmbTitle.Text & "','"
                StrSql = StrSql & TxtClockID.Text & "','"
                StrSql = StrSql & TxtFPid.Text & "',"
                StrSql = StrSql & IIf(ChkEPZPaid.Checked, 1, 0) & ","
                StrSql = StrSql & 1 & ","
                StrSql = StrSql & 1 & ","
                StrSql = StrSql & 0 & ","

                StrSql = StrSql & 0 & "," ' Basic salary
                StrSql = StrSql & 0 & ","
                StrSql = StrSql & 0 & ","
                StrSql = StrSql & 0 & ","
                StrSql = StrSql & 0 & ","
                StrSql = StrSql & 0 & ","
                StrSql = StrSql & 0 & ","
                StrSql = StrSql & 0 & ","
                StrSql = StrSql & 13 & ","

                StrSql = StrSql & 0 & ","
                StrSql = StrSql & 0 & ",'"
                StrSql = StrSql & IIf(OptMale.Checked, "Male", "Female") & "')"

            End If
            Try
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                lblMessage.Visible = True
                lblMessage.Text = "Employee Records Saved!!"

                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Employee with ID " & TxtEmpID.Text & " Created / Edited. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Create / Edit User','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "DELETE FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
                Cmd.ExecuteNonQuery()
                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Employee with ID " & TxtEmpID.Text & " deleted. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Delete User','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()

                TxtEmpID.Text = ""
                TxtFirst.Text = ""
                TxtLast.Text = ""
                TxtDOB.Text = ""
                TxtHireDate.Text = ""
                TxtClockID.Text = ""
                CmbCategory.SelectedIndex = 0
                CmbTitle.SelectedIndex = 0
                OptMale.Checked = True
                OptFemale.Checked = False
                OptYes.Checked = True
                OptNo.Checked = False
                ImgPhoto.ImageUrl = Nothing
                ChkEPZPaid.Checked = False
                TxtFPid.Text = ""
                CmbDepartment.SelectedIndex = 0
                CmbOutlet.SelectedIndex = 0
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Next" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID > '" & TxtEmpID.Text & "'"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
                lblMessage.Visible = False
                lblMessage.Text = ""
            Catch ex As Exception

            End Try

        End If
        If e.Item.CommandName = "Previous" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID < '" & TxtEmpID.Text & "' ORDER BY EmpID DESC"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
                lblMessage.Visible = False
                lblMessage.Text = ""
            Catch ex As Exception

            End Try
        End If
        If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtEmpID.Text)
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
        End If
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        'If Session("Level") = "FacUser" Then
        '    StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "' And DeptCode = 'FAC'"
        'Else
        '    StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        'End If
        StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        If Rdr.Read Then
            'On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
            CmbTitle.Text = Rdr("Title")
            CmbCategory.Text = Rdr("Category")
            TxtDOB.Text = Rdr("DOB")
            TxtHireDate.Text = Rdr("HireDate")
            OptMale.Checked = IIf(Rdr("Gender") = "Male", True, False)
            OptFemale.Checked = IIf(Rdr("Gender") = "Male", False, True)
            CmbCategory.Text = Rdr("Category")
            CmbDepartment.Text = Rdr("DeptCode").ToString
            CmbOutlet.Text = Rdr("OutletName").ToString
            OptYes.Checked = IIf(Rdr("Clocked"), True, False)
            OptNo.Checked = IIf(Rdr("Clocked"), False, True)
            TxtClockID.Text = Rdr("BatchNo")
            TxtFPid.Text = Rdr("FPid")
            If Rdr("EPZPaid") Then
                ChkEPZPaid.Checked = True
            Else
                ChkEPZPaid.Checked = False
            End If
            Session.Add("EmpID", Rdr("EmpID"))
            ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & Rdr("EmpID")

            Dim lastGenDetail As New HttpCookie("LastGenDetail")
            lastGenDetail.Values("LastTime") = Date.Now.ToShortTimeString()
            lastGenDetail.Values("DeptCode") = Rdr("DeptCode").ToString
            lastGenDetail.Values("HireDate") = Rdr("HireDate").ToString
            lastGenDetail.Expires = Date.Now.AddDays(1)
            Response.Cookies.Add(lastGenDetail)

            ''StrSqlUser = "Select *From [User] Where UserID = '" & TxtEmpID.Text & "'"
            ''CmdUser.Connection = Con
            ''CmdUser.CommandText = StrSqlUser
            ''RdrUser = CmdUser.ExecuteReader
            ''If RdrUser.Read Then
            ''    TxtPassword.Text = RdrUser("Password")
            ''End If
        Else
            Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(1))
            ToolBar1_ItemClick(Me, ev)
        End If
        Rdr.Close()
    End Sub

    Private Sub ButUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButUpload.Click

        Dim Img As FileUpload = CType(FileUpload1, FileUpload)
        If Img.HasFile AndAlso Not Img.PostedFile Is Nothing Then
            Dim ImgByte As Byte() = Nothing
            ' Insert the employee name and image into db
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT COUNT(*) FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
            If Cmd.ExecuteScalar > 0 Then
                StrSql = "UPDATE Employee SET  PhotoOle = @Photo, EmpID = @EmpID "
                StrSql = StrSql & "WHERE EmpID = '" & TxtEmpID.Text & "'"
            Else
                StrSql = "INSERT INTO Employee " & "(PhotoOle, EmpID) VALUES (@Photo, @EmpID)"
            End If
            'To create a PostedFile
            Dim File As HttpPostedFile = FileUpload1.PostedFile
            'Create byte Array with file len
            ImgByte = New Byte(File.ContentLength - 1) {}
            'force the control to load data in array
            File.InputStream.Read(ImgByte, 0, File.ContentLength)
            Dim Conn As String = "Data Source=ROBIN-PC\SQLEXPRESS;Initial Catalog=InterMart;User Id=admin;Password=swathi*;"
            'Dim Conn As String = "Provider=SQLOLEDB.1;Data Source=ROBIN-HP\SQLEXPRESS;User ID=admin;PWD=swathi*;Initial Catalog=TNTWEB"
            Dim Connection As SqlConnection = New SqlConnection(Conn)
            Connection.Open()
            Dim SqlCmd As SqlCommand = New SqlCommand(StrSql, Connection)
            SqlCmd.Parameters.Clear()
            SqlCmd.Parameters.AddWithValue("@Photo", ImgByte)
            SqlCmd.Parameters.AddWithValue("@EmpID", TxtEmpID.Text)
            Try
                SqlCmd.ExecuteNonQuery()
                ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & TxtEmpID.Text
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            Finally
                Connection.Dispose()
                SqlCmd.Dispose()
            End Try
        End If
    End Sub

    Private Sub ButCamera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCamera.Click
        Session.Add("EmpID", TxtEmpID.Text)
        Session.Add("Done", "False")
        ImgPhoto.ImageUrl = Nothing
    End Sub

    Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If Session("Done") = "True" Then
            Timer1.Enabled = False
            TxtEmpID.Text = Session("PstrCode")
        End If
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click
        ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & TxtEmpID.Text
    End Sub

    Private Sub CmdClockID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmdClockID.Click
        Session.Add("PstrCode", TxtClockID.Text)
        ScriptManager.RegisterStartupScript(CmdClockID, Me.GetType, "ShowFindClockID", "<script language='javascript'> ShowFindClockID();</script>", False)
    End Sub

End Class