﻿Public Partial Class FrmHolidays
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrAudit As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            TxtYear.Text = Year(Date.Today)
            FillGrid()
        End If
    End Sub

    Protected Sub FillGrid()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Cmd.Connection = Con
        StrSql = "SELECT * FROM Holiday WHERE [Year] = '" & TxtYear.Text & "'"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvLoan.DataSource = Rdr
        GdvLoan.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "New" Then
            TxtYear.Text = ""
            TxtDate.Text = ""
            TxtHoliday.Text = ""
            TxtDate.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Dim Trans As OleDb.OleDbTransaction
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
            Cmd.Connection = Con
            Cmd.Transaction = Trans
            Cmd.CommandText = "DELETE FROM Holiday WHERE [Date] = '" & Format(CDate(TxtDate.Text), "dd-MMM-yyyy") & "'"
            Try
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO Holiday (Year,[Date],Holiday) VALUES('"
                StrSql = StrSql & TxtYear.Text & "','"
                StrSql = StrSql & Format(CDate(TxtDate.Text), "dd-MMM-yyyy") & "','"
                StrSql = StrSql & TxtHoliday.Text & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                Trans.Commit()
                TxtDate.Text = ""
                TxtHoliday.Text = ""
                FillGrid()

                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Holiday Created / Edited. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Create / Edit Holiday','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                Trans.Rollback()
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Holiday WHERE [Date] = '" & Format(CDate(TxtDate.Text), "dd-MMM-yyyy") & "'"
            Try
                Cmd.ExecuteNonQuery()
                FillGrid()
                TxtDate.Text = ""
                TxtHoliday.Text = ""
                'Audit
                CmdAudit.Connection = Con
                StrAudit = "Holiday Deleted. "
                CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Delete Holiday','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                CmdAudit.ExecuteNonQuery()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
    End Sub

    Private Sub GdvLoan_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvLoan.RowDataBound
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvLoan, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvLoan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvLoan.SelectedIndexChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        TxtDate.Text = GdvLoan.SelectedRow.Cells(0).Text
        TxtHoliday.Text = GdvLoan.SelectedRow.Cells(1).Text
        TxtYear.Text = GdvLoan.SelectedRow.Cells(2).Text
    End Sub

    Private Sub GdvLoan_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvLoan.SelectedIndexChanging
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        GdvLoan.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        For i = 0 To Me.GdvLoan.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvLoan.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub TxtYear_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtYear.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        FillGrid()
    End Sub
End Class