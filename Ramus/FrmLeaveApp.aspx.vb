﻿Imports System.Net.Mail
Partial Public Class FrmLeaveApp
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdEmp As New OleDb.OleDbCommand
    Dim RdrEmp As OleDb.OleDbDataReader
    Dim CmdDept As New OleDb.OleDbCommand
    Dim RdrDept As OleDb.OleDbDataReader
    Dim TimexCmd As New OleDb.OleDbCommand
    Dim TimexRdr As OleDb.OleDbDataReader
    Dim CmdUser As New OleDb.OleDbCommand
    Dim RdrUser As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            If Session("Level") = "Leave Application" Then
                TxtEmpID.Text = Session("UserID")
                TxtEmpID.Enabled = False
            End If
            TxtDate.Text = Date.Today.ToString("dd/MM/yyyy")
            CmbCategory.Items.Clear()
            CmbCategory.Items.Add("Select...")
            CmbCategory.Items.Add("Emergency")
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT Name FROM LeaveMaster"
            Rdr = Cmd.ExecuteReader
            CmbType.Items.Add("Select...")
            While Rdr.Read
                CmbType.Items.Add(Rdr("Name"))
            End While
            Rdr.Close()
            CmdUser.Connection = Con
            CmdUser.CommandText = "Select *From [User] Where UserID = '" & Session("UserID") & "'"
            RdrUser = CmdUser.ExecuteReader
            If RdrUser.Read Then
                If RdrUser("Name") = "Leave Application" Then
                    TxtEmpID.Enabled = False
                    TxtEmpID.Text = RdrUser("Empid")
                    TxtEmpID_TextChanged(Me, e)
                End If
            End If
            RdrUser.Close()
        End If
    End Sub

    Protected Sub ClearAll()
        TxtFirst.Text = ""
        TxtLast.Text = ""
        TxtClockID.Text = ""
        TxtCategory.Text = ""
        TxtTitle.Text = ""
        TxtGender.Text = ""
        ImgPhoto.ImageUrl = Nothing
        TxtLocal.Text = ""
        TxtSick.Text = ""
        TxtAccSick.Text = ""
        TxtFrom.Text = ""
        TxtTo.Text = ""
        CmbCategory.SelectedIndex = 0
        CmbCategory.SelectedIndex = 0
        TxtRemarks.Text = ""
        GdvLeave.Dispose()
        GdvLeave.DataSource = ""
        GdvLeave.DataBind()
    End Sub
    Protected Sub FillGrid()
        'Leave Application
        TimexCmd.Connection = Con
        StrSql = "SELECT AppliedDate,[From],[To],Type,Leave,Category,Remarks "
        StrSql = StrSql & "FROM LeaveApp WHERE LeaveApp.EmpID = '" & TxtEmpID.Text & "'"
        StrSql = StrSql + " AND Year(LeaveApp.[From]) = " & CDate(TxtDate.Text).Year & " ORDER BY LeaveApp.[From]"
        TimexCmd.CommandText = StrSql
        TimexRdr = TimexCmd.ExecuteReader
        GdvLeave.DataSource = TimexRdr
        GdvLeave.DataBind()
        TimexRdr.Close()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ev As New System.EventArgs
        If Session("Level") <> "Leave Application" Then
            If e.Item.CommandName = "New" Then
                ClearAll()
                TxtEmpID.Text = ""
                TxtEmpID.Focus()
            End If
        End If
        If e.Item.CommandName = "Save" Then
            If CmbType.Text = "Select..." Then
                LblResults.Text = "Select Leave Type"
                CmbType.Focus()
                Exit Sub
            Else
                LblResults.Text = ""
            End If
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = "Select *From Employee Where EmpID = '" & TxtEmpID.Text & "'"
            CmdEmp.Connection = Con
            CmdEmp.CommandText = StrSql
            RdrEmp = CmdEmp.ExecuteReader
            Dim StrEmpGrade As String
            Dim StrEmpName As String
            Dim StrEmpEmail As String
            Dim StrDeptCode As String
            Dim StrHrEmail As String
            Dim StrGrade3 As String
            Dim StrGrade4 As String
            Dim StrGrade5 As String
            If RdrEmp.Read Then
                StrEmpGrade = RdrEmp("Grade")
                StrEmpEmail = RdrEmp("Email")
                StrDeptCode = RdrEmp("DeptCode")
                StrEmpName = RdrEmp("Last") & " " & RdrEmp("First")
                If StrDeptCode <> "" Then
                    CmdDept.Connection = Con
                    CmdDept.CommandText = "Select *From Dept Where DeptCode = '" & RdrEmp("DeptCode") & "'"
                    RdrDept = CmdDept.ExecuteReader
                    If RdrDept.Read Then
                        'StrHrEmail = RdrDept("HREmail")
                        ' StrGrade3 = RdrDept("Grade3") & ""
                        'StrGrade4 = RdrDept("Grade4") & ""
                        'StrGrade5 = RdrDept("Grade5") & ""
                    End If
                    RdrDept.Close()
                End If
            End If
            RdrEmp.Close()
            'If CmbType.Text = "Local Leave" Or CmbType.Text = "Sick Leave" Then
            '    StrSql = "SELECT HireDate FROM Employee WHERE EmpId = '" & TxtEmpID.Text & "'"
            '    Cmd.Connection = Con
            '    Cmd.CommandText = StrSql
            '    Dim DteEDate As Date = Cmd.ExecuteScalar
            '    DteEDate = DteEDate.AddYears(1)
            '    If CDate(TxtFrom.Text) < DteEDate Then
            '        LblResults.Text = "Employee has Less than one Year of Service"
            '        Exit Sub
            '    Else
            '        LblResults.Text = ""
            '    End If
            'End If
            Dim IntCountDays As Integer = 0
            Dim DteStart As Date = Format(CDate(TxtFrom.Text), "dd/MM/yyyy")
            Dim DteEnd As Date = Format(CDate(TxtTo.Text), "dd/MM/yyyy")
            While DteStart <= DteEnd
                IntCountDays = IntCountDays + 1
                DteStart = DteStart.AddDays(1)
            End While
            If Val(TxtLeave.Text) > 1 Then
                LblResults.Text = "MsgBox Days Cannot Exceed 1"
                TxtLeave.Text = 1
                Exit Sub
            ElseIf (Val(TxtLeave.Text) > 0 And Val(TxtLeave.Text) < 1) And (CDate(TxtTo.Text) > CDate(TxtFrom.Text)) Then
                If MsgBox("Leave Less Than 1 Date Range Should Be Same", vbQuestion + vbYesNo, "Confirm Date Change") = vbYes Then
                    TxtFrom.Text = TxtTo.Text
                Else
                    Exit Sub
                End If
            End If
            Dim SngCasual As Single = 0
            Dim SngSick As Single = 0

            StrSql = "SELECT * FROM Leave WHERE EmpID ='" & TxtEmpID.Text & "' And Year(Leave.[Date]) = "
            StrSql = StrSql & CDate(TxtDate.Text).Year
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                If Rdr("Type") = "Local Leave" Then
                    SngCasual = SngCasual + Rdr("Leave")
                End If
                If Rdr("Type") = "Sick Leave" Then
                    SngSick = SngSick + Rdr("Leave")
                End If
            End While
            Rdr.Close()
            Dim SngSickEnt As Single = 0
            Dim SngLocalEnt As Single = 0
            StrSql = "select * From LeaveHistory Where EmpID = '" & TxtEmpID.Text
            StrSql = StrSql & "' And Year = " & CDate(TxtDate.Text).Year
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                SngSickEnt = Rdr("SickYear")
                SngLocalEnt = Rdr("LocalYear")
            End While
            Rdr.Close()
            If CmbType.Text = "Local Leave" Then
                If SngCasual + IntCountDays > SngLocalEnt Then
                    LblResults.Text = "Local Leave will exceed the allowed limit."
                    CmbType.SelectedIndex = 0
                    CmbCategory.SelectedIndex = 0
                    Exit Sub
                End If
            End If
            If CmbType.Text = "Sick Leave" Then
                If SngSick + IntCountDays > SngSickEnt Then
                    LblResults.Text = "Sick Leave will exceed the allowed limit"
                    CmbType.SelectedIndex = 0
                    CmbCategory.SelectedIndex = 0
                    Exit Sub
                End If
            End If
            Try
                StrSql = "INSERT INTO LeaveApp (EmpID,AppliedDate,[From],[To],Type,Leave,Category,Remarks,Accumulated) VALUES('"
                StrSql = StrSql & TxtEmpID.Text & "','"
                StrSql = StrSql & Format(CDate(TxtDate.Text), "dd-MMM-yyyy") & "','"
                StrSql = StrSql & Format(CDate(TxtFrom.Text), "dd-MMM-yyyy") & "','"
                StrSql = StrSql & Format(CDate(TxtTo.Text), "dd-MMM-yyyy") & "','"
                StrSql = StrSql & CmbType.Text & "',"
                StrSql = StrSql & CSng(TxtLeave.Text) & ",'"
                StrSql = StrSql & IIf(CmbCategory.Text = "Select...", "", CmbCategory.Text) & "','"
                StrSql = StrSql & TxtRemarks.Text & "',"
                StrSql = StrSql & IIf(ChkAccLeave.Checked, 1, 0) & ")"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                LblResults.Text = "Leave Application Recorded"
                Try
                    ' ''Dim SmtpServer As New SmtpClient()
                    ' ''Dim mail As New MailMessage()
                    ' ''Dim StrMailBody As String
                    ' ''StrMailBody = ""
                    ' ''SmtpServer.Credentials = New Net.NetworkCredential("hrms@angsana.com", "1234")
                    ' ''SmtpServer.Port = 25
                    ' ''SmtpServer.Host = "smtp.intnet.mu"
                    ' ''mail = New MailMessage()
                    ' ''If StrEmpEmail <> "" Then
                    ' ''    mail.From = New MailAddress(StrEmpEmail)
                    ' ''End If
                    ' ''If StrEmpGrade = "1" Or StrEmpGrade = "2" Then
                    ' ''    If StrGrade3 <> "" Then
                    ' ''        mail.To.Add(StrGrade3)
                    ' ''    End If
                    ' ''    If StrGrade4 <> "" Then
                    ' ''        mail.CC.Add(StrGrade4)
                    ' ''    End If
                    ' ''    mail.CC.Add(StrHrEmail)
                    ' ''End If
                    ' ''If StrEmpGrade = "3" Then
                    ' ''    If StrGrade4 <> "" Then
                    ' ''        mail.To.Add(StrGrade4)
                    ' ''    End If
                    ' ''    mail.CC.Add(StrHrEmail)
                    ' ''End If
                    ' ''If StrEmpGrade = "4" Then
                    ' ''    If StrGrade4 <> "" Then
                    ' ''        mail.To.Add(StrGrade5)
                    ' ''    End If
                    ' ''    If StrGrade5 <> "" Then
                    ' ''        mail.CC.Add(StrGrade5)
                    ' ''    End If
                    ' ''    mail.CC.Add(StrHrEmail)
                    ' ''End If
                    ' ''If StrEmpGrade = "5" Then
                    ' ''    If StrGrade5 <> "" Then
                    ' ''        mail.CC.Add(StrGrade5)
                    ' ''    End If
                    ' ''    mail.CC.Add(StrHrEmail)
                    ' ''End If
                    ' ''mail.Subject = "Leave Application From " & TxtFrom.Text & " To " & TxtTo.Text & ""
                    ' ''StrMailBody = "Dear Sir," & vbCrLf & vbCrLf
                    ' ''StrMailBody = StrMailBody & "I'm applying for "
                    ' ''StrMailBody = StrMailBody & "'" & CmbType.Text & "'"
                    ' ''StrMailBody = StrMailBody & " starting from " & TxtFrom.Text & " to " & TxtTo.Text & " " & vbCrLf & vbCrLf
                    ' ''StrMailBody = StrMailBody & "Reason/Remarks: " & TxtRemarks.Text & vbCrLf & vbCrLf
                    ' ''StrMailBody = StrMailBody & "Best Regards," & vbCrLf & vbCrLf
                    ' ''StrMailBody = StrMailBody & StrEmpName & vbCrLf & vbCrLf
                    ' ''StrMailBody = StrMailBody & "Click On Link Below To Approve:" & " " & "http://192.168.64.18/ramus"
                    ' ''mail.Body = StrMailBody
                    ' ''SmtpServer.Send(mail)
                    ' ''LblResults.Visible = True
                    ' ''LblResults.Text = "Leave Application Sent Successfully!!!"
                Catch ex As Exception
                    LblResults.Visible = True
                    LblResults.Text = "Leave Application Failed!!!"
                End Try
                FillGrid()
                ClearAll()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try

        End If
        If e.Item.CommandName = "Delete" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "DELETE FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
                Cmd.ExecuteNonQuery()
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If Session("Level") <> "Leave Application" Then
            If e.Item.CommandName = "Next" Then
                Try
                    Cmd.Connection = Con
                    Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID > '" & TxtEmpID.Text & "'"
                    TxtEmpID.Text = Cmd.ExecuteScalar
                    TxtEmpID_TextChanged(Me, ev)
                Catch ex As Exception

                End Try

            End If
        End If
        If Session("Level") <> "Leave Application" Then
            If e.Item.CommandName = "Previous" Then
                Try
                    Cmd.Connection = Con
                    Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID < '" & TxtEmpID.Text & "' ORDER BY EmpID DESC"
                    TxtEmpID.Text = Cmd.ExecuteScalar
                    TxtEmpID_TextChanged(Me, ev)
                Catch ex As Exception

                End Try
            End If
        End If
        If Session("Level") <> "Leave Application" Then
            If e.Item.CommandName = "Find" Then
                Session.Add("PstrCode", TxtEmpID.Text)
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
            End If
        End If
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        Dim IntLocal As Integer = 0
        Dim IntSick As Integer = 0
        If Rdr.Read Then
            On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
            TxtTitle.Text = Rdr("Title")
            TxtCategory.Text = Rdr("Category")
            TxtGender.Text = Rdr("Gender")
            TxtCategory.Text = Rdr("Category")
            ChkClocked.Checked = IIf(Rdr("Clocked"), True, False)
            TxtClockID.Text = Rdr("BatchNo")
            IntLocal = Rdr("Local")
            IntSick = Rdr("Sick")
            Session.Add("EmpID", Rdr("EmpID"))
            ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & Rdr("EmpID")
            Dim SngCasual As Single = 0
            Dim SngSick As Single = 0
            Dim IntVacTaken As Integer = 0
            FillGrid()
            StrSql = "SELECT * FROM Leave WHERE Leave.EmpID = '" & TxtEmpID.Text & "'"
            StrSql = StrSql + " AND Year(Leave.[Date]) = " & CDate(TxtDate.Text).Year & " ORDER BY [Leave].[Date]"
            TimexCmd.CommandText = StrSql
            TimexRdr = TimexCmd.ExecuteReader
            While TimexRdr.Read
                If TimexRdr("Type") = "Local Leave" Then
                    SngCasual = SngCasual + TimexRdr("Leave")
                End If
                If TimexRdr("Type") = "Sick Leave" Then
                    SngSick = SngSick + TimexRdr("Leave")
                End If
            End While
            TxtLocal.Text = IntLocal - SngCasual
            TxtSick.Text = IntSick - SngSick

            If CDate(TxtDate.Text).Year = Date.Today.Year Then
                Dim StrEndDate As String = "31-Dec-" & Date.Today.Year - 1
                Dim SngSickTaken As Single = 0
                Dim SngSickAcc As Single = 0
                StrSql = "SELECT SUM(Leave) FROM Leave WHERE EmpID = '" & TxtEmpID.Text & "' AND Type = 'SICK LEAVE' And [Date] >='01-Jan-2009' And [Date] <= '" & StrEndDate & "'"
                TimexCmd.CommandText = StrSql
                SngSickTaken = TimexCmd.ExecuteScalar
                StrSql = "SELECT SUM(SickYear) FROM LeaveHistory WHERE EmpID = '" & TxtEmpID.Text & "' And [Year] < " & CDate(TxtDate.Text).Year
                TimexCmd.CommandText = StrSql
                SngSickAcc = TimexCmd.ExecuteScalar
                If SngSickAcc <> 0 Then
                    If SngSickAcc - SngSickTaken < 0 Then
                        SngSickAcc = 0
                    Else
                        SngSickAcc = SngSickAcc - SngSickTaken
                        If SngSickAcc > 90 Then
                            SngSickAcc = 90
                        End If
                    End If
                End If
                TxtAccSick.Text = SngSickAcc
                If CDate(TxtDate.Text).Year < Date.Today.Year Then
                    StrEndDate = "31-Dec-" & Date.Today.Year
                    SngSickTaken = 0
                    SngSickAcc = 0
                    StrSql = "SELECT SUM(Leave) FROM Leave WHERE EmpID = '" & TxtEmpID.Text & "' AND Type = 'SICK LEAVE' And [Date] >='01-Jan-2009' And [Date] <= '" & StrEndDate & "'"
                    TimexCmd.CommandText = StrSql
                    SngSickTaken = TimexCmd.ExecuteScalar
                    StrSql = "SELECT SUM(SickYear) FROM LeaveHistory WHERE EmpID = '" & TxtEmpID.Text & "' And [Year] < " & CDate(TxtDate.Text).Year
                    TimexCmd.CommandText = StrSql
                    SngSickAcc = TimexCmd.ExecuteScalar
                    If SngSickAcc <> 0 Then
                        If SngSickAcc - SngSickTaken < 0 Then
                            SngSickAcc = 0
                        Else
                            SngSickAcc = SngSickAcc - SngSickTaken
                            If SngSickAcc > 90 Then
                                SngSickAcc = 90
                            End If
                        End If
                    End If
                    TxtAccSick.Text = SngSickAcc
                End If
                StrSql = "Select Sum(Leave) From Leave Where EmpID = '" & TxtEmpID.Text
                StrSql = StrSql & "' And Type = 'SICK LEAVE' And Date>='01/01/2009' And Accumulated = 1"
                TimexCmd.CommandText = StrSql
                TxtAccSick.Text = Val(TxtAccSick.Text) - TimexCmd.ExecuteScalar
            Else
                Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(1))
                ToolBar1_ItemClick(Me, ev)
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('Invalid Employee ID','Error');</script>", False)
            End If
        End If
        Rdr.Close()
    End Sub

    Private Sub GdvLeave_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvLeave.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvLeave, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvLoan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvLeave.SelectedIndexChanged
        On Error Resume Next
        TxtDate.Text = GdvLeave.SelectedRow.Cells(0).Text
        TxtFrom.Text = GdvLeave.SelectedRow.Cells(1).Text
        TxtTo.Text = GdvLeave.SelectedRow.Cells(2).Text
        CmbType.Text = GdvLeave.SelectedRow.Cells(3).Text
        TxtLeave.Text = GdvLeave.SelectedRow.Cells(4).Text
        CmbCategory.Text = GdvLeave.SelectedRow.Cells(5).Text
        TxtRemarks.Text = GdvLeave.SelectedRow.Cells(6).Text.Replace("&nbsp;", "")
    End Sub

    Private Sub GdvLeave_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvLeave.SelectedIndexChanging
        GdvLeave.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvLeave.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvLeave.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub
End Class