DROP TABLE [InterMart].[dbo].[prgfParams] 

CREATE TABLE prgfParams (
    prgfId int, 
	prgfGovtRate real default(0), prgfEmployerRate real default(0), 
	prgfSalaryLimit real default(0), 
	prevMonthsProcessed smallint default(0),
	prgfPrevMonths2020 varchar(255) default(''),
	performPRGF smallint default(0)
);

--Insert table in prgfParams
INSERT INTO prgfParams 
(prgfId, prgfGovtRate, prgfEmployerRate, prgfSalaryLimit, prevMonthsProcessed, prgfPrevMonths2020,performPRGF) 
VALUES 
(1, 0.0, 4.5, 200000, 0, 'January;February;March',0);

