-- Employee expat
ALTER TABLE [InterMart].[dbo].[Employee] ADD Expatriate smallint default(0);
UPDATE [InterMart].[dbo].[Employee] set Expatriate = 0
SELECT * FROM [InterMart].[dbo].[Employee]

--Create table prgfParams
CREATE TABLE prgfParams (
    prgfId int, prgfGovtRate real default(0), prgfEmployerRate real default(0), prgfSalaryLimit real default(0), prgfJanFeb2020 smallint default(0)
);

--Insert table in prgfParams
INSERT INTO prgfParams (prgfId, prgfGovtRate, prgfEmployerRate, prgfSalaryLimit, prgfJanFeb2020) VALUES (1, 1.6, 2.9, 200000, 0);
UPDATE prgfParams SET prgfGovtRate=0.0, prgfEmployerRate=4.5 WHERE prgfId=1
SELECT * FROM [InterMart].[dbo].[prgfParams]

-- Salary
ALTER TABLE [InterMart].[dbo].[Salary] ADD prgfGovtRate real default(0), prgfGovtAmount real default(0), 
prgfEmployerRate real default(0), prgfEmployerAmount real default(0), prgfTotal real default(0), prgfTotalRemuneration real default(0); 

UPDATE [InterMart].[dbo].[Salary] SET prgfGovtRate=0, prgfGovtAmount=0, prgfEmployerRate=0, prgfEmployerAmount=0, prgfTotal=0, prgfTotalRemuneration=0 where prgfGovtRate is null
SELECT * FROM [InterMart].[dbo].[Salary]

-- Insert in table MnuMast
INSERT INTO [InterMart].[dbo].[MnuMast] ([Name],[Caption]) VALUES ('PRGFMonthlyReturn','Export Monthly PRGF')
INSERT INTO [InterMart].[dbo].[MnuMast]([Name],[Caption]) VALUES ('MnuRepPRGFYTD', 'PRGF YTD')

-- Access to report PRGF Monthly Return
INSERT INTO [InterMart].[dbo].[Access] ([UserId],[MenuName],[Description]) VALUES ('SLL', 'PRGFMonthlyReturn','Export Monthly PRGF')
INSERT INTO [InterMart].[dbo].[Access] ([UserId],[MenuName],[Description]) VALUES ('manish', 'PRGFMonthlyReturn','Export Monthly PRGF')

-- Access to report PRGF YTD
INSERT INTO [InterMart].[dbo].[Access]([UserId],[MenuName],[Description]) VALUES ('SLL', 'MnuRepPRGFYTD','PRGF YTD')
INSERT INTO [InterMart].[dbo].[Access]([UserId],[MenuName],[Description]) VALUES ('manish', 'MnuRepPRGFYTD','PRGF YTD')

-- Employer details
SELECT * FROM [InterMart].[dbo].[System]

