﻿Public Partial Class FrmGroupTraining
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            FillGrid()
            TxtDate.Text = Date.Today
            Cmd.Connection = Con
            Cmd.CommandText = "Select CourseCode From CourseMaster"
            Rdr = Cmd.ExecuteReader
            CmbTraining.Items.Add(" ")
            While Rdr.Read
                CmbTraining.Items.Add(Rdr("CourseCode"))
            End While
            Rdr.Close()
            CmbTraining.SelectedIndex = 0
        End If
    End Sub

    Protected Sub FillGrid()
        Cmd.Connection = Con
        StrSql = "SELECT EmpID, [Last], [First], Position FROM Employee Where Working =1"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvCheckList.DataSource = Rdr
        GdvCheckList.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ClearAll()
        TxtDate.Text = ""
        TxtStartDate.Text = ""
        TxtEndDate.Text = ""
        CmbTraining.SelectedIndex = 0
        TxtContent.Text = ""
        TxtHours.Text = ""
        TxtTimein.Text = ""
        TxtTimeOut.Text = ""
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ev As New System.EventArgs
        If e.Item.CommandName = "New" Then
            ClearAll()
            TxtDate.Text = ""
            TxtDate.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            If TxtDate.Text = "" Then
                LblError.Visible = True
                LblError.Text = "Please Input Date!!!"
                Exit Sub
            End If
            If CmbTraining.Text = "" Then
                LblError.Visible = True
                LblError.Text = "Please Select Training Title!!!"
                Exit Sub
            End If
            If TxtStartDate.Text = "" Then
                LblError.Visible = True
                LblError.Text = "Please Select Start Date!!!"
                Exit Sub
            End If
            If TxtEndDate.Text = "" Then
                LblError.Visible = True
                LblError.Text = "Please Select End Date!!!"
                Exit Sub
            End If
            Dim StrGroup As String
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Cmd.Connection = Con
            Cmd.CommandText = "Delete From GroupTraining Where Date = '" & Format(CDate(TxtDate.Text), "dd-MMM-yyyy") & "' And TrainingTitle = '" & CmbTraining.Text & "'"
            Cmd.ExecuteNonQuery()
            StrGroup = "Insert Into GroupTraining([Date],StartDate,EndDate,TrainingTitle,"
            StrGroup = StrGroup & "TrainingDetails,TrainingHours,StartTime,EndTime,Award,NonAward,TrainingPassport)Values('"
            StrGroup = StrGroup & Format(CDate(TxtDate.Text), "dd-MMM-yyyy") & "','"
            StrGroup = StrGroup & Format(CDate(TxtStartDate.Text), "dd-MMM-yyyy") & "','"
            StrGroup = StrGroup & Format(CDate(TxtEndDate.Text), "dd-MMM-yyyy") & "','"
            StrGroup = StrGroup & CmbTraining.Text & "','"
            StrGroup = StrGroup & TxtContent.Text & "','"
            StrGroup = StrGroup & Val(TxtHours.Text) & "','"
            StrGroup = StrGroup & TxtTimein.Text & "','"
            StrGroup = StrGroup & TxtTimeOut.Text & "',"
            If ChkAward.Checked Then
                StrGroup = StrGroup & 1 & ","
            Else
                StrGroup = StrGroup & 0 & ","
            End If
            If ChkNonAward.Checked Then
                StrGroup = StrGroup & 1 & ","
            Else
                StrGroup = StrGroup & 0 & ","
            End If
            If ChkTainingPassport.Checked Then
                StrGroup = StrGroup & 1 & ")"
            Else
                StrGroup = StrGroup & 0 & ")"
            End If
            Cmd.CommandText = StrGroup
            Cmd.ExecuteNonQuery()
            For Each Row As GridViewRow In GdvCheckList.Rows
                If DirectCast(Row.FindControl("ChkSubmitted"), CheckBox).Checked Then
                    StrSql = "Insert Into GroupTrainingAttend ([Date],EmpID,TrainingTitle,Attended) VALUES('"
                    StrSql = StrSql & Format(CDate(TxtDate.Text), "dd-MMM-yyyy") & "','"
                    StrSql = StrSql & Row.Cells(0).Text & "','"
                    StrSql = StrSql & CmbTraining.Text & "',1)"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                End If
            Next
            TxtDate.Text = ""
            TxtStartDate.Text = ""
            TxtEndDate.Text = ""
            CmbTraining.SelectedIndex = 0
            TxtContent.Text = ""
            TxtHours.Text = ""
            TxtTimein.Text = ""
            TxtTimeOut.Text = ""
        End If
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtDate.TextChanged
        StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtDate.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        If Rdr.Read Then
            On Error Resume Next
            
            Session.Add("EmpID", Rdr("EmpID"))

        Else
            Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(1))
            ToolBar1_ItemClick(Me, ev)
            'ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('Invalid Employee ID','Error');</script>", False)
        End If
        Rdr.Close()
        Cmd.CommandText = "SELECT * FROM CheckList WHERE EmpID = '" & TxtDate.Text & "'"
        Rdr = Cmd.ExecuteReader
        For Each Row As GridViewRow In GdvCheckList.Rows
            DirectCast(Row.FindControl("ChkSubmitted"), CheckBox).Checked = False
            Row.Cells(2).Text = ""
        Next
        While Rdr.Read
            For Each Row As GridViewRow In GdvCheckList.Rows
                If Not IsDBNull(Rdr("ExpiryDate")) Then
                    Row.Cells(2).Text = Rdr("ExpiryDate")
                End If
                If Row.Cells(0).Text = Rdr("Description") Then
                    DirectCast(Row.FindControl("ChkSubmitted"), CheckBox).Checked = True
                    Exit For
                End If
            Next
        End While
        Rdr.Close()
    End Sub

    Protected Sub GdvCheckList_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GdvCheckList.SelectedIndexChanged
        Session("Selected") = GdvCheckList.SelectedIndex
    End Sub

    Private Sub GdvCheckList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvCheckList.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvCheckList, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvCheckList_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvCheckList.SelectedIndexChanging
        GdvCheckList.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvCheckList.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvCheckList.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Protected Sub CmbTraining_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbTraining.SelectedIndexChanged
        Dim CmdTrMaster As New OleDb.OleDbCommand
        Dim RdrTrMaster As OleDb.OleDbDataReader
        CmdTrMaster.Connection = Con
        CmdTrMaster.CommandText = "Select *From CourseMaster Where CourseCode = '" & CmbTraining.Text & "'"
        RdrTrMaster = CmdTrMaster.ExecuteReader
        If RdrTrMaster.Read Then
            TxtName.Text = RdrTrMaster("Name")
            TxtHours.Text = RdrTrMaster("Hours")
            TxtContent.Text = RdrTrMaster("Content")
        End If
        RdrTrMaster.Close()
    End Sub
End Class