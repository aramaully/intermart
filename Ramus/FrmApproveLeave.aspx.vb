﻿Imports System.Net.Mail
Partial Public Class FrmApproveLeave
    Inherits System.Web.UI.Page
    Dim TimexCon As New OleDb.OleDbConnection
    Dim TimexCmd As New OleDb.OleDbCommand
    Dim RosterCmd As New OleDb.OleDbCommand
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdEmp As New OleDb.OleDbCommand
    Dim RdrEmp As OleDb.OleDbDataReader
    Dim CmdDept As New OleDb.OleDbCommand
    Dim RdrDept As OleDb.OleDbDataReader
    Dim CmdApproval As New OleDb.OleDbCommand
    Dim RdrApproval As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdLeave As New OleDb.OleDbCommand
    Dim StrEmpName As String
    Dim StrEmpEmail As String
    Dim StrEmpGrade As String
    Dim StrDeptCode As String
    Dim StrHrEmail As String
    Dim StrGrade3 As String
    Dim StrGrade4 As String
    Dim StrGrade5 As String
    Dim StrApprovalEmail As String
    Dim StrApprovalName As String
    Dim CmdLeaveBal As New OleDb.OleDbCommand
    Dim RdrLeaveBal As OleDb.OleDbDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        TimexCon.ConnectionString = Session("ConnString")
        TimexCon.Open()
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT DeptName FROM Dept"
            Rdr = Cmd.ExecuteReader
            CmbDept.Items.Add("ALL")
            While Rdr.Read
                CmbDept.Items.Add(Rdr("DeptName"))
            End While
            Rdr.Close()
            If Session("Level") = "HOD Processing" Then
                TxtEmpID.Enabled = False
                CmbDept.Text = Session("DeptName")
                CmbDept.Enabled = False
            End If
            Cmd.CommandText = "SELECT Name FROM LeaveMaster"
            Rdr = Cmd.ExecuteReader
            CmbApproved.Items.Add("Select...")
            While Rdr.Read
                CmbApproved.Items.Add(Rdr("Name"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If e.Item.CommandName = "Refresh" Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            If TxtFromDate.Text = "" Then
                LblResults.Visible = True
                LblResults.Text = "Please Select From Date!!!"
                Exit Sub
            End If
            If TxtToDate.Text = "" Then
                LblResults.Visible = True
                LblResults.Text = "Please Select To Date!!!"
                Exit Sub
            End If
            If CDate(TxtFromDate.Text) > CDate(TxtToDate.Text) Then
                LblResults.Visible = True
                LblResults.Text = "Invalid Date Please Enter Proper Date!!!"
                Exit Sub
            End If
            'If Session("Level") = "HOD Processing" Then
            '    StrSql = "SELECT LeaveApp.EmpId,Employee.Title+' '+Employee.[Last]+' '+Employee.[First] AS Name,"
            '    StrSql = StrSql & "LeaveApp.Type,LeaveApp.Category , LeaveApp.LeaveApproved, LeaveApp.Leave, AppliedDate, [From], "
            '    StrSql = StrSql & "[To], Approved, LeaveApp.Remarks, ApprovalRemarks "
            '    StrSql = StrSql & "FROM LeaveApp INNER JOIN Employee on Employee.EmpID = LeaveApp.EmpId And Employee.DeptCode = '" & CmbDept.Text & "' And LeaveApp.[From] >= '" & Format(CDate(TxtFromDate.Text), "dd-MMM-yyyy") & "' And LeaveApp.[To]<='" & Format(CDate(TxtToDate.Text), "dd-MMM-yyyy") & "'"
            'Else
            If CmbDept.Text = "ALL" Then
                StrSql = "SELECT LeaveApp.EmpId,Employee.Title+' '+Employee.[Last]+' '+Employee.[First] AS Name,"
                StrSql = StrSql & "LeaveApp.Type, LeaveApp.Category, LeaveApp.LeaveApproved, LeaveApp.Leave, AppliedDate, [From], "
                StrSql = StrSql & "[To], Approved, LeaveApp.Remarks, ApprovalRemarks "
                StrSql = StrSql & "FROM LeaveApp INNER JOIN Employee on Employee.EmpID = LeaveApp.EmpId And LeaveApp.[From] >= '" & Format(CDate(TxtFromDate.Text), "dd-MMM-yyyy") & "' And LeaveApp.[To]<='" & Format(CDate(TxtToDate.Text), "dd-MMM-yyyy") & "'"
            Else
                StrSql = "SELECT LeaveApp.EmpId,Employee.Title+' '+Employee.[Last]+' '+Employee.[First] AS Name,"
                StrSql = StrSql & "LeaveApp.Type,LeaveApp.Category , LeaveApp.LeaveApproved, LeaveApp.Leave, AppliedDate, [From], "
                StrSql = StrSql & "[To], Approved, LeaveApp.Remarks, ApprovalRemarks "
                StrSql = StrSql & "FROM LeaveApp INNER JOIN Employee on Employee.EmpID = LeaveApp.EmpId And Employee.DeptName = '" & CmbDept.Text & "' And LeaveApp.[From] >= '" & Format(CDate(TxtFromDate.Text), "dd-MMM-yyyy") & "' And LeaveApp.[To]<='" & Format(CDate(TxtToDate.Text), "dd-MMM-yyyy") & "'"
            End If
            'End If
            TimexCmd.Connection = TimexCon
            TimexCmd.CommandText = StrSql
            Rdr = TimexCmd.ExecuteReader
            GdvLeave.DataSource = Rdr
            GdvLeave.DataBind()
            Rdr.Close()
        End If
    End Sub

    Private Sub GdvLeave_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvLeave.RowDataBound
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvLeave, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvLoan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvLeave.SelectedIndexChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        On Error Resume Next
        TxtAppliedfor.Text = GdvLeave.SelectedRow.Cells(2).Text
        TxtLCategory.Text = GdvLeave.SelectedRow.Cells(3).Text
        CmbApproved.Text = GdvLeave.SelectedRow.Cells(4).Text
        TxtLeave.Text = GdvLeave.SelectedRow.Cells(5).Text
        TxtDate.Text = GdvLeave.SelectedRow.Cells(6).Text
        TxtFrom.Text = GdvLeave.SelectedRow.Cells(7).Text
        TxtTo.Text = GdvLeave.SelectedRow.Cells(8).Text
        ChkApproved.Checked = DirectCast(GdvLeave.SelectedRow.Cells(9).Controls(0), CheckBox).Checked
        TxtAppRemarks.Text = GdvLeave.SelectedRow.Cells(10).Text.Replace("&nbsp;", "")
        TxtRemarks.Text = GdvLeave.SelectedRow.Cells(11).Text.Replace("&nbsp;", "")
        TxtEmpID.Text = GdvLeave.SelectedRow.Cells(0).Text
        TxtEmpID_TextChanged(Me, e)
    End Sub

    Private Sub GdvLeave_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvLeave.SelectedIndexChanging
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        GdvLeave.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        For i = 0 To Me.GdvLeave.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvLeave.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub


    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        'ClearAll()
        Dim IntLocal As Integer = 0
        Dim IntSick As Integer = 0
        If Rdr.Read Then
            On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
            TxtTitle.Text = Rdr("Title")
            TxtCategory.Text = Rdr("Category")
            TxtGender.Text = Rdr("Gender")
            TxtCategory.Text = Rdr("Category")
            ChkClocked.Checked = IIf(Rdr("Clocked"), True, False)
            TxtClockID.Text = Rdr("BatchNo")
            IntLocal = Rdr("Local")
            IntSick = Rdr("Sick")
            Session.Add("EmpID", Rdr("EmpID"))
            ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & Rdr("EmpID")
        End If
        Dim SngCasual As Single = 0
        Dim SngSick As Single = 0
        CmdLeaveBal.Connection = Con
        StrSql = "SELECT * FROM Leave WHERE Leave.EmpID = '" & TxtEmpID.Text & "'"
        StrSql = StrSql + " AND Year(Leave.[Date]) = " & CDate(TxtDate.Text).Year & " ORDER BY [Leave].[Date]"
        CmdLeaveBal.CommandText = StrSql
        RdrLeaveBal = CmdLeaveBal.ExecuteReader
        While RdrLeaveBal.Read
            If RdrLeaveBal("Type") = "Local Leave" Then
                SngCasual = SngCasual + RdrLeaveBal("Leave")
            End If
            If RdrLeaveBal("Type") = "Sick Leave" Then
                SngSick = SngSick + RdrLeaveBal("Leave")
            End If
        End While
        TxtLocal.Text = IntLocal - SngCasual
        TxtSick.Text = IntSick - SngSick
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim CmdChkApproval As New OleDb.OleDbCommand
        Dim RdrChkApproval As OleDb.OleDbDataReader
        Dim CmdChkAccessLevel As New OleDb.OleDbCommand
        CmdChkApproval.Connection = Con
        CmdChkAccessLevel.Connection = Con
        CmdChkApproval.CommandText = "Select Grade From Employee Where EmpID = '" & TxtEmpID.Text & "'"
        RdrChkApproval = CmdChkApproval.ExecuteReader
        If RdrChkApproval.Read Then
            If RdrChkApproval("Grade") = "4" Then
                CmdChkAccessLevel.CommandText = "Select Count(*) From Access Where MenuName = 'ApproveHODLeave' And UserID = '" & Session("UserID") & "'"
                If CmdChkAccessLevel.ExecuteScalar > 0 Then

                Else
                    LblResults.Visible = True
                    LblResults.Text = "You Are Not Allowed to Approve this Leave"
                    Exit Sub
                End If
            ElseIf RdrChkApproval("Grade") = "3" Or RdrChkApproval("Grade") = "2" Or RdrChkApproval("Grade") = "1" Then
                CmdChkAccessLevel.CommandText = "Select Count(*) From Access Where MenuName = 'ApproveAssociateLeave' And UserID = '" & Session("UserID") & "'"
                If CmdChkAccessLevel.ExecuteScalar > 0 Then

                Else
                    LblResults.Visible = True
                    LblResults.Text = "You Are Not Allowed to Approve this Leave"
                    Exit Sub
                End If
            End If
        End If
        RdrChkApproval.Close()
        Dim DteFrom As Date
        Dim DteTo As Date
        DteFrom = CDate(TxtFrom.Text)
        DteTo = CDate(TxtTo.Text)
        If CDate(TxtFrom.Text) > CDate(TxtTo.Text) Then
            LblResults.Visible = True
            LblResults.Text = "Invalid Date Please Enter Proper Date!!!"
            Exit Sub
        End If
        If TxtEmpID.Text <> "" Then
            StrSql = "UPDATE LeaveApp SET "
            If CmbApproved.Text <> "Select..." Then
                StrSql = StrSql & "LeaveApproved = '" & CmbApproved.Text & "',"
            End If
            StrSql = StrSql & "Approved = 1,"
            If ChkApproved.Checked Then
                StrSql = StrSql & "LeaveDateApproved = '" & Format(Date.Today, "dd-MMM-yyyy") & "',"
            End If
            StrSql = StrSql & "ApprovalRemarks = '" & TxtRemarks.Text & "' "
            StrSql = StrSql & "WHERE EmpID = '" & TxtEmpID.Text & "' "
            StrSql = StrSql & "AND [From] = '" & Format(CDate(TxtFrom.Text), "dd-MMM-yyyy") & "' "
            StrSql = StrSql & "AND [To] = '" & Format(CDate(TxtTo.Text), "dd-MMM-yyyy") & "' "
            StrSql = StrSql & "AND Type = '" & TxtAppliedfor.Text & "'"
            TimexCmd.CommandText = StrSql
            TimexCmd.Connection = TimexCon
            CmdEmp.Connection = Con
            CmdEmp.CommandText = "Select *From Employee Where EmpID = '" & TxtEmpID.Text & "'"
            RdrEmp = CmdEmp.ExecuteReader
            If RdrEmp.Read Then
                StrEmpName = RdrEmp("Last") & " " & RdrEmp("First")
                StrEmpEmail = RdrEmp("Email")
                StrEmpGrade = RdrEmp("Grade")
                StrDeptCode = RdrEmp("DeptCode")
                If StrDeptCode <> "" Then
                    CmdDept.Connection = Con
                    CmdDept.CommandText = "Select *From Dept Where DeptName = '" & RdrEmp("DeptCode") & "'"
                    RdrDept = CmdDept.ExecuteReader
                    If RdrDept.Read Then
                        StrHrEmail = RdrDept("HREmail")
                        StrGrade3 = RdrDept("Grade3") & ""
                        StrGrade4 = RdrDept("Grade4") & ""
                        StrGrade5 = RdrDept("Grade5") & ""
                    End If
                    RdrDept.Close()
                End If
            End If
            RdrEmp.Close()
            CmdApproval.Connection = Con
            CmdApproval.CommandText = "Select *From Employee Where EmpID = '" & Session("EmpID") & "'"
            RdrApproval = CmdApproval.ExecuteReader
            If RdrApproval.Read Then
                StrApprovalEmail = RdrApproval("Email")
                StrApprovalName = RdrApproval("Last") & " " & RdrApproval("First")
            End If
            RdrApproval.Close()
            Try
                TimexCmd.ExecuteNonQuery()
                LblResults.Text = "Leave Approval Updated."
                Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(0))
                ToolBar1_ItemClick(Me, ev)
            Catch ex As Exception
                LblResults.Text = "Error Updating.. " & ex.Message
            End Try
            Try
                Dim SmtpServer As New SmtpClient()
                Dim mail As New MailMessage()
                Dim StrMailBody As String
                StrMailBody = ""
                SmtpServer.Credentials = New Net.NetworkCredential("hrms@angsana.com", "1234")
                SmtpServer.Port = 25
                SmtpServer.Host = "smtp.intnet.mu"
                mail = New MailMessage()
                mail.From = New MailAddress(StrApprovalEmail)
                If StrEmpEmail <> "" Then
                    mail.To.Add(StrEmpEmail)
                End If
                If StrEmpGrade = "1" Or StrEmpGrade = "2" Then
                    If StrGrade3 <> "" Then
                        mail.CC.Add(StrGrade3)
                    End If
                    If StrGrade4 <> "" Then
                        mail.CC.Add(StrGrade4)
                    End If
                    mail.CC.Add(StrHrEmail)
                End If
                If StrEmpGrade = "3" Then
                    If StrGrade4 <> "" Then
                        mail.CC.Add(StrGrade4)
                    End If
                    mail.CC.Add(StrHrEmail)
                End If
                If StrEmpGrade = "4" Then
                    If StrGrade5 <> "" Then
                        mail.CC.Add(StrGrade5)
                    End If
                    mail.CC.Add(StrHrEmail)
                End If
                If StrEmpGrade = "5" Then
                    mail.CC.Add(StrHrEmail)
                End If
                mail.Subject = "Leave Approval From " & TxtFrom.Text & " To " & TxtTo.Text & ""
                StrMailBody = "Dear " & UCase(TxtFirst.Text) & "," & vbCrLf & vbCrLf
                StrMailBody = StrMailBody & "This is to inform you that your "
                If CmbApproved.Text <> "Select..." Then
                    StrMailBody = StrMailBody & "'" & CmbApproved.Text & "'"
                Else
                    StrMailBody = StrMailBody & "'" & TxtAppliedfor.Text & "'"
                End If
                StrMailBody = StrMailBody & " application from " & TxtFrom.Text & " to " & TxtTo.Text & " has been approved" & vbCrLf & vbCrLf
                StrMailBody = StrMailBody & TxtRemarks.Text & vbCrLf & vbCrLf
                StrMailBody = StrMailBody & "Best Regards" & vbCrLf & vbCrLf
                StrMailBody = StrMailBody & StrApprovalName
                mail.Body = StrMailBody
                SmtpServer.Send(mail)
                LblResults.Visible = True
                LblResults.Text = "Leave Approval Sent Successfully!!!"
            Catch ex As Exception
                LblResults.Visible = True
                LblResults.Text = "Leave Approval Failed!!!"
            End Try

            If ChkApproved.Checked Then
                LblResults.Visible = True
                LblResults.Text = "Leave Already Approved!!!"
                Exit Sub
            Else
                Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                While DteFrom <= DteTo
                    RosterCmd.Connection = Con
                    RosterCmd.CommandText = "Select Count(*) From Roster Where EmpID = '" & TxtEmpID.Text & "' And [Date]= '" & Format(DteFrom, "dd-MMM-yyyy") & "' And Shift = 'OFF'"
                    If RosterCmd.ExecuteScalar = 0 Then
                        CmdLeave.Connection = Con
                        CmdLeave.CommandText = "Delete From Leave Where EmpID = '" & TxtEmpID.Text & "' And [Date] = '" & Format(DteFrom, "dd-MMM-yyyy") & "'"
                        CmdLeave.ExecuteNonQuery()
                        StrSql = "INSERT INTO Leave(EmpID,[Date],Type,Leave,[Year],Category,Accumulated) "
                        StrSql = StrSql & "VALUES('"
                        StrSql = StrSql & TxtEmpID.Text & "','"
                        StrSql = StrSql & Format(DteFrom, "dd-MMM-yyyy") & "','"
                        If CmbApproved.Text <> "Select..." Then
                            StrSql = StrSql & CmbApproved.Text & "','"
                        Else
                            StrSql = StrSql & TxtAppliedfor.Text & "','"
                        End If
                        StrSql = StrSql & TxtLeave.Text & "',"
                        StrSql = StrSql & Year(DteFrom) & ",'"
                        StrSql = StrSql & TxtLCategory.Text & "',"
                        StrSql = StrSql & IIf(ChkAccLeave.Checked, 1, 0) & ")"
                        Try
                            Cmd.Connection = Con
                            Cmd.CommandText = StrSql
                            Cmd.ExecuteNonQuery()
                        Catch ex As Exception
                            LblResults.Text = "Error Updating.. " & ex.Message
                        End Try
                    End If
                    DteFrom = DteFrom.AddDays(1)
                End While
                TxtEmpID.Text = ""
                TxtFirst.Text = ""
                TxtLast.Text = ""
                TxtClockID.Text = ""
                ChkClocked.Checked = False
                TxtCategory.Text = ""
                TxtGender.Text = ""
                CmbDept.SelectedIndex = 0
                TxtDate.Text = ""
                TxtAppliedfor.Text = ""
                TxtLCategory.Text = ""
                CmbApproved.SelectedIndex = 0
                ChkApproved.Checked = False
                TxtAppRemarks.Text = ""
                ChkAccLeave.Checked = False
                TxtFrom.Text = ""
                TxtTo.Text = ""
                TxtRemarks.Text = ""
                TxtLeave.Text = ""
                ImgPhoto.ImageUrl = Nothing
                Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(0))
                ToolBar1_ItemClick(Me, ev)
            End If
        Else
            LblResults.Visible = True
            LblResults.Text = "Please Select Proper Records!!!"
        End If
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click
        If TxtEmpID.Text <> "" Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = "UPDATE LeaveApp SET "
            If CmbApproved.Text <> "Select..." Then
                StrSql = StrSql & "LeaveApproved = '" & CmbApproved.Text & "',"
            End If
            StrSql = StrSql & "Approved = 0,"
            If ChkApproved.Checked Then
                StrSql = StrSql & "LeaveDateApproved = '" & Format(Date.Today, "dd-MMM-yyyy") & "',"
            End If
            StrSql = StrSql & "ApprovalRemarks = '" & TxtRemarks.Text & "' "
            StrSql = StrSql & "WHERE EmpID = '" & TxtEmpID.Text & "' "
            StrSql = StrSql & "AND [From] = '" & Format(CDate(TxtFrom.Text), "dd-MMM-yyyy") & "' "
            StrSql = StrSql & "AND [To] = '" & Format(CDate(TxtTo.Text), "dd-MMM-yyyy") & "' "
            StrSql = StrSql & "AND Type = '" & TxtAppliedfor.Text & "'"
            TimexCmd.CommandText = StrSql
            TimexCmd.Connection = TimexCon
            CmdEmp.Connection = Con
            CmdEmp.CommandText = "Select *From Employee Where EmpID = '" & TxtEmpID.Text & "'"
            RdrEmp = CmdEmp.ExecuteReader
            If RdrEmp.Read Then
                StrEmpName = RdrEmp("Last") & " " & RdrEmp("First")
                StrEmpEmail = RdrEmp("Email")
                StrEmpGrade = RdrEmp("Grade")
                StrDeptCode = RdrEmp("DeptCode")
                If StrDeptCode <> "" Then
                    CmdDept.Connection = Con
                    CmdDept.CommandText = "Select *From Dept Where DeptName = '" & RdrEmp("DeptCode") & "'"
                    RdrDept = CmdDept.ExecuteReader
                    If RdrDept.Read Then
                        StrHrEmail = RdrDept("HREmail")
                        StrGrade3 = RdrDept("Grade3") & ""
                        StrGrade4 = RdrDept("Grade4") & ""
                        StrGrade5 = RdrDept("Grade5") & ""
                    End If
                    RdrDept.Close()
                End If
            End If
            RdrEmp.Close()
            CmdApproval.Connection = Con
            CmdApproval.CommandText = "Select *From Employee Where EmpID = '" & Session("EmpID") & "'"
            RdrApproval = CmdApproval.ExecuteReader
            If RdrApproval.Read Then
                StrApprovalEmail = RdrApproval("Email")
                StrApprovalName = RdrApproval("Last") & " " & RdrApproval("First")
            End If
            RdrApproval.Close()
            Try
                TimexCmd.ExecuteNonQuery()
                LblResults.Text = "Leave Approval Updated."
                Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(0))
                ToolBar1_ItemClick(Me, ev)
            Catch ex As Exception
                LblResults.Text = "Error Updating.. " & ex.Message
            End Try
            Try
                Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                Dim SmtpServer As New SmtpClient()
                Dim mail As New MailMessage()
                Dim StrMailBody As String
                StrMailBody = ""
                SmtpServer.Credentials = New Net.NetworkCredential("hrms@angsana.com", "1234")
                SmtpServer.Port = 25
                SmtpServer.Host = "smtp.intnet.mu"
                mail = New MailMessage()
                mail.From = New MailAddress(StrApprovalEmail)
                If StrEmpEmail <> "" Then
                    mail.To.Add(StrEmpEmail)
                End If
                If StrEmpGrade = "1" Or StrEmpGrade = "2" Then
                    If StrGrade3 <> "" Then
                        mail.CC.Add(StrGrade3)
                    End If
                    If StrGrade4 <> "" Then
                        mail.CC.Add(StrGrade4)
                    End If
                    mail.CC.Add(StrHrEmail)
                End If
                If StrEmpGrade = "3" Then
                    If StrGrade4 <> "" Then
                        mail.CC.Add(StrGrade4)
                    End If
                    mail.CC.Add(StrHrEmail)
                End If
                If StrEmpGrade = "4" Then
                    If StrGrade5 <> "" Then
                        mail.CC.Add(StrGrade5)
                    End If
                    mail.CC.Add(StrHrEmail)
                End If
                If StrEmpGrade = "5" Then
                    mail.CC.Add(StrHrEmail)
                End If
                mail.Subject = "Leave Approval From " & TxtFrom.Text & " To " & TxtTo.Text & ""
                StrMailBody = "Dear " & UCase(TxtFirst.Text) & "," & vbCrLf & vbCrLf
                StrMailBody = StrMailBody & "This is to inform you that your "
                If CmbApproved.Text <> "Select..." Then
                    StrMailBody = StrMailBody & "'" & CmbApproved.Text & "'"
                Else
                    StrMailBody = StrMailBody & "'" & TxtAppliedfor.Text & "'"
                End If
                StrMailBody = StrMailBody & " application from " & TxtFrom.Text & " to " & TxtTo.Text & " has been rejected " & vbCrLf & vbCrLf

                StrMailBody = StrMailBody & "Reason: " & TxtRemarks.Text & vbCrLf & vbCrLf

                StrMailBody = StrMailBody & "Best Regards" & vbCrLf & vbCrLf
                StrMailBody = StrMailBody & StrApprovalName
                mail.Body = StrMailBody
                SmtpServer.Send(mail)
                LblResults.Visible = True
                LblResults.Text = "Leave Rejected Sent Successfully!!!"
            Catch ex As Exception
                LblResults.Visible = True
                LblResults.Text = "Leave Rejected Failed!!!"
            End Try
        End If
    End Sub
End Class