﻿Imports System.Web.Security
Imports System.Data
Imports System.Data.OleDb
Imports System.IO

Imports System.Configuration
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web.CrystalReportViewer
Imports CrystalDecisions.Shared
Imports System.Drawing.Printing

Partial Public Class FrmPaySheet
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim StrSql As String
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            CmbMonth.SelectedIndex = Date.Today.Month - 1
            TxtYear.Text = Date.Today.Year
            TxtDate.Text = CDate("28-" & CmbMonth.Text & "-" & TxtYear.Text)
            'Outlet
            Cmd.Connection = Con
            Cmd.CommandText = "Select OutletName from Outlet Order by OutletCode"
            CmbOutlet.Items.Add("ALL")
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                CmbOutlet.Items.Add(Rdr("OutletName"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub CmbCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbCategory.SelectedIndexChanged
        If CmbCategory.Text = "Monthly" Then
            TxtDate.Text = CDate("28-" & CmbMonth.Text & "-" & TxtYear.Text)
            CmbMonth.Visible = True
            TxtDate.Visible = False
            LblDate.Text = "Month"
        ElseIf CmbCategory.Text = "Weekly" Then
            CmbMonth.Visible = False
            TxtDate.Text = Date.Today
            TxtDate.Visible = True
            LblDate.Text = "Week Ending"
        Else
            CmbMonth.Visible = False
            TxtDate.Text = Date.Today
            TxtDate.Visible = True
            LblDate.Text = "Fortnight Ending"
        End If
    End Sub

    Protected Sub CmbMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbMonth.SelectedIndexChanged
        If CmbMonth.Text = "Bonus" Then
            TxtDate.Text = CDate("30-December" & "-" & TxtYear.Text)
        Else
            TxtDate.Text = CDate("28-" & CmbMonth.Text & "-" & TxtYear.Text)
        End If

    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            Session("FieldCount") = 0
            Dim StrSelFormula As String
            Dim SngTotalEmp As Single = 0
            Dim SngTotalNew As Single = 0
            Dim SngTotalLeft As Single = 0
            Dim SngTotalMale As Single = 0
            Dim SngTotalFemale As Single = 0
            Dim StrDate As String
            If CmbMonth.Text <> "Bonus" Then
                Dim DtePayDate As Date = CDate("28-" + CmbMonth.Text + "-" + TxtYear.Text)
                StrDate = Format(DtePayDate, "yyyy,MM,dd")
            End If
            If ChkPSD.Checked Then
                Session("ReportFile") = "PaysheetDeptSum.rpt"
                Session("RepHead") = "Pay Sheet for the month of  " & CmbMonth.Text & ", " & TxtYear.Text
            ElseIf ChkOT.Checked Then
                Session("ReportFile") = "ROvertime.rpt"
                Session("RepHead") = "Overtime for the month of  " & CmbMonth.Text & ", " & TxtYear.Text
            ElseIf ChkPayrollList.Checked Then
                Session("ReportFile") = "RAllowDeduct.rpt"
                Session("RepHead") = "Payroll Calculation List for the month of " & CmbMonth.Text & ", " & TxtYear.Text
            ElseIf ChkEmpDet.checked Then
                Session("ReportFile") = "REmpMonDet.rpt"
                Session("RepHead") = "Employee Details List for the month of " & CmbMonth.Text & ", " & TxtYear.Text
            Else
                Session("ReportFile") = "Paysheet.rpt"
                Session("RepHead") = "Pay Sheet for the month of " & CmbMonth.Text & ", " & TxtYear.Text
            End If

            If CmbMonth.Text = "Bonus" Then
                If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
                    StrSelFormula = "{Salary.Month} = 'Bonus' And {Salary.Year} = " & TxtYear.Text & ""
                Else
                    StrSelFormula = "{Salary.Month} = 'Bonus' And {Salary.OutletName} = '" & Session("Level") & "' and {Salary.Year} = " & TxtYear.Text & ""
                End If
            Else
                If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
                    StrSelFormula = "{Salary.Month}= '" & Trim(CmbMonth.Text) & "' And {Salary.Year} = " & TxtYear.Text & " "
                    'StrSelFormula = "{Salary.Date}=date(" & StrDate & ")"
                Else
                    StrSelFormula = "{Salary.Month}= '" & Trim(CmbMonth.Text) & "' And {Salary.Year} = " & TxtYear.Text & " And {Salary.OutletName} = '" & Session("Level") & "'"
                    ' StrSelFormula = "{Salary.Date}=date(" & StrDate & ") And {Salary.OutletName} = '" & Session("Level") & "'"
                End If
            End If
            If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
                If CmbOutlet.Text <> "ALL" Then
                    StrSelFormula = StrSelFormula & " And {Salary.OutletName} = '" & Trim(CmbOutlet.Text) & "'"
                End If
            End If
            StrSelFormula = StrSelFormula
            Session("SelFormula") = StrSelFormula
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        End If

        ''End If
    End Sub
End Class