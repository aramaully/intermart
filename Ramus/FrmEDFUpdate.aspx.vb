﻿Public Partial Class FrmEDFUpdate
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdLM As New OleDb.OleDbCommand
    Dim RdrLM As OleDb.OleDbDataReader
    Dim CmdLock As New OleDb.OleDbCommand
    Dim RdrLock As OleDb.OleDbDataReader
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrSql As String
    Dim I As Integer
    Dim SngYOS As Single = 0
    Dim SngAge As Single = 0
    Dim IntYear As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            TxtYear.Text = Year(Date.Today)
            Lblerror.Visible = False
            'Outlet
            Cmd.Connection = Con
            Cmd.CommandText = "Select OutletName from Outlet Order by OutletCode"
            Rdr = Cmd.ExecuteReader
            CmbDept.Items.Add("Select...")
            While Rdr.Read
                CmbDept.Items.Add(Rdr("OutletName"))
            End While
            Rdr.Close()
        End If
    End Sub

    Private Sub CmbDept_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbDept.SelectedIndexChanged
        Dim Dt As New DataTable
        Dim Dc As New DataColumn
        Dim Dr As DataRow
        Dim Count As Integer
        Dim CmdEmp As New OleDb.OleDbCommand
        Dim RdrEmp As OleDb.OleDbDataReader

        Lblerror.Visible = False
        Lblerror.Text = ""

        Cmd.Connection = Con
        StrSql = "SELECT count(*) FROM Employee where Working = 1 and  OutletName = '" & CmbDept.Text & "'"
        Cmd.CommandText = StrSql
        Count = Cmd.ExecuteScalar

        For I As Integer = 0 To Count
            Dr = Dt.NewRow
            Dt.Rows.Add(Dr)
        Next
        GdvTVList.DataSource = Dt
        GdvTVList.DataBind()

        CmdEmp.Connection = Con
        StrSql = "SELECT * FROM Employee where Working = 1 and OutletName = '" & CmbDept.Text & "' order by EmpID"
        CmdEmp.CommandText = StrSql
        RdrEmp = CmdEmp.ExecuteReader
        I = 0
        While RdrEmp.Read
            SngYOS = Math.Round((DateDiff(DateInterval.Day, RdrEmp("HireDate"), Date.Today) / IntYear), 2)
            DirectCast(GdvTVList.Rows(I).FindControl("TxtEmpID"), TextBox).Text = RdrEmp("EmpID")
            DirectCast(GdvTVList.Rows(I).FindControl("TxtName"), TextBox).Text = RdrEmp("Last") & " " & RdrEmp("First")

            CmdLM.Connection = Con
            CmdLM.CommandText = "Select * from MasterEDF order by Category"
            RdrLM = CmdLM.ExecuteReader
            While RdrLM.Read
                DirectCast(GdvTVList.Rows(I).FindControl("CmbEDFCat"), DropDownList).Items.Add(RdrLM("Category"))
            End While
            RdrLM.Close()

            I = I + 1
        End While
    End Sub

    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnSave.Click
        Lblerror.Visible = False
        'If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
        'Else
        '    Lblerror.Visible = True
        '    Lblerror.Text = "No Access Right to Update EDF!!!"
        '    Exit Sub
        'End If

        Dim Trans As OleDb.OleDbTransaction
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Transaction = Trans
        CmdLock.Transaction = Trans
        CmdLM.Transaction = Trans
        Try
            For Each Row As GridViewRow In GdvTVList.Rows
                If Trim(DirectCast(Row.FindControl("CmbEDFCat"), DropDownList).Text) <> "" Then

                    Dim SngEDFAmount As Single
                    SngEDFAmount = 0
                    CmdLM.Connection = Con
                    CmdLM.CommandText = "Select * from MasterEDF where Category = '" & Trim(DirectCast(Row.FindControl("CmbEDFCat"), DropDownList).Text) & "'"
                    RdrLM = CmdLM.ExecuteReader
                    While RdrLM.Read
                        SngEDFAmount = RdrLM("Amount")
                    End While
                    RdrLM.Close()

                    StrSql = "Update Employee set TotEDF =" & SngEDFAmount & " where EmpID = '" & Trim(DirectCast(Row.FindControl("TxtEmpID"), TextBox).Text) & "'"
                    Cmd.Connection = Con
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                    StrSql = ""
                End If
            Next
            Trans.Commit()
            'Audit
            Dim StrAudit As String
            CmdAudit.Connection = Con
            StrAudit = "Employee EDF for " & CmbDept.Text & " Updated. "
            CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Update Employee EDF','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
            CmdAudit.ExecuteNonQuery()

            Lblerror.Visible = True
            Lblerror.Text = "Employee Master Updated with New EDF!!!"
        Catch ex As Exception
            Trans.Rollback()
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
        End Try
    End Sub

End Class