﻿Public Partial Class FrmAttendanceSheet
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim TimexCon As New OleDb.OleDbConnection
    Dim TimexCmd As New OleDb.OleDbCommand

    Dim RdrEmp As OleDb.OleDbDataReader
    Dim StrSqlEmp As String
    Dim StrSql As String
    Dim DteSt As Date
    Dim DteEnd As Date
    Dim StrSelFormula As String
    Dim StrDateFrom As String
    Dim StrDateTo As String
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            TxtEmpID.Text = ""
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim ev As New System.EventArgs

        If e.Item.CommandName = "Print" Then
            Session("FieldCount") = 0
            LblMessage.Visible = False
            If TxtDateFrom.Text = "" Then
                LblMessage.Visible = True
                LblMessage.Text = "Please Enter Date From!!!"
                Exit Sub
            End If
            If TxtDateTo.Text = "" Then
                LblMessage.Visible = True
                LblMessage.Text = "Please Enter Date To !!!"
                Exit Sub
            End If
            Session("ReportFile") = "RDailyInputEmp.rpt"

            Dim DteFromDate As Date = CDate(TxtDateFrom.Text)
            Dim DteToDate As Date = CDate(TxtDateTo.Text)
            StrDateFrom = Format(DteFromDate, "yyyy,MM,dd")
            StrDateTo = Format(DteToDate, "yyyy,MM,dd")

            StrSelFormula = "{DailyInput.Date}>=Date(" & StrDateFrom & ") And {DailyInput.Date} <= Date(" & StrDateTo & ")"
            If TxtLast.Text <> "" Then
                StrSelFormula = StrSelFormula & " And {DailyInput.EmpID} = '" & TxtEmpID.Text & "'"
            End If
        End If

        Session("SelFormula") = StrSelFormula
        Session("RepHead") = "Daily Input for the period " & StrDateFrom & " to " & StrDateTo
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)

    End Sub

    Private Sub TxtFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles TxtFind.Click
        Session.Add("PstrCode", TxtEmpID.Text)
        ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)

    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
            StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Else
            StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "' And OutletName = '" & Session("Level") & "'"
        End If
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        TxtLast.Text = ""
        If TxtEmpID.Text = "ALL" Then Exit Sub
        If Rdr.Read Then
            On Error Resume Next
            TxtLast.Text = Rdr("Last") + " " + Rdr("First")
        Else
            Call TxtFind_Click(Me, sender)
        End If
        Rdr.Close()

    End Sub
End Class