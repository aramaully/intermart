﻿Public Partial Class FrmEmol
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            CmbMonth.SelectedIndex = Date.Today.Month - 1
            TxtFrom.Text = "01/01/" & Date.Today.Year
            TxtTo.Text = "31/12/" & Date.Today.Year
            TxtEmpID.Text = "ALL"
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT DeptName FROM Dept"
            Rdr = Cmd.ExecuteReader
            CmbDept.Items.Add("ALL")
            While Rdr.Read
                CmbDept.Items.Add(Rdr("DeptName"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ClearAll()
        TxtFirst.Text = ""
        TxtLast.Text = ""
        TxtClockID.Text = ""
        TxtCategory.Text = ""
        TxtTitle.Text = ""
        TxtGender.Text = ""
        ImgPhoto.ImageUrl = Nothing
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ev As New System.EventArgs
        If e.Item.CommandName = "New" Then
            ClearAll()
            TxtEmpID.Text = "ALL"
        End If
        If e.Item.CommandName = "Print" Then
            Session("FieldCount") = 0
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Dim StrSelFormula As String
            Dim DteFrom As Date
            Dim DteTo As Date
            Dim HireDate As Date
            Dim LastDay As Date

            If Trim(TxtCategory.Text) <> "" Then
                LastDay = TxtCategory.Text
            End If
            DteFrom = TxtFrom.Text
            DteTo = TxtTo.Text

            StrSelFormula = "{Salary.Date} >= Date(" & Format(CDate(TxtFrom.Text), "yyyy,MM,dd") & ") "
            StrSelFormula = StrSelFormula & " And {Salary.Date} <= Date(" & Format(CDate(TxtTo.Text), "yyyy,MM,dd") & ") "

            If (TxtClockID.Text <> "") Then
                HireDate = TxtClockID.Text

                If HireDate >= DteFrom And HireDate <= DteTo Then
                    DteFrom = HireDate
                End If
            End If

            If Trim(TxtCategory.Text) <> "" Then
                DteTo = LastDay
            End If

            Session("FieldCount") = 2
            Session("Field1") = "EmpTo"
            Session("FieldVal1") = DteTo

            Session("Field2") = "EmpFrom"
            Session("FieldVal2") = DteFrom

            If TxtEmpID.Text <> "ALL" Then
                If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
                    StrSelFormula = StrSelFormula & "And {Salary.EmpID} = '" & TxtEmpID.Text & "'"
                Else
                    StrSelFormula = StrSelFormula & "And {Salary.EmpID} = '" & TxtEmpID.Text & "' And {Salary.OutletName} = '" & Session("Level") & "'"
                End If
            End If
            If CmbDept.Text <> "ALL" Then
                StrSelFormula = StrSelFormula & "And {Salary.DeptCode} = '" & CmbDept.Text & "'"
            End If
            Session("ReportFile") = "REmol.rpt"
            Session("SelFormula") = StrSelFormula
            Session("RepHead") = "Emoluments & Tax Deduction From " & TxtFrom.Text & " to " & TxtTo.Text
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        End If

        If e.Item.CommandName = "Next" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID > '" & TxtEmpID.Text & "'"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try

        End If
        If e.Item.CommandName = "Previous" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 EmpID FROM Employee WHERE EmpID < '" & TxtEmpID.Text & "' ORDER BY EmpID DESC"
                TxtEmpID.Text = Cmd.ExecuteScalar
                TxtEmpID_TextChanged(Me, ev)
            Catch ex As Exception

            End Try
        End If
        If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtEmpID.Text)
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
        End If
    End Sub

    Private Sub TxtEmpID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEmpID.TextChanged
        If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
            StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "'"
        Else
            StrSql = "SELECT * FROM Employee WHERE EmpID = '" & TxtEmpID.Text & "' And OutletName = '" & Session("Level") & "'"
        End If
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        If TxtEmpID.Text = "ALL" Then Exit Sub
        If Rdr.Read Then
            On Error Resume Next
            TxtFirst.Text = Rdr("First")
            TxtLast.Text = Rdr("Last")
            TxtTitle.Text = Rdr("Title")
            TxtCategory.Text = Rdr("LastDay").ToString
            TxtGender.Text = Rdr("Gender")
            'TxtCategory.Text = Rdr("Category")
            ChkClocked.Checked = IIf(Rdr("Clocked"), True, False)
            TxtClockID.Text = Rdr("HireDate").ToString
            Session.Add("EmpID", Rdr("EmpID"))
            ImgPhoto.ImageUrl = "~/GetPhoto.ashx?id=" & Rdr("EmpID")
            'Personal Details
        Else
            Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(1))
            ToolBar1_ItemClick(Me, ev)
            'ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('Invalid Employee ID','Error');</script>", False)
        End If
        Rdr.Close()
    End Sub
End Class