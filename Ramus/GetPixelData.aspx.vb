﻿Imports System.Web
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data
Imports System.Data.SqlTypes

Partial Public Class GetPixelData1
    Inherits System.Web.UI.Page
    Private mImageFilePath As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim Cmd As New OleDb.OleDbCommand
        Dim Con As New OleDb.OleDbConnection
        Dim StrSql As String
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Dim pixelData As String = HttpContext.Current.Request("pixels")
        Dim videoWidth As Integer = Integer.Parse(HttpContext.Current.Request("width"))
        Dim id As New Guid()
        id = Guid.NewGuid()
        LblPixelData.Text = WebCamCapture.WebCamCapture.GetPixelAndSave(pixelData, "images/cap-test.jpg", videoWidth)
        mImageFilePath = Me.Server.MapPath(Me.Request.ApplicationPath) & "\images\cap-test.jpg"
        Dim fs As FileStream = New FileStream(mImageFilePath.ToString(), FileMode.Open)
        Dim ImgByte As Byte() = New Byte(fs.Length) {}
        fs.Read(ImgByte, 0, fs.Length)
        fs.Close()
        ' Insert the employee name and image into db
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT COUNT(*) FROM Employee WHERE EmpID = '" & Session("EmpID") & "'"
        If Cmd.ExecuteScalar > 0 Then
            StrSql = "UPDATE Employee SET  PhotoOle = @Photo, EmpID = @EmpID "
            StrSql = StrSql & "WHERE EmpID = '" & Session("EmpID") & "'"
        Else
            StrSql = "INSERT INTO Employee " & "(PhotoOle, EmpID) VALUES (@Photo, @EmpID)"
        End If
        'Dim Conn As String = "Provider=SQLOLEDB.1;Data Source=ROBIN-HP\SQLEXPRESS;User ID=admin;PWD=swathi*;Initial Catalog=TNTWEB"
        '"Data Source=(local);Initial Catalog=TNTWEB;User Id=admin;Password=swathi*;"
        Dim Connection As SqlConnection = New SqlConnection(Session("ConnString"))
        Connection.Open()
        Dim SqlCmd As SqlCommand = New SqlCommand(StrSql, Connection)
        SqlCmd.Parameters.Clear()
        SqlCmd.Parameters.AddWithValue("@Photo", ImgByte)
        SqlCmd.Parameters.AddWithValue("@EmpID", Session("EmpID"))
        Try
            SqlCmd.ExecuteNonQuery()
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
        End Try
    End Sub
End Class