﻿Public Partial Class FrmMonthlyInput
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdEmployee As New OleDb.OleDbCommand
    Dim RdrEmployee As OleDb.OleDbDataReader
    Dim CmdMonthlyI As New OleDb.OleDbCommand
    Dim RdrMonthlyI As OleDb.OleDbDataReader
    Dim CmdLM As New OleDb.OleDbCommand
    Dim RdrLM As OleDb.OleDbDataReader
    Dim CmdLock As New OleDb.OleDbCommand
    Dim RdrLock As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim I As Integer
    Dim SngYOS As Single = 0
    Dim SngAge As Single = 0
    Dim IntYear As Integer = 0
    Dim salaryDate As String
    Dim salary_ago_3m As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            TxtYear.Text = Year(Date.Today)
            Lblerror.Visible = False
            'Outlet
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT OutletName FROM Outlet ORDER BY OutletCode"
            Rdr = Cmd.ExecuteReader
            CmbDept.Items.Add("Select...")
            While Rdr.Read
                CmbDept.Items.Add(Rdr("OutletName"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnSave.Click
        Dim Trans As OleDb.OleDbTransaction
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Transaction = Trans
        CmdLock.Transaction = Trans

        Lblerror.Visible = False
        Lblerror.Text = ""

        If CmbDept.Text <> "Management" Then
            'Permission granted
        ElseIf CmbDept.Text = "Management" And (Session("Level") = "Administrator" Or Session("Level") = "Management") Then
            'Permission granted
        Else
            Lblerror.Visible = True
            Lblerror.Text = "No Access to Employee Management!!!"
            Exit Sub
        End If

        ''Checked if Locked
        CmdLock.Connection = Con
        StrSql = "SELECT locked FROM MonthlyInput WHERE Year = " & TxtYear.Text & " And Month = '" & CmbMonth.Text & "' And Locked = 1"
        CmdLock.CommandText = StrSql
        RdrLock = CmdLock.ExecuteReader
        If RdrLock.Read Then
            Lblerror.Visible = True
            Lblerror.Text = "Monthly Input for this month LOCKED!!!"
        Else
            'Cmd.Connection = Con
            'Cmd.CommandText = "DELETE FROM MonthlyInput where OutletName = '" & CmbDept.Text & "' and Year = " & TxtYear.Text & " and Month = '" & CmbMonth.Text & "'"
            Try
                'Cmd.ExecuteNonQuery()
                For Each Row As GridViewRow In GdvTVList.Rows
                    'Delete Records before saving
                    Cmd.Connection = Con
                    Cmd.CommandText = "DELETE FROM MonthlyInput where EmpID = '" & (Trim(DirectCast(Row.FindControl("TxtEmpID"), TextBox).Text)) & "' and OutletName = '" & CmbDept.Text & "' and Year = " & TxtYear.Text & " and Month = '" & CmbMonth.Text & "'"
                    Cmd.ExecuteNonQuery()

                    If Trim(DirectCast(Row.FindControl("TxtEmpID"), TextBox).Text) <> "" Then
                        StrSql = "Update Employee set YOSAmt =" & Val(Trim(DirectCast(Row.FindControl("TxtYOSAmt"), TextBox).Text)) & ", YOS = " & Val(Trim(DirectCast(Row.FindControl("TxtYOS"), TextBox).Text)) & " where EmpID = '" & Trim(DirectCast(Row.FindControl("TxtEmpID"), TextBox).Text) & "'"
                        Cmd.CommandText = StrSql
                        Cmd.ExecuteNonQuery()
                        StrSql = ""
                    End If
                    If (Trim(DirectCast(Row.FindControl("TxtEmpID"), TextBox).Text)) <> "" Then
                        StrSql = "Insert Into MonthlyInput Values('" & Trim(CmbDept.Text) & "','"
                        StrSql = StrSql & (CDate("28/" & Format(CmbMonth.SelectedIndex + 1, "00") & "/" & TxtYear.Text)).ToString("yyyy-MM-dd") & "','"
                        StrSql = StrSql & Trim(DirectCast(Row.FindControl("TxtEmpID"), TextBox).Text) & "','"
                        StrSql = StrSql & Trim(DirectCast(Row.FindControl("TxtName"), TextBox).Text) & "',"
                        StrSql = StrSql & Val(Trim(DirectCast(Row.FindControl("TxtOtherAllow"), TextBox).Text)) & ",'"
                        StrSql = StrSql & Trim(CmbMonth.Text) & "',"
                        StrSql = StrSql & Val(TxtYear.Text) & ","
                        StrSql = StrSql & Val(Trim(DirectCast(Row.FindControl("TxtTravel"), TextBox).Text)) & ","
                        StrSql = StrSql & Val(Trim(DirectCast(Row.FindControl("TxtShort"), TextBox).Text)) & ","
                        StrSql = StrSql & Val(Trim(DirectCast(Row.FindControl("TxtCCard"), TextBox).Text)) & ","
                        StrSql = StrSql & Val(Trim(DirectCast(Row.FindControl("TxtYOS"), TextBox).Text)) & ","
                        StrSql = StrSql & Val(Trim(DirectCast(Row.FindControl("TxtYOSAmt"), TextBox).Text)) & ","
                        StrSql = StrSql & 0 & ","
                        StrSql = StrSql & Val(Trim(DirectCast(Row.FindControl("TxtExGratia"), TextBox).Text)) & ","
                        StrSql = StrSql & Val(Trim(DirectCast(Row.FindControl("TxtOthDeduction"), TextBox).Text)) & ")"
                        Cmd.Connection = Con
                        Cmd.CommandText = StrSql
                        Cmd.ExecuteNonQuery()
                    End If
                Next
                Trans.Commit()
                Lblerror.Visible = True
                Lblerror.Text = "Data has been Saved!!!"
            Catch ex As Exception
                Trans.Rollback()
                Lblerror.Visible = True
                Lblerror.Text = ex.Message
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        RdrLock.Close()
    End Sub

    Protected Sub BtnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnView.Click
        Dim StrSelFormula As String

        Session("FieldCount") = 0
        Session("ReportFile") = "RYOS.rpt"
        If Session("Level") = "Administrator" Or Session("Level") = "Management" Then
            StrSelFormula = "{Employee.Working} = True AND {Employee.OutletName} <> 'ALL'"
        Else
            StrSelFormula = "{Employee.Working} = True AND {Employee.OutletName} = '" & Trim(Session("Level")) & "'"
        End If

        Session("SelFormula") = StrSelFormula
        Session("RepHead") = "Employee Year of Service"
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
    End Sub
    Function IsLeapYear() As Integer
        IsLeapYear = Date.IsLeapYear(Year(Date.Today))
        If IsLeapYear Then
            IntYear = 366
        Else
            IntYear = 365
        End If
        Return IntYear
    End Function

    Protected Sub ToolBar1_ItemClick(sender As Object, e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Refresh" Then
            Call LoadEmployees()
        End If
    End Sub

    Protected Sub LoadEmployees()
        Dim Dt As New DataTable
        Dim Dc As New DataColumn
        Dim Dr As DataRow
        Dim Count As Integer
        Try
            Cmd.Connection = Con
            CmdEmployee.Connection = Con
            CmdMonthlyI.Connection = Con

            Lblerror.Visible = False
            Lblerror.Text = ""

            If CmbDept.Text <> "Management" Then
                'Permission granted
            ElseIf CmbDept.Text = "Management" And (Session("Level") = "Administrator" Or Session("Level") = "Management") Then
                'Permission granted
            Else
                Lblerror.Visible = True
                Lblerror.Text = "No Access to Employee Management!!!"
                Exit Sub
            End If

            'Last 3 months for Employee.LastDay
            salaryDate = TxtYear.Text & "-" & Format(CmbMonth.SelectedIndex + 1, "00") & "-28"
            salary_ago_3m = Convert.ToDateTime(salaryDate).AddMonths(-4).ToString("yyyy-MM-dd")

            'SQL Intermart.Employee
            StrSql = "SELECT COUNT(*) FROM Employee WHERE Clocked=1 AND OutletName = '" & CmbDept.Text & "'"
            StrSql = StrSql & " AND (LastDay IS NULL OR LastDay > '" & salary_ago_3m & "')"
            'StrSql = StrSql & " AND Working <> 0"
            Cmd.CommandText = StrSql
            Count = Cmd.ExecuteScalar

            'Prepare GridView
            For I As Integer = 1 To Count
                Dr = Dt.NewRow
                Dt.Rows.Add(Dr)
            Next
            GdvTVList.DataSource = Dt
            GdvTVList.DataBind()

            'SQL Intermart.Employee
            StrSql = "SELECT * FROM Employee WHERE Clocked = 1"
            StrSql = StrSql & " AND OutletName = '" & CmbDept.Text & "'"
            StrSql = StrSql & " AND (LastDay IS NULL OR LastDay > '" & salary_ago_3m & "')"
            'StrSql = StrSql & " AND Working <> 0"
            StrSql = StrSql & " ORDER BY EmpID"
            CmdEmployee.CommandText = StrSql
            RdrEmployee = CmdEmployee.ExecuteReader

            I = 0
            IntYear = IsLeapYear()
            'Read employees
            While RdrEmployee.Read
                SngYOS = Math.Round((DateDiff(DateInterval.Day, RdrEmployee("HireDate"), Date.Today) / IntYear), 2)
                'Populate GridView
                DirectCast(GdvTVList.Rows(I).FindControl("TxtEmpID"), TextBox).Text = RdrEmployee("EmpID")
                DirectCast(GdvTVList.Rows(I).FindControl("TxtName"), TextBox).Text = RdrEmployee("Last") & " " & RdrEmployee("First")
                DirectCast(GdvTVList.Rows(I).FindControl("TxtBasic"), TextBox).Text = Val(RdrEmployee("BasicSalary") & "")
                DirectCast(GdvTVList.Rows(I).FindControl("TxtPosition"), TextBox).Text = RdrEmployee("Position") & ""
                DirectCast(GdvTVList.Rows(I).FindControl("TxtYOS"), TextBox).Text = SngYOS
                I = I + 1
            End While
            RdrEmployee.Close()

            I = 0
            For Each Row As GridViewRow In GdvTVList.Rows
                'SQL Intermart.MonthlyInput
                StrSql = "SELECT * FROM MonthlyInput"
                StrSql = StrSql & " WHERE EmpID = '" & (Trim(DirectCast(Row.FindControl("TxtEmpID"), TextBox).Text)) & "'"
                StrSql = StrSql & " AND Month = '" & CmbMonth.Text & "' AND Year = " & TxtYear.Text & ""
                StrSql = StrSql & " AND OutletName = '" & CmbDept.Text & "'"
                CmdMonthlyI.CommandText = StrSql
                RdrMonthlyI = CmdMonthlyI.ExecuteReader
                'Read MonthlyInput
                While RdrMonthlyI.Read
                    'Update GridView
                    DirectCast(GdvTVList.Rows(I).FindControl("TxtOtherAllow"), TextBox).Text = Val(RdrMonthlyI("OtherAllow") & "")
                    DirectCast(GdvTVList.Rows(I).FindControl("TxtTravel"), TextBox).Text = Val(RdrMonthlyI("Travel") & "")
                    DirectCast(GdvTVList.Rows(I).FindControl("TxtShort"), TextBox).Text = Val(RdrMonthlyI("Short") & "")
                    DirectCast(GdvTVList.Rows(I).FindControl("TxtCCard"), TextBox).Text = Val(RdrMonthlyI("CreditCard") & "")
                    DirectCast(GdvTVList.Rows(I).FindControl("TxtYOSAmt"), TextBox).Text = Val(RdrMonthlyI("YOSAmt") & "")
                    DirectCast(GdvTVList.Rows(I).FindControl("TxtExGratia"), TextBox).Text = Val(RdrMonthlyI("ExGratia") & "")
                    DirectCast(GdvTVList.Rows(I).FindControl("TxtOthDeduction"), TextBox).Text = Val(RdrMonthlyI("OtherDeduct") & "")
                    I = I + 1
                End While
                RdrMonthlyI.Close()
            Next
        Catch ex As Exception
            Lblerror.Visible = True
            Lblerror.Text = ex.Message
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
        End Try
    End Sub
End Class