﻿Public Partial Class FrmGovComp
    Inherits System.Web.UI.Page
    Dim Cmd As New OleDb.OleDbCommand
    Dim Con As New OleDb.OleDbConnection
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdSal As New OleDb.OleDbCommand
    Dim StrSql As String
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim CmdCareer As New OleDb.OleDbCommand
    Dim StrAudit As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            TxtYear.Text = Date.Today.Year
            Dim StrMsg As String = "This process will increase all working employee's "
            StrMsg = StrMsg & "Basic Salary by the amount specified for their band "
            StrMsg = StrMsg & "This process cannot be reversed automatically. If "
            StrMsg = StrMsg & "you need to reverse you will have to do it one by "
            StrMsg = StrMsg & "one through Edit Emplooyee File menu. "
            StrMsg = StrMsg & "Are you sure to proceed?"
            LblWarning.Text = StrMsg
        End If
    End Sub

    Private Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Update" Then
            ' Dim Trans As OleDb.OleDbTransaction
            'Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
            StrSql = "SELECT EmpID,BasicSalary FROM Employee WHERE Working = 1 and BasicSalary is not null"
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            'Cmd.Transaction = Trans
            Rdr = Cmd.ExecuteReader

            Dim IntAmount As Integer
            Dim SngPercent As Single
            CmdSal.Connection = Con
            ' CmdSal.Transaction = Trans
            Dim IntBasic As Integer
            Try
                While Rdr.Read
                    For I As Integer = 1 To 10
                        Dim TxtFrom As TextBox = DirectCast(FindControl("TxtFrom" & I), TextBox)
                        Dim TxtTo As TextBox = DirectCast(FindControl("TxtTo" & I), TextBox)
                        Dim TxtAmount As TextBox = DirectCast(FindControl("TxtAmount" & I), TextBox)
                        Dim TxtPercent As TextBox = DirectCast(FindControl("TxtPercent" & I), TextBox)
                        IntAmount = Val(TxtAmount.Text)
                        SngPercent = Val(TxtPercent.Text)
                        If Rdr("EmpID") = "3" Then
                            IntBasic = Rdr("BasicSalary")
                        End If
                        IntBasic = Rdr("BasicSalary")
                        If IntBasic >= Val(TxtFrom.Text) And IntBasic <= Val(TxtTo.Text) Then
                            Exit For
                        End If
                        If Val(TxtTo.Text) = 0 Then
                            Exit For
                        End If
                    Next
                    StrSql = "UPDATE Employee SET BasicSalaryOld = BasicSalary,BasicSalary = BasicSalary + "
                    If CLng(Rdr("BasicSalary") * SngPercent / 100) > IntAmount Then
                        IntAmount = CLng(IntBasic * SngPercent / 100)
                    End If
                    StrSql = StrSql & IntAmount
                    StrSql = StrSql & " WHERE EmpID = '" & Rdr("EmpID") & "'"
                    CmdSal.CommandText = StrSql
                    CmdSal.ExecuteNonQuery()

                    CmdCareer.Connection = Con
                    StrSql = "INSERT INTO Career (EmpID,[Date],Remarks) VALUES('" & Rdr("EmpId") & "','"
                    StrSql = StrSql & Date.Today.ToString("dd-MMM-yyyy") & "','"
                    StrSql = StrSql & "Salary increased from " & IntBasic & " by " & IntAmount & "')"
                    CmdCareer.CommandText = StrSql
                    CmdCareer.ExecuteNonQuery()

                    CmdAudit.Connection = Con
                    StrAudit = "Government Compensation - Employee Basic Salary Updated for " & Rdr("EmpID") & "."
                    CmdAudit.CommandText = "Insert Into EmpMasterAudit values('" & Session("UserID") & "','Government Compensation','" & StrAudit & "','','" & Session("UserID") & "','" & Format(CDate(Date.Now.ToShortDateString), "dd-MMM-yyyy") & "','" & Date.Now.ToShortTimeString & "')"
                    CmdAudit.ExecuteNonQuery()
                End While
                Rdr.Close()
                ' Trans.Commit()
                LblResults.Text = "Government Compensation Updated!"
            Catch ex As Exception
                'Trans.Rollback()
                LblResults.Text = "Update Failed " & ex.Message
            End Try
        End If
    End Sub
End Class