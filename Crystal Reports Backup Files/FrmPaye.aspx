﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmPaye.aspx.vb" Inherits="Ramus.FrmPaye" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<html>
<head id="Head1" runat="server">
    <title>Create / Edit PAYE Schemes</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>
    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.

            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Create / Edit PAYE Schemes'; //Set the name

            w.Width = 620; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 400;
            w.Top = 10;
            w.Left = 10;              
            w.Show(); //Show the window
        };
        function ShowFind() {
            wfind = window.frameElement.IDCWindow;
            wfind.CreateWindow('FindBank.aspx', w);
            return false;
        };
        function FunctionConfirm(Tlb,TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Save") {
                Index = '1';
                w.Confirm("Sure to Save", "Confirm",null,SaveFunction);
            }
            else if (TlbCmd == "New") {
                __doPostBack('ToolBar1', '0');
            }
            else if (TlbCmd == "Delete") {
                Index = '3';
                w.Confirm("Sure to Delete", "Confirm", null, SaveFunction);
            }
        }
        function SaveFunction(Sender,RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        }
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            direction: ltr;
            width: 101px;
        }
        .style3
        {
            width: 18px;
        }
        .style4
        {
            width: 101px;
        }
    </style>

</head>
<body onload="init();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px"
                runat="server" Height="32px" Width="32px" BorderStyle="None" Visible="True">
                <div id="divImage" style="display: none">
                    <asp:Image ID="img1" runat="server" ImageUrl="/Images/Wait.gif" />
                </div>
            </asp:Panel>
            <div id="toolbar" style="width: 100%;">
                <eo:ToolBar ID="ToolBar1" runat="server" BackgroundImage="00100103" ClientSideOnItemClick="FunctionConfirm"
                    BackgroundImageLeft="00100101" BackgroundImageRight="00100102" SeparatorImage="00100104"
                    Width="100%">
                    <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 1px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: #335ea8 1px solid; background-color: #99afd4; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <Items>
                        <eo:ToolBarItem CommandName="New" ImageUrl="00101001" ToolTip="New">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Save" ImageUrl="00101003" ToolTip="Save">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Delete" ImageUrl="~/images/delete.png" ToolTip="Delete">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Help" ImageUrl="~/images/help.png" ToolTip="Help">
                        </eo:ToolBarItem>
                    </Items>
                    <ItemTemplates>
                        <eo:ToolBarItem Type="Custom">
                            <NormalStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <HoverStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <DownStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="DropDownMenu">
                            <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                            <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; background-image: url(00100106); background-position-x: right;" />
                            <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: none; background-color:transparent; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                        </eo:ToolBarItem>
                    </ItemTemplates>
                    <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                </eo:ToolBar>
            </div>
            <div id="Content" style="width: 100%; height: 357px; overflow: auto; padding: 2px;
                border-right: solid 1px #cddaea;">
                <table class="style1">
                    <tr>
                        <td height="100%" class="style4">
                            Scheme Code</td>
                        <td class="style3">
                            <asp:TextBox ID="TxtSchemeCode" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style2">
                            From - 1</td>
                        <td class="style3">
                            <asp:TextBox ID="TxtFrom1" runat="server" Width="75px"></asp:TextBox>
                        </td>
                        <td>
                            To - 1</td>
                        <td>
                            <asp:TextBox ID="TxtTo1" runat="server" Width="75px"></asp:TextBox>
                        </td>
                        <td>
                            Percent - 1</td>
                        <td>
                            <asp:TextBox ID="TxtPercent1" runat="server" Width="75px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style4">
                            From - 2</td>
                        <td class="style3">
                            <asp:TextBox ID="TxtFrom2" runat="server" Width="75px"></asp:TextBox>
                        </td>
                        <td>
                            To - 2</td>
                        <td>
                            <asp:TextBox ID="TxtTo2" runat="server" Width="75px"></asp:TextBox>
                        </td>
                        <td>
                            Percent - 2</td>
                        <td>
                            <asp:TextBox ID="Txtpercent2" runat="server" Width="75px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style4">
                            From - 3</td>
                        <td class="style3">
                            <asp:TextBox ID="TxtFrom3" runat="server" Width="75px"></asp:TextBox>
                        </td>
                        <td>
                            To - 3</td>
                        <td>
                            <asp:TextBox ID="TxtTo3" runat="server" Width="75px"></asp:TextBox>
                        </td>
                        <td>
                            Percent - 3</td>
                        <td>
                            <asp:TextBox ID="TxtPercent3" runat="server" Width="75px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style4">
                            From - 4</td>
                        <td class="style3">
                            <asp:TextBox ID="TxtFrom4" runat="server" Width="75px"></asp:TextBox>
                        </td>
                        <td>
                            To - 4</td>
                        <td>
                            <asp:TextBox ID="TxtTo4" runat="server" Width="75px"></asp:TextBox>
                        </td>
                        <td>
                            Percent - 4</td>
                        <td>
                            <asp:TextBox ID="TxtPercent4" runat="server" Width="75px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style4">
                            From - 5</td>
                        <td class="style3">
                            <asp:TextBox ID="TxtFrom5" runat="server" Width="75px"></asp:TextBox>
                        </td>
                        <td>
                            To - 5</td>
                        <td>
                            <asp:TextBox ID="TxtTo5" runat="server" Width="75px"></asp:TextBox>
                        </td>
                        <td>
                            Percent - 5</td>
                        <td>
                            <asp:TextBox ID="TxtPercent5" runat="server" Width="75px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="Panel10" runat="server" BorderStyle="Inset" BorderWidth="1px" 
                                Height="100%" ScrollBars="Both" Width="100%">
                                <asp:GridView ID="GdvPaye" runat="server" AutoGenerateColumns="False">
                                    <Columns>
                                        <asp:BoundField DataField="PayeScheme" HeaderText="Scheme Code" 
                                            HtmlEncode="False">
                                            <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="From-1" HeaderText="From - 1" HtmlEncode="False" 
                                            HtmlEncodeFormatString="False">
                                            <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="To-1" HeaderText="To - 1" HtmlEncode="False">
                                            <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="TaxPercent-1" HeaderText="Percent - 1" 
                                            HtmlEncode="False">
                                            <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="From-2" HeaderText="From - 2" HtmlEncode="False">
                                            <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="To-2" HeaderText="To - 2" HtmlEncode="False">
                                            <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="TaxPercent-2" HeaderText="Percent - 2">
                                            <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="From-3" HeaderText="From - 3">
                                            <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="To-3" HeaderText="To - 3">
                                            <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="TaxPercent-3" HeaderText="Percent - 3">
                                            <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="From-4" HeaderText="From - 4">
                                            <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="To-4" HeaderText="To - 4">
                                            <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="TaxPercent-4" HeaderText="Percent - 4">
                                            <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="From-5" HeaderText="From - 5">
                                            <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="To-5" HeaderText="To - 5">
                                            <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="TaxPercent-5" HeaderText="Percent - 5">
                                            <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                        </asp:BoundField>
                                    </Columns>
                                    <HeaderStyle BackColor="#0080C0" />
                                    <AlternatingRowStyle BackColor="#CAE4FF" />
                                </asp:GridView>
                            </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_InitializeRequest);
        prm.add_endRequest(prm_EndRequest);

        function prm_InitializeRequest(sender, args) {
            var panelProg = $get('divImage');
            if (args._postBackElement.id != "Timer1") {
            panelProg.style.display = '';}
        };

        function prm_EndRequest(sender, args) {
            var panelProg = $get('divImage');
            panelProg.style.display = 'none';
        };
    </script>
</body>
</html>
